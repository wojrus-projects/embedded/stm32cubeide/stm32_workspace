cls
echo off

rem Requirements:
rem 1. ST-LINK V2/V3 or Segger J-Link
rem 2. STM32CubeProgrammer v2.18.0+

rem Select programming interface:
rem 1. J-Link:  IF_TYPE=JLINK
rem 2. ST-LINK: IF_TYPE=SWD
set IF_TYPE=JLINK

set STM32_PROGRAMMER="d:\ST\STM32CubeProgrammer\bin\STM32_Programmer_CLI.exe"
set FIRMWARE_FILE="DAQ_M02_1.3.0_Release.hex"

echo Flash firmware
echo MCU type: STM32G431CB
echo WARNING: erase all flash

%STM32_PROGRAMMER% --connect port=%IF_TYPE% --erase all
if %ERRORLEVEL% NEQ 0 exit /B %ERRORLEVEL%

%STM32_PROGRAMMER% --connect port=%IF_TYPE% --write %FIRMWARE_FILE%
if %ERRORLEVEL% NEQ 0 exit /B %ERRORLEVEL%

echo Flash Option Bytes

%STM32_PROGRAMMER% --connect port=%IF_TYPE% --optionbytes RDP=0xAA BOR_LEV=3 nSWBOOT0=0 nBOOT0=1 BOOT_LOCK=0
if %ERRORLEVEL% NEQ 0 exit /B %ERRORLEVEL%

echo Done
pause
