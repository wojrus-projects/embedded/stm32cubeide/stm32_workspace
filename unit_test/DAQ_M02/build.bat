echo off
cls

echo Required toolchain: clang v18, cmake, ninja for Windows 10.

cmake ^
-S . ^
-B build/release ^
-G "Ninja" ^
-D CMAKE_CXX_COMPILER=clang++ ^
-D CMAKE_MAKE_PROGRAM="ninja.exe" ^
-D CMAKE_VERBOSE_MAKEFILE=OFF ^
-D CMAKE_EXPORT_COMPILE_COMMANDS=ON ^
-D CMAKE_BUILD_TYPE=Release

if %ERRORLEVEL% NEQ 0 exit /B %ERRORLEVEL%

cmake --build build/release --clean-first --parallel 4

if %ERRORLEVEL% NEQ 0 exit /B %ERRORLEVEL%

echo Done
pause
