#include <memory>
#include <bitset>
#include <cstdlib>

#include "catch/catch_amalgamated.hpp"

#include "../../project/DAQ_M02/Application/Bit_Word_Packer.h"

static uint32_t GetWord(const uint8_t * pBuffer, uint32_t wordIndexBits, uint32_t wordSizeBits)
{
    union WordChunk
    {
        uint32_t Integer = 0;
        uint8_t Bytes[sizeof(Integer)];
    };

    WordChunk chunk;

    const uint64_t WordMask = (1ull << wordSizeBits) - 1ull;
    const size_t ChunkIndexBytes = wordIndexBits / 8;
    const uint32_t ChunkSizeBits = (wordIndexBits % 8) + wordSizeBits;
    const size_t ChunkSizeBytes = ChunkSizeBits / 8 + (ChunkSizeBits % 8 != 0);

    memcpy(chunk.Bytes, &pBuffer[ChunkIndexBytes], ChunkSizeBytes);
    
    chunk.Integer >>= (wordIndexBits % 8);
    chunk.Integer &= WordMask;
    
    return chunk.Integer;
}

template<size_t WordSizeBits>
void Test()
{
    constexpr size_t BufferSizeWords = 1000;
    constexpr size_t BufferSizeBits = BufferSizeWords * WordSizeBits;
    constexpr size_t BufferSizeBytes = BufferSizeBits / 8 + (BufferSizeBits % 8 != 0);
    constexpr uint64_t WordMask = (1ull << WordSizeBits) - 1ull;
    
    BitWordPacker<WordSizeBits> packer;
    auto buffer = std::make_unique<uint8_t[]>(BufferSizeBytes);
    
    REQUIRE(packer.Initialize(buffer.get(), BufferSizeBytes) == true);
    
    //
    // Fill buffer.
    //
    srand(0);
    
    for (int i = 0; i < BufferSizeWords; i++)
    {
        const uint32_t valueTest = rand();
        
        REQUIRE(packer.AddWord(valueTest) == true);
        
        const uint32_t datalength_Bits = (i + 1) * WordSizeBits;
        
        const size_t datalength_Test = packer.GetDataLength();
        const size_t datalength_Expected = datalength_Bits / 8 + (datalength_Bits % 8 != 0);
        
        REQUIRE(datalength_Test == datalength_Expected);
    }
    
    //
    // Verify buffer.
    //
    srand(0);
    
    for (int i = 0; i < BufferSizeWords; i++)
    {
        const uint32_t valueTest = rand() & WordMask;
        
        REQUIRE(GetWord(buffer.get(), i * WordSizeBits, WordSizeBits) == valueTest);
    }
}

TEST_CASE("WordPacker all word sizes")
{
    Test<1>();
    Test<2>();
    Test<3>();
    Test<4>();
    Test<5>();
    Test<6>();
    Test<7>();
    Test<8>();
    Test<9>();
    Test<10>();
    
    Test<11>();
    Test<12>();
    Test<13>();
    Test<14>();
    Test<15>();
    Test<16>();
    Test<17>();
    Test<18>();
    Test<19>();
    Test<20>();
    
    Test<21>();
    Test<22>();
    Test<23>();
    Test<24>();
    Test<25>();
    Test<26>();
    Test<27>();
    Test<28>();
    Test<29>();
    Test<30>();
    
    Test<31>();
    Test<32>();
}
