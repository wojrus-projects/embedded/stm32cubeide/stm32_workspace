#include <cstdint>
#include <cstdbool>
#include <algorithm>
#include <limits>

#include "catch/catch_amalgamated.hpp"

#include "../../project/DAQ_M01/Application/FIFO_Queue.h"

template<typename ItemType>
void Test_01_Put_Get_Basic()
{   
    constexpr size_t ItemsMax = 2;
    
    FIFO_Queue<ItemType, ItemsMax> fifo;
    
    REQUIRE(fifo.get_capacity() == ItemsMax);
    REQUIRE(fifo.get_free() == ItemsMax);
    REQUIRE(fifo.get_length() == 0);
    
    REQUIRE(fifo.put(1) == true);
    REQUIRE(fifo.get_free() == 1);
    REQUIRE(fifo.get_length() == 1);
    
    REQUIRE(fifo.put(2) == true);
    REQUIRE(fifo.get_free() == 0);
    REQUIRE(fifo.get_length() == ItemsMax);
    
    SECTION("try put more")
    {
        REQUIRE(fifo.get_free() == 0);
        REQUIRE(fifo.get_length() == ItemsMax);
        
        REQUIRE(fifo.put(3) == false);
        REQUIRE(fifo.put(4) == false);
    
        fifo.flush_by_writer();
    
        REQUIRE(fifo.get_free() == ItemsMax);
        REQUIRE(fifo.get_length() == 0);
        
        REQUIRE(fifo.put(5) == true);
        REQUIRE(fifo.put(6) == true);
        REQUIRE(fifo.put(7) == false);
        
        REQUIRE(fifo.get_free() == 0);
        REQUIRE(fifo.get_length() == ItemsMax);
    }
    
    SECTION("get")
    {
        ItemType item;
        
        item = 0;
        REQUIRE(fifo.get(&item) == true);
        REQUIRE(item == 1);
        
        REQUIRE(fifo.get_free() == 1);
        REQUIRE(fifo.get_length() == 1);
        
        item = 0;
        REQUIRE(fifo.get(&item) == true);
        REQUIRE(item == 2);
        
        REQUIRE(fifo.get_free() == ItemsMax);
        REQUIRE(fifo.get_length() == 0);
        
        item = 0;
        REQUIRE(fifo.get(&item) == false);
        REQUIRE(item == 0);
    }
    
    SECTION("peek")
    {
        const ItemType dataReference[ItemsMax]{ 1, 2 };
        ItemType dataRead[ItemsMax];
        
        std::fill(dataRead, &dataRead[ItemsMax], 0);
        REQUIRE(fifo.peek(0, dataRead, ItemsMax) == ItemsMax);
        REQUIRE(std::ranges::equal(dataRead, dataReference) == true);
        
        std::fill(dataRead, &dataRead[ItemsMax], 0);
        REQUIRE(fifo.peek(0, dataRead, ItemsMax) == ItemsMax);
        REQUIRE(std::ranges::equal(dataRead, dataReference) == true);
        
        std::fill(dataRead, &dataRead[ItemsMax], 0);
        REQUIRE(fifo.peek(1, dataRead, ItemsMax) == (ItemsMax - 1));
        REQUIRE(dataRead[0] == 2);
        
        std::fill(dataRead, &dataRead[ItemsMax], 0);
        REQUIRE(fifo.peek(2, dataRead, ItemsMax) == 0);
        REQUIRE(fifo.peek(3, dataRead, ItemsMax) == 0);
        
        fifo.flush_by_writer();
        
        REQUIRE(fifo.peek(0, dataRead, ItemsMax) == 0);
    }
    
    SECTION("flush by writer")
    {
        REQUIRE(fifo.get_free() == 0);
        REQUIRE(fifo.get_length() == ItemsMax);
        
        fifo.flush_by_writer();
    
        REQUIRE(fifo.get_free() == ItemsMax);
        REQUIRE(fifo.get_length() == 0);
    }
    
    SECTION("flush by reader")
    {
        REQUIRE(fifo.get_free() == 0);
        REQUIRE(fifo.get_length() == ItemsMax);
        
        fifo.flush_by_reader();
    
        REQUIRE(fifo.get_free() == ItemsMax);
        REQUIRE(fifo.get_length() == 0);
    }
}

template<typename ItemType>
void Test_02_Write_And_Peek()
{   
    constexpr size_t ItemsMax = 8;
    
    FIFO_Queue<ItemType, ItemsMax> fifo;
    ItemType buffer[ItemsMax]{};
    
    REQUIRE(fifo.get_capacity() == ItemsMax);
    REQUIRE(fifo.get_free() == ItemsMax);
    REQUIRE(fifo.get_length() == 0);
    
    buffer[0] = 1;
    buffer[1] = 2;
    buffer[2] = 3;
    buffer[3] = 4;
    REQUIRE(fifo.write(buffer, 4) == 4);
    
    buffer[0] = 5;
    buffer[1] = 6;
    buffer[2] = 7;
    REQUIRE(fifo.write(buffer, 3) == 3);
    
    buffer[0] = 8;
    buffer[1] = 9;
    buffer[2] = 0;
    REQUIRE(fifo.write(buffer, 3) == 1);
    
    REQUIRE(fifo.get_free() == 0);
    REQUIRE(fifo.get_length() == ItemsMax);
    
    SECTION("peek all")
    {
        ItemType bufferRead[ItemsMax]{};
        
        REQUIRE(fifo.peek(0, bufferRead, ItemsMax) == ItemsMax);
        
        for (size_t i = 0; i < ItemsMax; i++)
        {
            REQUIRE(bufferRead[i] == (1 + i));
        }
        
        REQUIRE(fifo.remove(ItemsMax) == ItemsMax);
        
        REQUIRE(fifo.get_free() == ItemsMax);
        REQUIRE(fifo.get_length() == 0);
    }
    
    SECTION("peek in loop")
    {
        for (size_t i = 0; i < ItemsMax; i++)
        {
            ItemType item{};
            
            REQUIRE(fifo.peek(i, &item, 1) == 1);
            REQUIRE(item == (1 + i));
        }
    }
    
    SECTION("peek/remove in loop")
    {
        for (size_t i = 0; i < ItemsMax; i++)
        {
            ItemType item{};
            
            REQUIRE(fifo.peek(0, &item, 1) == 1);
            REQUIRE(item == (1 + i));
            
            REQUIRE(fifo.remove(1) == 1);
        }
        
        REQUIRE(fifo.get_free() == ItemsMax);
        REQUIRE(fifo.get_length() == 0);
    }
    
    SECTION("peek/remove corner cases")
    {
        ItemType item{};
        
        REQUIRE(fifo.peek(ItemsMax, &item, 1) == 0);
        REQUIRE(fifo.peek(0, &item, 0) == 0);
        
        REQUIRE(fifo.remove(0) == 0);
        REQUIRE(fifo.remove(ItemsMax + 1) == ItemsMax);
        
        REQUIRE(fifo.get_free() == ItemsMax);
        REQUIRE(fifo.get_length() == 0);
    }
}

template<typename ItemType>
void Test_03_Write_Read_Full_Multiple()
{   
    constexpr size_t ItemsMax = 8;
    constexpr int IterationMax = 3;
    
    FIFO_Queue<ItemType, ItemsMax> fifo;
    ItemType buffer[ItemsMax];
    
    for (int iteration = 0; iteration < IterationMax; iteration++)
    {
        REQUIRE(fifo.get_capacity() == ItemsMax);
        REQUIRE(fifo.get_free() == ItemsMax);
        REQUIRE(fifo.get_length() == 0);
        
        for (size_t i = 0; i < ItemsMax; i++)
        {
            buffer[i] = (1 + i + iteration);
        }
        
        REQUIRE(fifo.write(buffer, ItemsMax) == ItemsMax);
        REQUIRE(fifo.get_free() == 0);
        REQUIRE(fifo.get_length() == ItemsMax);
        
        std::fill(buffer, &buffer[ItemsMax], 0);
        REQUIRE(fifo.read(buffer, ItemsMax) == ItemsMax);
        
        for (size_t i = 0; i < ItemsMax; i++)
        {
            REQUIRE(buffer[i] == (1 + i + iteration));
        }
        
        REQUIRE(fifo.get_free() == ItemsMax);
        REQUIRE(fifo.get_length() == 0);
    }
}

template<typename ItemType>
void Test_04_Write_Read_And_Check_Internal_Index_Wrap()
{
    using FIFO_Internal_Index_Type = size_t;
    
    static_assert(sizeof(FIFO_Internal_Index_Type) == 4);
    
    constexpr size_t ItemsMax = 64 * 1024;
    constexpr size_t WriteLength = ItemsMax / 3;
    
    FIFO_Queue<ItemType, ItemsMax> fifo;
    ItemType bufferWrite[WriteLength];
    ItemType bufferRead[std::size(bufferWrite)];
    
    constexpr uint64_t InternalIndexMax = std::numeric_limits<FIFO_Internal_Index_Type>::max();
    constexpr uint64_t IterationMax = (InternalIndexMax * 3) / std::size(bufferWrite);
    
    for (auto& item : bufferWrite)
    {
        item = rand();
    }
    
    WARN("Test takes a long time (iterations: " << IterationMax << ")");
    
    for (uint64_t iteration = 0; iteration < IterationMax; iteration++)
    {
        //
        // Prepare buffers
        //
        for (auto& item : bufferWrite)
        {
            item++;
        }
        
        std::fill(bufferRead, &bufferRead[std::size(bufferRead)], 0);
    
        //
        // Write
        //
        REQUIRE(fifo.write(bufferWrite, std::size(bufferWrite)) == std::size(bufferWrite));
        REQUIRE(fifo.get_length() == std::size(bufferWrite));
        
        //
        // Read and verify
        //
        REQUIRE(fifo.read(bufferRead, std::size(bufferRead)) == std::size(bufferRead));
        REQUIRE(std::ranges::equal(bufferRead, bufferWrite) == true);
        
        REQUIRE(fifo.get_free() == ItemsMax);
        REQUIRE(fifo.get_length() == 0);
    }
}

TEST_CASE("FIFO put/get basic")
{   
    Test_01_Put_Get_Basic<uint8_t>();
    Test_01_Put_Get_Basic<uint16_t>();
    Test_01_Put_Get_Basic<uint32_t>();
}

TEST_CASE("FIFO write and peek")
{   
    Test_02_Write_And_Peek<uint8_t>();
    Test_02_Write_And_Peek<uint16_t>();
    Test_02_Write_And_Peek<uint32_t>();
}

TEST_CASE("FIFO full write/read multiple times")
{   
    Test_03_Write_Read_Full_Multiple<uint8_t>();
    Test_03_Write_Read_Full_Multiple<uint16_t>();
    Test_03_Write_Read_Full_Multiple<uint32_t>();
}

TEST_CASE("FIFO write, read and check internal index wrap")
{   
    Test_04_Write_Read_And_Check_Internal_Index_Wrap<uint8_t>();
}
