#define CATCH_CONFIG_MAIN

#include <cstdint>
#include <cstdbool>
#include <cstddef>

#include "catch/catch_amalgamated.hpp"

// Check if host (i386) C++ basic types are compatible with target (ARM Cortex-M4F).
static_assert(sizeof(bool)      == 1 &&
              sizeof(char)      == 1 &&
              sizeof(short)     == 2 &&
              sizeof(int)       == 4 &&
              sizeof(long)      == 4 &&
              sizeof(long long) == 8 &&
              sizeof(float)     == 4 &&
              sizeof(double)    == 8 &&
              sizeof(size_t)    == 4 &&
              sizeof(void *)    == 4);
