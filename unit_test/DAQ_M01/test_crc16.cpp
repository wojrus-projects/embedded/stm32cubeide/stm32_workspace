#include "catch/catch_amalgamated.hpp"

#include "../../project/DAQ_M01/Application/CRC16.h"

TEST_CASE("CRC16")
{
    // Reference: https://crccalc.com
    const uint8_t testData[10] = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09 };
    
    uint16_t crc = CRC16_INITIALIZE_VALUE;
    
    crc = CRC16(crc, &testData[0], 5);
    crc = CRC16(crc, &testData[5], 3);
    crc = CRC16(crc, &testData[8], 2);
    
    REQUIRE(crc == 0x4574);    
}
