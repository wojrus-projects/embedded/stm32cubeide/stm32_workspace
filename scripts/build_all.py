"""
Build all STM32CubeIDE projects (Windows 10).
"""

import os
import sys
import subprocess
import shutil
from timeit import default_timer as timer
from typing import NamedTuple

class Project(NamedTuple):
    """
    Project description.
    """
    path: str           # Path relative to workspace.
    name: str           # Logical (Eclipse) name.
    build_type: str     # Build type (typical Debug or Release).
    version: str        # Version with format "Major.Minor.Patch".

#
# Projects to build.
#

projects = (
    #--------------------------------------+---------------------------+-----------------------+-------------+
    #         Relative path                |   Logical (Eclipse) name  |   Build type          |   Version   |
    #--------------------------------------+---------------------------+-----------------------+-------------+
    Project(r"CANUSB_M01",                     "CANUSB_M01",               "Debug",                "1.0.0"),
    Project(r"CANUSB_M01",                     "CANUSB_M01",               "Release",              "1.0.0"),
    
    Project(r"DAQ_M01",                        "DAQ_M01",                  "Debug",                "2.1.0"),
    Project(r"DAQ_M01",                        "DAQ_M01",                  "Release",              "2.1.0"),
    
    Project(r"DAQ_M02",                        "DAQ_M02",                  "Debug",                "1.3.0"),
    Project(r"DAQ_M02",                        "DAQ_M02",                  "Release",              "1.3.0"),
    
    Project(r"RELAY_USB_M01",                  "RELAY_USB_M01",            "Debug",                "1.0.0"),
    Project(r"RELAY_USB_M01",                  "RELAY_USB_M01",            "Release",              "1.0.0"),
    
    Project(r"VREF_M01",                       "VREF_M01",                 "Debug",                "1.0.0"),
    Project(r"VREF_M01",                       "VREF_M01",                 "Release",              "1.0.0")
)

#
# Script require prior definition environment variables.
#

STM32CUBEIDE_PATH = os.environ["STM32CUBEIDE_PATH"]
WORKSPACE_PATH = os.environ["WORKSPACE_PATH"]
RELEASE_PATH = os.environ["RELEASE_PATH"]


def check_string(text: str) -> None:
    """
    Checks whether the string contains only uppercase and lowercase English letters,
    the underscore character, and the dot character.
    """
    is_valid = all((char.isalnum() or char in ('_', '.')) for char in text)
    if not is_valid:
        raise ValueError(rf"Invalid character in '{text}'")


type BuildResult = tuple[int, str]

def build_project(project: Project) -> BuildResult:
    """
    Build one project.
    
    :param project:    Project to build.
    
    :return:     Build result: tuple(return code, log data: stdout + stderr)
    """
    check_string(project.path)
    check_string(project.name)
    check_string(project.build_type)
    check_string(project.version)
    
    stm32cubeidec_path = os.path.join(STM32CUBEIDE_PATH, "stm32cubeidec")
    workspace_project_path = os.path.join(WORKSPACE_PATH, project.path)
    
    headless_build = rf"{stm32cubeidec_path} --launcher.suppressErrors -no-indexer -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild"
    build_cmd = rf"{headless_build} -data {WORKSPACE_PATH} -import {workspace_project_path} -cleanBuild {project.name}/{project.build_type}"

    print(f"\nBuild {project.name} ({project.build_type}) in '{project.path}'")

    time_start = timer()
    
    process_result = subprocess.run(args=build_cmd, capture_output=True, text=True, check=False)
    
    time_end = timer()
    time_elapsed = time_end - time_start
    print(f"Process end (time: {time_elapsed:.3f} s)")
    
    log_str = ""
    if process_result.stdout:
        log_str += "<STDOUT>\n" + process_result.stdout + "</STDOUT>\n"
    if process_result.stderr:
        log_str += "<STDERR>\n" + process_result.stderr + "</STDERR>\n"
    
    return (process_result.returncode, log_str)


def normalize_path(path: str) -> str:
    """
    Replace (back)slashes with '_'.
    
    :param path:    Path to normalize.
    
    :return:        New path.
    """
    path_split = os.path.split(os.path.normpath(path))
    L = list(filter(None, path_split))
    path_normalized = '_'.join(L)
    
    return path_normalized


def rename_file(path: str,
                file_name_old: str,
                file_name_new: str) -> None:
    """
    Rename file.
    
    :param path:            Path to file.
    :param file_name_old:   File old name.
    :param file_name_new:   File new name.
    """
    path_old = os.path.join(path, file_name_old)
    path_new = os.path.join(path, file_name_new)
    
    if os.path.exists(path_new):
        os.remove(path_new)
    
    os.rename(path_old, path_new)


def copy_artifacts(project: Project) -> None:
    """
    Copy ELF/HEX/BIN files from build to release directories.
    
    :param project:    Project to copy.
    """
    if project.path != project.name:
        raise ValueError("Projects in subdirectories are no supported yet")
        
    build_path = os.path.join(WORKSPACE_PATH, project.name, project.build_type)
    relase_path = os.path.join(RELEASE_PATH, project.name)        
    
    for extension in ("elf", "hex", "bin"):
        file_name_old = f"{project.name}.{extension}"
        file_name_new = f"{project.name}_{project.version}_{project.build_type}.{extension}"
        
        rename_file(build_path, file_name_old, file_name_new)

        copy_path_src = os.path.join(build_path, file_name_new)
        copy_path_dst = os.path.join(relase_path, file_name_new)

        print(f"Copy '{copy_path_src}' to '{copy_path_dst}'")
        shutil.copy(copy_path_src, copy_path_dst)


def main() -> None:
    """
    Builder main function.
    """
    #
    # Check environment variables.
    #
    
    if STM32CUBEIDE_PATH is None:
        print("Shell variable STM32CUBEIDE_PATH is not defined")
        sys.exit(1)
        
    if WORKSPACE_PATH is None:
        print("Shell variable WORKSPACE_PATH is not defined")
        sys.exit(1)
        
    if RELEASE_PATH is None:
        print("Shell variable RELEASE_PATH is not defined")
        sys.exit(1)

    print(f"STM32CUBEIDE_PATH: '{STM32CUBEIDE_PATH}'")
    print(f"WORKSPACE_PATH: '{WORKSPACE_PATH}'")
    print(f"RELEASE_PATH: '{RELEASE_PATH}'")

    #
    # Prepare log directory.
    #

    LOG_DIR_NAME = "build_log"

    shutil.rmtree(LOG_DIR_NAME, ignore_errors=True)
    os.makedirs(LOG_DIR_NAME)

    #
    # Build all.
    #

    time_start = timer()

    for project in projects:
        return_code, log_str = build_project(project)
        
        log_file_name = f"{normalize_path(project.path)}___{project.name}___{project.build_type}.txt"
        log_path = os.path.join(LOG_DIR_NAME, log_file_name)
        with open(log_path, "w", encoding="utf-8") as log_file:
            log_file.write(log_str)
        
        if return_code != 0:
            print(f"Build error (return code: {return_code}, see log: '{log_path}')")
            sys.exit(return_code)
            
        copy_artifacts(project)

    time_end = timer()
    time_elapsed = time_end - time_start
    print(f"\nBuild all completed (time: {time_elapsed:.3f} s)")


if __name__ == "__main__":
    main()
