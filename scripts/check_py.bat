cls
echo off

rem Python scripts tests
rem Required: ruff + pylint + mypy

echo Linter #1
set FLAG_RUFF=check --no-cache --select PLR6301,ANN001,ANN002,ANN003,ANN201,ANN202,ANN204,ANN205,ANN206 --preview
ruff %FLAG_RUFF% .

echo Linter #2
set FLAG_PYLINT=--output-format=colorized --disable C0103,C0115,C0116,C0209,C0301,C0303,C0325,R0801,R0902,R0903,R0912,R0913,R0914,R0915,R0917,R1716,R1730,R1731,W0201,W0246,W0707,W0718,W1202,W1203
pylint %FLAG_PYLINT% .

echo Type checker
set FLAG_MYPY=--follow-untyped-imports --cache-dir=nul
mypy %FLAG_MYPY% .

pause
