echo off

set USER_HOME=c:\Users\wr

set DIR_TO_CLEAN=%USER_HOME%\.stm32cubeide

IF EXIST "%DIR_TO_CLEAN%" (
    cd /d %DIR_TO_CLEAN%
    for /F "delims=" %%i in ('dir /b') do (rmdir "%%i" /s/q || del "%%i" /s/q)
)
