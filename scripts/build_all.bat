cls
echo off

set STM32CUBEIDE_PATH=d:\ST\STM32CubeIDE_1.18.0\STM32CubeIDE

set WORKSPACE_PATH=g:\projects\private\STM32CubeIDE\project
set RELEASE_PATH=g:\projects\private\STM32CubeIDE\release

python build_all.py

pause
