set WORKSPACE_DIR=..\project

rmdir /s /q build_log

rmdir /s /q %WORKSPACE_DIR%\CANUSB_M01\Debug
rmdir /s /q %WORKSPACE_DIR%\CANUSB_M01\Release

rmdir /s /q %WORKSPACE_DIR%\DAQ_M01\Debug
rmdir /s /q %WORKSPACE_DIR%\DAQ_M01\Release

rmdir /s /q %WORKSPACE_DIR%\DAQ_M02\Debug
rmdir /s /q %WORKSPACE_DIR%\DAQ_M02\Release

rmdir /s /q %WORKSPACE_DIR%\DAQ_M03\Appli\Debug
rmdir /s /q %WORKSPACE_DIR%\DAQ_M03\Appli\Release
rmdir /s /q %WORKSPACE_DIR%\DAQ_M03\Boot\Debug
rmdir /s /q %WORKSPACE_DIR%\DAQ_M03\Boot\Release
rmdir /s /q %WORKSPACE_DIR%\DAQ_M03\ExtMemLoader\Debug
rmdir /s /q %WORKSPACE_DIR%\DAQ_M03\ExtMemLoader\Release

rmdir /s /q %WORKSPACE_DIR%\RELAY_USB_M01\Debug
rmdir /s /q %WORKSPACE_DIR%\RELAY_USB_M01\Release

rmdir /s /q %WORKSPACE_DIR%\TMP_RH_RS485_M01\Debug
rmdir /s /q %WORKSPACE_DIR%\TMP_RH_RS485_M01\Release

rmdir /s /q %WORKSPACE_DIR%\VREF_M01\Debug
rmdir /s /q %WORKSPACE_DIR%\VREF_M01\Release

rmdir /s /q %WORKSPACE_DIR%\WGSM_150\Debug
rmdir /s /q %WORKSPACE_DIR%\WGSM_150\Release
