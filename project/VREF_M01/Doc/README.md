# VREF_M01

Project includes firmware for the device **VREF_M01**: https://gitlab.com/wojrus-projects/altium-designer/vref_m01_v1_1

## Project requirements

- Host OS: Windows 10+
- IDE: STM32CubeIDE 1.18.0
    - Download: https://www.st.com/en/development-tools/stm32cubeide.html
    - Replace installed `make.exe` with version from `STM32CubeIDE 1.13.2` (GNU Make 4.2.1_st_20200221-0903_longpath).<br>Directory: `STM32CubeIDE\plugins\com.st.stm32cube.ide.mcu.externaltools.make.XXX\tools\bin`
- Toolchain:
    - GCC 14.2
    - Release: arm-gnu-toolchain-14.2.rel1-mingw-w64-i686-arm-none-eabi
    - Download: https://developer.arm.com/downloads/-/arm-gnu-toolchain-downloads
    - Install new GCC in STM32CubeIDE:
        - Open: Main menu -> Window -> Preferences -> STM32Cube -> Toolchain Manager -> Add Local...
        - `Name` = `GCC 14.2`
        - `Prefix` = `arm-none-eabi-`
        - `Location` = `INSTALL_PATH\arm-gnu-toolchain-14.2.rel1-mingw-w64-i686-arm-none-eabi\bin`
- Language: C++20 GNU (with C++23 extensions)
- Hardware debugger:
    - [JLink EDU V10+](https://wiki.segger.com/J-Link_EDU_V10) or similar
    - SWD cable adapter: [Olimex ARM-JTAG-20-10](https://www.olimex.com/Products/ARM/JTAG/ARM-JTAG-20-10/) or similar
- Debugging tools:
    - Segger [Ozone](https://www.segger.com/products/development-tools/ozone-j-link-debugger/)
    - Segger [SystemView](https://www.segger.com/products/development-tools/systemview/)

## MCU flash programming

Use script: https://gitlab.com/wojrus-projects/embedded/stm32cubeide/stm32_workspace/-/blob/main/release/VREF_M01/flash.bat?ref_type=heads
