/*
 * Application HAL (board layer).
 */

#ifndef App_HAL_Board_hpp
#define App_HAL_Board_hpp

#include <cstdint>

#include "App_HAL_CubeMX.h"
#include "Interrupt_Priority.hpp"

namespace HAL::Board
{

inline constexpr uint32_t VBUF_ENABLE_DELAY_ms = 20; 

inline auto& SPI_DAC = hspi1;

}

#endif
