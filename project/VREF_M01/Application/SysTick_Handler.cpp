/*
 * SysTick application handler.
 */

#include <cstdint>

#include "App_Main.hpp"

static volatile bool SysTick_Handler_IsEnabled = false;
static volatile uint16_t FrequencyDivider_10ms = 0; 

void SysTick_Handler_Enable()
{
    FrequencyDivider_10ms = 0;
    SysTick_Handler_IsEnabled = true;
}

void SysTick_Handler_Disable()
{
    SysTick_Handler_IsEnabled = false;
    FrequencyDivider_10ms = 0;
}

extern "C" void App_SysTick(void)
{
    if (SysTick_Handler_IsEnabled)
    {
        //
        // Tasks executed with f=1000 Hz.
        //
        
        VREF.LED_Status.TimerHandler();
        
        //
        // Tasks executed with f=100 Hz.
        //
        
        if (++FrequencyDivider_10ms >= 10)
        {
            FrequencyDivider_10ms = 0;
            
            // Add app. code here.
        }
    }
}
