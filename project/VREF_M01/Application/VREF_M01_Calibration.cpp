/*
 * VREF_M01: calibration.
 */

#include <utility>
#include <cmath>

#include "Debug.h"
#include "App_Main.hpp"

namespace VREF_M01
{

static_assert(::Calibration::IsNVMCompatible<CalibrationDataContainer>());

void CalibrationCoefficient::Clear()
{
    Gain_V = 0.0f;
}

void CalibrationCoefficient::SetNominal()
{
    Gain_V = Nominal::Vout_Max_V;
}

bool CalibrationCoefficient::Verify() const
{
    return CalibrationDevice::VerifyVoltageMeasured(Gain_V);
}

void CalibrationData::Clear()
{
    for (auto& coefficient : CalibrationCoefficients)
    {
        coefficient.Clear();
    }
}

void CalibrationDevice::Reset()
{
    Container.Clear();
    CalibrationStageExpected = CalibrationStage::Invalid;
}

StatusCode CalibrationDevice::ProcessCalibrationGain(CalibrationStage calibrationStage, float voltageMeasured_V)
{
    StatusCode statusCode = StatusCode::Failure;
    
    DEBUG_PRINTF("Cal: gain calibration begin (stage: %u, Vout: %f V)\n",
            calibrationStage, (double)voltageMeasured_V);
    
    switch (calibrationStage)
    {
        case CalibrationStage::Stage_1_TestVoltageSet:
            {
                VREF.Reset(VoltageResetState::Off);
                
                statusCode = VREF.SetVoltageMax();
                if (statusCode == StatusCode::Success)
                {
                    CalibrationStageExpected = CalibrationStage::Stage_2_TestVoltageMeasure;
                }
            }
            break;
            
        case CalibrationStage::Stage_2_TestVoltageMeasure:
            {
                if (CalibrationStageExpected != CalibrationStage::Stage_2_TestVoltageMeasure)
                {
                    statusCode = StatusCode::Invalid_State;
                    goto ExitError;
                }
                
                if (not VerifyVoltageMeasured(voltageMeasured_V))
                {
                    statusCode = StatusCode::ADC_Calibration_Invalid_Voltage;
                    goto ExitError;
                }
                
                auto& coefficientNew = Container.Data.CalibrationCoefficients[std::to_underlying(VoltageOutputEnum::Output_1)];
                
                coefficientNew.Gain_V = voltageMeasured_V;
                
                statusCode = StatusCode::ADC_Calibration_Completed;
            }
            break;
            
        case CalibrationStage::Invalid:
        
        default:
            statusCode = StatusCode::Invalid_Argument;
    }
    
ExitError:
    
    if ((statusCode == StatusCode::Success) ||
        (statusCode == StatusCode::ADC_Calibration_Completed))
    {
        DEBUG_PRINTF("Cal: gain calibration success (completed: %u)\n",
                     statusCode == StatusCode::ADC_Calibration_Completed);
    }
    else
    {
        DEBUG_PRINTF("Cal: gain calibration error: %u\n", statusCode);
    }

    return statusCode;
}

std::optional<CalibrationCoefficient> CalibrationDevice::NVM_ReadCalibrationCoefficient()
{
    std::optional<CalibrationCoefficient> calibrationCoefficient;
    const auto pContainer = Container.NVM_GetAddress();
    
    if (pContainer->Verify())
    {
        const auto& coefficient_NVM = pContainer->Data.CalibrationCoefficients[std::to_underlying(VoltageOutputEnum::Output_1)];
        
        if (coefficient_NVM.Verify())
        {
            calibrationCoefficient = coefficient_NVM;
        }
    }
    
    return calibrationCoefficient;
}

StatusCode CalibrationDevice::NVM_Write()
{   
    Container.Build();
    
    return Container.NVM_Write();
}

bool CalibrationDevice::VerifyVoltageMeasured(float voltageMeasured_V)
{   
    return (std::isfinite(voltageMeasured_V) &&
            (voltageMeasured_V >= Calibration::VoltageMeasured_Min_V) &&
            (voltageMeasured_V <= Calibration::VoltageMeasured_Max_V));
}

}
