/*
 * VREF_M01: definitions.
 */

#ifndef VREF_M01_Definitions_hpp
#define VREF_M01_Definitions_hpp

#include <cstdint>
#include <cstddef>
#include <limits>

namespace VREF_M01
{

inline constexpr size_t VoltageOutputCount = 1;

enum class VoltageOutputEnum : size_t
{
    Output_1
};

// Raw DAC sample type.
using DAC_Sample = uint16_t;

// Extended DAC sample type for integer computations.
using DAC_SampleExt = uint32_t;

inline constexpr auto DAC_Sample_Min = std::numeric_limits<DAC_Sample>::min();
inline constexpr auto DAC_Sample_Max = std::numeric_limits<DAC_Sample>::max();

namespace Nominal
{
inline constexpr float Vref_V = 2.5f;
inline constexpr float BufferGain_VperV = 4.2f;
inline constexpr float Vout_Min_V = 0.0f;
inline constexpr float Vout_Max_V = Vref_V * BufferGain_VperV;
}

namespace Limits
{
inline constexpr float Vout_Min_V = Nominal::Vout_Min_V;
inline constexpr float Vout_Max_V = Nominal::Vout_Max_V + 1.0f;
}

namespace Calibration
{
inline constexpr float VoltageMeasured_Tolerance_PercentOfRange = 10.0f;
inline constexpr float VoltageMeasured_Min_V = Nominal::Vout_Max_V * ((100.0f - VoltageMeasured_Tolerance_PercentOfRange) / 100.0f);
inline constexpr float VoltageMeasured_Max_V = Nominal::Vout_Max_V * ((100.0f + VoltageMeasured_Tolerance_PercentOfRange) / 100.0f);
}

}

#endif
