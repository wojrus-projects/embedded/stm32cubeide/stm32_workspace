/*
 * Application CubeMX bindings.
 */

#ifndef App_Main_CubeMX_h
#define App_Main_CubeMX_h

#include <stddef.h>

#include "Debug.h"

#ifdef __cplusplus
extern "C" {
#endif

#define APP_USB_SERIAL_NUMBER_STRING_LENGTH_MAX     32U
#define APP_USB_SERIAL_NUMBER_DIGITS_MAX            10U

void App_Main(void);
void App_SysTick(void);
void App_GetUSBSerialNumber(uint8_t * const pStringBuffer, size_t bufferSize);

#ifdef __cplusplus
}
#endif

#endif
