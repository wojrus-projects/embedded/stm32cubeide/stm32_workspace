/*
 * VREF_M01: main file.
 */

#ifndef VREF_M01_hpp
#define VREF_M01_hpp

#include <cstdint>
#include <optional>

#include "Status_Code.hpp"
#include "App_HAL_MCU.hpp"
#include "App_HAL_Board.hpp"
#include "LED.hpp"
#include "USB_Queue.hpp"
#include "VREF_M01_Definitions.hpp"
#include "VREF_M01_Calibration.hpp"

namespace VREF_M01
{

namespace LED_Color_Status
{
inline constexpr LED_Color Off               = LED_Color::Black;
inline constexpr LED_Color Reset             = LED_Color::Cyan;
inline constexpr LED_Color Exception         = LED_Color::Red;
inline constexpr LED_Color Serial_Port_Close = LED_Color::Yellow;
inline constexpr LED_Color Serial_Port_Open  = LED_Color::Green;
inline constexpr LED_Color USB_Suspend       = LED_Color::Blue;
inline constexpr LED_Color USB_Resume        = Serial_Port_Close;
}

enum class VoltageResetState
{
    Off,
    Remain
};

class VREF_M01
{
public:
    
    LED                                   LED_Status;
    USB_Queue                             USB_TxQueue;
    CalibrationDevice                     Calibration;
    std::optional<CalibrationCoefficient> CalibrationCoefficientNVM;
    
    void Initialize();
    void Reset(VoltageResetState voltageResetState);
    void VBUF_PowerUp();
    void VBUF_PowerDown();
    StatusCode SetVoltage(float voltage_V);
    StatusCode SetVoltageMin();
    StatusCode SetVoltageMax();
    
private:
    
    bool VBUF_IsEnabled = false;
    
    void VBUF_Enable(bool powerState);
    DAC_Sample DAC_VoltageToSample(float voltage_V);
    StatusCode DAC_WriteSample(DAC_Sample dacSample);
};

}

#endif
