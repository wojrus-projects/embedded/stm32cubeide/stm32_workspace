/*
 * Utility functions.
 */

#ifndef Utilities_hpp
#define Utilities_hpp

#include <cstdint>
#include <cstddef>
#include <cstring>
#include <type_traits>

namespace Utilities
{

/*
 * Operator builder for enum class types.
 */
#define DEFINE_ENUM_CLASS_OPERATORS(Enum)                           \
    inline constexpr Enum operator|(Enum Lhs, Enum Rhs) {           \
        return static_cast<Enum>(                                   \
            static_cast<std::underlying_type_t<Enum>>(Lhs) |        \
            static_cast<std::underlying_type_t<Enum>>(Rhs));        \
    }                                                               \
    inline constexpr Enum operator&(Enum Lhs, Enum Rhs) {           \
        return static_cast<Enum>(                                   \
            static_cast<std::underlying_type_t<Enum>>(Lhs) &        \
            static_cast<std::underlying_type_t<Enum>>(Rhs));        \
    }                                                               \
    inline constexpr Enum operator^(Enum Lhs, Enum Rhs) {           \
        return static_cast<Enum>(                                   \
            static_cast<std::underlying_type_t<Enum>>(Lhs) ^        \
            static_cast<std::underlying_type_t<Enum>>(Rhs));        \
    }                                                               \
    inline constexpr Enum operator~(Enum E) {                       \
        return static_cast<Enum>(                                   \
            ~static_cast<std::underlying_type_t<Enum>>(E));         \
    }                                                               \
    inline Enum& operator|=(Enum& Lhs, Enum Rhs) {                  \
        return Lhs = static_cast<Enum>(                             \
                   static_cast<std::underlying_type_t<Enum>>(Lhs) | \
                   static_cast<std::underlying_type_t<Enum>>(Rhs)); \
    }                                                               \
    inline Enum& operator&=(Enum& Lhs, Enum Rhs) {                  \
        return Lhs = static_cast<Enum>(                             \
                   static_cast<std::underlying_type_t<Enum>>(Lhs) & \
                   static_cast<std::underlying_type_t<Enum>>(Rhs)); \
    }                                                               \
    inline Enum& operator^=(Enum& Lhs, Enum Rhs) {                  \
        return Lhs = static_cast<Enum>(                             \
                   static_cast<std::underlying_type_t<Enum>>(Lhs) ^ \
                   static_cast<std::underlying_type_t<Enum>>(Rhs)); \
    }                                                               \
    inline Enum operator++(Enum& E, int) {                          \
        const auto oldValue = E;                                    \
                                                                    \
        E = static_cast<Enum>(                                      \
            static_cast<std::underlying_type_t<Enum>>(E) + 1);      \
                                                                    \
        return oldValue;                                            \
    }

/*
 * Fast memset() variant.
 */
void memset_fast(void * pMemory, uint8_t fillValue, size_t byteCount);

/*
 * Return aligned object from bytes.
 */
template<typename ValueType>
ValueType FromBytes(const void * const pBytes)
{
    static_assert(std::is_const_v<ValueType> == false, "'const' is not supported");
    static_assert(std::is_volatile_v<ValueType> == false, "'volatile' is not supported");
    
    ValueType value;
    
    memcpy(&value, pBytes, sizeof(ValueType));
    
    return value;
}

/*
 * Copy object to bytes.
 */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
template<typename ValueType>
void ToBytes(const ValueType& value, void * const pBytes)
{
    memcpy(pBytes, (const void *)&value, sizeof(ValueType));
};
#pragma GCC diagnostic pop

/*
 * Integer unsigned division with ceiling.
 */
template<typename T>
constexpr T DivideIntegerCeil(T divident, T divisor)
{
    static_assert(std::is_integral_v<T> && std::is_unsigned_v<T>);
    
    return (divident / divisor) + (divident % divisor != 0);
}

}

#endif
