/*
 * Device information data.
 */

#include "Device_Info_Data.hpp"

namespace DeviceInfo
{

static constexpr DeviceInfo DeviceInfo_VREF_M01
{
    .Magic { 'I', 'N', 'F', 'O' },
    
    .HardwareType    = 4,
    .HardwareVariant = 0,
    
    .Firmware
    {
        .Version
        {
            .Major   = 1,
            .Minor   = 0,
            .Patch   = 0,
            .Variant = 0
        },
        .Date
        {
            .Year  = 2025,
            .Month = 3,
            .Day   = 4
        }
    }
};

__attribute__((section(".device_info"), __used__))
constinit const DeviceInfo DeviceInfoData = DeviceInfo_VREF_M01;

}
