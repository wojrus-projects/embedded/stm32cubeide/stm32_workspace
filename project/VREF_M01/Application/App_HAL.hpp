/*
 * Application HAL top level file.
 */

#ifndef App_HAL_hpp
#define App_HAL_hpp

#include "App_HAL_CMSIS.h"      // Layer 1: CMSIS
#include "App_HAL_CubeMX.h"     // Layer 2: CubeMX HAL
#include "App_HAL_MCU.hpp"      // Layer 3: MCU hardware
#include "App_HAL_Board.hpp"    // Layer 4: board hardware

#endif
