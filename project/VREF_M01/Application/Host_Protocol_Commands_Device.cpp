/*
 * Host protocol: device commands.
 */

#include "App_Main.hpp"
#include "Utilities.hpp"
#include "Thread_Main.hpp"
#include "Host_Protocol_Commands.hpp"

namespace Host_Protocol
{

//
// Command payload: [OUTPUT_VOLTAGE 4B (float32, V)]
//
//  Answer payload: -
//
StatusCode Command_Set_Voltage(PayloadBuffer& commandPayloadBuffer, [[maybe_unused]] BufferByte& answerPayloadBuffer)
{
    StatusCode statusCode = StatusCode::Failure;
    
    if (commandPayloadBuffer.DataSize != 4)
    {
        return StatusCode::Invalid_Argument;
    }
    
    //-----------------------------------------------------------------------------------------
    Mutex_VREF_Lock();
    //-----------------------------------------------------------------------------------------
    
    const auto outputVoltage_V = Utilities::FromBytes<float>(&commandPayloadBuffer.Data[0]);
    
    statusCode = VREF.SetVoltage(outputVoltage_V);
    
    //-----------------------------------------------------------------------------------------
    Mutex_VREF_Unlock();
    //-----------------------------------------------------------------------------------------
    
    return statusCode;
}

//
// Command payload: Case 1 = [CALIBRATION_STAGE 1B]                                if CALIBRATION_STAGE = Stage_1_TestVoltageSet
//                  Case 2 = [CALIBRATION_STAGE 1B][TEST_VOLTAGE 4B (float32, V)]  if CALIBRATION_STAGE = Stage_2_TestVoltageMeasure
//
//  Answer payload: -
//
StatusCode Command_Calibration_Process(PayloadBuffer& commandPayloadBuffer, [[maybe_unused]] BufferByte& answerPayloadBuffer)
{
    StatusCode statusCode = StatusCode::Failure;
    VREF_M01::CalibrationStage calibrationStage;
    
    // Minimum: CALIBRATION_STAGE.
    
    if (commandPayloadBuffer.DataSize < 1)
    {
        return StatusCode::Invalid_Argument;
    }
    
    //-----------------------------------------------------------------------------------------
    Mutex_VREF_Lock();
    //-----------------------------------------------------------------------------------------
    
    calibrationStage = (VREF_M01::CalibrationStage)commandPayloadBuffer.Data[0];
    
    switch (calibrationStage)
    {
        case VREF_M01::CalibrationStage::Stage_1_TestVoltageSet:
            {
                if (commandPayloadBuffer.DataSize != 1)
                {
                    statusCode = StatusCode::Invalid_Argument;
                    goto CancelOperation;
                }
                
                statusCode = VREF.Calibration.ProcessCalibrationGain(calibrationStage, 0.0f);
            }
            break;
            
        case VREF_M01::CalibrationStage::Stage_2_TestVoltageMeasure:
            {   
                if (commandPayloadBuffer.DataSize != (1 + 4))
                {
                    statusCode = StatusCode::Invalid_Argument;
                    goto CancelOperation;
                }
                
                const auto voltageMeasured_V = Utilities::FromBytes<float>(&commandPayloadBuffer.Data[1]);
                
                statusCode = VREF.Calibration.ProcessCalibrationGain(calibrationStage, voltageMeasured_V);
                if (statusCode == StatusCode::ADC_Calibration_Completed)
                {
                    statusCode = VREF.Calibration.NVM_Write();
                    if (statusCode == StatusCode::Success)
                    {
                        VREF.Reset(VREF_M01::VoltageResetState::Off);
                    }
                }
            }
            break;
        
        case VREF_M01::CalibrationStage::Invalid:
        
        default:
            statusCode = StatusCode::Invalid_Argument;
            goto CancelOperation;
    }

CancelOperation:
    
    if (statusCode != StatusCode::Success)
    {
        VREF.Reset(VREF_M01::VoltageResetState::Off);
    }

    //-----------------------------------------------------------------------------------------
    Mutex_VREF_Unlock();
    //-----------------------------------------------------------------------------------------

    return statusCode;
}

//
// Command payload: -
//
//  Answer payload: [CALIBRATION_DATA nB]
//
StatusCode Command_Calibration_Data_Read(PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer)
{
    static_assert(sizeof(VREF_M01::CalibrationDataContainer) <= PAYLOAD_SIZE_MAX);
    
    if (commandPayloadBuffer.DataSize != 0)
    {
        return StatusCode::Invalid_Argument;
    }
    
    //-----------------------------------------------------------------------------------------
    Mutex_VREF_Lock();
    //-----------------------------------------------------------------------------------------
    
    memcpy(&answerPayloadBuffer.Data[0], VREF.Calibration.Container.NVM_GetAddress(),
            sizeof(VREF_M01::CalibrationDataContainer));
    
    //-----------------------------------------------------------------------------------------
    Mutex_VREF_Unlock();
    //-----------------------------------------------------------------------------------------
    
    answerPayloadBuffer.DataSize = sizeof(VREF_M01::CalibrationDataContainer);
    
    return StatusCode::Success;
}

//
// Command payload: [CALIBRATION_DATA nB]
//  Answer payload: -
//
StatusCode Command_Calibration_Data_Write(PayloadBuffer& commandPayloadBuffer, [[maybe_unused]] BufferByte& answerPayloadBuffer)
{
    static_assert(sizeof(VREF_M01::CalibrationDataContainer) <= PAYLOAD_SIZE_MAX);
    
    StatusCode statusCode = StatusCode::Failure;
    
    if (commandPayloadBuffer.DataSize != sizeof(VREF_M01::CalibrationDataContainer))
    {
        return StatusCode::Invalid_Argument;
    }
    
    //-----------------------------------------------------------------------------------------
    Mutex_VREF_Lock();
    //-----------------------------------------------------------------------------------------
    
    const auto pCalibrationDataContainer = reinterpret_cast<const VREF_M01::CalibrationDataContainer *>(&commandPayloadBuffer.Data[0]);

    statusCode = pCalibrationDataContainer->NVM_Write();
    if (statusCode == StatusCode::Success)
    {
        VREF.Reset(VREF_M01::VoltageResetState::Off);
    }
    
    //-----------------------------------------------------------------------------------------
    Mutex_VREF_Unlock();
    //-----------------------------------------------------------------------------------------
    
    return statusCode;
}

}
