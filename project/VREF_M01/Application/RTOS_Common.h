/*
 * RTOS common code.
 */

#ifndef RTOS_Common_h
#define RTOS_Common_h

#include "tx_api.h"
#include "ux_api.h"

#ifdef __cplusplus
extern "C" {
#endif

UINT RTOS_AppInitialize(void);
void RTOS_CheckStack(void);

inline void RTOS_Sleep(ULONG time_ms)
{
    tx_thread_sleep(UX_MS_TO_TICK_NON_ZERO(time_ms));
}

#ifdef __cplusplus
}
#endif

#endif
