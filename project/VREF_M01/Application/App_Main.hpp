/*
 * VREF_M01 firmware.
 */

#ifndef App_Main_hpp
#define App_Main_hpp

#include "VREF_M01.hpp"
#include "Host_Protocol.hpp"

#define DEVICE_NAME     "VREF_M01"

extern VREF_M01::VREF_M01 VREF;
extern Host_Protocol::Host_Protocol HostProtocol;

#endif
