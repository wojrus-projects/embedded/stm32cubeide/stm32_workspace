/*
 * SysTick application handler.
 */

#ifndef SysTick_Handler_hpp
#define SysTick_Handler_hpp

void SysTick_Handler_Enable();
void SysTick_Handler_Disable();

#endif
