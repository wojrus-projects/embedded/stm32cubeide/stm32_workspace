/*
 * Calibration data container.
 */

#ifndef Calibration_Data_Container_hpp
#define Calibration_Data_Container_hpp

#include <cstdint>
#include <cstddef>
#include <concepts>
#include <type_traits>

#include "Debug.h"
#include "App_HAL_MCU.hpp"
#include "Status_Code.hpp"
#include "CRC16.hpp"
#include "Device_Info.hpp"

namespace Calibration
{

enum class ContainerState : uint8_t
{
    Invalid = 0,
    Valid   = 1
};

struct [[gnu::packed]] ContainerHeader
{
    char           Magic[4] { 'C', 'A', 'L', '0' };
    uint32_t       Size = 0;
    uint32_t       HardwareType = 0;
    uint32_t       HardwareVariant = 0;
    ContainerState State = ContainerState::Invalid;
    uint8_t        Reserved[16] {};
    
    void Clear();
    bool Verify() const;
};

constexpr auto CalibrationDataStorageSize = HAL::CONFIG_APPLICATION_SIZE_BYTES;

template<typename T>
constexpr bool IsNVMCompatible()
{   
    return (std::is_class_v<T> &&
            std::is_standard_layout_v<T> &&
            std::is_trivially_copyable_v<T> &&
            (sizeof(T) <= CalibrationDataStorageSize) &&
            (alignof(T) == 1));
}

namespace Concept
{

template<typename T>
concept Clearable = requires(T data)
{
    { data.Clear() } -> std::same_as<void>;
};

template<typename T>
concept CalibrationData = IsNVMCompatible<T>() && Clearable<T>;

}

template
<
    Concept::CalibrationData      T_CalibrationData,
    const DeviceInfo::DeviceInfo& T_DeviceInfo
>
struct [[gnu::packed]] CalibrationDataContainer
{
    ContainerHeader   Header;
    T_CalibrationData Data {};
    uint16_t          CRC16 = 0;
    
    size_t GetSize() const
    {
        return sizeof(*this);
    }
    
    void Clear()
    {
        Header.Clear();
        Data.Clear();
        CRC16 = 0;
    }
    
    void BuildFrom(const T_CalibrationData * const pCalibrationData)
    {
        Header.Clear();

        Header.Size = GetSize();
        Header.HardwareType = T_DeviceInfo.HardwareType;
        Header.HardwareVariant = T_DeviceInfo.HardwareVariant;
        Header.State = ContainerState::Valid;
        
        if (pCalibrationData != nullptr)
        {
            Data = *pCalibrationData;
        }
        
        const size_t crcDataSize = GetSize() - sizeof(CRC16);
        
        CRC16 = CRC16_Calculate(CRC16_InitializeValue, this, crcDataSize);
    }
    
    void Build()
    {
        BuildFrom(nullptr);
    }
    
    bool Verify() const
    {
        const size_t crcDataSize = GetSize() - sizeof(CRC16);
        
        const auto crcCalculated = CRC16_Calculate(CRC16_InitializeValue, this, crcDataSize);
        
        const bool isContainerValid = (crcCalculated == CRC16) &&
                                      Header.Verify() &&
                                      (Header.Size == GetSize()) &&
                                      (Header.HardwareType == T_DeviceInfo.HardwareType) &&
                                      (Header.HardwareVariant == T_DeviceInfo.HardwareVariant);
        return isContainerValid;
    }
    
    const CalibrationDataContainer * NVM_GetAddress() const
    {
        return reinterpret_cast<const CalibrationDataContainer *>(HAL::CONFIG_APPLICATION_ADDRESS);
    }
    
    bool NVM_Verify() const
    {
        const auto pContainerNVM = NVM_GetAddress();
        
        return pContainerNVM->Verify();
    }
    
    void NVM_Read()
    {
        const auto pContainerNVM = NVM_GetAddress();
        
        *this = *pContainerNVM;
    }
    
    StatusCode NVM_Write() const
    {
        StatusCode statusCode = StatusCode::Failure;
        const uint32_t flashAddress = reinterpret_cast<uint32_t>(NVM_GetAddress());
        
        if (not Verify())
        {
            DEBUG_PRINTF("Cal: calibration verify error\n");
            
            return StatusCode::Invalid_Calibration;
        }
        
        DEBUG_PRINTF("Cal: write calibration to flash at %p (size: %u)\n",
                flashAddress, GetSize());
        
        statusCode = HAL::FlashErase(flashAddress, GetSize());
        if (statusCode == StatusCode::Success)
        {
            statusCode = HAL::FlashWrite(flashAddress, this, GetSize());
        }
        
        DEBUG_PRINTF("Cal: write status: %u\n", statusCode);
        
        return statusCode;
    }
};

}

#endif
