/*
 * VREF_M01: calibration.
 */

/*
 * Device require gain calibration.
 * Gain calibration is one point type for max. output (set DAC=0xFFFF then measure Vout).
 * Calibration data is stored in MCU flash memory.
 * If device is uncalibrated then use nominal (ideal) gain coefficient.
 * 
 * Full calibration procedure (2 steps in row, order is important, USB Serial Port must not disconnect between steps):
 * ------------+----------------+---------------------------+----------------------------------------------------------
 * Step number | Description    | Required status           | API function
 * ------------+----------------+---------------------------+----------------------------------------------------------
 * 1           | Set DAC=0xFFFF | Success                   | ProcessCalibrationGain(Stage_1_TestVoltageSet, 0.0)
 * 2           | Measure Vout   | ADC_Calibration_Completed | ProcessCalibrationGain(Stage_2_TestVoltageMeasure, Vout)
 * ------------+----------------+---------------------------+----------------------------------------------------------
 */

#ifndef VREF_M01_Calibration_hpp
#define VREF_M01_Calibration_hpp

#include <cstdint>
#include <optional>

#include "Status_Code.hpp"
#include "Calibration_Data_Container.hpp"
#include "Device_Info_Data.hpp"

namespace VREF_M01
{

enum class CalibrationStage : uint8_t
{
    Invalid,
    Stage_1_TestVoltageSet,
    Stage_2_TestVoltageMeasure
};

struct [[gnu::packed]] CalibrationCoefficient
{
    // Gain coefficient (defined as measured output voltage after set DAC value = 0xFFFF).
    
    float Gain_V = 0.0f;
    
    void Clear();
    void SetNominal();
    bool Verify() const;
};

struct [[gnu::packed]] CalibrationData
{
    CalibrationCoefficient CalibrationCoefficients[VoltageOutputCount];
    
    void Clear();
};

using CalibrationDataContainer = ::Calibration::CalibrationDataContainer<CalibrationData, DeviceInfo::DeviceInfoData>;

class CalibrationDevice
{
public:

    CalibrationDataContainer Container;
    
    void                                  Reset();
    StatusCode                            ProcessCalibrationGain(CalibrationStage calibrationStage, float voltageMeasured_V);
    std::optional<CalibrationCoefficient> NVM_ReadCalibrationCoefficient();
    StatusCode                            NVM_Write();
    
    static bool                           VerifyVoltageMeasured(float voltageMeasured_V);
    
private:

    CalibrationStage CalibrationStageExpected = CalibrationStage::Invalid;    
};

}

#endif
