/*
 * VREF_M01: main file.
 */

#include <cmath>
#include <algorithm>

#include "Debug.h"
#include "RTOS_Common.h"
#include "VREF_M01.hpp"

namespace VREF_M01
{

void VREF_M01::Initialize()
{
    LED_Status.Initialize(LED_HAL_SetColorStatus);
    
    Reset(VoltageResetState::Off);
    VBUF_PowerDown();
}

void VREF_M01::Reset(VoltageResetState voltageResetState)
{
    DEBUG_PRINTF("VREF: reset\n");
    
    Calibration.Reset();
    CalibrationCoefficientNVM = Calibration.NVM_ReadCalibrationCoefficient();
    USB_TxQueue.Clear();
    
    if (voltageResetState == VoltageResetState::Off)
    {
        SetVoltageMin();
    }
    else if (voltageResetState == VoltageResetState::Remain)
    {
        if (VBUF_IsEnabled)
        {
            // Pass
        }
        else
        {
            SetVoltageMin();
        }
    }
    else
    {
        DEBUG_EXCEPTION();
    }
}

void VREF_M01::VBUF_Enable(bool powerState)
{
    VBUF_IsEnabled = not not powerState;
    
    HAL_GPIO_WritePin(VBUF_EN_GPIO_Port, VBUF_EN_Pin,
            VBUF_IsEnabled ? GPIO_PIN_SET : GPIO_PIN_RESET);
    
    DEBUG_PRINTF_EXT("VBUF: %u", VBUF_IsEnabled);
}

void VREF_M01::VBUF_PowerUp()
{
    if (not VBUF_IsEnabled)
    {
        VBUF_Enable(true);
        
        RTOS_Sleep(HAL::Board::VBUF_ENABLE_DELAY_ms);
    }
}

void VREF_M01::VBUF_PowerDown()
{
    VBUF_Enable(false);
}

StatusCode VREF_M01::SetVoltage(float voltage_V)
{
    if (not std::isfinite(voltage_V))
    {
        return StatusCode::Invalid_Argument;
    }
    
    const DAC_Sample dacSample = DAC_VoltageToSample(voltage_V);
    
    DEBUG_PRINTF("VREF: set %f V, sample: %u\n",
            (double)voltage_V, dacSample);
    
    return DAC_WriteSample(dacSample);
}

StatusCode VREF_M01::SetVoltageMin()
{
    return DAC_WriteSample(DAC_Sample_Min);
}

StatusCode VREF_M01::SetVoltageMax()
{
    return DAC_WriteSample(DAC_Sample_Max);
}

DAC_Sample VREF_M01::DAC_VoltageToSample(float voltage_V)
{
    CalibrationCoefficient calibrationCoefficient;
    
    if (CalibrationCoefficientNVM)
    {
        calibrationCoefficient = CalibrationCoefficientNVM.value();
    }
    else
    {
        calibrationCoefficient.SetNominal();
    }
    
    DEBUG_ASSERT(calibrationCoefficient.Verify());
    
    voltage_V = std::clamp(voltage_V, Limits::Vout_Min_V, Limits::Vout_Max_V);
    
    const DAC_SampleExt dacSampleExt = (DAC_SampleExt)((float)DAC_Sample_Max * (voltage_V / calibrationCoefficient.Gain_V));
    
    const DAC_Sample dacSample = (DAC_Sample)std::clamp(dacSampleExt, (DAC_SampleExt)DAC_Sample_Min, (DAC_SampleExt)DAC_Sample_Max);
    
    return dacSample;
}

StatusCode VREF_M01::DAC_WriteSample(DAC_Sample dacSample)
{
    const uint8_t spiData[sizeof(DAC_Sample)]
    {
        (uint8_t)(dacSample >> 0),
        (uint8_t)(dacSample >> 8)
    };
    
    const HAL_StatusTypeDef halStatus = HAL_SPI_Transmit(&HAL::Board::SPI_DAC, spiData, 1, HAL_MAX_DELAY);

    if (halStatus != HAL_OK)
    {
        return StatusCode::Failure;
    }
    
    return StatusCode::Success;
}

}
