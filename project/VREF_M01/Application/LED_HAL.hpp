/*
 * LED driver HAL.
 */

#ifndef LED_HAL_hpp
#define LED_HAL_hpp

#include "LED_Color.hpp"

void LED_HAL_SetColorStatus(LED_Color color);

#endif
