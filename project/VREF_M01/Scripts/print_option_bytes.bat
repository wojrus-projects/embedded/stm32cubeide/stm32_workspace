cls
echo off

rem Requirements:
rem 1. ST-LINK V2/V3 or Segger J-Link
rem 2. STM32CubeProgrammer v2.18.0+

rem Select programming interface:
rem 1. J-Link:  IF_TYPE=JLINK
rem 2. ST-LINK: IF_TYPE=SWD
set IF_TYPE=JLINK

set STM32_PROGRAMMER="d:\ST\STM32CubeProgrammer\bin\STM32_Programmer_CLI.exe"

echo Print Option Bytes

%STM32_PROGRAMMER% --connect port=%IF_TYPE% --optionbytes displ
if %ERRORLEVEL% NEQ 0 exit /B %ERRORLEVEL%

echo Done
pause
