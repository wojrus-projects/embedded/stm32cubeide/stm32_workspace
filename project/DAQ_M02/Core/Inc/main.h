/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include "spi.h"
#include "tim.h"
#include "iwdg.h"
#include "usb.h"

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MUX_A0_Pin GPIO_PIN_2
#define MUX_A0_GPIO_Port GPIOA
#define MUX_A1_Pin GPIO_PIN_3
#define MUX_A1_GPIO_Port GPIOA
#define LED_TRG_R_Pin GPIO_PIN_4
#define LED_TRG_R_GPIO_Port GPIOC
#define LED_TRG_G_Pin GPIO_PIN_0
#define LED_TRG_G_GPIO_Port GPIOB
#define LED_TRG_B_Pin GPIO_PIN_1
#define LED_TRG_B_GPIO_Port GPIOB
#define REL_H_Pin GPIO_PIN_2
#define REL_H_GPIO_Port GPIOB
#define REL_L_Pin GPIO_PIN_12
#define REL_L_GPIO_Port GPIOB
#define TRIGGER_Pin GPIO_PIN_13
#define TRIGGER_GPIO_Port GPIOB
#define LED_STA_G_Pin GPIO_PIN_15
#define LED_STA_G_GPIO_Port GPIOB
#define LED_STA_R_Pin GPIO_PIN_8
#define LED_STA_R_GPIO_Port GPIOA
#define LED_STA_B_Pin GPIO_PIN_9
#define LED_STA_B_GPIO_Port GPIOA
#define VISO_ON_Pin GPIO_PIN_6
#define VISO_ON_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
