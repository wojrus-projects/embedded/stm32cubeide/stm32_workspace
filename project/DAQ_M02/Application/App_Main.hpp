/*
 * DAQ_M02 firmware.
 */

#ifndef App_Main_hpp
#define App_Main_hpp

#include "DAQ_M02.hpp"
#include "Host_Protocol.hpp"

#define DEVICE_NAME     "DAQ_M02"

extern DAQ_M02::DAQ_M02 DAQ;
extern Host_Protocol::Host_Protocol HostProtocol;

#endif
