/*
 * Host protocol: standard commands.
 */

#ifndef Host_Protocol_Commands_Standard_hpp
#define Host_Protocol_Commands_Standard_hpp

#include "Host_Protocol_Definitions.hpp"
#include "Status_Code.hpp"

namespace Host_Protocol
{

StatusCode Command_Reset          (PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer);
StatusCode Command_Get_Device_Info(PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer);
StatusCode Command_Memory_Write   (PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer);
StatusCode Command_Memory_Read    (PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer);
StatusCode Command_Memory_Erase   (PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer);
StatusCode Command_Memory_Program (PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer);

}

#endif
