/*
 * ThreadX debugging utilities.
 */

#include "Debug.h"
#include "Debug_RTOS.h"

void Debug_RTOS_PrintPoolInfo(TX_BYTE_POOL * pBytePool)
{
    UINT status;
    
    CHAR * pName;
    ULONG availableBytes;
    ULONG fragments;
    ULONG suspendedCount;
    
    status = tx_byte_pool_info_get(pBytePool, &pName, &availableBytes, &fragments, nullptr, &suspendedCount, nullptr);
    DEBUG_ASSERT(status == TX_SUCCESS);
    
    DEBUG_PRINTF("TX byte pool info:\n");
    DEBUG_PRINTF("  name: '%s'\n", pName);
    DEBUG_PRINTF("  available bytes: %u\n", availableBytes);
    DEBUG_PRINTF("  fragments: %u\n", fragments);
    DEBUG_PRINTF("  suspended_count: %u\n", suspendedCount);
}
