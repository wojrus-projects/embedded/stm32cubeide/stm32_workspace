/*
 * ADC Microchip MCP33131D.
 */

#ifndef ADC_MCP33131D_hpp
#define ADC_MCP33131D_hpp

#include <utility>
#include <optional>

#include "Debug.h"
#include "App_HAL_CubeMX.h"
#include "Status_Code.hpp"
#include "Utilities.hpp"
#include "ADC_MCP33131D_Definitions.hpp"

namespace ADC
{

template
<
    SPI_HandleTypeDef& SPI,
    TIM_HandleTypeDef& Timer,
    size_t BufferStaticSizeSamples
>
class ADC_MCP33131D
{
public:
    
    using ReadCompletedCallback = void (*)(Sample * pSamples, size_t samplesCount);
    
    static void Initialize(ReadCompletedCallback callback)
    {
        ReadCompletedCb = callback;
        
        //
        // DMA channel Timer (function: ADC sampling period, trigger DMA SPI Tx)
        //
        
        HAL_DMA_RegisterCallback(DMA_Timer, HAL_DMA_XFER_ERROR_CB_ID, SPI_Tx_DMA_Error);
        
        __HAL_TIM_ENABLE_DMA(&Timer, TIM_DMA_UPDATE);
        
        //
        // DMA channel SPI Rx (function: ADC data read)
        //
        
        HAL_DMA_RegisterCallback(DMA_SPI_Rx, HAL_DMA_XFER_CPLT_CB_ID, SPI_Rx_DMA_Completed);
        HAL_DMA_RegisterCallback(DMA_SPI_Rx, HAL_DMA_XFER_HALFCPLT_CB_ID, SPI_Rx_DMA_Half);
        HAL_DMA_RegisterCallback(DMA_SPI_Rx, HAL_DMA_XFER_ERROR_CB_ID, SPI_Rx_DMA_Error);
        
        SET_BIT(SPI.Instance->CR2, SPI_CR2_TXDMAEN);
        SET_BIT(SPI.Instance->CR2, SPI_CR2_RXDMAEN);
        
        __HAL_SPI_ENABLE(&SPI);
    }

    static void Deinitialize()
    {
        Stop();
        
        __HAL_SPI_DISABLE(&SPI);
        
        CLEAR_BIT(SPI.Instance->CR2, SPI_CR2_TXDMAEN);
        CLEAR_BIT(SPI.Instance->CR2, SPI_CR2_RXDMAEN);
        
        __HAL_TIM_DISABLE_DMA(&Timer, TIM_DMA_UPDATE);
    }
    
    static Sample * GetBuffer()
    {
        return SpiRxBuffer;
    }
    
    static constexpr size_t GetBufferSizeMinSamples()
    {
        return BufferSizeMinSamples;
    }
    
    static constexpr size_t GetBufferSizeMaxSamples()
    {
        return BufferStaticSizeSamples;
    }
    
    static size_t GetBufferSizeSamples()
    {
        return BufferDynamicSizeSamples;
    }
    
    static StatusCode Start(uint32_t samplingFrequency_Hz,
                            uint32_t * const pSamplingFrequencyReal_Hz,
                            bool dmaBufferResize,
                            size_t dmaBufferNewSize)
    {
        HAL_StatusTypeDef halStatus;
        
        const auto timerRegisters = Timer_CalculateRegisters(HAL_RCC_GetPCLK1Freq(), samplingFrequency_Hz);
        
        if (not timerRegisters)
        {
            return StatusCode::Invalid_Argument;
        }
        
        const auto [TIM_PSC, TIM_ARR] = *timerRegisters;
        
        Timer_SetRegisters(TIM_PSC, TIM_ARR);
        
        *pSamplingFrequencyReal_Hz = Timer_CalculateFrequency(TIM_PSC, TIM_ARR);
        
        if (dmaBufferResize)
        {
            if ((dmaBufferNewSize < BufferSizeMinSamples) ||
                (dmaBufferNewSize > BufferStaticSizeSamples) ||
                ((dmaBufferNewSize % 2) != 0))
            {
                return StatusCode::Invalid_Argument;
            }
            
            BufferDynamicSizeSamples = dmaBufferNewSize;
        }
        else
        {
            BufferDynamicSizeSamples = BufferStaticSizeSamples;
        }
        
        const uint32_t SpiTxRegister = (uint32_t)&SPI.Instance->DR;
        const uint32_t SpiRxRegister = (uint32_t)&SPI.Instance->DR;
        
        // OPT: timer DMA do not increment source address SPI Tx buffer.
        static Sample SpiTxBuffer = SDI_DefaultState;
        
        halStatus = HAL_DMA_Start_IT(DMA_Timer, (uint32_t)&SpiTxBuffer, SpiTxRegister, BufferDynamicSizeSamples);
        DEBUG_ASSERT(halStatus == HAL_OK);
        
        halStatus = HAL_DMA_Start_IT(DMA_SPI_Rx, SpiRxRegister, (uint32_t)&SpiRxBuffer[0], BufferDynamicSizeSamples);
        DEBUG_ASSERT(halStatus == HAL_OK);
        
        halStatus = HAL_TIM_Base_Start(&Timer);
        DEBUG_ASSERT(halStatus == HAL_OK);

        return StatusCode::Success;
    }
    
    static void Stop()
    {
        HAL_TIM_Base_Stop(&Timer);
        HAL_DMA_Abort_IT(DMA_Timer);
        HAL_DMA_Abort_IT(DMA_SPI_Rx);
    }
    
    // ADC require clean/refresh 2-stage pipeline right before acquisition.
    // Pipeline consists stages:
    // - First stage (t_cnv):  ADC convert new sample.
    // - Second stage (t_acq): ADC SPI send old sample.
    static void ClearPipeline()
    {
        constexpr uint32_t SamplingPeriod_us = Utilities::DivideIntegerCeil(1'000'000ul, ADC_SAMPLING_FREQUENCY_MAX_Hz);
        
        HAL_StatusTypeDef halStatus;
        Sample txBuffer = SDI_DefaultState;
        
        halStatus = HAL_SPI_Transmit(&SPI, (uint8_t *)&txBuffer, 1, HAL_MAX_DELAY);
        DEBUG_ASSERT(halStatus == HAL_OK);
        
        DEBUG_TIMER_DELAY_US(SamplingPeriod_us);
        
        halStatus = HAL_SPI_Transmit(&SPI, (uint8_t *)&txBuffer, 1, HAL_MAX_DELAY);
        DEBUG_ASSERT(halStatus == HAL_OK);
        
        DEBUG_TIMER_DELAY_US(SamplingPeriod_us);
    }

private:
    
    using TimerRegisters = std::pair<uint16_t, uint16_t>;
    
    static std::optional<TimerRegisters> Timer_CalculateRegisters(uint32_t timerClockFrequency_Hz, uint32_t adcSamplingFrequency_Hz)
    {
        constexpr uint32_t TimerBits = 16;
        constexpr uint32_t TimerCountMax = 1UL << TimerBits;
        
        uint32_t TIM_PSC = 1;
        uint32_t TIM_ARR = 1;
        
        if ((timerClockFrequency_Hz == 0) ||
            (adcSamplingFrequency_Hz < ADC_SAMPLING_FREQUENCY_MIN_Hz) ||
            (adcSamplingFrequency_Hz > ADC_SAMPLING_FREQUENCY_MAX_Hz))
        {
            return {};
        }

        TIM_ARR = timerClockFrequency_Hz / (adcSamplingFrequency_Hz * TIM_PSC);
        if (TIM_ARR > TimerCountMax)
        {
            TIM_PSC = Utilities::DivideIntegerCeil(timerClockFrequency_Hz, adcSamplingFrequency_Hz * TimerCountMax);
            TIM_ARR = timerClockFrequency_Hz / (adcSamplingFrequency_Hz * TIM_PSC);
        }
        
        if (not ((TIM_PSC >= 1) && (TIM_PSC <= TimerCountMax) &&
                 (TIM_ARR >= 1) && (TIM_ARR <= TimerCountMax)))
        {
            return {};
        }

        return { TimerRegisters{ TIM_PSC - 1, TIM_ARR - 1 } };
    }
    
    static void Timer_SetRegisters(uint16_t TIM_PSC, uint16_t TIM_ARR)
    {
        Timer.Instance->ARR = TIM_ARR;
        Timer.Instance->PSC = TIM_PSC;
    }
    
    static uint32_t Timer_CalculateFrequency(uint16_t TIM_PSC, uint16_t TIM_ARR)
    {
        const uint32_t frequency_Hz = HAL_RCC_GetPCLK1Freq() / (((uint32_t)TIM_PSC + 1UL) * ((uint32_t)TIM_ARR + 1UL));
        
        return frequency_Hz;
    }
    
    [[noreturn]]
    static void SPI_Tx_DMA_Error([[maybe_unused]] DMA_HandleTypeDef * pDMA)
    {
        DEBUG_PRINTF("ADC: SPI Tx DMA error: %u\n", HAL_DMA_GetError(pDMA));
        DEBUG_EXCEPTION();
    }
    
    static void SPI_Rx_DMA_Half([[maybe_unused]] DMA_HandleTypeDef * pDMA)
    {
        ReadCompletedCb(&SpiRxBuffer[0], BufferDynamicSizeSamples / 2);
    }
    
    static void SPI_Rx_DMA_Completed([[maybe_unused]] DMA_HandleTypeDef * pDMA)
    {
        ReadCompletedCb(&SpiRxBuffer[BufferDynamicSizeSamples / 2], BufferDynamicSizeSamples / 2);
    }
    
    [[noreturn]]
    static void SPI_Rx_DMA_Error([[maybe_unused]] DMA_HandleTypeDef * pDMA)
    {
        DEBUG_PRINTF("ADC: SPI Rx DMA error: %u\n", HAL_DMA_GetError(pDMA));
        DEBUG_EXCEPTION();
    }
    
    static constexpr size_t HalfBufferSizeMinSamples = 2;
    static constexpr size_t HalfBufferSizeMaxSamples = 2048;
    
    static constexpr size_t BufferSizeMinSamples = 2 * HalfBufferSizeMinSamples;
    static constexpr size_t BufferSizeMaxSamples = 2 * HalfBufferSizeMaxSamples;
    
    static_assert((BufferStaticSizeSamples >= BufferSizeMinSamples) &&
                  (BufferStaticSizeSamples <= BufferSizeMaxSamples) &&
                  ((BufferStaticSizeSamples % 2) == 0));
    
    // DS chapter 7.0: ADC SDI pin must maintain "High" during the entire t_CYC=1/fs.
    static constexpr Sample SDI_DefaultState = (Sample)0xFFFF;
    
    static inline ReadCompletedCallback ReadCompletedCb {};
    static inline Sample SpiRxBuffer[BufferStaticSizeSamples];
    static inline size_t BufferDynamicSizeSamples = BufferStaticSizeSamples;
    
    static inline auto& DMA_Timer = Timer.hdma[TIM_DMA_ID_UPDATE];
    static inline auto& DMA_SPI_Rx = SPI.hdmarx;
};

}

#endif
