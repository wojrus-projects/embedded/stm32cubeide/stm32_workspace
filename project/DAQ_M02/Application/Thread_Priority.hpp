/*
 * Threads priority.
 */

#ifndef Thread_Priority_hpp
#define Thread_Priority_hpp

#include "tx_api.h"
#include "ux_device_cdc_acm.h"

enum class ThreadPriority : UINT
{
    Default    = UX_THREAD_PRIORITY_ENUM,   // Default
    ThreadMain = Default,                   // Default
    ThreadADC  = Default - 1                // Above default
};

static_assert(((UINT)ThreadPriority::Default == UX_THREAD_PRIORITY_ENUM) &&
              ((UINT)ThreadPriority::Default == UX_THREAD_PRIORITY_CLASS) &&
              ((UINT)ThreadPriority::Default > 0));

#endif
