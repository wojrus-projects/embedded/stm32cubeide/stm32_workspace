/*
 * DAQ_M02: ADC calibration.
 */

#include <cmath>
#include <algorithm>
#include <optional>

#include "Debug.h"
#include "RTOS_Common.h"
#include "App_Main.hpp"
#include "CRC16.hpp"
#include "Thread_ADC.hpp"

namespace DAQ_M02
{

static_assert(::Calibration::IsNVMCompatible<CalibrationDataContainer>());

void CalibrationCoefficient::Clear()
{
    Offset_sample = 0;
    Gain_AmperePerSample = 0.0f;
}

void CalibrationCoefficient::SetNominal(CurrentRange currentRange)
{
    Offset_sample = 0;
    Gain_AmperePerSample = ADC_CalculateGainCalibrationCoefficient_Nominal(currentRange);
}

void CalibrationData::Clear()
{
    for (auto& coefficient : CalibrationCoefficients)
    {
        coefficient.Clear();
    }
}

std::expected<CalibrationCoefficient, StatusCode> CalibrationData::GetCalibrationCoefficient(CurrentRange currentRange) const
{
    if (currentRange > CurrentRange::Last)
    {
        return std::unexpected(StatusCode::Invalid_Argument);
    }
    
    return CalibrationCoefficients[(size_t)currentRange];
}

StatusCode CalibrationData::SetOffset(CurrentRange currentRange, ADC::Sample coefficientOffset)
{
    if (currentRange > CurrentRange::Last)
    {
        return StatusCode::Invalid_Argument;
    }
    
    CalibrationCoefficients[(size_t)currentRange].Offset_sample = coefficientOffset;
    
    return StatusCode::Success;
}

std::expected<ADC::Sample, StatusCode> CalibrationData::GetOffset(CurrentRange currentRange) const
{
    if (currentRange > CurrentRange::Last)
    {
        return std::unexpected(StatusCode::Invalid_Argument);
    }
    
    const ADC::Sample coefficientOffset = CalibrationCoefficients[(size_t)currentRange].Offset_sample;
    
    return coefficientOffset;
}

StatusCode CalibrationData::SetGain(CurrentRange currentRange, float coefficientGain)
{
    if (currentRange > CurrentRange::Last)
    {
        return StatusCode::Invalid_Argument;
    }
    
    CalibrationCoefficients[(size_t)currentRange].Gain_AmperePerSample = coefficientGain;
    
    return StatusCode::Success;
}

std::expected<float, StatusCode> CalibrationData::GetGain(CurrentRange currentRange) const
{
    if (currentRange > CurrentRange::Last)
    {
        return std::unexpected(StatusCode::Invalid_Argument);
    }
    
    const float coefficientGain = CalibrationCoefficients[(size_t)currentRange].Gain_AmperePerSample;
    
    return coefficientGain;
}

void ADC_Calibration::Reset()
{
    Container.Clear();
    CalibrationStageExpected = CalibrationStage::Invalid;
    CalibrationCurrentRangeExpected = CurrentRange::Default;
    MeasurementPositive.Clear();
    MeasurementNegative.Clear();
}

StatusCode ADC_Calibration::ProcessCalibrationOffset()
{
    StatusCode statusCode = StatusCode::Failure;
    
    DEBUG_PRINTF("Cal: offset calibration begin\n");
    
    DAQ.Reset();
    
    for (CurrentRange currentRange = CurrentRange::First; currentRange <= CurrentRange::Last; currentRange++)
    {
        // Get average sample from ADC.
        
        DEBUG_PRINTF("Cal: current range: %u\n", currentRange);
        
        const ADC::Sample currentOffset_sample = MeasureCurrent(currentRange, 0);
        
        // Check offset limit.
        
        static_assert((OffsetCoefficient_CurrentMeasuredMax_PercentOfRange >= 10.0f) &&
                      (OffsetCoefficient_CurrentMeasuredMax_PercentOfRange <= 90.0f));
        
        const float currentRange_ABS_A = CurrentRanges[(size_t)currentRange];
        const float currentLimit_ABS_A = currentRange_ABS_A * (OffsetCoefficient_CurrentMeasuredMax_PercentOfRange / 100.0f);
        
        const SampleExt currentLimit_ABS_sample = ADC_CurrentToSample_Nominal(currentRange, currentLimit_ABS_A);
        
        DEBUG_ASSERT((currentLimit_ABS_sample >= 0) &&
                     (currentLimit_ABS_sample <= ADC_SAMPLE_VALID_MAX));
        
        DEBUG_PRINTF("Cal: current limit ABS: %f A / %d samples\n",
                (double)currentLimit_ABS_A, currentLimit_ABS_sample);
        
        if (std::abs(currentOffset_sample) > currentLimit_ABS_sample)
        {
            statusCode = StatusCode::ADC_Calibration_Invalid_Input_Current;
            goto ExitError;
        }
        
        // Append offset coefficient to volatile calibration data.
        
        const ADC::Sample coefficientOffset_sample = (ADC::Sample)(-currentOffset_sample);
        
        DEBUG_PRINTF("Cal: set offset coefficient: %d\n", coefficientOffset_sample);
        
        Container.Data.SetOffset(currentRange, coefficientOffset_sample);
    }
    
    CalibrationStageExpected = CalibrationStage::Stage_2_Gain_Current_Positive;
    CalibrationCurrentRangeExpected = CurrentRange::First;
    
    statusCode = StatusCode::Success;
    
ExitError:
    
    if (statusCode == StatusCode::Success)
    {
        DEBUG_PRINTF("Cal: offset calibration success\n");
    }
    else
    {
        DEBUG_PRINTF("Cal: offset calibration error: %u\n", statusCode);
    }

    return statusCode;
}

StatusCode ADC_Calibration::ProcessCalibrationGain(CalibrationStage_t calibrationStage,
                                                   CurrentRange currentRange,
                                                   float currentReference_A)
{
    StatusCode statusCode = StatusCode::Failure;
    
    DEBUG_PRINTF("Cal: gain calibration begin (stage: %u, range: %u, reference: %f A)\n",
            calibrationStage, currentRange, (double)currentReference_A);
    
    switch (calibrationStage)
    {
        case CalibrationStage::Stage_2_Gain_Current_Positive:
        case CalibrationStage::Stage_3_Gain_Current_Negative:
            {
                if (currentRange > CurrentRange::Last)
                {
                    statusCode = StatusCode::Invalid_Argument;
                    goto ExitError;
                }
                
                if ((calibrationStage != CalibrationStageExpected) ||
                    (currentRange != CalibrationCurrentRangeExpected))
                {
                    statusCode = StatusCode::Invalid_State;
                    goto ExitError;
                }
                
                const auto coefficientOffset_sample = Container.Data.GetOffset(currentRange);
                
                DEBUG_ASSERT(coefficientOffset_sample);
                
                const ADC::Sample currentMeasured_sample = MeasureCurrent(currentRange, *coefficientOffset_sample);
                
                if (calibrationStage == CalibrationStage::Stage_2_Gain_Current_Positive)
                {
                    MeasurementPositive = { currentReference_A, currentMeasured_sample };
                    
                    //
                    // Next stage: Stage_3_Gain_Current_Negative with old range.
                    //
                    
                    CalibrationStageExpected = CalibrationStage::Stage_3_Gain_Current_Negative;
                    
                    statusCode = StatusCode::Success;
                }
                else if (calibrationStage == CalibrationStage::Stage_3_Gain_Current_Negative)
                {
                    MeasurementNegative = { currentReference_A, currentMeasured_sample };
                    
                    //
                    // Calculate gain coefficient for selected range.
                    //
                    
                    const auto coefficientGain_AmperePerSample = CalculateGainCoefficient(currentRange);
                    
                    if (not coefficientGain_AmperePerSample)
                    {
                        statusCode = coefficientGain_AmperePerSample.error();
                        goto ExitError;
                    }
                    
                    DEBUG_PRINTF("Cal: set gain coefficient: %f\n", (double)*coefficientGain_AmperePerSample);
                    
                    Container.Data.SetGain(currentRange, *coefficientGain_AmperePerSample);
                    
                    if (CalibrationCurrentRangeExpected == CurrentRange::Last)
                    {
                        //
                        // Calibration is completed.
                        //
                        
                        statusCode = StatusCode::ADC_Calibration_Completed;
                    }
                    else
                    {
                        //
                        // Next stage: Stage_2_Gain_Current_Positive with new range.
                        //
                        
                        MeasurementPositive.Clear();
                        MeasurementNegative.Clear();
                        
                        CalibrationCurrentRangeExpected++;
                        CalibrationStageExpected = CalibrationStage::Stage_2_Gain_Current_Positive;
                        
                        statusCode = StatusCode::Success;
                    }
                }
            }
            break;
            
        case CalibrationStage::Invalid:
        case CalibrationStage::Stage_1_Offset:
        
        default:
            statusCode = StatusCode::Invalid_Argument;
    }
    
ExitError:
    
    if ((statusCode == StatusCode::Success) ||
        (statusCode == StatusCode::ADC_Calibration_Completed))
    {
        DEBUG_PRINTF("Cal: gain calibration success (completed: %u)\n",
                     statusCode == StatusCode::ADC_Calibration_Completed);
    }
    else
    {
        DEBUG_PRINTF("Cal: gain calibration error: %u\n", statusCode);
    }

    return statusCode;
}

std::optional<CalibrationCoefficient> ADC_Calibration::NVM_ReadCalibrationCoefficient(CurrentRange currentRange)
{
    std::optional<CalibrationCoefficient> calibrationCoefficient;
    const auto pContainer = Container.NVM_GetAddress();
    
    if (pContainer->Verify())
    {
        const auto calibrationCoefficient_NVM = pContainer->Data.GetCalibrationCoefficient(currentRange);
        
        if (calibrationCoefficient_NVM)
        {
            calibrationCoefficient = calibrationCoefficient_NVM.value();
        }
    }
    
    return calibrationCoefficient;
}

StatusCode ADC_Calibration::NVM_Write()
{   
    Container.Build();
    
    return Container.NVM_Write();
}

ADC::Sample ADC_Calibration::MeasureCurrent(CurrentRange currentRange, ADC::Sample offsetCoefficient)
{
    StatusCode statusCode;
    bool result;
    
    // Prepare acquisition.
    
    DEBUG_ASSERT(DAQ.VISO_IsEnabled);
            
    DAQ.AcquisitionConfiguration.Reset();
    
    DAQ.AcquisitionConfiguration.CurrentRange = currentRange;
    DAQ.AcquisitionConfiguration.SamplingFrequency_Hz = CalibrationSamplingFrequency_Hz;
    DAQ.AcquisitionConfiguration.SampleCountMax = CalibrationSampleCountMax;
    
    result = DAQ.AcquisitionConfiguration.Verify();
    DEBUG_ASSERT(result);
    
    DAQ.SwitchCurrentRange(currentRange);
    RTOS_Sleep(HAL::Board::SWITCH_CURRENT_RANGE_DELAY_ms);
    
    // Start acquisition.
    
    statusCode = DAQ.Acquisition_Start(true);
    DEBUG_ASSERT(statusCode == StatusCode::Success);
    
    // Wait for ADC DMA completion.
    
    Event_Wait_AdcCalibration();
    
    DEBUG_ASSERT((DAQ.Sampler.State == SamplerState::Completed) &&
                 (DAQ.Sampler.Counter.SampleAcquisition == CalibrationSampleCountMax) &&
                 (DAQ.Sampler.Counter.BufferOverflowAcquisition == 0) &&
                 (DAQ.ADC_SampleBuffer.DataSize == CalibrationSampleCountMax));
    
    // Correct offset error.
    
    for (size_t i = 0; i < DAQ.ADC_SampleBuffer.DataSize; i++)
    {
        SampleExt adcSample = (SampleExt)DAQ.ADC_SampleBuffer.Data[i] + (SampleExt)offsetCoefficient;
        
        adcSample = std::clamp(adcSample, (SampleExt)ADC::ADC_SAMPLE_MIN, (SampleExt)ADC::ADC_SAMPLE_MAX);
        
        DAQ.ADC_SampleBuffer.Data[i] = (ADC::Sample)adcSample;
    }
    
    // Denoise samples by averaging.
    
    const auto averageSample = ADC_AverageSamples(DAQ.ADC_SampleBuffer.Data, DAQ.ADC_SampleBuffer.DataSize);
    
    DEBUG_ASSERT(averageSample);
    
    return averageSample.value();
}

std::expected<float, StatusCode> ADC_Calibration::CalculateGainCoefficient(CurrentRange currentRange)
{
    static_assert((GainCoefficient_CurrentReferenceMin_PercentOfRange >= 50.0f) &&
                  (GainCoefficient_CurrentReferenceMin_PercentOfRange <= 150.0f));
    
    static_assert((GainCoefficient_CurrentReferenceMax_PercentOfRange >= 50.0f) &&
                  (GainCoefficient_CurrentReferenceMax_PercentOfRange <= 150.0f));
    
    static_assert(GainCoefficient_CurrentReferenceMax_PercentOfRange > GainCoefficient_CurrentReferenceMin_PercentOfRange);
    
    static_assert((GainCoefficient_CurrentMeasuredMin_PercentOfRange >= 50.0f) &&
                  (GainCoefficient_CurrentMeasuredMin_PercentOfRange <= 150.0f));
    
    static_assert((GainCoefficient_CurrentMeasuredMax_PercentOfRange >= 50.0f) &&
                  (GainCoefficient_CurrentMeasuredMax_PercentOfRange <= 150.0f));
    
    static_assert(GainCoefficient_CurrentMeasuredMax_PercentOfRange > GainCoefficient_CurrentMeasuredMin_PercentOfRange);
    
    //
    // Nominal saturation current (absolute value).
    //
    
    constexpr SampleExt ADC_SAMPLE_SATURATION_ABS = std::min(std::abs(ADC_SAMPLE_VALID_MIN),
                                                             std::abs(ADC_SAMPLE_VALID_MAX));
    
    const float currentSaturation_ABS_A = ADC_SampleToCurrent_Nominal(currentRange, ADC_SAMPLE_SATURATION_ABS);
    
    DEBUG_PRINTF("Cal: nominal current saturated: %f A / %d sample\n",
            (double)currentSaturation_ABS_A, ADC_SAMPLE_SATURATION_ABS);
    
    //
    // Nominal reference current range <min, max> (unit: amperes).
    //
    
    const float currentReferenceRange_ABS_A = CurrentRanges[(size_t)currentRange];
    
    const float currentReferenceMin_ABS_A = currentReferenceRange_ABS_A * (GainCoefficient_CurrentReferenceMin_PercentOfRange / 100.0f);
    const float currentReferenceMax_ABS_A = currentReferenceRange_ABS_A * (GainCoefficient_CurrentReferenceMax_PercentOfRange / 100.0f);
    
    DEBUG_PRINTF("Cal: reference current limits ABS: MIN: %f A, MAX: %f A\n",
            (double)currentReferenceMin_ABS_A, (double)currentReferenceMax_ABS_A);
    
    DEBUG_ASSERT((currentReferenceMin_ABS_A >= 0.0f) &&
                 (currentReferenceMin_ABS_A < currentReferenceMax_ABS_A) &&
                 (currentReferenceMax_ABS_A >= 0.0f) &&
                 (currentReferenceMax_ABS_A <= currentSaturation_ABS_A));
    
    //
    // Nominal measured current range <min, max> (unit: samples).
    //
    
    constexpr SampleExt ADC_SAMPLE_MIN_ABS = 0;
    constexpr SampleExt ADC_SAMPLE_MAX_ABS = ADC_SAMPLE_SATURATION_ABS;
    
    const SampleExt currentMeasuredRange_ABS_sample = ADC_CurrentToSample_Nominal(currentRange, currentReferenceRange_ABS_A);
    
    const SampleExt currentMeasuredMin_ABS_sample = (SampleExt)((float)currentMeasuredRange_ABS_sample * (GainCoefficient_CurrentMeasuredMin_PercentOfRange / 100.0f));
    const SampleExt currentMeasuredMax_ABS_sample = (SampleExt)((float)currentMeasuredRange_ABS_sample * (GainCoefficient_CurrentMeasuredMax_PercentOfRange / 100.0f));
    
    DEBUG_PRINTF("Cal: measured current limits ABS: MIN: %d, MAX: %d sample\n",
            currentMeasuredMin_ABS_sample, currentMeasuredMax_ABS_sample);
    
    DEBUG_ASSERT((currentMeasuredMin_ABS_sample >= ADC_SAMPLE_MIN_ABS) &&
                 (currentMeasuredMin_ABS_sample < currentMeasuredMax_ABS_sample) &&
                 (currentMeasuredMax_ABS_sample >= ADC_SAMPLE_MIN_ABS) &&
                 (currentMeasuredMax_ABS_sample <= ADC_SAMPLE_MAX_ABS));
    
    //
    // Check current ranges.
    //
    
    uint32_t errorCounter = 0;
    
    if ((MeasurementPositive.CurrentReference_A < +currentReferenceMin_ABS_A) ||
        (MeasurementPositive.CurrentReference_A > +currentReferenceMax_ABS_A))
    {
        errorCounter++;
        DEBUG_PRINTF("Cal: invalid reference (+): %f A\n", (double)MeasurementPositive.CurrentReference_A);
    }
    
    if ((MeasurementNegative.CurrentReference_A > -currentReferenceMin_ABS_A) ||
        (MeasurementNegative.CurrentReference_A < -currentReferenceMax_ABS_A))
    {
        errorCounter++;
        DEBUG_PRINTF("Cal: invalid reference (-): %f A\n", (double)MeasurementNegative.CurrentReference_A);
    }

    if ((MeasurementPositive.CurrentMeasured_sample < +currentMeasuredMin_ABS_sample) ||
        (MeasurementPositive.CurrentMeasured_sample > +currentMeasuredMax_ABS_sample))
    {
        errorCounter++;
        DEBUG_PRINTF("Cal: invalid measured (+): %d sample\n", MeasurementPositive.CurrentMeasured_sample);
    }
    
    if ((MeasurementNegative.CurrentMeasured_sample > -currentMeasuredMin_ABS_sample) ||
        (MeasurementNegative.CurrentMeasured_sample < -currentMeasuredMax_ABS_sample))
    {
        errorCounter++;
        DEBUG_PRINTF("Cal: invalid measured (-): %d sample\n", MeasurementNegative.CurrentMeasured_sample);
    }

    if (errorCounter > 0)
    {
        return std::unexpected(StatusCode::ADC_Calibration_Invalid_Input_Current);
    }
    
    //
    // Calculate gain coefficient.
    //
    
    DEBUG_PRINTF("Cal: ref+: %f A, ref-: %f A, meas+: %d, meas-: %d sample\n", 
            (double)MeasurementPositive.CurrentReference_A,
            (double)MeasurementNegative.CurrentReference_A,
            MeasurementPositive.CurrentMeasured_sample,
            MeasurementNegative.CurrentMeasured_sample);
    
    const float currentReferenceDelta_A = MeasurementPositive.CurrentReference_A - MeasurementNegative.CurrentReference_A;
    const SampleExt currentMeasuredDelta_sample = (SampleExt)MeasurementPositive.CurrentMeasured_sample - 
                                                  (SampleExt)MeasurementNegative.CurrentMeasured_sample;
    
    if (not ((currentReferenceDelta_A > 0.0f) && (currentMeasuredDelta_sample > 0)))
    {
        return std::unexpected(StatusCode::ADC_Calibration_Invalid_Input_Current);
    }
    
    const float gainCoefficient_AmperePerSample = currentReferenceDelta_A / (float)currentMeasuredDelta_sample;
    
    //
    // Sanity check.
    //
    
    const float currentCheck_ABS_A = gainCoefficient_AmperePerSample * (float)currentMeasuredRange_ABS_sample;
    
    if (currentCheck_ABS_A > currentSaturation_ABS_A)
    {
        return std::unexpected(StatusCode::ADC_Calibration_Invalid_Input_Current);
    }
    
    return gainCoefficient_AmperePerSample;
}

void ADC_Calibration::MeasurementPoint::Clear()
{
    CurrentReference_A = 0.0f;
    CurrentMeasured_sample = 0;
}

}
