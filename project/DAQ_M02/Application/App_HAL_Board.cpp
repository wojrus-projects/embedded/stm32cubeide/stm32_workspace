/*
 * Application HAL (board layer).
 */

#include "App_Main.hpp"

extern "C" void DAQ_M02_Trigger_IRQHandler(void)
{
    HAL_EXTI_IRQHandler(DAQ.Trigger.GetEXTI());
}

namespace DAQ_M02
{

void DAQ_M02::Trigger_Callback_InputChanged_Static()
{
    DAQ.Trigger_Callback_InputChanged();
}

void DAQ_M02::ADC_Callback_ReadCompleted_Static(ADC::Sample * pSamples, size_t samplesCount)
{
    DAQ.ADC_Callback_ReadCompleted(pSamples, samplesCount);
}

}
