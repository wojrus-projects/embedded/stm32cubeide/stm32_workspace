/*
 * Utility functions.
 */

#include <new>
#include <algorithm>

#include "Utilities.hpp"

/*
 * Prohibit usage new and delete operators.
 * This is linker level protection.
 * Not compatible with LTO enabled build.
 */
extern void * operator_new_or_delete_detector_fake_function();

void * operator new(std::size_t)
{
    return operator_new_or_delete_detector_fake_function();
}

void * operator new(std::size_t, std::align_val_t)
{
    return operator_new_or_delete_detector_fake_function();
}

void * operator new(std::size_t, const std::nothrow_t &) noexcept
{
    return operator_new_or_delete_detector_fake_function();
}

void * operator new[](std::size_t)
{
    return operator_new_or_delete_detector_fake_function();
}

void * operator new[](std::size_t, std::align_val_t)
{
    return operator_new_or_delete_detector_fake_function();
}

void * operator new[](std::size_t, const std::nothrow_t &) noexcept
{
    return operator_new_or_delete_detector_fake_function();
}

void operator delete(void *) noexcept
{
    operator_new_or_delete_detector_fake_function();
}

void operator delete(void *, std::size_t) noexcept
{
    operator_new_or_delete_detector_fake_function();
}

void operator delete(void *, const std::nothrow_t &) noexcept
{
    operator_new_or_delete_detector_fake_function();
}

void operator delete[](void *) noexcept
{
    operator_new_or_delete_detector_fake_function();
}

void operator delete[](void *, std::size_t) noexcept
{
    operator_new_or_delete_detector_fake_function();
}

void operator delete[](void *, const std::nothrow_t &) noexcept
{
    operator_new_or_delete_detector_fake_function();
}

namespace Utilities
{

static constexpr size_t MEMSET_FAST_THRESHOLD_BYTE_COUNT = 64;

/*
 * Fast memset() variant.
 */
void memset_fast(void * pMemory, uint8_t fillValue, size_t byteCount)
{   
    using Word_t = uint32_t;
    constexpr size_t WordSize = sizeof(Word_t);
    
    static_assert(MEMSET_FAST_THRESHOLD_BYTE_COUNT >= (8 * WordSize));
    
    uint8_t * pBytes = (uint8_t *)pMemory;
    
    // For small buffers process always unaligned, slow.
    
    if (byteCount < MEMSET_FAST_THRESHOLD_BYTE_COUNT)
    {
        while (byteCount-- > 0)
        {
            *pBytes++ = fillValue;
        }
    }
    else
    {
        // Initial segment (unaligned, slow).
        
        const size_t unalignedSize = (uintptr_t)pBytes % WordSize;
        
        if (unalignedSize > 0)
        {
            size_t initialSegmentSize = std::min(WordSize - unalignedSize, byteCount);
            
            byteCount -= initialSegmentSize;
            
            while (initialSegmentSize-- > 0)
            {
                *pBytes++ = fillValue;
            }
            
            if (byteCount == 0)
            {
                return;
            }
        }
        
        // Main segment (aligned, fast).
        
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-align"
        Word_t * pWords = (Word_t *)pBytes;
#pragma GCC diagnostic pop
        
        size_t alignedWordCount = byteCount / WordSize;
        
        byteCount -= alignedWordCount * WordSize;
        pBytes    += alignedWordCount * WordSize;
        
        static_assert(sizeof(Word_t) == 4);
        
        const Word_t fillValueWord = ((Word_t)fillValue << 0)  | (Word_t)(fillValue << 8) |
                                     ((Word_t)fillValue << 16) | (Word_t)(fillValue << 24); 
        
        while (alignedWordCount-- > 0)
        {
            *pWords++ = fillValueWord;
        }
        
        // Final segment (unaligned, slow).
        
        while (byteCount-- > 0)
        {
            *pBytes++ = fillValue;
        }
    }
}

}
