/*
 * DAQ_M02: acquisition common code.
 */

#ifndef DAQ_M02_Acquisition_hpp
#define DAQ_M02_Acquisition_hpp

#include <cstdint>

#include "DAQ_M02_Measurement_Definitions.hpp"

namespace DAQ_M02
{

struct AcquisitionConfiguration
{
    //
    // Generic parameters
    //
    
    uint64_t SampleCountMax = 0;
    uint32_t SamplingFrequency_Hz = 0;
 
    //
    // Device specific parameters
    //
    
    DAQ_M02::TriggerMode TriggerMode = TriggerMode::Default;
    DAQ_M02::CurrentRange CurrentRange = CurrentRange::Default;
    
    void Reset();
    bool Verify();
    bool IsTriggerEnabled();
};

}

#endif
