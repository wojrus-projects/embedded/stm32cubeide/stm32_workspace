/*
 * DAQ_M02: acquisition state observer.
 */

#ifndef DAQ_M02_Observer_hpp
#define DAQ_M02_Observer_hpp

void DAQ_M02_Observer_Enable();
void DAQ_M02_Observer_Disable();
void DAQ_M02_Observer_TimerHandler();

#endif
