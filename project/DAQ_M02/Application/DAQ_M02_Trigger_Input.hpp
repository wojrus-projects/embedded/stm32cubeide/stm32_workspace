/*
 * DAQ_M02: acquisition trigger input.
 */

#ifndef DAQ_M02_Trigger_hpp
#define DAQ_M02_Trigger_hpp

#include <cstdint>

#include "App_HAL_CubeMX.h"
#include "Status_Code.hpp"
#include "DAQ_M02_Measurement_Definitions.hpp"

namespace DAQ_M02
{

template
<
    uint32_t  EXTI_Line,
    uint32_t  EXTI_GPIOSel,
    IRQn_Type EXTI_IRQn,
    uint32_t  EXTI_IRQ_Priority
>
class TriggerInput
{
public:

    using InputChangedCallback = void (*)();
    
    static void Initialize(InputChangedCallback callback)
    {
        HAL_EXTI_RegisterCallback(&EXTI_Handle, HAL_EXTI_COMMON_CB_ID, callback);
        HAL_NVIC_SetPriority(EXTI_IRQn, EXTI_IRQ_Priority, 0);
    }
    
    static void Deinitialize()
    {
        HAL_NVIC_DisableIRQ(EXTI_IRQn);
        HAL_NVIC_ClearPendingIRQ(EXTI_IRQn);
        
        HAL_EXTI_ClearConfigLine(&EXTI_Handle);
        HAL_EXTI_ClearPending(&EXTI_Handle, 0);
    }
    
    static StatusCode Start(TriggerMode triggerMode)
    {
        HAL_StatusTypeDef halStatus;
        uint32_t extiTrigger;

        switch (triggerMode)
        {
            case TriggerMode::Disabled:
                return StatusCode::Invalid_Argument;
                
            case TriggerMode::EdgeRising:
                extiTrigger = EXTI_TRIGGER_RISING;
                break;
                
            case TriggerMode::EdgeFalling:
                extiTrigger = EXTI_TRIGGER_FALLING;
                break;
                
            case TriggerMode::EdgeBoth:
                extiTrigger = EXTI_TRIGGER_RISING_FALLING;
                break;
                
            default:
                return StatusCode::Invalid_Argument;
        };
        
        EXTI_ConfigTypeDef extiConfig
        {
            .Line = EXTI_Line,
            .Mode = EXTI_MODE_INTERRUPT,
            .Trigger = extiTrigger,
            .GPIOSel = EXTI_GPIOSel
        };
        
        halStatus = HAL_EXTI_SetConfigLine(&EXTI_Handle, &extiConfig);
        if (halStatus != HAL_OK)
        {
            return StatusCode::Invalid_Argument;
        }
        
        HAL_EXTI_ClearPending(&EXTI_Handle, 0);
        HAL_NVIC_ClearPendingIRQ(EXTI_IRQn);
        HAL_NVIC_EnableIRQ(EXTI_IRQn);
        
        return StatusCode::Success;
    }
    
    static void Stop()
    {
        Deinitialize();
    }
    
    static EXTI_HandleTypeDef * GetEXTI()
    {
        return &EXTI_Handle;
    }
    
    static bool ReadInput()
    {
        return HAL_GPIO_ReadPin(TRIGGER_GPIO_Port, TRIGGER_Pin);
    }
    
private:
    
    static inline EXTI_HandleTypeDef EXTI_Handle {};
};

}

#endif
