/*
 * ThreadX debugging utilities.
 */

#ifndef Debug_RTOS_h
#define Debug_RTOS_h

#include "tx_api.h"

#ifdef __cplusplus
extern "C" {
#endif

void Debug_RTOS_PrintPoolInfo(TX_BYTE_POOL * pBytePool);

#ifdef __cplusplus
}
#endif

#endif
