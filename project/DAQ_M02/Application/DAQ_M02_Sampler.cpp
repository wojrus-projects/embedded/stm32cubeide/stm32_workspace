/*
 * DAQ_M02: ADC data sampler.
 */

#include "DAQ_M02_Sampler.hpp"

namespace DAQ_M02
{

void Sampler::Reset()
{    
    Counter.SampleAcquisition = 0;
    Counter.SampleTotal = 0;
    Counter.BufferOverflowAcquisition = 0;
    Counter.BufferOverflowTotal = 0;
    Counter.Trigger = 0;
    
    DataSequenceCounter = 0;
    
    State = SamplerState::Default;
}

bool Sampler::IsBusy()
{
    return ((State == SamplerState::WaitForTrigger) ||
            (State == SamplerState::Acquisition));
}

bool Sampler::IsAcquisition()
{
    return (State == SamplerState::Acquisition);
}

bool Sampler::IsWaitForTrigger()
{
    return (State == SamplerState::WaitForTrigger);
}

bool Sampler::IsDisabled()
{
    return ((State == SamplerState::Idle) ||
            (State == SamplerState::Stop));
}

}
