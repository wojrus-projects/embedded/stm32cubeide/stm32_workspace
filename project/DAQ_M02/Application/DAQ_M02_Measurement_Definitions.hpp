/*
 * DAQ_M02: measurement definitions.
 */

#ifndef DAQ_M02_Measurement_Definitions_hpp
#define DAQ_M02_Measurement_Definitions_hpp

#include <cstdint>
#include <cstddef>
#include <array>
#include <expected>

#include "Status_Code.hpp"
#include "Utilities.hpp"
#include "ADC_MCP33131D.hpp"
#include "Host_Protocol_Definitions.hpp"

namespace DAQ_M02
{

// Extended ADC sample type for integer computations.
using SampleExt = int32_t;

enum class TriggerMode : uint8_t
{
    Disabled,
    EdgeRising,
    EdgeFalling,
    EdgeBoth,
    
    Last = EdgeBoth,
    Default = Disabled
};

enum class CurrentRange : uint8_t
{
    Range_5_mA,
    Range_50_mA,
    Range_500_mA,
    Range_5_A,
    
    First = Range_5_mA,
    Last = Range_5_A,
    Default = First
};

inline constexpr CurrentRange CurrentRangeDefault = CurrentRange::Range_5_A;
inline constexpr size_t CurrentRangeCount = 4;

inline constexpr std::array<float, CurrentRangeCount> CurrentRanges
{
    5e-3f,
    50e-3f,
    500e-3f,
    5.0f
};

inline constexpr std::array<float, CurrentRangeCount> R_Shunts
{
    // R_shunt:     Range:
    10.0f,       // 5 mA
    1.0f,        // 50 mA
    0.1f,        // 500 mA
    0.01f        // 5 A
};

// With ADC data Sequence Counter (SC) host application can track 'ADC data lost' events if device USB Tx process
// is slow down by USB host and then PDU lost occurs.
// SC is always incremented after ADC DMA data interrupt.
// SC in incremented from 0 to 0xFFFF, then overflows to 0.
// First PDU with ADC data has SC = 0.

using ADC_DataSequenceCounter_t = uint16_t;

inline constexpr size_t ADC_DATA_SEQUENCE_COUNTER_SIZE = sizeof(ADC_DataSequenceCounter_t);

// Host PDU payload offsets.
//
// Host PDU with acquisition data comprise two fields in payload:
// - ADC data Sequence Counter (SC), size: 16 bits
// - ADC samples in packed format

enum class HostPduPayloadOffset : size_t
{
    AdcDataSequenceCounter = 0,
    AdcSamples             = AdcDataSequenceCounter + ADC_DATA_SEQUENCE_COUNTER_SIZE
};

// Bit packed samples in PDU.

inline constexpr size_t PDU_SAMPLES_OFFSET = Host_Protocol::PAYLOAD_OFFSET + (size_t)HostPduPayloadOffset::AdcSamples;
inline constexpr size_t PDU_SAMPLES_PAYLOAD_SIZE_MAX = Host_Protocol::PAYLOAD_SIZE_MAX - ADC_DATA_SEQUENCE_COUNTER_SIZE;
inline constexpr size_t PDU_SAMPLES_COUNT = PDU_SAMPLES_PAYLOAD_SIZE_MAX / ADC::ADC_SAMPLE_SIZE;

// ADC DMA buffer size.

inline constexpr size_t ADC_DMA_DOUBLE_BUFFER_COUNT = 2;
inline constexpr size_t ADC_DMA_BUFFER_SIZE_SAMPLES = ADC_DMA_DOUBLE_BUFFER_COUNT * PDU_SAMPLES_COUNT;

// Nominal (ideal) parameters.

inline constexpr float VREF_V = 3.3f;
inline constexpr float ADC_FULL_SCALE_RANGE_V = 2.0f * VREF_V;
inline constexpr float R_SHUNT_SENSIVITY_V = 0.05f;
inline constexpr float DIFFERENTIAL_AMPLIFIER_GAIN_VperV = 62.0f;

// ADC sample values: saturated (excluded from normal use).

inline constexpr ADC::Sample ADC_SAMPLE_SATURATED_NEG = ADC::ADC_SAMPLE_MIN;
inline constexpr ADC::Sample ADC_SAMPLE_SATURATED_POS = ADC::ADC_SAMPLE_MAX;

// ADC sample values: usable range.

inline constexpr ADC::Sample ADC_SAMPLE_VALID_MIN = ADC_SAMPLE_SATURATED_NEG + 1;
inline constexpr ADC::Sample ADC_SAMPLE_VALID_MAX = ADC_SAMPLE_SATURATED_POS - 1;

// Utilities to calculate nominal values. 

float ADC_SampleToVoltage_Nominal(SampleExt adcSample);
SampleExt ADC_VoltageToSample_Nominal(float voltage_V);
float ADC_SampleToCurrent_Nominal(CurrentRange currentRange, SampleExt adcSample);
SampleExt ADC_CurrentToSample_Nominal(CurrentRange currentRange, float current_A);
float ADC_CalculateGainCalibrationCoefficient_Nominal(CurrentRange currentRange);

// Miscellaneous functions.

std::expected<ADC::Sample, StatusCode> ADC_AverageSamples(const ADC::Sample * pSamples, size_t samplesCount);

constexpr void ADC_SampleToBytes(ADC::Sample sample, uint8_t * pOutBytes)
{
    *pOutBytes++ = (uint8_t)(sample >> 0);
    *pOutBytes   = (uint8_t)(sample >> 8);
};

DEFINE_ENUM_CLASS_OPERATORS(CurrentRange);

}

#endif
