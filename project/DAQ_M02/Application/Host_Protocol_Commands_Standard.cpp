/*
 * Host protocol: standard commands.
 */

#include <limits>

#include "Debug.h"
#include "App_HAL.hpp"
#include "Host_Protocol_Commands.hpp"
#include "Host_Protocol_Commands_Standard.hpp"
#include "Device_Info_Data.hpp"
#include "Utilities.hpp"
#include "Buffer.hpp"

namespace Host_Protocol
{

//
// Command payload: [RESET_MODE 1B]
//
//  Answer payload: -
//
StatusCode Command_Reset(PayloadBuffer& commandPayloadBuffer, [[maybe_unused]] BufferByte& answerPayloadBuffer)
{
    if (commandPayloadBuffer.DataSize != 1)
    {
        return StatusCode::Invalid_Argument;
    }
    
    const DeviceResetMode resetMode = (DeviceResetMode)commandPayloadBuffer.Data[0];
    const auto pCallback = GetCommandCallback();
    
    if (pCallback && pCallback->Reset)
    {
        pCallback->Reset(resetMode);
    }
    
    return StatusCode::Success;
}

//
// Command payload: -
//
//  Answer payload: [DEVICE_STATUS 4B][DEVICE_IDENTIFICATION 28B]
//
StatusCode Command_Get_Device_Info(PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer)
{   
    struct [[gnu::packed]] DeviceIdentification
    {
        uint32_t                 HardwareType    = 0;
        uint32_t                 HardwareVariant = 0;
        uint32_t                 SerialNumber    = 0;
        DeviceInfo::SoftwareInfo Application;
        DeviceInfo::SoftwareInfo Bootloader;
    };

    struct [[gnu::packed]] DeviceStatusFlags
    {
        uint16_t FlashloaderEnabled : 1  = 0;
        uint16_t Reserved           : 15 = 0;
    };

    struct [[gnu::packed]] DeviceStatus
    {
        uint16_t          FrameLengthMax = 0;
        DeviceStatusFlags Flags;
    };
    
    DeviceStatus deviceStatus;
    DeviceIdentification deviceIdentification;
 
    if (commandPayloadBuffer.DataSize != 0)
    {
        return StatusCode::Invalid_Argument;
    }
    
    deviceIdentification.HardwareType = DeviceInfo::DeviceInfoData.HardwareType;
    deviceIdentification.HardwareVariant = DeviceInfo::DeviceInfoData.HardwareVariant;
    deviceIdentification.SerialNumber = HAL::GetBoardSerialNumber();
    deviceIdentification.Application = DeviceInfo::DeviceInfoData.Firmware;
    
    deviceStatus.FrameLengthMax = PDU_SIZE_MAX;
    deviceStatus.Flags.FlashloaderEnabled = 0;
    
    memcpy(&answerPayloadBuffer.Data[0],                    &deviceStatus,         sizeof(deviceStatus));
    memcpy(&answerPayloadBuffer.Data[sizeof(deviceStatus)], &deviceIdentification, sizeof(deviceIdentification));
    
    answerPayloadBuffer.DataSize = sizeof(deviceStatus) + sizeof(deviceIdentification);
    
    return StatusCode::Success;
}

//
// Command payload: [MEMORY_ID 1B][WRITE_OFFSET 4B][WRITE_DATA nB]
//
//  Answer payload: [MEMORY_ID 1B][WRITE_OFFSET 4B][WRITE_SIZE 2B]
//
StatusCode Command_Memory_Write(PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer)
{
    StatusCode statusCode;
    
    if (commandPayloadBuffer.DataSize < (1 + 4))
    {
        return StatusCode::Invalid_Argument;
    }
    
    const MemoryID memoryID = (MemoryID)commandPayloadBuffer.Data[0];
    const auto writeOffset = Utilities::FromBytes<uint32_t>(&commandPayloadBuffer.Data[1]);
    const BufferByte writeData =
    {
        .Data = &commandPayloadBuffer.Data[1 + 4],
        .DataSize = commandPayloadBuffer.DataSize - (1 + 4)
    };
    
    size_t writeSizeActual = 0;
    const auto pCallback = GetCommandCallback();
    
    if (pCallback && pCallback->MemoryWrite)
    {
        statusCode = pCallback->MemoryWrite(memoryID, writeOffset, writeData, &writeSizeActual);
    }
    else
    {
        statusCode = StatusCode::Success;
    }
    
    using Field_WriteSize_t = uint16_t;
    
    DEBUG_ASSERT(writeSizeActual <= std::numeric_limits<Field_WriteSize_t>::max());
    
    const Field_WriteSize_t writeSizeAnswer = (Field_WriteSize_t)writeSizeActual;
    
    answerPayloadBuffer.Data[0] = (uint8_t)memoryID;
    Utilities::ToBytes(writeOffset, &answerPayloadBuffer.Data[1]);
    Utilities::ToBytes(writeSizeAnswer, &answerPayloadBuffer.Data[1 + 4]);
    
    answerPayloadBuffer.DataSize = 1 + 4 + 2;
    
    return statusCode;
}

//
// Command payload: [MEMORY_ID 1B][READ_OFFSET 4B][READ_SIZE 2B]
//
//  Answer payload: [MEMORY_ID 1B][READ_OFFSET 4B][READ_DATA nB]
//
StatusCode Command_Memory_Read(PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer)
{
    StatusCode statusCode;
    
    if (commandPayloadBuffer.DataSize != (1 + 4 + 2))
    {
        return StatusCode::Invalid_Argument;
    }
    
    const MemoryID memoryID = (MemoryID)commandPayloadBuffer.Data[0];
    const auto readOffset = Utilities::FromBytes<uint32_t>(&commandPayloadBuffer.Data[1]);
    const auto readSize = Utilities::FromBytes<uint16_t>(&commandPayloadBuffer.Data[1 + 4]);
    const BufferByte readBuffer =
    {
        .Data = &answerPayloadBuffer.Data[1 + 4],
        .DataSize = PAYLOAD_SIZE_MAX - (1 + 4)
    };
    
    size_t readSizeActual = 0;
    const auto pCallback = GetCommandCallback();
    
    if (pCallback && pCallback->MemoryRead)
    {
        statusCode = pCallback->MemoryRead(memoryID, readOffset, readSize, readBuffer, &readSizeActual);
    }
    else
    {
        statusCode = StatusCode::Success;
    }

    answerPayloadBuffer.Data[0] = (uint8_t)memoryID;
    Utilities::ToBytes(readOffset, &answerPayloadBuffer.Data[1]);
    
    answerPayloadBuffer.DataSize = 1 + 4 + readSizeActual;
    
    return statusCode;
}

//
// Command payload: [MEMORY_ID 1B][ERASE_OFFSET 4B][ERASE_SIZE 4B]
//
//  Answer payload: -
//
StatusCode Command_Memory_Erase(PayloadBuffer& commandPayloadBuffer, [[maybe_unused]] BufferByte& answerPayloadBuffer)
{
    StatusCode statusCode;
    
    if (commandPayloadBuffer.DataSize != (1 + 4 + 4))
    {
        return StatusCode::Invalid_Argument;
    }
    
    const MemoryID memoryID = (MemoryID)commandPayloadBuffer.Data[0];
    const auto eraseOffset = Utilities::FromBytes<uint32_t>(&commandPayloadBuffer.Data[1]);
    const auto eraseSize = Utilities::FromBytes<uint32_t>(&commandPayloadBuffer.Data[1 + 4]);
    
    const auto pCallback = GetCommandCallback();
    
    if (pCallback && pCallback->MemoryErase)
    {
        statusCode = pCallback->MemoryErase(memoryID, eraseOffset, eraseSize);
    }
    else
    {
        statusCode = StatusCode::Success;
    }
    
    return statusCode;
}

//
// Command payload: [MEMORY_ID 1B]
//
//  Answer payload: -
//
StatusCode Command_Memory_Program([[maybe_unused]] PayloadBuffer& commandPayloadBuffer,
                                  [[maybe_unused]] BufferByte& answerPayloadBuffer)
{
    // Not implemented in this device.
    
    return StatusCode::Invalid_Command;
}

}
