/*
 * USB serial port Tx callback.
 */

#include "Debug.h"
#include "App_Main.hpp"
#include "Thread_Main.hpp"
#include "USB_CDC_Common.hpp"
#include "USB_CDC_Tx.hpp"

UINT USB_CDC_Tx_Callback([[maybe_unused]] UX_SLAVE_CLASS_CDC_ACM * cdc_acm,
                         [[maybe_unused]] UINT cdc_status,
                         [[maybe_unused]] ULONG length)
{
    Host_Protocol::PduBuffer * pTxBuffer;
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Lock();
    //-----------------------------------------------------------------------------------------
    
    DEBUG_SYSVIEW_MARK_STOP(Debug_SysView_MarkerID_UsbTx);
    DEBUG_PRINTF_EXT("USB: Tx event: %u", length);
    
    USB_CDC_State.IsTxPending = false;
    
    // Remove most recently sent PDU from queue.
    
    pTxBuffer = DAQ.USB_TxQueue.PeekBuffer();
    if (pTxBuffer)
    {
        DEBUG_PRINTF("USB: Tx: remove: %p, size: %u\n",
                pTxBuffer, pTxBuffer->DataSize);
        
        // Triggering new acquisition is possible only after sent last data chunk from previous acquisition.
        // This slow down data stream and guarantee to not lost last data chunk.
        // Host application require last ADC data chunk with status = Acquisition_Data_End
        // to detect end of data stream.
        
        const StatusCode pduStatusCode = (StatusCode)(Host_Protocol::Host_Protocol::GetStatusFromPDU(pTxBuffer)); 
        
        if (pduStatusCode == StatusCode::Acquisition_Data_End)
        {
            if (DAQ.Trigger_RequireRearm)
            {
                DAQ.Trigger_RequireRearm = false;
                
                DEBUG_ASSERT(DAQ.AcquisitionConfiguration.IsTriggerEnabled());
                
                DAQ.Acquisition_Retrigger();
            }
        }
        
        pTxBuffer->Clear();
        
        DAQ.USB_TxQueue.RemoveBuffer();
    }
    else
    {
        // Tx queue is empty.
        
        goto CancelOperation;
    }
    
    // Start send next PDU from queue. 
    
    pTxBuffer = DAQ.USB_TxQueue.PeekBuffer();
    if (pTxBuffer)
    {
        USB_CDC_SendPdu(pTxBuffer);
    }
    else
    {
        // Tx queue is empty.
    }

CancelOperation:
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Unlock();
    //-----------------------------------------------------------------------------------------
    
    return UX_SUCCESS;
}
