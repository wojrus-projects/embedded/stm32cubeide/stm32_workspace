/*
 * DAQ_M02: measurement definitions.
 */

#include <type_traits>
#include <limits>
#include <algorithm>

#include "Utilities.hpp"
#include "DAQ_M02_Measurement_Definitions.hpp"

namespace DAQ_M02
{

float ADC_SampleToVoltage_Nominal(SampleExt adcSample)
{
    const float voltage_V = (ADC_FULL_SCALE_RANGE_V * (float)adcSample) / (float)ADC::ADC_TOTAL_COUNTS;
    
    return voltage_V;
}

SampleExt ADC_VoltageToSample_Nominal(float voltage_V)
{
    const SampleExt adcSample = (SampleExt)(((float)ADC::ADC_TOTAL_COUNTS * voltage_V) / ADC_FULL_SCALE_RANGE_V);
    
    return adcSample;
}

float ADC_SampleToCurrent_Nominal(CurrentRange currentRange, SampleExt adcSample)
{
    const float adcInputVoltage_V = ADC_SampleToVoltage_Nominal(adcSample);
    const float R_Shunt_Voltage_V = adcInputVoltage_V / DIFFERENTIAL_AMPLIFIER_GAIN_VperV;
    const float R_Shunt_Resistance_Ohm = R_Shunts[(size_t)currentRange];
    const float R_Shunt_Current_A = R_Shunt_Voltage_V / R_Shunt_Resistance_Ohm;
    
    return R_Shunt_Current_A;
}

SampleExt ADC_CurrentToSample_Nominal(CurrentRange currentRange, float current_A)
{
    const float R_Shunt_Resistance_Ohm = R_Shunts[(size_t)currentRange];
    const float R_Shunt_Voltage_V = R_Shunt_Resistance_Ohm * current_A;
    const float adcInputVoltage_V = R_Shunt_Voltage_V * DIFFERENTIAL_AMPLIFIER_GAIN_VperV;
    const SampleExt adcSample = ADC_VoltageToSample_Nominal(adcInputVoltage_V);
    
    return adcSample;
}

float ADC_CalculateGainCalibrationCoefficient_Nominal(CurrentRange currentRange)
{
    const float currentMin_A = ADC_SampleToCurrent_Nominal(currentRange, ADC::ADC_SAMPLE_MIN);
    const float currentMax_A = ADC_SampleToCurrent_Nominal(currentRange, ADC::ADC_SAMPLE_MAX);
    const float currentDelta_A = currentMax_A - currentMin_A;
    const float adcSampleDelta = (float)ADC::ADC_SAMPLE_MAX - (float)ADC::ADC_SAMPLE_MIN;
    const float gainCalibrationCoefficient_AmperePerSample = currentDelta_A / adcSampleDelta;
    
    return gainCalibrationCoefficient_AmperePerSample;
}

std::expected<ADC::Sample, StatusCode> ADC_AverageSamples(const ADC::Sample * pSamples, size_t samplesCount)
{
    using SampleAccumulator_t = SampleExt;
    
    static_assert(sizeof(SampleAccumulator_t) > sizeof(ADC::Sample));
    
    constexpr size_t SamplesMax_DomainNegative = std::numeric_limits<SampleAccumulator_t>::min() / std::numeric_limits<ADC::Sample>::min();
    constexpr size_t SamplesMax_DomainPositive = std::numeric_limits<SampleAccumulator_t>::max() / std::numeric_limits<ADC::Sample>::max();
    constexpr size_t SamplesMax = std::min(SamplesMax_DomainNegative, SamplesMax_DomainPositive);
    
    ADC::Sample samplesAverage;
    SampleAccumulator_t samplesAccumulator = 0;
    
    if ((samplesCount == 0) || (samplesCount > SamplesMax))
    {
        return std::unexpected(StatusCode::Invalid_Argument);
    }
    
    for (size_t i = 0; i < samplesCount; i++)
    {
        samplesAccumulator += *pSamples++;
    }
    
    samplesAverage = (ADC::Sample)(samplesAccumulator / static_cast<std::make_signed_t<size_t>>(samplesCount));
    
    return samplesAverage;
}

}
