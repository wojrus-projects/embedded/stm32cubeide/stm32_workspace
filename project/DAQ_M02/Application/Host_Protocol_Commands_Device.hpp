/*
 * Host protocol: device commands.
 */

#ifndef Host_Protocol_Commands_Device_hpp
#define Host_Protocol_Commands_Device_hpp

#include "Host_Protocol_Definitions.hpp"
#include "Status_Code.hpp"

namespace Host_Protocol
{

StatusCode Command_Acquisition_Start     (PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer);
StatusCode Command_Acquisition_Stop      (PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer);
StatusCode Command_Acquisition_Get_Status(PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer);
StatusCode Command_Calibration_Process   (PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer);
StatusCode Command_Calibration_Data_Read (PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer);
StatusCode Command_Calibration_Data_Write(PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer);

}

#endif
