/*
 * Interrupts priority.
 */

#ifndef Interrupt_Priority_hpp
#define Interrupt_Priority_hpp

#include <cstdint>

//
// Application interrupts priorities.
//
enum class InterruptPriority : uint32_t
{
    // Configured in CubeMX.
    
    ARM_Exception         = 0,                      // Highest
    Device_Driver         = 1,
    
    ADC_SPI_DMA1_Channel1 = Device_Driver,
    ADC_SPI_DMA1_Channel2 = Device_Driver,
    Trigger_EXTI15_10     = Device_Driver,
    HAL_Timer_TIM6_DAC    = Device_Driver,
    USB_LP                = Device_Driver + 1,
    
    // Hardcoded in RTOS sources.
    
    ThreadX_SysTick       = 4,
    ThreadX_SVCall        = 15, 
    ThreadX_PendSV        = 15                      // Lowest
};

#endif
