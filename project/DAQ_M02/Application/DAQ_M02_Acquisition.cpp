/*
 * DAQ_M02: acquisition common code.
 */

#include "DAQ_M02_Acquisition.hpp"

namespace DAQ_M02
{

void AcquisitionConfiguration::Reset()
{
    TriggerMode = TriggerMode::Default;
    CurrentRange = CurrentRange::Default;
    SamplingFrequency_Hz = 0;
    SampleCountMax = 0;
}

bool AcquisitionConfiguration::Verify()
{
    return ((TriggerMode <= TriggerMode::Last) &&
            (CurrentRange <= CurrentRange::Last) &&
            (SamplingFrequency_Hz >= ADC::ADC_SAMPLING_FREQUENCY_MIN_Hz) &&
            (SamplingFrequency_Hz <= ADC::ADC_SAMPLING_FREQUENCY_MAX_Hz) &&
            (SampleCountMax > 0));
}

bool AcquisitionConfiguration::IsTriggerEnabled()
{
    return ((TriggerMode == TriggerMode::EdgeRising) ||
            (TriggerMode == TriggerMode::EdgeFalling) ||
            (TriggerMode == TriggerMode::EdgeBoth)); 
}

}
