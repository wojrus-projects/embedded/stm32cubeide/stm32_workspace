/*
 * DAQ_M02: main file.
 */

#ifndef DAQ_M02_hpp
#define DAQ_M02_hpp

#include <cstdint>
#include <cstddef>
#include <optional>

#include "Status_Code.hpp"
#include "Buffer.hpp"
#include "App_HAL_MCU.hpp"
#include "App_HAL_Board.hpp"
#include "LED.hpp"
#include "ADC_MCP33131D.hpp"
#include "USB_Queue.hpp"
#include "DAQ_M02_Measurement_Definitions.hpp"
#include "DAQ_M02_Trigger_Input.hpp"
#include "DAQ_M02_Sampler.hpp"
#include "DAQ_M02_Acquisition.hpp"
#include "DAQ_M02_Calibration.hpp"
#include "DAQ_M02_Observer.hpp"

namespace DAQ_M02
{

namespace LED_Color_Status
{
inline constexpr LED_Color Off               = LED_Color::Black;
inline constexpr LED_Color Reset             = LED_Color::Cyan;
inline constexpr LED_Color Exception         = LED_Color::Red;
inline constexpr LED_Color Serial_Port_Close = LED_Color::Yellow;
inline constexpr LED_Color Serial_Port_Open  = LED_Color::Green;
inline constexpr LED_Color USB_Suspend       = LED_Color::Blue;
inline constexpr LED_Color USB_Resume        = Serial_Port_Close;
}

namespace LED_Color_Trigger
{
inline constexpr LED_Color Off               = LED_Color::Black;
inline constexpr LED_Color Reset             = LED_Color::Cyan;
inline constexpr LED_Color Exception         = LED_Color::Red;
inline constexpr LED_Color WaitForTrigger    = LED_Color::Magenta;
inline constexpr LED_Color Acquisition       = LED_Color::Green;
}

using ADC_t = ADC::ADC_MCP33131D<HAL::Board::ADC::SPI,
                                 HAL::Board::ADC::Timer,
                                 ADC_DMA_BUFFER_SIZE_SAMPLES>;

using TriggerInput_t = TriggerInput<HAL::Board::Trigger::Line,
                                    HAL::Board::Trigger::Port,
                                    HAL::Board::Trigger::IRQ_Number,
                                    HAL::Board::Trigger::IRQ_Priority>;

using Sampler_t = Sampler;
using AcquisitionConfiguration_t = AcquisitionConfiguration;
using ADC_Calibration_t = ADC_Calibration;

struct ADC_SaturationCurrent
{
    float Positive {};
    float Negative {};
};

class DAQ_M02
{
public:
    
    ADC_t                                 ADC;
    ADC_Calibration_t                     ADC_Calibration;
    std::optional<CalibrationCoefficient> ADC_CalibrationCoefficient;
    uint32_t                              ADC_SamplingFrequencyReal_Hz = 0;
    Buffer<ADC::Sample>                   ADC_SampleBuffer;
    Host_Protocol::PduBuffer              PduBuffer_DataContinue_First;
    Host_Protocol::PduBuffer              PduBuffer_DataContinue_Second;
    Host_Protocol::PduBuffer              PduBuffer_DataEnd;
    AcquisitionConfiguration_t            AcquisitionConfiguration;
    TriggerInput_t                        Trigger;
    bool                                  Trigger_RequireRearm = false;
    Sampler_t                             Sampler;
    USB_Queue                             USB_TxQueue;
    LED                                   LED_Status;
    LED                                   LED_Trigger;
    bool                                  VISO_IsEnabled = false;
    
    void Initialize();
    void Reset();
    void VISO_PowerUp();
    void VISO_PowerDown();
    StatusCode SwitchCurrentRange(CurrentRange range);
    
    StatusCode Acquisition_Start(bool isCalibrationMode);
    StatusCode Acquisition_Retrigger();
    void Acquisition_Stop();
    void Acquisition_CountSamples();
    void Acquisition_CountSamplesOverflowed();
    void Acquisition_PostprocessSamples(Host_Protocol::PduBuffer * const pOutputPduBuffer);
    
    ADC_SaturationCurrent ADC_GetSaturationCurrent();
    bool PduBuffersAreEmpty();
    
    void Trigger_Callback_InputChanged();
    void ADC_Callback_ReadCompleted(ADC::Sample * pSamples, size_t samplesCount);
    
    static void Trigger_Callback_InputChanged_Static();
    static void ADC_Callback_ReadCompleted_Static(ADC::Sample * pSamples, size_t samplesCount);
    
private:
    
    bool IsCalibrationMode = false;
    
    void VISO_Enable(bool powerState);
    size_t ADC_CalculateBufferSize(uint32_t samplingFrequency_Hz, uint64_t sampleCountMax);
};

}

#endif
