/*
 * DAQ_M02: ADC calibration.
 */

/*
 * DAQ require ADC calibration.
 * Calibration consists offset and gain calibrations, which are processed independent, with specific order.
 * Calibration data is stored in MCU flash memory.
 * 
 * Offset calibration is one point type.
 * All analog inputs are connected to COMMON input and calibration
 * is automatically executed at once for all inputs.
 * 
 * Gain calibration is two point type:
 * - Point 1: for selected input set reference current +I_REF
 * - Point 2: for selected input (from point 1) set reference current -I_REF
 * Gain calibration is processed for all four current ranges in order.
 * Reference current value is ABS(I_REF) = <I_RANGE - 1.0 %, I_RANGE + 1.0 %>. 
 * 
 * Full calibration procedure (9 steps in row, order is important, USB Serial Port must not disconnect between steps):
 * ------------+--------------+---------------+----------+---------------------------+--------------------------
 * Step number | Stage number | Current input | I_RANGE  | Required status           | API function
 * ------------+--------------+---------------+----------+---------------------------+--------------------------
 * 1           | 1            | N.A.          | N.A.     | Success                   | ProcessCalibrationOffset
 * 2           | 2            | low +         | 5 mA     | Success                   | ProcessCalibrationGain
 * 3           | 3            | low -         | 5 mA     | Success                   | ProcessCalibrationGain
 * 4           | 2            | low +         | 50 mA    | Success                   | --//--
 * 5           | 3            | low -         | 50 mA    | Success                   | --//--
 * 6           | 2            | low +         | 500 mA   | Success                   | --//--
 * 7           | 3            | low -         | 500 mA   | Success                   | --//--
 * 8           | 2            | high +        | 5 A      | Success                   | --//--
 * 9           | 3            | high -        | 5 A      | ADC_Calibration_Completed | ProcessCalibrationGain
 * ------------+--------------+---------------+----------+---------------------------+--------------------------
 *
 * Where:
 * - Stage number = enum CalibrationStage
 * - Status = enum StatusCode
 */

#ifndef DAQ_M02_Calibration_hpp
#define DAQ_M02_Calibration_hpp

#include <cstdint>
#include <optional>
#include <expected>
#include <algorithm>

#include "Status_Code.hpp"
#include "ADC_MCP33131D_Definitions.hpp"
#include "DAQ_M02_Measurement_Definitions.hpp"
#include "Calibration_Data_Container.hpp"
#include "Device_Info_Data.hpp"

namespace DAQ_M02
{

enum class CalibrationStage : uint8_t
{
    Invalid,
    Stage_1_Offset,
    Stage_2_Gain_Current_Positive,
    Stage_3_Gain_Current_Negative
};

struct [[gnu::packed]] CalibrationCoefficient
{
    ADC::Sample Offset_sample = 0;
    float       Gain_AmperePerSample = 0.0f;
    
    void Clear();
    void SetNominal(CurrentRange currentRange);
};

struct [[gnu::packed]] CalibrationData
{
    CalibrationCoefficient CalibrationCoefficients[CurrentRangeCount];
    
    void Clear();
    
    std::expected<CalibrationCoefficient, StatusCode> GetCalibrationCoefficient(CurrentRange currentRange) const;
    StatusCode                                        SetOffset(CurrentRange currentRange, ADC::Sample coefficientOffset);
    std::expected<ADC::Sample, StatusCode>            GetOffset(CurrentRange currentRange) const;    
    StatusCode                                        SetGain(CurrentRange currentRange, float coefficientGain);
    std::expected<float, StatusCode>                  GetGain(CurrentRange currentRange) const;
};

using CalibrationDataContainer = ::Calibration::CalibrationDataContainer<CalibrationData, DeviceInfo::DeviceInfoData>;

class ADC_Calibration
{
public:

    using CalibrationStage_t = DAQ_M02::CalibrationStage;
    
    CalibrationDataContainer Container;
    
    void       Reset();
    StatusCode ProcessCalibrationOffset();
    StatusCode ProcessCalibrationGain(CalibrationStage_t calibrationStage,
                                      CurrentRange currentRange,
                                      float currentReference_A);
    
    std::optional<CalibrationCoefficient> NVM_ReadCalibrationCoefficient(CurrentRange currentRange);
    StatusCode                            NVM_Write();
    
private:
    
    // Ref: AN TI SPRAD55A (https://www.ti.com/lit/pdf/sprad55)
    static constexpr size_t AdcOversamplingFactor = 256;
    
    static constexpr uint64_t CalibrationSampleCountMax = std::min(AdcOversamplingFactor, PDU_SAMPLES_COUNT);
    static constexpr uint32_t CalibrationSamplingFrequency_Hz = 10'000;
    
    static constexpr float OffsetCoefficient_CurrentMeasuredMax_PercentOfRange = 50.0f;
    
    static constexpr float GainCoefficient_CurrentReferenceMin_PercentOfRange = 99.0f;
    static constexpr float GainCoefficient_CurrentReferenceMax_PercentOfRange = 101.0f;
    
    static constexpr float GainCoefficient_CurrentMeasuredMin_PercentOfRange = 50.0f;
    static constexpr float GainCoefficient_CurrentMeasuredMax_PercentOfRange = 105.0f;
    
    struct MeasurementPoint
    {
        float       CurrentReference_A = 0.0f;
        ADC::Sample CurrentMeasured_sample = 0;
        
        void Clear();
    };
    
    CalibrationStage_t CalibrationStageExpected = CalibrationStage::Invalid;
    CurrentRange       CalibrationCurrentRangeExpected = CurrentRange::Default;
    
    MeasurementPoint MeasurementPositive;
    MeasurementPoint MeasurementNegative;
    
    ADC::Sample MeasureCurrent(CurrentRange currentRange, ADC::Sample offsetCoefficient);
    std::expected<float, StatusCode> CalculateGainCoefficient(CurrentRange currentRange);
};

}

#endif
