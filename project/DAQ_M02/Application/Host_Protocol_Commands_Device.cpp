/*
 * Host protocol: device commands.
 */

#include <cmath>

#include "Debug.h"
#include "RTOS_Common.h"
#include "App_Main.hpp"
#include "Utilities.hpp"
#include "Critical_Section.hpp"
#include "Thread_Main.hpp"
#include "Host_Protocol_Commands.hpp"

namespace Host_Protocol
{

//                  |                Generic parameters                    |  Device specific parameters  |
// Command payload: [ADC_SAMPLING_FREQUENCY_Hz 4B][ADC_SAMPLE_COUNT_MAX 8B]|[TRIGGER_MODE 1B][ADC_RANGE 1B]
//
//  Answer payload: [GAIN_COEFFICIENT 4B (float32, A/sample)]
//
StatusCode Command_Acquisition_Start(PayloadBuffer& commandPayloadBuffer, [[maybe_unused]] BufferByte& answerPayloadBuffer)
{
    StatusCode statusCode = StatusCode::Failure;
    bool samplerIsBusy;
    DAQ_M02::CalibrationCoefficient calibrationCoefficient;
    
    if (commandPayloadBuffer.DataSize != (4 + 8 + 1 + 1))
    {
        return StatusCode::Invalid_Argument;
    }
   
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Lock();
    //-----------------------------------------------------------------------------------------
    
    //
    // ADC acquisition process must be stopped.
    //
    
    CRITICAL_SECTION_ENTER;
    samplerIsBusy = DAQ.Sampler.IsBusy();
    CRITICAL_SECTION_EXIT;
    
    if (samplerIsBusy)
    {
        statusCode = StatusCode::Busy;
        goto ExitError;
    }
    
    //
    // All ADC data from previous acquisition must be sent to host.
    //
    
    if (not DAQ.PduBuffersAreEmpty())
    {
        statusCode = StatusCode::Invalid_State;
        goto ExitError;
    }
    
    //
    // Set ADC configuration.
    //
    
    DAQ.Reset();
    
    DAQ.AcquisitionConfiguration.SamplingFrequency_Hz = Utilities::FromBytes<uint32_t>(&commandPayloadBuffer.Data[0]);
    DAQ.AcquisitionConfiguration.SampleCountMax       = Utilities::FromBytes<uint64_t>(&commandPayloadBuffer.Data[4]);
    DAQ.AcquisitionConfiguration.TriggerMode          = (DAQ_M02::TriggerMode) commandPayloadBuffer.Data[4 + 8];
    DAQ.AcquisitionConfiguration.CurrentRange         = (DAQ_M02::CurrentRange)commandPayloadBuffer.Data[4 + 8 + 1];
    
    if (not DAQ.AcquisitionConfiguration.Verify())
    {
        DAQ.AcquisitionConfiguration.Reset();
        statusCode = StatusCode::Invalid_Argument;
        goto ExitError;
    }
    
    //
    // Set measurement range.
    //
    
    DAQ.SwitchCurrentRange(DAQ.AcquisitionConfiguration.CurrentRange);        
    RTOS_Sleep(HAL::Board::SWITCH_CURRENT_RANGE_DELAY_ms);
    
    //
    // Refresh ADC calibration coefficients.
    //
    
    DAQ.ADC_CalibrationCoefficient = DAQ.ADC_Calibration.NVM_ReadCalibrationCoefficient(DAQ.AcquisitionConfiguration.CurrentRange);
    
    //
    // Start acquisition.
    //
    
    statusCode = DAQ.Acquisition_Start(false);
    DEBUG_ASSERT(statusCode == StatusCode::Success);
    
    //
    // Return gain coefficient.
    // Coefficient is used by host application to convert raw ADC samples to amperes.
    //
    
    if (DAQ.ADC_CalibrationCoefficient)
    {
        calibrationCoefficient = *DAQ.ADC_CalibrationCoefficient;
    }
    else
    {
        calibrationCoefficient.SetNominal(DAQ.AcquisitionConfiguration.CurrentRange);
    }
    
    answerPayloadBuffer.AppendBytes(&calibrationCoefficient.Gain_AmperePerSample, sizeof(calibrationCoefficient.Gain_AmperePerSample));
    
ExitError:
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Unlock();
    //-----------------------------------------------------------------------------------------
    
    return statusCode;
}

//
// Command payload: -
//
//  Answer payload: -
//
StatusCode Command_Acquisition_Stop(PayloadBuffer& commandPayloadBuffer, [[maybe_unused]] BufferByte& answerPayloadBuffer)
{
    if (commandPayloadBuffer.DataSize != 0)
    {
        return StatusCode::Invalid_Argument;
    }
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Lock();
    //-----------------------------------------------------------------------------------------
    
    DAQ.Acquisition_Stop();
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Unlock();
    //-----------------------------------------------------------------------------------------
    
    return StatusCode::Success;
}

//
// Command payload: -
//
//  Answer payload: [DAQ_STATUS 1B]
//                  [ADC_SAMPLING_FREQUENCY_REAL_Hz 4B]
//                  [SATURATION+ 4B][SATURATION- 4B]
//                  [COUNTER_SAMPLE_ACQUISITION 8B][COUNTER_SAMPLE_TOTAL 8B]
//                  [COUNTER_BUFFER_OVERFLOW_ACQUISITION 8B][COUNTER_BUFFER_OVERFLOW_TOTAL 8B]
//                  [COUNTER_TRIGGER 8B]
//
StatusCode Command_Acquisition_Get_Status(PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer)
{
    union DAQ_Status
    {
        uint8_t Byte = 0;
        
        struct DAQ_StatusFlag
        {
            uint8_t CalibrationIsValid : 1;
            uint8_t AcquisitionState   : 3;
            uint8_t TriggerInputState  : 1;
            uint8_t Reserved           : 3;
        } Flag;
    };
    
    static_assert(sizeof(DAQ_Status) == 1);
    
    DAQ_Status daqStatus;
    DAQ_M02::Sampler daqSampler;
    DAQ_M02::ADC_SaturationCurrent adcSaturationCurrent;
    uint32_t adcSamplingFrequencyReal_Hz;
    
    if (commandPayloadBuffer.DataSize != 0)
    {
        return StatusCode::Invalid_Argument;
    }
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Lock();
    //-----------------------------------------------------------------------------------------
    
    CRITICAL_SECTION_ENTER;
    
    daqSampler = DAQ.Sampler;
    daqStatus.Flag.TriggerInputState = DAQ.Trigger.ReadInput();
    adcSamplingFrequencyReal_Hz = DAQ.ADC_SamplingFrequencyReal_Hz;
    
    CRITICAL_SECTION_EXIT;
    
    adcSaturationCurrent = DAQ.ADC_GetSaturationCurrent();
    daqStatus.Flag.CalibrationIsValid = DAQ.ADC_Calibration.Container.NVM_Verify();
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Unlock();
    //-----------------------------------------------------------------------------------------
    
    daqStatus.Flag.AcquisitionState = (uint8_t)daqSampler.State & 0b111;
    
    answerPayloadBuffer.Clear();
    answerPayloadBuffer.AppendBytes(&daqStatus.Byte,                               sizeof(daqStatus.Byte));
    answerPayloadBuffer.AppendBytes(&adcSamplingFrequencyReal_Hz,                  sizeof(adcSamplingFrequencyReal_Hz));
    answerPayloadBuffer.AppendBytes(&adcSaturationCurrent.Positive,                sizeof(adcSaturationCurrent.Positive));
    answerPayloadBuffer.AppendBytes(&adcSaturationCurrent.Negative,                sizeof(adcSaturationCurrent.Negative));
    answerPayloadBuffer.AppendBytes(&daqSampler.Counter.SampleAcquisition,         sizeof(daqSampler.Counter.SampleAcquisition));
    answerPayloadBuffer.AppendBytes(&daqSampler.Counter.SampleTotal,               sizeof(daqSampler.Counter.SampleTotal));
    answerPayloadBuffer.AppendBytes(&daqSampler.Counter.BufferOverflowAcquisition, sizeof(daqSampler.Counter.BufferOverflowAcquisition));
    answerPayloadBuffer.AppendBytes(&daqSampler.Counter.BufferOverflowTotal,       sizeof(daqSampler.Counter.BufferOverflowTotal));
    answerPayloadBuffer.AppendBytes(&daqSampler.Counter.Trigger,                   sizeof(daqSampler.Counter.Trigger));
    
    return StatusCode::Success;
}

//
// Command payload: Case 1 = [CALIBRATION_STAGE 1B]                                                        if CALIBRATION_STAGE = Stage_1_Offset
//                  Case 2 = [CALIBRATION_STAGE 1B][CURRENT_RANGE 1B][CURRENT_REFERENCE+ 4B (float32, A)]  if CALIBRATION_STAGE = Stage_2_Gain_Current_Positive
//                  Case 3 = [CALIBRATION_STAGE 1B][CURRENT_RANGE 1B][CURRENT_REFERENCE- 4B (float32, A)]  if CALIBRATION_STAGE = Stage_3_Gain_Current_Negative
//
//  Answer payload: -
//
StatusCode Command_Calibration_Process(PayloadBuffer& commandPayloadBuffer, [[maybe_unused]] BufferByte& answerPayloadBuffer)
{
    StatusCode statusCode = StatusCode::Failure;
    bool samplerIsBusy;
    DAQ_M02::CalibrationStage calibrationStage;
    
    //
    // Minimum: CALIBRATION_STAGE.
    //
    
    if (commandPayloadBuffer.DataSize < 1)
    {
        return StatusCode::Invalid_Argument;
    }
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Lock();
    //-----------------------------------------------------------------------------------------
    
    //
    // ADC acquisition process must be stopped.
    //
    
    CRITICAL_SECTION_ENTER;
    samplerIsBusy = DAQ.Sampler.IsBusy();
    CRITICAL_SECTION_EXIT;
    
    if (samplerIsBusy)
    {
        statusCode = StatusCode::Invalid_State;
        goto CancelOperation;
    }
    
    //
    // All ADC data from previous acquisition must be sent to host.
    //
    
    if (not DAQ.PduBuffersAreEmpty())
    {
        statusCode = StatusCode::Invalid_State;
        goto CancelOperation;
    }
    
    //
    // Process calibration.
    //
    
    calibrationStage = (DAQ_M02::CalibrationStage)commandPayloadBuffer.Data[0];
    
    switch (calibrationStage)
    {
        case DAQ_M02::CalibrationStage::Stage_1_Offset:
            {
                if (commandPayloadBuffer.DataSize != 1)
                {
                    statusCode = StatusCode::Invalid_Argument;
                    goto CancelOperation;
                }
                
                statusCode = DAQ.ADC_Calibration.ProcessCalibrationOffset();
            }
            break;
            
        case DAQ_M02::CalibrationStage::Stage_2_Gain_Current_Positive:
        case DAQ_M02::CalibrationStage::Stage_3_Gain_Current_Negative:
            {   
                if (commandPayloadBuffer.DataSize != (1 + 1 + 4))
                {
                    statusCode = StatusCode::Invalid_Argument;
                    goto CancelOperation;
                }
                
                const DAQ_M02::CurrentRange currentRange = (DAQ_M02::CurrentRange)commandPayloadBuffer.Data[1];
                
                if (currentRange > DAQ_M02::CurrentRange::Last)
                {
                    statusCode = StatusCode::Invalid_Argument;
                    goto CancelOperation;
                }
                
                const auto currentReference_A = Utilities::FromBytes<float>(&commandPayloadBuffer.Data[2]);
                
                if (not std::isfinite(currentReference_A))
                {
                    statusCode = StatusCode::Invalid_Argument;
                    goto CancelOperation;
                }
                
                statusCode = DAQ.ADC_Calibration.ProcessCalibrationGain(calibrationStage, currentRange, currentReference_A);
                
                if (calibrationStage == DAQ_M02::CalibrationStage::Stage_3_Gain_Current_Negative)
                {
                    if (currentRange == DAQ_M02::CurrentRange::Last)
                    {
                        if (statusCode == StatusCode::ADC_Calibration_Completed)
                        {
                            statusCode = DAQ.ADC_Calibration.Container.NVM_Write();
                        
                            if (statusCode == StatusCode::Success)
                            {
                                DAQ.Reset();
                            }
                        }
                    }
                }
            }
            break;
        
        case DAQ_M02::CalibrationStage::Invalid:
        
        default:
            statusCode = StatusCode::Invalid_Argument;
            goto CancelOperation;
    }
    
    if (not ((statusCode == StatusCode::Success) ||
             (statusCode == StatusCode::ADC_Calibration_Completed)))
    {
        DAQ.Reset();
    }

CancelOperation:
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Unlock();
    //-----------------------------------------------------------------------------------------

    return statusCode;
}

//
// Command payload: -
//
//  Answer payload: [DAQ_CALIBRATION_DATA nB]
//
StatusCode Command_Calibration_Data_Read(PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer)
{
    static_assert(sizeof(DAQ_M02::CalibrationDataContainer) <= PAYLOAD_SIZE_MAX);
    
    if (commandPayloadBuffer.DataSize != 0)
    {
        return StatusCode::Invalid_Argument;
    }
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Lock();
    //-----------------------------------------------------------------------------------------
    
    memcpy(&answerPayloadBuffer.Data[0], DAQ.ADC_Calibration.Container.NVM_GetAddress(),
           sizeof(DAQ_M02::CalibrationDataContainer));
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Unlock();
    //-----------------------------------------------------------------------------------------
    
    answerPayloadBuffer.DataSize = sizeof(DAQ_M02::CalibrationDataContainer);
    
    return StatusCode::Success;
}

//
// Command payload: [DAQ_CALIBRATION_DATA nB]
//  Answer payload: -
//
StatusCode Command_Calibration_Data_Write(PayloadBuffer& commandPayloadBuffer, [[maybe_unused]] BufferByte& answerPayloadBuffer)
{
    StatusCode statusCode = StatusCode::Failure;
    bool samplerIsBusy;
    
    static_assert(sizeof(DAQ_M02::CalibrationDataContainer) <= PAYLOAD_SIZE_MAX);
    
    if (commandPayloadBuffer.DataSize != sizeof(DAQ_M02::CalibrationDataContainer))
    {
        return StatusCode::Invalid_Argument;
    }
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Lock();
    //-----------------------------------------------------------------------------------------
    
    const auto pCalibrationDataContainer = reinterpret_cast<const DAQ_M02::CalibrationDataContainer *>(&commandPayloadBuffer.Data[0]);
    
    //
    // ADC acquisition process must be stopped.
    //
    
    CRITICAL_SECTION_ENTER;
    samplerIsBusy = DAQ.Sampler.IsBusy();
    CRITICAL_SECTION_EXIT;
    
    if (samplerIsBusy)
    {
        statusCode = StatusCode::Invalid_State;
        goto ExitError;
    }
    
    //
    // Write calibration data to NVM.
    //
    
    statusCode = pCalibrationDataContainer->NVM_Write();
    if (statusCode != StatusCode::Success)
    {
        goto ExitError;
    }
    
    //
    // Restart device with new calibration.
    //
    
    DAQ.Reset();
    
    statusCode = StatusCode::Success;
    
ExitError:
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Unlock();
    //-----------------------------------------------------------------------------------------
    
    return statusCode;
}

}
