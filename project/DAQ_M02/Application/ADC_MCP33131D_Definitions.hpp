/*
 * ADC definitions.
 */

#ifndef ADC_MCP33131D_Definitions_hpp
#define ADC_MCP33131D_Definitions_hpp

#include <cstdint>
#include <cstddef>
#include <limits>

namespace ADC
{

// Type used for storage ADC raw samples (16-bit signed).
using Sample = int16_t;

inline constexpr uint32_t ADC_BITS = 16;
inline constexpr uint32_t ADC_TOTAL_COUNTS = 1UL << ADC_BITS;

inline constexpr Sample   ADC_SAMPLE_MIN = std::numeric_limits<Sample>::min();
inline constexpr Sample   ADC_SAMPLE_MAX = std::numeric_limits<Sample>::max();

inline constexpr uint32_t ADC_CHANNEL_COUNT = 1;
inline constexpr size_t   ADC_SAMPLE_SIZE = sizeof(Sample);

inline constexpr uint32_t ADC_SAMPLING_FREQUENCY_MIN_Hz = 1;
inline constexpr uint32_t ADC_SAMPLING_FREQUENCY_MAX_Hz = 250'000;

}

#endif
