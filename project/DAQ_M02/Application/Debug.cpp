/*
 * Debug utilities (C/C++ compatible).
 * 
 * Define symbol DEBUG_LOG to enable debugging API.
 */

#define NANOPRINTF_USE_FIELD_WIDTH_FORMAT_SPECIFIERS    1
#define NANOPRINTF_USE_PRECISION_FORMAT_SPECIFIERS      1
#define NANOPRINTF_USE_LARGE_FORMAT_SPECIFIERS          1
#define NANOPRINTF_USE_FLOAT_FORMAT_SPECIFIERS          1
#define NANOPRINTF_USE_BINARY_FORMAT_SPECIFIERS         1
#define NANOPRINTF_USE_WRITEBACK_FORMAT_SPECIFIERS      0

#define NANOPRINTF_VISIBILITY_STATIC
#define NANOPRINTF_IMPLEMENTATION

#include "nanoprintf.h"
#include "Segger_SystemView/SEGGER/Segger_RTT.h"

#include "Debug.h"
#include "App_HAL.hpp"

#if defined(DEBUG_LOG)
    #warning "DEBUG_LOG is enabled"
#endif

#if defined(DEBUG_SYSVIEW)
    #warning "DEBUG_SYSVIEW is enabled"
#endif

static DebugExceptionCallback DebugExceptionCb = nullptr;

void DebugInitialize(DebugExceptionCallback callback)
{
    // Enable Usage, Bus & Memory fault handlers. 
    SCB->SHCSR |= SCB_SHCSR_USGFAULTENA_Msk | SCB_SHCSR_BUSFAULTENA_Msk | SCB_SHCSR_MEMFAULTENA_Msk;
    
    // Enable divide by 0 trap.
    SCB->CCR |= SCB_CCR_DIV_0_TRP_Msk;
    
    DebugExceptionCb = callback;
    
#if defined(DEBUG_SYSVIEW)
    SEGGER_SYSVIEW_Conf();
#elif defined(DEBUG_LOG)
    SEGGER_RTT_Init();
#endif
    
    DEBUG_TIMER_INITIALIZE();
}

#if defined(DEBUG_LOG)
static void DebugPutc(int c, [[maybe_unused]] void * ctx)
{
    SEGGER_RTT_PutCharSkipNoLock(0, (char)c);
}

int DebugPrintf(char const * format, ...)
{
    va_list val;
    
    va_start(val, format);
    const int rv = npf_vpprintf(DebugPutc, nullptr, format, val);
    va_end(val);
    
    return rv;
}

void DebugHex(const void * pData, size_t dataSize)
{
    const uint8_t * pByte = (const uint8_t *)pData;
  
    while (dataSize-- > 0)
    {
        DEBUG_PRINTF("%02X ", *pByte++);
    }
}

bool DebugGetKey(char * pKey)
{
    const auto readCount = SEGGER_RTT_Read(0, pKey, 1);
    
    return (readCount > 0);
}
#endif

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wnull-dereference"
__attribute__((__noreturn__))
void DebugTriggerFaultException(void)
{
    constexpr uint32_t InvalidWriteAddress = 0x0000'0000;
    
    uint8_t * const ptr = (uint8_t *)InvalidWriteAddress;

    *ptr = 0xA5;
    
    for (;;)
    {
    }
}
#pragma GCC diagnostic pop

__attribute__((__noreturn__))
void DebugException(const void * const PC)
{
    __disable_irq();

    // Process application callback.
    
    if (DebugExceptionCb)
    {
        DebugExceptionCb(PC);
    }

    DEBUG_PRINTF("\n\nException at %p\n", PC);
    
    // Stop MCU if debugger is connected.
    
    if (DEBUG_IS_DEBUGGER_ATTACHED())
    {
        DEBUG_BREAK();
    }
    
    // Wait constant time for watchdog reset.
    
    DEBUG_PRINTF("Wait for WDT reset...\n");
    
    HAL::WatchdogReset();
    
    for (;;)
    {
    }
}
