/*
 * Application status codes.
 */

#ifndef Status_Code_hpp
#define Status_Code_hpp

#include <cstdint>

enum class StatusCode : uint8_t
{
    //-------------------------------------------------------------------------
    // Standard codes (range 0-127).
    //-------------------------------------------------------------------------
    
    Standard_Codes = 0,
    
    Success = Standard_Codes,
    Failure,
    Invalid_Command,
    Invalid_Argument,
    Invalid_Format,
    Invalid_Version,
    Invalid_Configuration,
    Invalid_Calibration,
    Invalid_Checksum,
    Invalid_State,
    Invalid_Memory_Type,
    Invalid_Memory_Offset,
    Invalid_Memory_Address,
    Invalid_Memory_Size,
    Invalid_Memory_Alignment,
    Buffer_Overflow,
    Out_Of_Memory,
    Timeout,
    Busy,
    No_Operation,
    Hardware_Failure,
    Connection_Failure,
    Memory_Write_Error,
    Memory_Read_Error,
    Memory_Erase_Error,
    Memory_Program_Error,
    
    //-------------------------------------------------------------------------
    // Device specific codes (range 128-255).
    //-------------------------------------------------------------------------
    
    Device_Codes = 128,
    
    //
    // Add device codes here, i.e.:
    // Custom_Code_1 = Device_Codes,
    // Custom_Code_2,
    // ...
    
    //
    // Device: DAQ_M02
    //
    
    Acquisition_Data_Begin = Device_Codes,
    Acquisition_Data_Continue,
    Acquisition_Data_End,
    ADC_Calibration_Completed,
    ADC_Calibration_Invalid_Input_Current
};

#endif
