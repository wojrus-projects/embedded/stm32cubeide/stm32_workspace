/*
 * Application HAL (board layer).
 */

#ifndef App_HAL_Board_hpp
#define App_HAL_Board_hpp

#include <cstdint>

#include "App_HAL_CubeMX.h"
#include "Interrupt_Priority.hpp"

namespace HAL::Board
{

inline constexpr uint32_t SWITCH_CURRENT_RANGE_DELAY_ms = 20;
inline constexpr uint32_t VISO_ENABLE_DELAY_ms = 60;

namespace ADC
{
inline auto& SPI = hspi1;
inline auto& Timer = htim7;
}

namespace Trigger
{
inline constexpr uint32_t Line = EXTI_LINE_13;
inline constexpr uint32_t Port = EXTI_GPIOB;
inline constexpr IRQn_Type IRQ_Number = EXTI15_10_IRQn;
inline constexpr uint32_t IRQ_Priority = (uint32_t)InterruptPriority::Trigger_EXTI15_10;
}

}

#endif
