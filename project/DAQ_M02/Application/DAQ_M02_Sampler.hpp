/*
 * DAQ_M02: ADC data sampler.
 */

#ifndef DAQ_M02_Sampler_hpp
#define DAQ_M02_Sampler_hpp

#include <cstdint>

#include "DAQ_M02_Measurement_Definitions.hpp"

namespace DAQ_M02
{

enum class SamplerState : uint8_t
{
    Idle,
    WaitForTrigger,
    Acquisition,
    Completed,
    Stop,
    
    Default = Idle
};

class Sampler
{
public:
    
    struct DataCounter
    {
        volatile uint64_t SampleAcquisition = 0;
        volatile uint64_t SampleTotal = 0;
        volatile uint64_t BufferOverflowAcquisition = 0;
        volatile uint64_t BufferOverflowTotal = 0;
        volatile uint64_t Trigger = 0;
    };
    
    struct DataCounter Counter;
    volatile ADC_DataSequenceCounter_t DataSequenceCounter = 0;
    volatile SamplerState State = SamplerState::Default;
    
    void Reset();
    bool IsBusy();
    bool IsAcquisition();
    bool IsWaitForTrigger();
    bool IsDisabled();
};

}

#endif
