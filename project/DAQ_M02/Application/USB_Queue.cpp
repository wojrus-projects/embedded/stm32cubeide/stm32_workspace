/*
 * USB serial interface Tx PDU queue.
 */

#include "Debug.h"
#include "USB_Queue.hpp"

void USB_Queue::Clear()
{   
    QueueItemType pBuffer;
    
    while (txQueue.get(&pBuffer))
    {
        if (pBuffer)
        {
            pBuffer->Clear();
        }
    }
    
    txQueue.initialize();
}

size_t USB_Queue::GetLength()
{
    return txQueue.get_length();
}

bool USB_Queue::AppendBuffer(QueueItemType pBuffer)
{
    return txQueue.put(pBuffer);
}

USB_Queue::QueueItemType USB_Queue::PeekBuffer()
{
    QueueItemType pBuffer {};
    
    if (txQueue.peek(0, &pBuffer, 1) == 1)
    {
        DEBUG_ASSERT(pBuffer);
        
        return pBuffer;
    }
    
    return nullptr;
}

void USB_Queue::RemoveBuffer()
{
    txQueue.remove(1);
}
