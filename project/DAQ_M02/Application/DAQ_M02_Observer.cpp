/*
 * DAQ_M02: acquisition state observer.
 */

#include "App_Main.hpp"

static constexpr uint32_t LED_BLINK_PERIOD_ADC_ACQUISITION_ms = 100;

static volatile bool DAQ_M02_Observer_IsEnabled = false;
static volatile bool DAQ_M02_Observer_IsAcquisitionPrevious = false;

void DAQ_M02_Observer_Enable()
{
    DAQ_M02_Observer_IsAcquisitionPrevious = false;
    DAQ_M02_Observer_IsEnabled = true;
}

void DAQ_M02_Observer_Disable()
{
    DAQ_M02_Observer_IsEnabled = false;
    DAQ_M02_Observer_IsAcquisitionPrevious = false;
}

void DAQ_M02_Observer_TimerHandler()
{
    if (DAQ_M02_Observer_IsEnabled == false)
    {
        return;
    }
    
    //
    // LED Status
    //
    
    const bool isAcquisitionNow = DAQ.Sampler.IsAcquisition();
    
    if (isAcquisitionNow != DAQ_M02_Observer_IsAcquisitionPrevious)
    {
        DAQ_M02_Observer_IsAcquisitionPrevious = isAcquisitionNow;
        
        if (isAcquisitionNow)
        {
            DAQ.LED_Status.BlinkStart(LED_BLINK_PERIOD_ADC_ACQUISITION_ms);
        }
        else
        {
            DAQ.LED_Status.BlinkStop();
        }
    }
    
    //
    // LED Trigger
    //
    
    namespace Colors = DAQ_M02::LED_Color_Trigger;
    
    if (DAQ.AcquisitionConfiguration.IsTriggerEnabled())
    {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch-enum"
        switch (DAQ.Sampler.State)
        {
            case DAQ_M02::SamplerState::WaitForTrigger:
                DAQ.LED_Trigger.SetColor(Colors::WaitForTrigger);
                break;
                
            case DAQ_M02::SamplerState::Acquisition:
                DAQ.LED_Trigger.SetColor(Colors::Acquisition);
                break;
                
            default:
                DAQ.LED_Trigger.SetColor(Colors::Off);
        }
#pragma GCC diagnostic pop
    }
    else
    {
        DAQ.LED_Trigger.SetColor(Colors::Off);
    }
}
