/*
 * Device information structures.
 */

#ifndef Device_Info_hpp
#define Device_Info_hpp

#include <cstdint>

namespace DeviceInfo
{

struct [[gnu::packed]] SoftwareVersion
{
    uint8_t Major   = 0;
    uint8_t Minor   = 0;
    uint8_t Patch   = 0;
    uint8_t Variant = 0;
    
    constexpr bool IsValid() const;
};

struct [[gnu::packed]] SoftwareDate
{
    uint16_t Year  = 0;
    uint8_t  Month = 0;
    uint8_t  Day   = 0;
    
    constexpr bool IsValid() const;
};

struct [[gnu::packed]] SoftwareInfo
{
    SoftwareVersion Version;
    SoftwareDate    Date;
    
    constexpr bool IsValid() const;
};

struct [[gnu::packed]] DeviceInfo
{
    char         Magic[4] { 'I', 'N', 'F', 'O' };
    uint32_t     HardwareType = 0;
    uint32_t     HardwareVariant = 0;
    SoftwareInfo Firmware;
    
    constexpr bool IsValid() const;
};

void DeviceInfoPrint(const DeviceInfo * const pDeviceInfo);

}

#endif
