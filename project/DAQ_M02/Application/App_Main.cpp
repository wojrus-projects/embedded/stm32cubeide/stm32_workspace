/*
 * DAQ_M02 firmware.
 */

#ifdef __GNUC__
#   if !(__GNUC__ == 14 && __GNUC_MINOR__ == 2)
#       error "Code require GCC v14.2"
#   endif
#else
#   error "Code require GCC."
#endif

#if __cplusplus < 202302L
#   error "Code require C++20 with some C++23 features"
#endif

#include "Debug.h"
#include "App_Main.hpp"
#include "App_HAL.hpp"
#include "App_Main_Host_Protocol.hpp"
#include "SysTick_Handler.hpp" 
#include "Device_Info_Data.hpp"

DAQ_M02::DAQ_M02 DAQ;
Host_Protocol::Host_Protocol HostProtocol(HostProtocol_GetAppCallback());

static void ExceptionCallback(const void * const PC);
static void LED_BlinkAtReset();

/*
 * Application main function.
 * Function is executed before RTOS and USB initialization.
 */
extern "C" void App_Main(void)
{   
    HAL::HalInitialize();
    
    DebugInitialize(ExceptionCallback);
    
    DEBUG_PRINTF("\n" DEVICE_NAME
                 "\nBuild: " __DATE__ ", " __TIME__
                 "\nC++: %u\n", __cplusplus);
    
    DEBUG_PRINTF("Reset reason: %s\n", HAL::GetResetReasonName(HAL::GetResetReason()));
    
    DeviceInfo::DeviceInfoPrint(&DeviceInfo::DeviceInfoData);
    
    DAQ.Initialize();
    
    HostProtocol.Reset();
    Host_Protocol::SetCommandCallback(HostProtocol_GetAppCommandCallback());
    
    LED_BlinkAtReset();
    
    SysTick_Handler_Enable();
}

static void ExceptionCallback([[maybe_unused]] const void * const PC)
{
    DEBUG_PRINTF("App deinit\n");
    
    DAQ.VISO_PowerDown();
    DAQ.Reset();
    DAQ.LED_Status.SetColor(DAQ_M02::LED_Color_Status::Exception);
    DAQ.LED_Trigger.SetColor(DAQ_M02::LED_Color_Trigger::Exception);
    HAL::UsbDisable();
}

static void LED_BlinkAtReset()
{
    DAQ.LED_Status.SetColor(LED_Color::Red);
    HAL_Delay(200);
    DAQ.LED_Status.SetColor(LED_Color::Green);
    HAL_Delay(200);
    DAQ.LED_Status.SetColor(LED_Color::Blue);
    HAL_Delay(200);
    DAQ.LED_Status.SetColor(LED_Color::Black);
    HAL_Delay(200);
}
