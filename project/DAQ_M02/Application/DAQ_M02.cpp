/*
 * DAQ_M02: main file.
 */

#include <algorithm>
#include <cmath>

#include "Debug.h"
#include "RTOS_Common.h"
#include "Critical_Section.hpp"
#include "Thread_ADC.hpp"
#include "DAQ_M02.hpp"

namespace DAQ_M02
{

void DAQ_M02::Initialize()
{
    ADC.Initialize(ADC_Callback_ReadCompleted_Static);
    Trigger.Initialize(Trigger_Callback_InputChanged_Static);
    LED_Status.Initialize(LED_HAL_SetColorStatus);
    LED_Trigger.Initialize(LED_HAL_SetColorTrigger);
    
    VISO_PowerDown();
    Reset();
    
    DAQ_M02_Observer_Enable();
}

void DAQ_M02::Reset()
{
    DEBUG_PRINTF("DAQ: reset\n");
    
    CRITICAL_SECTION_ENTER;
    
    ADC.Stop();
    Trigger.Stop();
    
    CRITICAL_SECTION_EXIT;
    
    SwitchCurrentRange(CurrentRangeDefault);
    
    Sampler.Reset();
    Trigger_RequireRearm = false;
    AcquisitionConfiguration.Reset();
    ADC_Calibration.Reset();
    ADC_CalibrationCoefficient = ADC_Calibration.NVM_ReadCalibrationCoefficient(CurrentRangeDefault);
    ADC_SamplingFrequencyReal_Hz = 0;
    ADC_SampleBuffer.Clear();
    PduBuffer_DataContinue_First.Clear();
    PduBuffer_DataContinue_Second.Clear();
    PduBuffer_DataEnd.Clear();
    USB_TxQueue.Clear();
    IsCalibrationMode = false;
    
    LED_Trigger.SetColor(LED_Color_Trigger::Off);
}

void DAQ_M02::VISO_Enable(bool powerState)
{
    VISO_IsEnabled = not not powerState;
    
    HAL_GPIO_WritePin(VISO_ON_GPIO_Port, VISO_ON_Pin,
            VISO_IsEnabled ? GPIO_PIN_SET : GPIO_PIN_RESET);
    
    DEBUG_PRINTF_EXT("VISO: %u", VISO_IsEnabled);
}

void DAQ_M02::VISO_PowerUp()
{
    if (not VISO_IsEnabled)
    {
        VISO_Enable(true);
        
        RTOS_Sleep(HAL::Board::VISO_ENABLE_DELAY_ms);
    }
}

void DAQ_M02::VISO_PowerDown()
{
    VISO_Enable(false);
}

StatusCode DAQ_M02::SwitchCurrentRange(CurrentRange range)
{
    switch (range)
    {
        case CurrentRange::Range_5_mA:
            HAL_GPIO_WritePin(REL_L_GPIO_Port, REL_L_Pin, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(REL_H_GPIO_Port, REL_H_Pin, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(MUX_A0_GPIO_Port, MUX_A0_Pin, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(MUX_A1_GPIO_Port, MUX_A1_Pin, GPIO_PIN_RESET);
            break;
            
        case CurrentRange::Range_50_mA:
            HAL_GPIO_WritePin(REL_L_GPIO_Port, REL_L_Pin, GPIO_PIN_SET);
            HAL_GPIO_WritePin(REL_H_GPIO_Port, REL_H_Pin, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(MUX_A0_GPIO_Port, MUX_A0_Pin, GPIO_PIN_SET);
            HAL_GPIO_WritePin(MUX_A1_GPIO_Port, MUX_A1_Pin, GPIO_PIN_RESET);
            break;
            
        case CurrentRange::Range_500_mA:
            HAL_GPIO_WritePin(REL_L_GPIO_Port, REL_L_Pin, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(REL_H_GPIO_Port, REL_H_Pin, GPIO_PIN_SET);
            HAL_GPIO_WritePin(MUX_A0_GPIO_Port, MUX_A0_Pin, GPIO_PIN_SET);
            HAL_GPIO_WritePin(MUX_A1_GPIO_Port, MUX_A1_Pin, GPIO_PIN_SET);
            break;
            
        case CurrentRange::Range_5_A:
            HAL_GPIO_WritePin(REL_L_GPIO_Port, REL_L_Pin, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(REL_H_GPIO_Port, REL_H_Pin, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(MUX_A0_GPIO_Port, MUX_A0_Pin, GPIO_PIN_SET);
            HAL_GPIO_WritePin(MUX_A1_GPIO_Port, MUX_A1_Pin, GPIO_PIN_SET);
            break;
            
        default:
            return StatusCode::Invalid_Argument;
    }
    
    return StatusCode::Success;
}

StatusCode DAQ_M02::Acquisition_Start(bool isCalibrationMode)
{
    StatusCode statusCode;
    
    const size_t adcBufferSize = ADC_CalculateBufferSize(AcquisitionConfiguration.SamplingFrequency_Hz,
                                                         AcquisitionConfiguration.SampleCountMax);
    
    if (AcquisitionConfiguration.IsTriggerEnabled())
    {
        DEBUG_PRINTF_EXT("DAQ: trigger start");
    }
    else
    {
        DEBUG_PRINTF_EXT("DAQ: ADC start: buffer: %u, Fs: %u Hz",
                adcBufferSize, AcquisitionConfiguration.SamplingFrequency_Hz);
    }
    
    CRITICAL_SECTION_ENTER;
    
    Sampler.Reset();
    ADC_SampleBuffer.Clear();
    
    IsCalibrationMode = isCalibrationMode;
    
    if (AcquisitionConfiguration.IsTriggerEnabled())
    {   
        Sampler.State = SamplerState::WaitForTrigger;
        statusCode = Trigger.Start(AcquisitionConfiguration.TriggerMode);
    }
    else
    {   
        ADC.ClearPipeline();
        
        Sampler.State = SamplerState::Acquisition;
        statusCode = ADC.Start(AcquisitionConfiguration.SamplingFrequency_Hz, &ADC_SamplingFrequencyReal_Hz, true, adcBufferSize);
    }
    
    CRITICAL_SECTION_EXIT;
    
    return statusCode;
}

StatusCode DAQ_M02::Acquisition_Retrigger()
{
    StatusCode statusCode;
    
    DEBUG_PRINTF_EXT("DAQ: trigger restart");
    
    CRITICAL_SECTION_ENTER;
    
    ADC_SampleBuffer.Clear();
    
    Sampler.State = SamplerState::WaitForTrigger;
    statusCode = Trigger.Start(AcquisitionConfiguration.TriggerMode);
    
    CRITICAL_SECTION_EXIT;
    
    return statusCode;
}

void DAQ_M02::Acquisition_Stop()
{
    DEBUG_PRINTF_EXT("DAQ: ADC stop");
    
    CRITICAL_SECTION_ENTER;
    
    ADC.Stop();
    Trigger.Stop();
    ADC_SampleBuffer.Clear();
    
    Sampler.State = SamplerState::Stop;
    
    CRITICAL_SECTION_EXIT;
}

void DAQ_M02::Acquisition_CountSamples()
{
    CRITICAL_SECTION_ENTER;
    
    Sampler.Counter.SampleAcquisition += ADC_SampleBuffer.DataSize;
    Sampler.Counter.SampleTotal       += ADC_SampleBuffer.DataSize;
    
    CRITICAL_SECTION_EXIT;
}

void DAQ_M02::Acquisition_CountSamplesOverflowed()
{
    CRITICAL_SECTION_ENTER;
    
    Sampler.Counter.BufferOverflowAcquisition += ADC_SampleBuffer.DataSize;
    Sampler.Counter.BufferOverflowTotal       += ADC_SampleBuffer.DataSize;
    
    CRITICAL_SECTION_EXIT;
}

void DAQ_M02::Trigger_Callback_InputChanged(void)
{
    StatusCode statusCode;

    CRITICAL_SECTION_ENTER;
    
    Trigger.Stop();
    
    CRITICAL_SECTION_EXIT;
    
    Sampler.Counter.Trigger++;
    Sampler.Counter.SampleAcquisition = 0;
    Sampler.Counter.BufferOverflowAcquisition = 0;
    
    Sampler.DataSequenceCounter = 0;
    
    Sampler.State = SamplerState::Acquisition;
    
    const size_t adcBufferSize = ADC_CalculateBufferSize(AcquisitionConfiguration.SamplingFrequency_Hz,
                                                         AcquisitionConfiguration.SampleCountMax);
    
    DEBUG_PRINTF_EXT("DAQ: ADC triggered: Fs: %u Hz, sample max: %llu, buffer: %u",
            AcquisitionConfiguration.SamplingFrequency_Hz,
            AcquisitionConfiguration.SampleCountMax,
            adcBufferSize);
    
    ADC.ClearPipeline();
    
    statusCode = ADC.Start(AcquisitionConfiguration.SamplingFrequency_Hz,
                           &ADC_SamplingFrequencyReal_Hz,
                           true, adcBufferSize);
    DEBUG_ASSERT(statusCode == StatusCode::Success);
}

void DAQ_M02::ADC_Callback_ReadCompleted(ADC::Sample * pAdcSamples, size_t adcSamplesCount)
{
    // CRITICAL: ADC DMA overflow.
    
    DEBUG_ASSERT(ADC_SampleBuffer.IsEmpty());
    
    // Skip first sample.
    // First sample conversion time is always asynchronous relative to next samples.
    
    DEBUG_ASSERT(adcSamplesCount > 1);
    
    if (Sampler.Counter.SampleAcquisition == 0)
    {
        adcSamplesCount -= 1;
        pAdcSamples += 1;
    }
    
    const uint64_t sampleCountRemaining = AcquisitionConfiguration.SampleCountMax - Sampler.Counter.SampleAcquisition;
    const uint64_t sampleCountChunk = std::min((uint64_t)adcSamplesCount, sampleCountRemaining);
    const uint64_t sampleCountTotal = Sampler.Counter.SampleAcquisition + sampleCountChunk; 
    
    // Acquisition is completed.
    
    if (sampleCountTotal >= AcquisitionConfiguration.SampleCountMax)
    {
        ADC.Stop();
        Sampler.State = SamplerState::Completed;
        
        DEBUG_PRINTF_EXT("DAQ: ADC completed");
    }
    else
    {
        // Expected next ADC data chunk.
    }
    
    // Process ADC data.
    
    ADC_SampleBuffer = { pAdcSamples, (size_t)sampleCountChunk };
    
    if (IsCalibrationMode)
    {
        Sampler.Counter.SampleAcquisition += sampleCountChunk;
        
        Event_Emit_AdcCalibration();
    }
    else
    {
        Event_Emit_AdcPostprocessing();
    }
}

void DAQ_M02::Acquisition_PostprocessSamples(Host_Protocol::PduBuffer * const pOutputPduBuffer)
{
    uint8_t * pPduPayload = &pOutputPduBuffer->Data[PDU_SAMPLES_OFFSET];
    
    CalibrationCoefficient calibrationCoefficient;
    
    if (ADC_CalibrationCoefficient)
    {
        calibrationCoefficient = *ADC_CalibrationCoefficient;
    }
    else
    {
        calibrationCoefficient.SetNominal(AcquisitionConfiguration.CurrentRange);
    }
    
    for (size_t i = 0; i < ADC_SampleBuffer.DataSize; i++)
    {
        SampleExt adcSample = (SampleExt)ADC_SampleBuffer.Data[i] + (SampleExt)calibrationCoefficient.Offset_sample;
        
        adcSample = std::clamp(adcSample, (SampleExt)ADC::ADC_SAMPLE_MIN, (SampleExt)ADC::ADC_SAMPLE_MAX);
        
        ADC_SampleToBytes((ADC::Sample)adcSample, pPduPayload);
        
        pPduPayload += ADC::ADC_SAMPLE_SIZE;
    }
    
    pOutputPduBuffer->DataSize = ADC_SampleBuffer.DataSize * ADC::ADC_SAMPLE_SIZE;
}

ADC_SaturationCurrent DAQ_M02::ADC_GetSaturationCurrent()
{
    CalibrationCoefficient calibrationCoefficient;
    
    if (ADC_CalibrationCoefficient)
    {
        calibrationCoefficient = *ADC_CalibrationCoefficient;
    }
    else
    {
        calibrationCoefficient.SetNominal(AcquisitionConfiguration.CurrentRange);
    }
    
    const float currentSaturation_POS_A = ADC_SAMPLE_SATURATED_POS * calibrationCoefficient.Gain_AmperePerSample;
    const float currentSaturation_NEG_A = ADC_SAMPLE_SATURATED_NEG * calibrationCoefficient.Gain_AmperePerSample;
    
    return { .Positive = currentSaturation_POS_A,
             .Negative = currentSaturation_NEG_A };
}

bool DAQ_M02::PduBuffersAreEmpty()
{
    return (PduBuffer_DataContinue_First.IsEmpty() &&
            PduBuffer_DataContinue_Second.IsEmpty() &&
            PduBuffer_DataEnd.IsEmpty());
}

size_t DAQ_M02::ADC_CalculateBufferSize(uint32_t samplingFrequency_Hz, uint64_t sampleCountMax)
{
    DEBUG_ASSERT((samplingFrequency_Hz > 0) && (sampleCountMax > 0));

    // - Define min. acquisition latency at fastest sampling frequency (250 kHz) and entire DMA half-buffer.
    // - Slow down ADC DMA interrupt rate.
    // - Define safe time margin (without rat races) for data postprocessing in ADC thread.
    
    constexpr float ACQUISITION_LATENCY_MIN_s = (float)(ADC.GetBufferSizeMaxSamples() / ADC_DMA_DOUBLE_BUFFER_COUNT) / (float)ADC::ADC_SAMPLING_FREQUENCY_MAX_Hz;
    
    // DAQ must send any ADC data in this arbitrary time window.
    // This time define max. acquisition latency at slowest sampling frequency (1 Hz)
    // for maintain continuous ADC data stream over USB.
    
    constexpr float ACQUISITION_LATENCY_MAX_s = 2.0f;
    
    size_t halfBufferSize_samples = (size_t)std::clamp(sampleCountMax, (uint64_t)(ADC.GetBufferSizeMinSamples() / ADC_DMA_DOUBLE_BUFFER_COUNT),
                                                                       (uint64_t)(ADC.GetBufferSizeMaxSamples() / ADC_DMA_DOUBLE_BUFFER_COUNT));
        
    const float halfBufferAcquisitionTime_s = (float)halfBufferSize_samples / (float)samplingFrequency_Hz;
    
    if (halfBufferAcquisitionTime_s < ACQUISITION_LATENCY_MIN_s)
    {
        halfBufferSize_samples = (size_t)std::ceil(ACQUISITION_LATENCY_MIN_s * (float)samplingFrequency_Hz);
    }
    else if (halfBufferAcquisitionTime_s > ACQUISITION_LATENCY_MAX_s)
    {
        halfBufferSize_samples = (size_t)std::ceil(ACQUISITION_LATENCY_MAX_s * (float)samplingFrequency_Hz);
    }
    else
    {
        // No time boundary.
    }
    
    halfBufferSize_samples = std::clamp(halfBufferSize_samples, (ADC.GetBufferSizeMinSamples() / ADC_DMA_DOUBLE_BUFFER_COUNT),
                                                                (ADC.GetBufferSizeMaxSamples() / ADC_DMA_DOUBLE_BUFFER_COUNT));
    
    const size_t fullBufferSize_samples = halfBufferSize_samples * ADC_DMA_DOUBLE_BUFFER_COUNT;
    
    return fullBufferSize_samples;
}

}
