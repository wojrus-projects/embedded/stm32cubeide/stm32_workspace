/*
 * USB CDC common code.
 */

#include "Debug.h"
#include "USB_CDC_Callback.h"
#include "USB_CDC_Common.hpp"
#include "USB_CDC_Tx.hpp"
#include "USB_CDC_Rx.hpp"

class USB_CDC_State USB_CDC_State;

static constexpr size_t USB_CDC_EventQueue_MessageMax = 4;
static constexpr size_t USB_CDC_EventQueue_MessageSizeWords = 1;
static constexpr size_t USB_CDC_EventQueue_MessageSizeBytes = USB_CDC_EventQueue_MessageSizeWords * sizeof(ULONG);

static TX_QUEUE USB_CDC_EventQueue;
static uint8_t USB_CDC_EventQueue_Buffer[USB_CDC_EventQueue_MessageMax * USB_CDC_EventQueue_MessageSizeBytes];

static_assert(sizeof(USB_CDC_Event) == sizeof(ULONG));

UX_SLAVE_CLASS_CDC_ACM_CALLBACK_PARAMETER USB_CDC_TransferCallback =
{
    .ux_device_class_cdc_acm_parameter_write_callback = USB_CDC_Tx_Callback,
    .ux_device_class_cdc_acm_parameter_read_callback = USB_CDC_Rx_Callback
};

static void USB_CDC_PutEventInQueue(USB_CDC_Event event);
static void USB_CDC_OnSuspend();
static void USB_CDC_OnResume();
static void USB_CDC_OnOpen();
static void USB_CDC_OnClose();

const struct USB_CDC_ControlCallback USB_CDC_ControlCallback
{
    .Suspend = USB_CDC_OnSuspend,
    .Resume = USB_CDC_OnResume,
    .Open = USB_CDC_OnOpen,
    .Close = USB_CDC_OnClose
};

static void USB_CDC_OnSuspend()
{
    USB_CDC_PutEventInQueue(USB_CDC_Event::Suspend);
}

static void USB_CDC_OnResume()
{
    USB_CDC_PutEventInQueue(USB_CDC_Event::Resume);
}

static void USB_CDC_OnOpen()
{
    USB_CDC_PutEventInQueue(USB_CDC_Event::Open);
}

static void USB_CDC_OnClose()
{
    USB_CDC_PutEventInQueue(USB_CDC_Event::Close);
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
UINT USB_CDC_InitializeEventQueue()
{
    UINT status;
    
    status = tx_queue_create(&USB_CDC_EventQueue,
                             (CHAR *)"USB event queue",
                             USB_CDC_EventQueue_MessageSizeWords,
                             USB_CDC_EventQueue_Buffer, sizeof(USB_CDC_EventQueue_Buffer));
    return status;
}
#pragma GCC diagnostic pop

static void USB_CDC_PutEventInQueue(USB_CDC_Event event)
{
    UINT status;
    
    status = tx_queue_send(&USB_CDC_EventQueue, &event, TX_NO_WAIT);
    DEBUG_ASSERT(status == TX_SUCCESS);
}

std::optional<USB_CDC_Event> USB_CDC_GetEventFromQueue(ULONG timeout_ms)
{
    UINT status;
    USB_CDC_Event event;
    std::optional<USB_CDC_Event> optionalEvent;
    
    status = tx_queue_receive(&USB_CDC_EventQueue, &event, UX_MS_TO_TICK_NON_ZERO(timeout_ms));    
    if (status == TX_SUCCESS)
    {
        optionalEvent = event;
    }
    else if (status == TX_QUEUE_EMPTY)
    {
        // Return no event.
    }
    else
    {
        DEBUG_EXCEPTION();
    }
    
    return optionalEvent;
}

void USB_CDC_SendPdu(Host_Protocol::PduBuffer * pPdu)
{
    UINT status;
    
    DEBUG_ASSERT(pPdu && (not pPdu->IsEmpty()));
                        
    DEBUG_PRINTF_EXT("USB: Tx: %p, size: %u",
            pPdu->Data, pPdu->DataSize);
    
    DEBUG_SYSVIEW_MARK_START(Debug_SysView_MarkerID_UsbTx);
    
    DEBUG_ASSERT((not USB_CDC_State.IsTxPending) &&
                 (USBD_CDC_ACM_Instance->ux_slave_class_cdc_acm_scheduled_write == UX_FALSE));
    
    status = ux_device_class_cdc_acm_write_with_callback(USBD_CDC_ACM_Instance,
            (UCHAR *)pPdu->Data, pPdu->DataSize);
    
    if (status == UX_SUCCESS)
    {
        USB_CDC_State.IsTxPending = true;
    }
    else if (status == UX_CONFIGURATION_HANDLE_UNKNOWN)
    {
        DEBUG_PRINTF("USB: Tx warning: configuration fault\n");
    }
    else
    {
        DEBUG_PRINTF("USB: Tx error: 0x%X\n", status);
        
        DEBUG_EXCEPTION();
    }
}

void USB_CDC_State::Reset()
{
    IsConnected = false;
    IsTxPending = false;
    StateChangeCounter = 0;
    StateChangeCounter_Backup = 0;
}

void USB_CDC_State::Update()
{
    StateChangeCounter++;
}

void USB_CDC_State::Save()
{
    StateChangeCounter_Backup = StateChangeCounter;
}

bool USB_CDC_State::IsUnchanged()
{
    return (StateChangeCounter == StateChangeCounter_Backup);
}
