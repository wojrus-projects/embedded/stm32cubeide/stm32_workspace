/*
 * Host protocol: application callbacks and command handlers.
 */

#ifndef App_Main_Host_Protocol_hpp
#define App_Main_Host_Protocol_hpp

#include "Host_Protocol.hpp"

const Host_Protocol::ProtocolCallback * HostProtocol_GetAppCallback();
const Host_Protocol::CommandCallback * HostProtocol_GetAppCommandCallback();

#endif
