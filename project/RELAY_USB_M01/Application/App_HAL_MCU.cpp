/*
 * Application HAL (MCU layer).
 */

#include "Debug.h"
#include "App_HAL_CMSIS.h"
#include "App_HAL_CubeMX.h"
#include "App_HAL_MCU.hpp"
#include "Utilities.hpp"

#if !defined(STM32L412xx)
#error "Required MCU STM32L412xx"
#endif

static_assert((PREFETCH_ENABLE == 1) && (INSTRUCTION_CACHE_ENABLE == 1) && (DATA_CACHE_ENABLE == 1));

namespace HAL
{

void HalInitialize()
{   
    HAL_PWR_DisableSleepOnExit();
    HAL_PWREx_EnableVddUSB();
}

[[noreturn]] void McuReset()
{
    __disable_irq();
    NVIC_SystemReset();
    
    for (;;)
    {
    }
}

void McuSleep()
{
    HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
}

void WatchdogReset()
{
    HAL_IWDG_Refresh(&hiwdg);
}

void UsbDisable()
{
    HAL_PCD_Stop(&hpcd_USB_FS);
    HAL_PWREx_DisableVddUSB();
}

/*
 * MCU flash memory erase.
 * 
 * Flash address and erase size are internally aligned to 2 KB boundary.
 */
StatusCode FlashErase(uint32_t flashAddress, size_t eraseSizeBytes)
{
    StatusCode status = StatusCode::Failure;
    size_t flashMemoryAvailable;
    HAL_StatusTypeDef halStatus;
    FLASH_EraseInitTypeDef eraseInit {};
    uint32_t pageError {};
    
    DEBUG_PRINTF("Flash: erase begin (address: 0x%08X, size: %u)\n",
        flashAddress, eraseSizeBytes);
    
    if ((flashAddress < FLASH_BASE_ADDRESS) ||
        (flashAddress >= (FLASH_BASE_ADDRESS + FLASH_SIZE_BYTES)))
    {
        status = StatusCode::Invalid_Memory_Address;
        goto ExitError;
    }
    
    if (eraseSizeBytes > FLASH_SIZE_BYTES)
    {
        status = StatusCode::Invalid_Memory_Size;
        goto ExitError;
    }
    
    flashMemoryAvailable = FLASH_SIZE_BYTES - (flashAddress - FLASH_BASE_ADDRESS);
    
    if (eraseSizeBytes > flashMemoryAvailable)
    {
        status = StatusCode::Invalid_Memory_Size;
        goto ExitError;
    }
    
    if (eraseSizeBytes == 0)
    {
        goto ExitSuccess;
    }
    
    halStatus = HAL_FLASH_Unlock();
    if (halStatus != HAL_OK)
    {
        status = StatusCode::Hardware_Failure;
        goto ExitError;
    }
    
    eraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
    eraseInit.Banks = FLASH_BANK_1;
    eraseInit.Page = (flashAddress - FLASH_BASE_ADDRESS) / FLASH_PAGE_SIZE_BYTES;
    eraseInit.NbPages = Utilities::DivideIntegerCeil(eraseSizeBytes, FLASH_PAGE_SIZE_BYTES);
    
    DEBUG_PRINTF("Flash: erase page: %u, page count: %u\n",
            eraseInit.Page, eraseInit.NbPages);
    
    halStatus = HAL_FLASHEx_Erase(&eraseInit, &pageError);
    if (halStatus != HAL_OK)
    {
        status = StatusCode::Memory_Erase_Error;
        goto ExitError;
    }
    
    halStatus = HAL_FLASH_Lock();
    if (halStatus != HAL_OK)
    {
        status = StatusCode::Hardware_Failure;
        goto ExitError;
    }
    
    if (pageError != 0xFFFF'FFFFU)
    {
        status = StatusCode::Memory_Erase_Error;
        goto ExitError;
    }
    
    for (uint32_t verifyAddressEnd = flashAddress + eraseSizeBytes,
                  verifyAddress = flashAddress; verifyAddress < verifyAddressEnd; verifyAddress++)
    {
        if (*reinterpret_cast<const uint8_t *>(verifyAddress) != 0xFFU)
        {
            status = StatusCode::Memory_Erase_Error;
            goto ExitError;
        }
    }
    
ExitSuccess:
    
    status = StatusCode::Success;
    
ExitError:
    
    DEBUG_PRINTF("Flash: erase end (status: %u)\n", status);

    return status;
}

/*
 * MCU flash memory write.
 * 
 * Flash address must be aligned to 8 bytes boundary.
 * Write data address alignment is not important.
 * Write data size is internally aligned to 8 byte chunks with 0xFF padding at MSB.
 */
StatusCode FlashWrite(uint32_t flashAddress, const void * pWriteData, size_t writeDataSizeBytes)
{
    StatusCode status = StatusCode::Failure;
    size_t flashMemoryAvailable;
    uint32_t flashChunkAddress;
    size_t writeDataAvailable;
    const uint8_t * pWriteDataChunk;
    HAL_StatusTypeDef halStatus;
    
    DEBUG_PRINTF("Flash: write begin (address: 0x%08X, size: %u)\n",
        flashAddress, writeDataSizeBytes);
    
    if ((flashAddress < FLASH_BASE_ADDRESS) ||
        (flashAddress >= (FLASH_BASE_ADDRESS + FLASH_SIZE_BYTES)))
    {
        status = StatusCode::Invalid_Memory_Address;
        goto ExitError;
    }
    
    if (writeDataSizeBytes > FLASH_SIZE_BYTES)
    {
        status = StatusCode::Invalid_Memory_Size;
        goto ExitError;
    }
    
    if ((flashAddress - FLASH_BASE_ADDRESS) % FLASH_WRITE_ALIGNMENT_BYTES)
    {
        status = StatusCode::Invalid_Memory_Alignment;
        goto ExitError;
    }
    
    flashMemoryAvailable = FLASH_SIZE_BYTES - (flashAddress - FLASH_BASE_ADDRESS);
    
    if (writeDataSizeBytes > flashMemoryAvailable)
    {
        status = StatusCode::Invalid_Memory_Size;
        goto ExitError;
    }
    
    if (writeDataSizeBytes == 0)
    {
        goto ExitSuccess;
    }
    
    halStatus = HAL_FLASH_Unlock();
    if (halStatus != HAL_OK)
    {
        status = StatusCode::Hardware_Failure;
        goto ExitError;
    } 
    
    flashChunkAddress = flashAddress;
    writeDataAvailable = writeDataSizeBytes;
    pWriteDataChunk = reinterpret_cast<const uint8_t *>(pWriteData);
    
    while (writeDataAvailable > 0)
    {
        const size_t writeChunkSizeBytes = (writeDataAvailable > FLASH_WRITE_CHUNK_SIZE_BYTES)
                ? FLASH_WRITE_CHUNK_SIZE_BYTES : writeDataAvailable;

        uint64_t writeChunk = 0xFFFF'FFFF'FFFF'FFFFU;
        
        for (uint32_t i = 0; i < writeChunkSizeBytes; i++)
        {
            const uint8_t byte = *pWriteDataChunk++;
            const uint32_t bitOffsetInChunk = 8 * i;

            writeChunk &= ~(0xFFULL << bitOffsetInChunk);
            writeChunk |= ((uint64_t)byte << bitOffsetInChunk);
        }
        
        DEBUG_PRINTF("Flash: write chunk: available: %u, size: %u, address: 0x%08X\n",
                writeDataAvailable, writeChunkSizeBytes, flashChunkAddress);
        
        halStatus = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, flashChunkAddress, writeChunk);
        if (halStatus != HAL_OK)
        {
            status = StatusCode::Memory_Program_Error;
            goto ExitError;
        }
        
        flashChunkAddress += writeChunkSizeBytes;
        writeDataAvailable -= writeChunkSizeBytes;
    }
    
    halStatus = HAL_FLASH_Lock();
    if (halStatus != HAL_OK)
    {
        status = StatusCode::Hardware_Failure;
        goto ExitError;
    }
    
    if (memcmp(reinterpret_cast<const void *>(flashAddress), pWriteData, writeDataSizeBytes) != 0)
    {
        status = StatusCode::Memory_Program_Error;
        goto ExitError;
    }
    
ExitSuccess:
    
    status = StatusCode::Success;
    
ExitError:
    
    DEBUG_PRINTF("Flash: write end (status: %u)\n", status);

    return status;
}

template
<
    const uint8_t * MemoryStartAddress,
    size_t          MemorySize
>
StatusCode ApplicationMemoryWrite(uint32_t writeOffset,
                                  const BufferByte writeData,
                                  size_t * const pOutWriteSizeActual)
{
    StatusCode status = StatusCode::Failure;
    
    if (writeOffset >= MemorySize)
    {
        return StatusCode::Invalid_Memory_Offset;
    }
    
    const size_t writeAvailable = MemorySize - writeOffset;
    size_t writeSize = writeData.DataSize;
    
    if (writeSize > writeAvailable)
    {
        writeSize = writeAvailable;
    }
    
    const uint32_t writeAddress = reinterpret_cast<uint32_t>(&MemoryStartAddress[writeOffset]);
    
    status = FlashWrite(writeAddress, writeData.Data, writeSize);
    if (status == StatusCode::Success)
    {
        *pOutWriteSizeActual = writeSize;
    }
    
    return status;
}

template
<
    const uint8_t * MemoryStartAddress,
    size_t          MemorySize
>
StatusCode ApplicationMemoryRead(uint32_t readOffset,
                                 size_t readSize,
                                 const BufferByte readBuffer,
                                 size_t * const pOutReadSizeActual)
{
    if (readOffset >= MemorySize)
    {
        return StatusCode::Invalid_Memory_Offset;
    }
    
    const size_t readAvailable = MemorySize - readOffset;
    
    if (readSize > readAvailable)
    {
        readSize = readAvailable;
    }
    
    if (readSize > readBuffer.DataSize)
    {
        readSize = readBuffer.DataSize;
    }
    
    const uint8_t * readAddress = &MemoryStartAddress[readOffset];
    
    memcpy(readBuffer.Data, readAddress, readSize);
    
    *pOutReadSizeActual = readSize;
    
    return StatusCode::Success;
}

template
<
    const uint8_t * MemoryStartAddress,
    size_t          MemorySize
>
StatusCode ApplicationMemoryErase(uint32_t eraseOffset, size_t eraseSize)
{
    StatusCode status = StatusCode::Failure;
    
    if (eraseOffset >= MemorySize)
    {
        return StatusCode::Invalid_Memory_Offset;
    }
    
    const size_t eraseAvailable = MemorySize - eraseOffset;
    
    if (eraseSize > eraseAvailable)
    {
        eraseSize = eraseAvailable;
    }
    
    const uint32_t eraseAddress = reinterpret_cast<uint32_t>(&MemoryStartAddress[eraseOffset]);
    
    status = FlashErase(eraseAddress, eraseSize);
    
    return status;
}

StatusCode ConfigurationWrite_Factory(uint32_t writeOffset,
                                      const BufferByte writeData,
                                      size_t * const pOutWriteSizeActual)
{
    return ApplicationMemoryWrite<CONFIG_FACTORY_ADDRESS, CONFIG_FACTORY_SIZE_BYTES>
            (writeOffset, writeData, pOutWriteSizeActual);
}

StatusCode ConfigurationRead_Factory(uint32_t readOffset,
                                     size_t readSize,
                                     const BufferByte readBuffer,
                                     size_t * const pOutReadSizeActual)
{
    return ApplicationMemoryRead<CONFIG_FACTORY_ADDRESS, CONFIG_FACTORY_SIZE_BYTES>
            (readOffset, readSize, readBuffer, pOutReadSizeActual);
}

StatusCode ConfigurationErase_Factory(uint32_t eraseOffset, size_t eraseSize)
{
    return ApplicationMemoryErase<CONFIG_FACTORY_ADDRESS, CONFIG_FACTORY_SIZE_BYTES>
            (eraseOffset, eraseSize);
}

StatusCode ConfigurationWrite_Firmware(uint32_t writeOffset,
                                       const BufferByte writeData,
                                       size_t * const pOutWriteSizeActual)
{
    return ApplicationMemoryWrite<CONFIG_APPLICATION_ADDRESS, CONFIG_APPLICATION_SIZE_BYTES>
            (writeOffset, writeData, pOutWriteSizeActual);
}

StatusCode ConfigurationRead_Firmware(uint32_t readOffset,
                                      size_t readSize,
                                      const BufferByte readBuffer,
                                      size_t * const pOutReadSizeActual)
{
    return ApplicationMemoryRead<CONFIG_APPLICATION_ADDRESS, CONFIG_APPLICATION_SIZE_BYTES>
            (readOffset, readSize, readBuffer, pOutReadSizeActual);
}

StatusCode ConfigurationErase_Firmware(uint32_t eraseOffset, size_t eraseSize)
{
    return ApplicationMemoryErase<CONFIG_APPLICATION_ADDRESS, CONFIG_APPLICATION_SIZE_BYTES>
            (eraseOffset, eraseSize);
}

uint32_t GetBoardSerialNumber()
{
    const uint8_t * const pSerialNumberAddress = CONFIG_FACTORY_ADDRESS;
    
    return Utilities::FromBytes<uint32_t>(pSerialNumberAddress);
}

void PrintOptionBytes()
{
    // Ref. RM0394 Rev 5, 3.7.8 Flash option register (FLASH_OPTR)
    
    DEBUG_PRINTF("FLASH->OPTR = 0x%08X\n", FLASH->OPTR);
    
    // Bits 0..15
    DEBUG_PRINTF("  RDP        = 0x%X\n", READ_BIT(FLASH->OPTR, FLASH_OPTR_RDP_Msk)        >> FLASH_OPTR_RDP_Pos);
    DEBUG_PRINTF("  BOR_LEV    = %u\n",   READ_BIT(FLASH->OPTR, FLASH_OPTR_BOR_LEV_Msk)    >> FLASH_OPTR_BOR_LEV_Pos);
    DEBUG_PRINTF("  nRST_STOP  = %u\n",   READ_BIT(FLASH->OPTR, FLASH_OPTR_nRST_STOP_Msk)  >> FLASH_OPTR_nRST_STOP_Pos);
    DEBUG_PRINTF("  nRST_STDBY = %u\n",   READ_BIT(FLASH->OPTR, FLASH_OPTR_nRST_STDBY_Msk) >> FLASH_OPTR_nRST_STDBY_Pos);
    DEBUG_PRINTF("  nRST_SHDW  = %u\n",   READ_BIT(FLASH->OPTR, FLASH_OPTR_nRST_SHDW_Msk)  >> FLASH_OPTR_nRST_SHDW_Pos);
    
    // Bits 16..31
    DEBUG_PRINTF("  IWDG_SW    = %u\n",   READ_BIT(FLASH->OPTR, FLASH_OPTR_IWDG_SW_Msk)    >> FLASH_OPTR_IWDG_SW_Pos);
    DEBUG_PRINTF("  IWDG_STOP  = %u\n",   READ_BIT(FLASH->OPTR, FLASH_OPTR_IWDG_STOP_Msk)  >> FLASH_OPTR_IWDG_STOP_Pos);
    DEBUG_PRINTF("  IWDG_STDBY = %u\n",   READ_BIT(FLASH->OPTR, FLASH_OPTR_IWDG_STDBY_Msk) >> FLASH_OPTR_IWDG_STDBY_Pos);
    DEBUG_PRINTF("  WWDG_SW    = %u\n",   READ_BIT(FLASH->OPTR, FLASH_OPTR_WWDG_SW_Msk)    >> FLASH_OPTR_WWDG_SW_Pos);
    DEBUG_PRINTF("  nBOOT1     = %u\n",   READ_BIT(FLASH->OPTR, FLASH_OPTR_nBOOT1_Msk)     >> FLASH_OPTR_nBOOT1_Pos);
    DEBUG_PRINTF("  SRAM2_PE   = %u\n",   READ_BIT(FLASH->OPTR, FLASH_OPTR_SRAM2_PE_Msk)   >> FLASH_OPTR_SRAM2_PE_Pos);
    DEBUG_PRINTF("  SRAM2_RST  = %u\n",   READ_BIT(FLASH->OPTR, FLASH_OPTR_SRAM2_RST_Msk)  >> FLASH_OPTR_SRAM2_RST_Pos);
    DEBUG_PRINTF("  nSWBOOT0   = %u\n",   READ_BIT(FLASH->OPTR, FLASH_OPTR_nSWBOOT0_Msk)   >> FLASH_OPTR_nSWBOOT0_Pos);
    DEBUG_PRINTF("  nBOOT0     = %u\n",   READ_BIT(FLASH->OPTR, FLASH_OPTR_nBOOT0_Msk)     >> FLASH_OPTR_nBOOT0_Pos);
}

void PrintInterrupts([[maybe_unused]] bool onlyEnabled)
{
#if defined(DEBUG_LOG)
    
    // Ref. CMSIS stm32l412xx.h
    
    static const int8_t irqArray[]
    {
        // Cortex-M4 exceptions numbers
        -14,
        -13,
        -12,
        -11,
        -10,
        -5, 
        -4, 
        -2, 
        -1,
        
        // STM32 specific interrupt numbers
        0, 
        1, 
        2, 
        3, 
        4, 
        5, 
        6, 
        7, 
        8, 
        9, 
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        23,
        24,
        25,
        26,
        27,
        28,
        31,
        32,
        33,
        34,
        35,
        36,
        37,
        38,
        39,
        40,
        41,
        54,
        56,
        57,
        58,
        59,
        60,
        64,
        65,
        66,
        67,
        68,
        69,
        70,
        71,
        72,
        73,
        77,
        80,
        81,
        82
    };
    
    DEBUG_PRINTF("Interrupts [number, enabled, priority]:\n");
    
    for (auto irq : irqArray)
    {
        const bool isEnabled = (irq >= 0) ? __NVIC_GetEnableIRQ(static_cast<IRQn_Type>(irq)) : 1;
        
        if ((onlyEnabled && isEnabled) || (not onlyEnabled))
        {
            DEBUG_PRINTF("%3d: %u, %u\n",
                    irq, isEnabled, __NVIC_GetPriority(static_cast<IRQn_Type>(irq)));
        }
    }
#endif
}

ResetReason GetResetReason()
{
    ResetReason reason = ResetReason::Unknown;
    
    // Ref. RM0394 Rev 5, 6.4.29 Control/status register (RCC_CSR)
    
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_FWRST))
    {
        reason = ResetReason::Firewall;
    }
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_OBLRST))
    {
        reason = ResetReason::OptionByteLoader;
    }
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_PINRST))
    {
        reason = ResetReason::PinReset;
    }
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_BORRST))
    {
        reason = ResetReason::BOR;
    }
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_SFTRST))
    {
        reason = ResetReason::Software;
    }
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST))
    {
        reason = ResetReason::IndependentWatchdog;
    }
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST))
    {
        reason = ResetReason::WindowWatchdog;
    }
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_LPWRRST))
    {
        reason = ResetReason::LowPower;
    }
    else
    {
        reason = ResetReason::Unknown;
    }
    
    __HAL_RCC_CLEAR_RESET_FLAGS();
    
    return reason;
}

const char * GetResetReasonName(ResetReason value)
{
    switch (value)
    {
        using enum ResetReason;
                
        case Unknown:               return "Unknown";
        case Firewall:              return "Firewall";
        case OptionByteLoader:      return "Option Byte Loader";
        case PinReset:              return "Pin Reset";
        case BOR:                   return "BOR";
        case Software:              return "Software";
        case IndependentWatchdog:   return "Independent Watchdog";
        case WindowWatchdog:        return "Window Watchdog";
        case LowPower:              return "Low Power";
        
        default:
            return "Unknown";
    }
}

}
