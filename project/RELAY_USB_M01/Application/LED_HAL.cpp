/*
 * LED driver HAL.
 */

#include "App_HAL.hpp"
#include "LED_HAL.hpp"

void LED_HAL_SetColorStatus(LED_Color color)
{
    const GPIO_PinState pinStateRed   = ((uint32_t)color & (1U << 0)) ? GPIO_PIN_RESET : GPIO_PIN_SET;
    const GPIO_PinState pinStateGreen = ((uint32_t)color & (1U << 1)) ? GPIO_PIN_RESET : GPIO_PIN_SET;
    const GPIO_PinState pinStateBlue  = ((uint32_t)color & (1U << 2)) ? GPIO_PIN_RESET : GPIO_PIN_SET;

    HAL_GPIO_WritePin(LED_STATUS_R_GPIO_Port, LED_STATUS_R_Pin, pinStateRed);
    HAL_GPIO_WritePin(LED_STATUS_G_GPIO_Port, LED_STATUS_G_Pin, pinStateGreen);
    HAL_GPIO_WritePin(LED_STATUS_B_GPIO_Port, LED_STATUS_B_Pin, pinStateBlue);
}
