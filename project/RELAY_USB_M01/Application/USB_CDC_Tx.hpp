/*
 * USB serial port Tx callback.
 */

#ifndef USB_CDC_Tx_hpp
#define USB_CDC_Tx_hpp

#include "ux_api.h"
#include "ux_device_class_cdc_acm.h"

#ifdef __cplusplus
extern "C" {
#endif

UINT USB_CDC_Tx_Callback(struct UX_SLAVE_CLASS_CDC_ACM_STRUCT *cdc_acm, UINT status, ULONG length);

#ifdef __cplusplus
}
#endif

#endif
