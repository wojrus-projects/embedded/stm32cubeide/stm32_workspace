/*
 * RELAY_USB_M01: main file.
 */

#ifndef RELAY_USB_M01_hpp
#define RELAY_USB_M01_hpp

#include <cstdint>
#include <optional>

#include "Status_Code.hpp"
#include "App_HAL_MCU.hpp"
#include "App_HAL_Board.hpp"
#include "LED.hpp"
#include "USB_Queue.hpp"

namespace RELAY_USB_M01
{

namespace LED_Color_Status
{
inline constexpr LED_Color Off               = LED_Color::Black;
inline constexpr LED_Color Reset             = LED_Color::Cyan;
inline constexpr LED_Color Exception         = LED_Color::Red;
inline constexpr LED_Color Serial_Port_Close = LED_Color::Yellow;
inline constexpr LED_Color Serial_Port_Open  = LED_Color::Green;
inline constexpr LED_Color USB_Suspend       = LED_Color::Blue;
inline constexpr LED_Color USB_Resume        = Serial_Port_Close;
}

using RelayState = uint8_t;

class RELAY_USB_M01
{
public:
    
    LED       LED_Status;
    USB_Queue USB_TxQueue;
    
    void Initialize();
    void Reset();
    void SetRelay(RelayState relayState);
    void DisableRelays();
    
private:
    
    static GPIO_PinState GetPinState(RelayState relayState, uint32_t relayIndex); 
};

}

#endif
