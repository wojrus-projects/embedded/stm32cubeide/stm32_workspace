/*
 * Application HAL (MCU layer).
 */

#ifndef App_HAL_MCU_hpp
#define App_HAL_MCU_hpp

#include <cstdint>
#include <cstddef>

#include "Status_Code.hpp"
#include "Buffer.hpp"

namespace HAL
{

/* Values from linker script. */
inline constexpr uint32_t FLASH_BASE_ADDRESS = 0x0800'0000;
inline constexpr size_t FLASH_SIZE_BYTES = 128 * 1024;
inline constexpr size_t FLASH_PAGE_SIZE_BYTES = 2 * 1024;
inline constexpr size_t FLASH_WRITE_ALIGNMENT_BYTES = 8;
inline constexpr size_t FLASH_WRITE_CHUNK_SIZE_BYTES = 8;

inline constexpr size_t CONFIG_APPLICATION_SIZE_BYTES = 1 * FLASH_PAGE_SIZE_BYTES;
inline constexpr size_t CONFIG_FACTORY_SIZE_BYTES = 1 * FLASH_PAGE_SIZE_BYTES;

/* Configurations addresses in MCU flash (from linker script). */
extern "C" const uint8_t _config_application_data_start[HAL::CONFIG_APPLICATION_SIZE_BYTES];
extern "C" const uint8_t _config_factory_data_start[HAL::CONFIG_FACTORY_SIZE_BYTES];

inline constexpr const uint8_t * CONFIG_APPLICATION_ADDRESS = &_config_application_data_start[0];
inline constexpr const uint8_t * CONFIG_FACTORY_ADDRESS = &_config_factory_data_start[0];

enum class ResetReason
{
    Unknown,
    Firewall,
    OptionByteLoader,
    PinReset,
    BOR,    
    Software,
    IndependentWatchdog,
    WindowWatchdog,
    LowPower
};

void HalInitialize();
[[noreturn]] void McuReset();
void McuSleep();
void WatchdogReset();
void UsbDisable();

StatusCode FlashErase(uint32_t flashAddress, size_t eraseSizeBytes);
StatusCode FlashWrite(uint32_t flashAddress, const void * pWriteData, size_t writeDataSizeBytes);

StatusCode ConfigurationWrite_Factory(uint32_t writeOffset, const BufferByte writeData, size_t * const pOutWriteSizeActual);
StatusCode ConfigurationRead_Factory(uint32_t readOffset, size_t readSize, const BufferByte readBuffer, size_t * const pOutReadSizeActual);
StatusCode ConfigurationErase_Factory(uint32_t eraseOffset, size_t eraseSize);

StatusCode ConfigurationWrite_Firmware(uint32_t writeOffset, const BufferByte writeData, size_t * const pOutWriteSizeActual);
StatusCode ConfigurationRead_Firmware(uint32_t readOffset, size_t readSize, const BufferByte readBuffer, size_t * const pOutReadSizeActual);
StatusCode ConfigurationErase_Firmware(uint32_t eraseOffset, size_t eraseSize);

uint32_t GetBoardSerialNumber();
void PrintOptionBytes();
void PrintInterrupts(bool onlyEnabled);
ResetReason GetResetReason();
const char * GetResetReasonName(ResetReason value);

}

#endif
