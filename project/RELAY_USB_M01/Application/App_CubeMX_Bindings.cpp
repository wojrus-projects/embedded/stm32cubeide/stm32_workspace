/*
 * Application CubeMX bindings.
 */

#include <cstdio>

#include "App_Main.hpp"
#include "App_HAL_MCU.hpp"
#include "App_CubeMX_Bindings.h"

extern "C" void App_GetUSBSerialNumber(uint8_t * const pStringBuffer, size_t bufferSize)
{
    const uint32_t boardSN = HAL::GetBoardSerialNumber();
    
    snprintf(reinterpret_cast<char *>(pStringBuffer), bufferSize, DEVICE_NAME "_%010lu", boardSN);
}
