/*
 * Debug utilities (C/C++ compatible).
 * 
 * Define symbol DEBUG_LOG to enable debugging API.
 */

#ifndef Debug_h
#define Debug_h

#include <stddef.h>
#include <stdbool.h>

#include "App_HAL_CMSIS.h"
#include "Debug_Timer.h"
#include "Debug_SystemView.h"

#ifdef __cplusplus
extern "C" {
#endif

#if defined(DEBUG_LOG)
    #define DEBUG_PRINTF(format, ...)           DebugPrintf(format __VA_OPT__(,) __VA_ARGS__)
    #define DEBUG_HEX(data, size)               DebugHex(data, size)
    #define DEBUG_GET_KEY(result, key)          result = DebugGetKey(&key)
#else
    #define DEBUG_PRINTF(format, ...)
    #define DEBUG_HEX(data, size)
    #define DEBUG_GET_KEY()
#endif

#define DEBUG_PRINTF_EXT(format, ...)                               \
    do {                                                            \
        DEBUG_PRINTF(format "\n" __VA_OPT__(,) __VA_ARGS__);        \
        DEBUG_SYSVIEW_PRINTF(format __VA_OPT__(,) __VA_ARGS__);     \
    } while (0)

typedef void (*DebugExceptionCallback)(const void * const PC);

void DebugInitialize(DebugExceptionCallback callback);
int DebugPrintf(char const * format, ...);
void DebugHex(const void * pData, size_t dataSize);
bool DebugGetKey(char * pKey);

__attribute__((__noreturn__))
void DebugTriggerFaultException(void);

__attribute__((__always_inline__))
static inline const void * DebugGetSP()
{
    void * SP;
    
    __asm__ volatile ("mov %0, lr" : "=r"(SP));
    
    return SP;
}

__attribute__((__always_inline__))
static inline const void * DebugGetLR()
{
    void * LR;
    
    __asm__ volatile ("mov %0, lr" : "=r"(LR));
    
    return LR;
}

__attribute__((__always_inline__))
static inline const void * DebugGetPC()
{
    void * PC;
    
    __asm__ volatile ("mov %0, pc" : "=r"(PC));
    
    return PC;
}

__attribute__((__noreturn__))
void DebugException(const void * const PC);

#define DEBUG_EXCEPTION()                       DebugException(DebugGetPC())
#define DEBUG_IS_DEBUGGER_ATTACHED()            ((DBGMCU->CR & 0x7UL) != 0)
#define DEBUG_BREAK()                           __asm__ volatile ("bkpt #0")

#define DEBUG_ASSERT(condition)                        \
    do {                                               \
        const void * volatile const PC = DebugGetPC(); \
                                                       \
        if (!(condition))                              \
        {                                              \
            DebugException(PC);                        \
        }                                              \
    } while (0)

#ifdef __cplusplus
}
#endif

#endif
