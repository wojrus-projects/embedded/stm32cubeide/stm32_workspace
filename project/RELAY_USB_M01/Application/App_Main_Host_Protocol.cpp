/*
 * Host protocol: application callbacks and command handlers.
 */

#include "Debug.h"
#include "App_HAL.hpp"
#include "App_Main.hpp"
#include "App_Main_Host_Protocol.hpp"
#include "Thread_Main.hpp"
#include "USB_CDC_Common.hpp"

static void HostProtocol_Callback_CommandPre(Host_Protocol::CommandCode command, Host_Protocol::PayloadBuffer& payloadBuffer);
static void HostProtocol_Callback_CommandPost(Host_Protocol::CommandCode command, StatusCode status);
static void HostProtocol_Callback_Answer(Host_Protocol::PduBuffer& answerBuffer);

static const Host_Protocol::ProtocolCallback HostProtocol_Callback
{
    .CommandPre  = HostProtocol_Callback_CommandPre,
    .CommandPost = HostProtocol_Callback_CommandPost,
    .Answer      = HostProtocol_Callback_Answer        
};

static void       HostProtocol_CommandCallback_Reset(Host_Protocol::DeviceResetMode resetMode);
static StatusCode HostProtocol_CommandCallback_MemoryWrite(Host_Protocol::MemoryID memoryID, uint32_t writeOffset, const BufferByte writeData, size_t * const pOutWriteSizeActual);
static StatusCode HostProtocol_CommandCallback_MemoryRead(Host_Protocol::MemoryID memoryID, uint32_t readOffset, size_t readSize, const BufferByte readBuffer, size_t * const pOutReadSizeActual);
static StatusCode HostProtocol_CommandCallback_MemoryErase(Host_Protocol::MemoryID memoryID, uint32_t eraseOffset, size_t eraseSize);
static StatusCode HostProtocol_CommandCallback_DigitalIOWrite(Host_Protocol::Digital_IO_State digitalOutputState);

static const Host_Protocol::CommandCallback HostProtocol_CommandCallback
{
    .Reset          = HostProtocol_CommandCallback_Reset,
    .MemoryWrite    = HostProtocol_CommandCallback_MemoryWrite,
    .MemoryRead     = HostProtocol_CommandCallback_MemoryRead,
    .MemoryErase    = HostProtocol_CommandCallback_MemoryErase,
    .DigitalIOWrite = HostProtocol_CommandCallback_DigitalIOWrite
};

const Host_Protocol::ProtocolCallback * HostProtocol_GetAppCallback()
{
    return &HostProtocol_Callback;
}

const Host_Protocol::CommandCallback * HostProtocol_GetAppCommandCallback()
{
    return &HostProtocol_CommandCallback;
}

static void HostProtocol_Callback_CommandPre([[maybe_unused]] Host_Protocol::CommandCode command,
                                             [[maybe_unused]] Host_Protocol::PayloadBuffer& payloadBuffer)
{
    DEBUG_SYSVIEW_MARK_START(Debug_SysView_MarkerID_HostCommand);
    DEBUG_SYSVIEW_PRINTF("Host: execute command: 0x%02X, payload: %u",
            command, payloadBuffer.DataSize);
    
    // Command processing require stable USB CDC connection.
    
    USB_CDC_State.Save();
}

static void HostProtocol_Callback_CommandPost([[maybe_unused]] Host_Protocol::CommandCode command,
                                              [[maybe_unused]] StatusCode status)
{
    DEBUG_SYSVIEW_MARK_STOP(Debug_SysView_MarkerID_HostCommand);
    
    //-----------------------------------------------------------------------------------------
    Mutex_Device_Lock();
    //-----------------------------------------------------------------------------------------
    
    // This lock is only to build answer PDU which require
    // atomic set Sequence Counter and append to USB Tx queue.
    //
    // Unlocked in HostProtocol_Callback_Answer().
    //
}

static void HostProtocol_Callback_Answer(Host_Protocol::PduBuffer& answerBuffer)
{
    //
    // Locked in HostProtocol_Callback_CommandPost().
    //
    
    bool sendAnswerPdu = false;
    
    DEBUG_PRINTF("Ans: %u\n", answerBuffer.DataSize);
    
    // Check USB CDC connection state.
    
    if (USB_CDC_State.IsConnected && USB_CDC_State.IsUnchanged())
    {
        // Add answer PDU to Tx queue.
        
        const bool result = RELAY_USB.USB_TxQueue.AppendBuffer(&answerBuffer);
        
        DEBUG_ASSERT(result);
        
        sendAnswerPdu = true;
    }
    else
    {
        // Drop answer PDU if USB serial port is disconnected or was reconnected.
        
        DEBUG_PRINTF("Ans: USB is off\n");
    }
    
    //-----------------------------------------------------------------------------------------
    Mutex_Device_Unlock();
    //-----------------------------------------------------------------------------------------
    
    if (not sendAnswerPdu)
    {
        return;
    }
    
    // Time window to allow start now high priority threads.
    
    //-----------------------------------------------------------------------------------------
    Mutex_Device_Lock();
    //-----------------------------------------------------------------------------------------
    
    // Try send PDU if USB connection is stable.
    
    if (USB_CDC_State.IsConnected && USB_CDC_State.IsUnchanged())
    {
        if (not USB_CDC_State.IsTxPending)
        {
            // Start send oldest PDU from queue.
            
            const auto pTxBuffer = RELAY_USB.USB_TxQueue.PeekBuffer();
            
            USB_CDC_SendPdu(pTxBuffer);
        }
        else
        {
            // USB CDC Tx is busy.
            // Postpone PDU send to future Tx callback.
            
            DEBUG_PRINTF("Ans: USB Tx busy\n");
        }
    }
    else
    {
        // USB serial port is disconnected or was reconnected.
        // USB Tx queue is empty.
        
        DEBUG_PRINTF("Ans: USB is off\n");
        
        DEBUG_ASSERT(RELAY_USB.USB_TxQueue.GetLength() == 0);
    }
    
    //-----------------------------------------------------------------------------------------
    Mutex_Device_Unlock();
    //-----------------------------------------------------------------------------------------
}

static void HostProtocol_CommandCallback_Reset([[maybe_unused]] Host_Protocol::DeviceResetMode resetMode)
{
    Thread_Main_ActivateMcuReset();
}

static StatusCode HostProtocol_CommandCallback_MemoryWrite(Host_Protocol::MemoryID memoryID,
                                                           uint32_t writeOffset,
                                                           const BufferByte writeData,
                                                           size_t * const pOutWriteSizeActual)
{
    StatusCode status = StatusCode::Failure;
    
    switch (memoryID)
    {
        using enum Host_Protocol::MemoryID;
        
        case Configuration_Factory:
            status = HAL::ConfigurationWrite_Factory(writeOffset, writeData, pOutWriteSizeActual);
            break;
            
        case Configuration_Firmware:
            status = HAL::ConfigurationWrite_Firmware(writeOffset, writeData, pOutWriteSizeActual);
            break;
            
        default:
            status = StatusCode::Invalid_Memory_Type;
    }
    
    return status;
}

static StatusCode HostProtocol_CommandCallback_MemoryRead(Host_Protocol::MemoryID memoryID,
                                                          uint32_t readOffset,
                                                          size_t readSize,
                                                          const BufferByte readBuffer,
                                                          size_t * const pOutReadSizeActual)
{
    StatusCode status = StatusCode::Failure;
    
    switch (memoryID)
    {
        using enum Host_Protocol::MemoryID;
        
        case Configuration_Factory:
            status = HAL::ConfigurationRead_Factory(readOffset, readSize, readBuffer, pOutReadSizeActual);
            break;
            
        case Configuration_Firmware:
            status = HAL::ConfigurationRead_Firmware(readOffset, readSize, readBuffer, pOutReadSizeActual);
            break;
            
        default:
            status = StatusCode::Invalid_Memory_Type;
    }
    
    return status;
}

static StatusCode HostProtocol_CommandCallback_MemoryErase(Host_Protocol::MemoryID memoryID,
                                                           uint32_t eraseOffset,
                                                           size_t eraseSize)
{
    StatusCode status = StatusCode::Failure;
    
    switch (memoryID)
    {
        using enum Host_Protocol::MemoryID;
        
        case Configuration_Factory:
            status = HAL::ConfigurationErase_Factory(eraseOffset, eraseSize);
            break;
            
        case Configuration_Firmware:
            status = HAL::ConfigurationErase_Firmware(eraseOffset, eraseSize);
            break;
            
        default:
            status = StatusCode::Invalid_Memory_Type;
    }
    
    return status; 
}

static StatusCode HostProtocol_CommandCallback_DigitalIOWrite(Host_Protocol::Digital_IO_State digitalOutputState)
{
    const auto relayState = static_cast<RELAY_USB_M01::RelayState>(digitalOutputState); 

    RELAY_USB.SetRelay(relayState);

    return StatusCode::Success;
}
