/*
 * Device information structures.
 */

#include <cstring>

#include "Debug.h"
#include "App_HAL.hpp"
#include "Device_Info.hpp"

namespace DeviceInfo
{

constexpr bool SoftwareVersion::IsValid() const
{
    return not (((Major == 0x00) && (Minor == 0x00) && (Patch == 0x00) && (Variant == 0x00)) ||
                ((Major == 0xFF) && (Minor == 0xFF) && (Patch == 0xFF) && (Variant == 0xFF)));
}

constexpr bool SoftwareDate::IsValid() const
{
    return not (((Year == 0x0000) && (Month == 0x00) && (Day == 0x00)) ||
                ((Year == 0xFFFF) && (Month == 0xFF) && (Day == 0xFF)));
}

constexpr bool SoftwareInfo::IsValid() const
{
    return Version.IsValid() && Date.IsValid();
}

constexpr bool DeviceInfo::IsValid() const
{
    return (memcmp(Magic, "INFO", sizeof(Magic)) == 0) &&
            Firmware.IsValid();
}

void DeviceInfoPrint([[maybe_unused]] const DeviceInfo * const pDeviceInfo)
{
#if defined(DEBUG_LOG)
    const auto& firmware = pDeviceInfo->Firmware;
    
    DEBUG_PRINTF("Hardware Type: %u\nHardware Variant: %u\n",
            pDeviceInfo->HardwareType,
            pDeviceInfo->HardwareVariant);
    
    DEBUG_PRINTF("Firmware Version: %u.%u.%u / %04u-%02u-%02u\n",
            firmware.Version.Major, firmware.Version.Minor, firmware.Version.Patch,
            firmware.Date.Year, firmware.Date.Month, firmware.Date.Day);
    
    DEBUG_PRINTF("Firmware Variant: %u\n", firmware.Version.Variant);    
#endif
}

}
