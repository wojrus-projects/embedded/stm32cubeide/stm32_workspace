/*
 * Host protocol: command definitions.
 */

#include "Host_Protocol_Commands.hpp"
#include "Host_Protocol_Commands_Standard.hpp"
#include "Host_Protocol_Commands_Device.hpp"

namespace Host_Protocol
{

static const CommandCallback * pCommandCallback {};

struct CommandDescriptor
{
    CommandCode code;
    CommandHandler handler;
};

static const CommandDescriptor CommandMap[] =
{
    //
    // Standard commands
    //
    
    { CommandCode::Get_Device_Info,         Command_Get_Device_Info },
    { CommandCode::Reset,                   Command_Reset },
    { CommandCode::Memory_Write,            Command_Memory_Write },
    { CommandCode::Memory_Read,             Command_Memory_Read },
    { CommandCode::Memory_Erase,            Command_Memory_Erase },
    { CommandCode::Memory_Program,          Command_Memory_Program },
    { CommandCode::Digital_IO_Write,        Command_Digital_IO_Write },
    
    //
    // Device specific commands
    //
};

CommandHandler FindCommandHandler(CommandCode commandCode)
{
    for (const auto& item : CommandMap)
    {
        if (item.code == commandCode)
        {
            return item.handler;
        }
    }
    
    return nullptr;
}

void SetCommandCallback(const CommandCallback * const pCallback)
{
    pCommandCallback = pCallback;
}

const CommandCallback * GetCommandCallback()
{
    return pCommandCallback;
}

}
