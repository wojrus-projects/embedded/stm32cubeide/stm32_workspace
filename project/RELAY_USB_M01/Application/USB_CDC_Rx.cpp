/*
 * USB serial port Rx callback.
 */

#include "Debug.h"
#include "App_Main.hpp"
#include "USB_CDC_Rx.hpp"

UINT USB_CDC_Rx_Callback([[maybe_unused]] UX_SLAVE_CLASS_CDC_ACM * cdc_acm,
                         [[maybe_unused]] UINT status,
                         UCHAR * data_pointer,
                         ULONG length)
{
    DEBUG_PRINTF_EXT("USB: Rx event: %u", length);
    
    if (not HostProtocol.IsAnswerPDU())
    {
        HostProtocol.ProcessCommandPDU({ data_pointer, length });
    }
    else
    {
        // USB is sending previous answer PDU.
        // Reject all new Rx data.
    }
    
    return UX_SUCCESS;
}
