/*
 * Device information data.
 */

#ifndef Device_Info_Data_hpp
#define Device_Info_Data_hpp

#include <cstddef>

#include "Device_Info.hpp"

/* DeviceInfo structure offset from firmware start address (from linker script). */
extern "C" const size_t _DEVICE_INFO_OFFSET;

#define DEVICE_INFO_OFFSET      reinterpret_cast<size_t>(&_DEVICE_INFO_OFFSET)

namespace DeviceInfo
{

extern const DeviceInfo DeviceInfoData;

}

#endif
