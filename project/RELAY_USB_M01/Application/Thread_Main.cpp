/*
 * Application main thread.
 */

#include "Debug.h"
#include "RTOS_Common.h"
#include "App_HAL.hpp"
#include "App_Main.hpp"
#include "Critical_Section.hpp"
#include "USB_CDC_Common.hpp"
#include "Thread_Main.hpp"

#include "ux_api.h"

static constexpr ULONG USB_EVENT_QUEUE_TIMEOUT_ms = 100;

TX_THREAD Thread_Main;
TX_MUTEX Mutex_Device;

static bool IsActivatedMcuReset = false;

#if defined(DEBUG_LOG)
static bool Debug_IsStackMonitoring = false;
#endif

static void TaskCheckUsb();
static void TaskCheckStack();
static void TaskDebug();
static void TaskResetMcu();
[[noreturn]] static void ResetMcuGracefully();

[[noreturn]] void Thread_Main_Worker([[maybe_unused]] ULONG threadArg)
{
#if defined(DEBUG_LOG)
    DEBUG_PRINTF("Thread start (main)\n"
                 "RTT Viewer: press '1' to control stack monitoring.\n");
    
    HAL::PrintInterrupts(true);
#endif
    
    // Thread main loop (period = USB_EVENT_QUEUE_TIMEOUT_ms).
    
    for (;;)
    {
        HAL::WatchdogReset();
        
        TaskResetMcu();
      
        TaskCheckUsb();
        
        TaskCheckStack();
        
        TaskDebug();
    }
}

void Thread_Main_ActivateMcuReset()
{
    IsActivatedMcuReset = true;
}

static void TaskCheckUsb()
{
    const auto usbEvent = USB_CDC_GetEventFromQueue(USB_EVENT_QUEUE_TIMEOUT_ms);
    
    if (usbEvent)
    {
        //-----------------------------------------------------------------------------------------
        Mutex_Device_Lock();
        //-----------------------------------------------------------------------------------------
        
        switch (*usbEvent)
        {
            case USB_CDC_Event::Suspend:
                DEBUG_PRINTF("USB: suspend\n");
                
                RELAY_USB.DisableRelays();
                RELAY_USB.Reset();
                RELAY_USB.LED_Status.SetColor(RELAY_USB_M01::LED_Color_Status::USB_Suspend);
                
                USB_CDC_State.IsConnected = false;
                break;
                
            case USB_CDC_Event::Resume:
                DEBUG_PRINTF("USB: resume\n");
                
                RELAY_USB.Reset();
                RELAY_USB.LED_Status.SetColor(RELAY_USB_M01::LED_Color_Status::USB_Resume);
                
                USB_CDC_State.IsConnected = false;
                break;
                
            case USB_CDC_Event::Open:
                DEBUG_PRINTF_EXT("USB: CDC open");
                
                RELAY_USB.Reset();
                RELAY_USB.LED_Status.SetColor(RELAY_USB_M01::LED_Color_Status::Serial_Port_Open);
                
                USB_CDC_State.IsConnected = true;
                break;
                
            case USB_CDC_Event::Close:
                DEBUG_PRINTF_EXT("USB: CDC close");
                
                RELAY_USB.Reset();
                RELAY_USB.LED_Status.SetColor(RELAY_USB_M01::LED_Color_Status::Serial_Port_Close);
                
                USB_CDC_State.IsConnected = false;
                break;
                
            default:
                DEBUG_EXCEPTION();
        }

        USB_CDC_State.Update();
        USB_CDC_State.IsTxPending = false;
        
        //-----------------------------------------------------------------------------------------
        Mutex_Device_Unlock();
        //-----------------------------------------------------------------------------------------
    }
}

static void TaskCheckStack()
{
#if defined(DEBUG_LOG)
    constexpr ULONG CHECK_PERIOD_s = 5;
    
    static ULONG timeOld_ticks = 0;
    static ULONG timeCounter_s = 0;
    
    if (Debug_IsStackMonitoring)
    {
        const ULONG timeNew_ticks = tx_time_get();
        const ULONG timeDelta_ticks = timeNew_ticks - timeOld_ticks;
        
        if (timeDelta_ticks > UX_MS_TO_TICK_NON_ZERO(CHECK_PERIOD_s * 1000))
        {
            timeOld_ticks = timeNew_ticks;
            timeCounter_s += CHECK_PERIOD_s;
            
            DEBUG_PRINTF("Time: %u s\n", timeCounter_s);
            RTOS_CheckStack();
        }
    }
#endif
}

static void TaskDebug()
{
#if defined(DEBUG_LOG)
    bool isKey = false;
    char key;
    
    DEBUG_GET_KEY(isKey, key);
    
    if (isKey)
    {
        switch (key)
        {
            case '1':
                Debug_IsStackMonitoring = !Debug_IsStackMonitoring;
                DEBUG_PRINTF("Stack monitoring: %u\n", Debug_IsStackMonitoring);
                break;
                
            default:
                ;
        }
    }
#endif
}

static void TaskResetMcu()
{
    constexpr ULONG COMMAND_ANSWER_DELAY_ms = 1000;
    
    if (IsActivatedMcuReset)
    {
        static bool  resetTimerIsActivated = false;
        static ULONG resetTimerStart_ticks = 0;
        
        if (resetTimerIsActivated == false)
        {
            resetTimerIsActivated = true;
            resetTimerStart_ticks = tx_time_get();
            
            DEBUG_PRINTF("Device reset is pending...\n");
        }
        else
        {
            const ULONG timeDelta_ticks = tx_time_get() - resetTimerStart_ticks;
            
            if (timeDelta_ticks > UX_MS_TO_TICK_NON_ZERO(COMMAND_ANSWER_DELAY_ms))
            {   
                ResetMcuGracefully();
            }
        }
    }
}

[[noreturn]] static void ResetMcuGracefully()
{
    constexpr ULONG USB_DISCONNECT_DELAY_ms = 2000;
    
    //-----------------------------------------------------------------------------------------
    Mutex_Device_Lock();
    //-----------------------------------------------------------------------------------------
    
    CRITICAL_SECTION_ENTER;
    
    RELAY_USB.DisableRelays();
    RELAY_USB.Reset();
    RELAY_USB.LED_Status.SetColor(RELAY_USB_M01::LED_Color_Status::Reset);
    
    HAL::WatchdogReset();
    HAL::UsbDisable();
    
    CRITICAL_SECTION_EXIT;
    
    //-----------------------------------------------------------------------------------------
    Mutex_Device_Unlock();
    //-----------------------------------------------------------------------------------------
    
    DEBUG_PRINTF("Disconnect USB...\n");
    
    RTOS_Sleep(USB_DISCONNECT_DELAY_ms);
    
    HAL::McuReset();
    
    for (;;)
    {
    }
}

void Mutex_Device_Lock()
{
    UINT status;
    
    status = tx_mutex_get(&Mutex_Device, TX_WAIT_FOREVER);
    
    DEBUG_ASSERT(status == TX_SUCCESS);
}

void Mutex_Device_Unlock()
{
    UINT status;
    
    status = tx_mutex_put(&Mutex_Device);
    
    DEBUG_ASSERT(status == TX_SUCCESS);
}
