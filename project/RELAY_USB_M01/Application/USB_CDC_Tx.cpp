/*
 * USB serial port Tx callback.
 */

#include "Debug.h"
#include "App_Main.hpp"
#include "Thread_Main.hpp"
#include "USB_CDC_Common.hpp"
#include "USB_CDC_Tx.hpp"

UINT USB_CDC_Tx_Callback([[maybe_unused]] UX_SLAVE_CLASS_CDC_ACM * cdc_acm,
                         [[maybe_unused]] UINT cdc_status,
                         [[maybe_unused]] ULONG length)
{
    Host_Protocol::PduBuffer * pTxBuffer;
    
    //-----------------------------------------------------------------------------------------
    Mutex_Device_Lock();
    //-----------------------------------------------------------------------------------------
    
    DEBUG_SYSVIEW_MARK_STOP(Debug_SysView_MarkerID_UsbTx);
    DEBUG_PRINTF_EXT("USB: Tx event: %u", length);
    
    USB_CDC_State.IsTxPending = false;
    
    // Remove most recently sent PDU from queue.
    
    pTxBuffer = RELAY_USB.USB_TxQueue.PeekBuffer();
    if (pTxBuffer)
    {
        DEBUG_PRINTF("USB: Tx: remove: %p, size: %u\n",
                pTxBuffer, pTxBuffer->DataSize);
        
        pTxBuffer->Clear();
        
        RELAY_USB.USB_TxQueue.RemoveBuffer();
    }
    else
    {
        // Tx queue is empty.
        
        goto CancelOperation;
    }
    
    // Start send next PDU from queue. 
    
    pTxBuffer = RELAY_USB.USB_TxQueue.PeekBuffer();
    if (pTxBuffer)
    {
        USB_CDC_SendPdu(pTxBuffer);
    }
    else
    {
        // Tx queue is empty.
    }

CancelOperation:
    
    //-----------------------------------------------------------------------------------------
    Mutex_Device_Unlock();
    //-----------------------------------------------------------------------------------------
    
    return UX_SUCCESS;
}
