/*
 * Generic low-level buffers.
 */

#ifndef Buffer_hpp
#define Buffer_hpp

#include <cstdint>
#include <cstddef>
#include <cstring>
#include <type_traits>

/*
 * Buffer descriptor (without internal storage).
 */
template
<
    typename ItemType = uint8_t
>
struct Buffer
{
    ItemType * Data = nullptr;
    volatile size_t DataSize = 0;
    
    void Clear()
    {
        DataSize = 0;
    }
    
    bool IsEmpty()
    {
        return (DataSize == 0);
    }
    
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
    void AppendBytes(volatile const void * pData, size_t dataSize)
    {
        static_assert(sizeof(ItemType) == 1,
            "Method 'AppendBytes' is only valid for byte buffers");
        
        const uint8_t * const pSrcData = (const uint8_t *)pData;
              uint8_t * const pDstData = (uint8_t *)&Data[DataSize];
        
        memcpy(pDstData, pSrcData, dataSize);
        
        DataSize += dataSize;
    }
#pragma GCC diagnostic pop
    
    static_assert(std::is_const_v<ItemType> == false, "'const' is not supported");
};

using BufferByte = Buffer<uint8_t>;

/*
 * Buffer with internal storage.
 */
template
<
    size_t BufferSize,
    typename ItemType = uint8_t
>
struct BufferStatic
{
    ItemType Data[BufferSize];
    volatile size_t DataSize = 0;
    
    constexpr size_t GetCapacity()
    {
        return BufferSize;
    }
    
    void Clear()
    {
        DataSize = 0;
    }
    
    bool IsEmpty()
    {
        return (DataSize == 0);
    }
    
    bool Append(ItemType item)
    {
        if (DataSize >= BufferSize)
        {
            return false;
        }
        
        Data[DataSize++] = item;
        
        return true;
    }
    
    size_t AppendBytes(volatile const void * pData, size_t dataSize)
    {
        static_assert(sizeof(ItemType) == 1,
            "Method 'AppendBytes' is only valid for byte buffers");
        
        const size_t availableBufferSize = BufferSize - DataSize;
        
        if (dataSize > availableBufferSize)
        {
            dataSize = availableBufferSize;
        }
        
        memcpy((void *)&Data[DataSize], (const void *)pData, dataSize);
        
        DataSize += dataSize;
        
        return dataSize;
    }
    
    static_assert(std::is_const_v<ItemType> == false, "'const' is not supported");
};

#endif
