/*
 * Host protocol: command definitions.
 */

#ifndef Host_Protocol_Commands_hpp
#define Host_Protocol_Commands_hpp

#include <cstdint>

#include "Host_Protocol_Definitions.hpp"
#include "Status_Code.hpp"

namespace Host_Protocol
{

enum class CommandCode : uint8_t
{
    //-------------------------------------------------------------------------
    // Standard commands (range 0-127).
    //-------------------------------------------------------------------------
    
    Standard_Commands = 0,
    
    //
    // Basic commands.
    //
            
    Get_Device_Info = Standard_Commands,
    Reset,
    
    //
    // Memory operations commands.
    //
    
    Memory_Write = Standard_Commands + 16,
    Memory_Read,
    Memory_Erase,
    Memory_Program,
    
    //
    // Digital inputs/outputs commands.
    //
    
    Digital_IO_Write = Memory_Write + 8,
    
    //-------------------------------------------------------------------------
    // Device specific commands (range 128-255).
    //-------------------------------------------------------------------------
    
    Device_Commands = 128,
    
    // Add device commands here, i.e.:
    // Custom_Command_1 = Device_Commands,
    // Custom_Command_2,
    // ...
};

enum class DeviceResetMode : uint8_t
{
    Standard    = 0,
    Flashloader = 1
};

enum class MemoryID : uint8_t
{
    Configuration_Factory,
    Configuration_Firmware
};

using Digital_IO_State = uint64_t;

struct CommandCallback
{
    using ResetCb          = void       (*)(DeviceResetMode resetMode);
    using MemoryWriteCb    = StatusCode (*)(MemoryID memoryID, uint32_t writeOffset, const BufferByte writeData, size_t * const pOutWriteSizeActual);
    using MemoryReadCb     = StatusCode (*)(MemoryID memoryID, uint32_t readOffset, size_t readSize, const BufferByte readBuffer, size_t * const pOutReadSizeActual);
    using MemoryEraseCb    = StatusCode (*)(MemoryID memoryID, uint32_t eraseOffset, size_t eraseSize);
    using DigitalIOWriteCb = StatusCode (*)(Digital_IO_State digitalOutputState);
    
    ResetCb          Reset{};
    MemoryWriteCb    MemoryWrite{};
    MemoryReadCb     MemoryRead{};
    MemoryEraseCb    MemoryErase{};
    DigitalIOWriteCb DigitalIOWrite{};
};

using CommandHandler = StatusCode (*)(PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer);

CommandHandler FindCommandHandler(CommandCode commandCode);
void SetCommandCallback(const CommandCallback * const pCallback);
const CommandCallback * GetCommandCallback();

}

#endif
