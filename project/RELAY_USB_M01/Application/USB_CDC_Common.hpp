/*
 * USB CDC common code.
 */

#ifndef USB_CDC_Common_hpp
#define USB_CDC_Common_hpp

#include <cstdint>
#include <optional>

#include "Host_Protocol.hpp"

#include "tx_api.h"
#include "ux_device_cdc_acm.h"

enum class USB_CDC_Event : ULONG
{
    Suspend,
    Resume,
    Open,
    Close
};

class USB_CDC_State
{
public:
    
    volatile bool IsConnected = false;
    volatile bool IsTxPending = false;
    
    void Reset();
    void Update();
    void Save();
    bool IsUnchanged();
    
private:
    
    uint8_t StateChangeCounter = 0;
    uint8_t StateChangeCounter_Backup = 0;
};

UINT USB_CDC_InitializeEventQueue();
std::optional<USB_CDC_Event> USB_CDC_GetEventFromQueue(ULONG timeout_ms);
void USB_CDC_SendPdu(Host_Protocol::PduBuffer * pPdu);

extern USB_CDC_State USB_CDC_State;

#endif
