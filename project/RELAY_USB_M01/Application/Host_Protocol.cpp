/*
 * Host protocol: main file.
 */

#include <cstring>

#include "Debug.h"
#include "Host_Protocol.hpp"

namespace Host_Protocol
{

void CommandParser::Reset()
{
    State = State::Header;
    FieldOffset = 0;
    PDU_Header.Clear();
    PDU_Payload.Clear();
    Calculated_Checksum = CRC16_InitializeValue;
}

Host_Protocol::Host_Protocol(const ProtocolCallback * const pCallback)
    : Callback(pCallback)
{
}

void Host_Protocol::Reset()
{
    CommandParser.Reset();
    AnswerPduBuffer.Clear();
    AnswerPayloadBuffer.Clear();
    AnswerPayloadBuffer.Data = &AnswerPduBuffer.Data[HEADER_SIZE];
    AnswerSequenceCounter = SEQUENCE_COUNTER_MIN;
}

bool Host_Protocol::ProcessCommandPDU(BufferByte commandBuffer)
{
    bool isAnswer = false;
    
    for (size_t i = 0; i < commandBuffer.DataSize; i++)
    {
        ParseCommandPDU(commandBuffer.Data[i]);
        
        switch (CommandParser.State)
        {
            case CommandParser::State::Idle:
                DEBUG_EXCEPTION();
                
            case CommandParser::State::Header:
            case CommandParser::State::Payload:
            case CommandParser::State::Checksum:
                // Continue parsing.
                break;
                
            case CommandParser::State::EndSucces:
                {
                    StatusCode commandStatus;
                    const CommandCode commandCode = (CommandCode)CommandParser.PDU_Header.Command;
                    const auto commandHandler = FindCommandHandler(commandCode);
                    
                    AnswerPayloadBuffer.Clear();
                    
                    DEBUG_PRINTF("Host: execute command: 0x%02X, payload: %u\n",
                            commandCode, CommandParser.PDU_Payload.DataSize);
                    
                    if (Callback->CommandPre)
                    {
                        Callback->CommandPre(commandCode, CommandParser.PDU_Payload);
                    }
                    
                    if (commandHandler)
                    {
                        commandStatus = commandHandler(CommandParser.PDU_Payload, AnswerPayloadBuffer);
                        
                        DEBUG_PRINTF("Host: command status: %u\n", commandStatus);
                    }
                    else
                    {
                        commandStatus = StatusCode::Invalid_Command;
                        
                        DEBUG_PRINTF("Host: unknown command: 0x%02X\n", commandCode);
                    }
                    
                    if (Callback->CommandPost)
                    {
                        Callback->CommandPost(commandCode, commandStatus);
                    }
                    
                    BuildAnswerPDU(commandCode, commandStatus);
                    
                    if (Callback->Answer)
                    {
                        DEBUG_PRINTF("Host: send answer PDU (size: %u)\n", AnswerPduBuffer.DataSize);
                        
                        Callback->Answer(AnswerPduBuffer);
                    }
                    
                    // Remaining command raw data is rejected.
                    // Host must wait for answer PDU or timeout before send new command PDU. 
                    CommandParser.Reset();
                    
                    isAnswer = true;
                    
                    goto FinishProcess;
                }
                break;
                
            case CommandParser::State::EndError:
                DEBUG_PRINTF("Host: parse error\n");
                
                // Remaining command raw data is rejected.
                CommandParser.Reset();
                
                goto FinishProcess;
                
            default:
                DEBUG_EXCEPTION();
        }
    }
    
FinishProcess:

    return isAnswer;
}

void Host_Protocol::ParseCommandPDU(uint8_t commandByte)
{
    if ((CommandParser.State == CommandParser::State::Header) ||
        (CommandParser.State == CommandParser::State::Payload))
    {
        CommandParser.Calculated_Checksum = CRC16_Calculate(CommandParser.Calculated_Checksum, &commandByte, 1);
    }
    
    switch (CommandParser.State)
    {
        case CommandParser::State::Idle:
            break;
            
        case CommandParser::State::Header:
            if (CommandParser.FieldOffset < HEADER_SIZE)
            {
                uint8_t * const pHeaderBytes = (uint8_t *)&CommandParser.PDU_Header;
                
                pHeaderBytes[CommandParser.FieldOffset] = commandByte;
                
                if (++CommandParser.FieldOffset == HEADER_SIZE)
                {
                    CommandParser.FieldOffset = 0;
                    
                    const Header& header = CommandParser.PDU_Header;
                    
                    if ((header.PDU_Type != (uint8_t)PDU_Type::Command) ||
                        (header.Status != 0) ||
                        (header.PDU_Length < PDU_SIZE_MIN) ||
                        (header.PDU_Length > PDU_SIZE_MAX))
                    {
                        DEBUG_PRINTF("Host: invalid command header\n");
                        CommandParser.State = CommandParser::State::EndError;
                    }
                    else
                    {
                        const size_t payloadSize = CommandParser.PDU_Header.PDU_Length - PDU_SIZE_MIN;
                        
                        if (payloadSize > 0)
                        {
                            CommandParser.State = CommandParser::State::Payload;
                        }
                        else
                        {
                            CommandParser.State = CommandParser::State::Checksum;
                        }
                    }
                }
                else
                {
                    // Continue header.
                }
            }
            else
            {
                CommandParser.State = CommandParser::State::EndError;
            }
            break;
            
        case CommandParser::State::Payload:
            {
                const size_t payloadSize = CommandParser.PDU_Header.PDU_Length - PDU_SIZE_MIN;
                
                if (CommandParser.FieldOffset < payloadSize)
                {
                    bool result;
                    
                    result = CommandParser.PDU_Payload.Append(commandByte);
                    DEBUG_ASSERT(result);
                   
                    if (++CommandParser.FieldOffset == payloadSize)
                    {
                        CommandParser.FieldOffset = 0;
                        CommandParser.State = CommandParser::State::Checksum;
                    }
                    else
                    {
                        // Continue payload.
                    }
                }
                else
                {
                    CommandParser.State = CommandParser::State::EndError;
                }
            }
            break;
   
        case CommandParser::State::Checksum:
            if (CommandParser.FieldOffset < CHECKSUM_SIZE)
            {
                uint8_t * const pChecksumBytes = (uint8_t *)&CommandParser.PDU_Checksum;
                
                pChecksumBytes[CommandParser.FieldOffset] = commandByte;
                
                if (++CommandParser.FieldOffset == CHECKSUM_SIZE)
                {
                    if (CommandParser.Calculated_Checksum == CommandParser.PDU_Checksum)
                    {
                        CommandParser.State = CommandParser::State::EndSucces;
                    }
                    else
                    {
                        DEBUG_PRINTF("Host: CRC mismatch: PDU: 0x%04X, calculated: 0x%04X\n",
                                CommandParser.PDU_Checksum, CommandParser.Calculated_Checksum);
                        
                        CommandParser.State = CommandParser::State::EndError;
                    }
                }
                else
                {
                    // Continue CRC16.
                }
            }
            else
            {
                CommandParser.State = CommandParser::State::EndError;
            }
            break;
            
        case CommandParser::State::EndSucces:
            break;
            
        case CommandParser::State::EndError:
            break;
            
        default:
            DEBUG_EXCEPTION();
    }
}

void Host_Protocol::BuildAnswerPDU(CommandCode command, StatusCode status)
{
    Header header;
    uint16_t crc;
    
    header.PDU_Type = (uint8_t)PDU_Type::Answer;
    header.Sequence_Counter = IncrementAnswerSequenceCounter() & SEQUENCE_COUNTER_BITMASK;
    header.PDU_Length = (uint16_t)(PDU_SIZE_MIN + AnswerPayloadBuffer.DataSize);
    header.Command = (uint8_t)command;
    header.Status = (uint8_t)status;
    
    memcpy(&AnswerPduBuffer.Data[0], &header, HEADER_SIZE);
    
    crc = CRC16_Calculate(CRC16_InitializeValue, &AnswerPduBuffer.Data[0],
                header.PDU_Length - CHECKSUM_SIZE);
    
    memcpy(&AnswerPduBuffer.Data[header.PDU_Length - CHECKSUM_SIZE], &crc, CHECKSUM_SIZE);
    
    AnswerPduBuffer.DataSize = header.PDU_Length;
}

void Host_Protocol::BuildAnswerPDU(CommandCode command,
                                   StatusCode status,
                                   uint8_t answerSequenceCounter,
                                   const BufferByte * const pPayloadBuffer,
                                   PduBuffer * const pAnswerBuffer)
{
    Header header;
    uint16_t crc;
    
    header.PDU_Type = (uint8_t)PDU_Type::Answer;
    header.Sequence_Counter = answerSequenceCounter & SEQUENCE_COUNTER_BITMASK;
    header.PDU_Length = (uint16_t)(PDU_SIZE_MIN + pPayloadBuffer->DataSize);
    header.Command = (uint8_t)command;
    header.Status = (uint8_t)status;
    
    memcpy(&pAnswerBuffer->Data[0], &header, HEADER_SIZE);
    memcpy(&pAnswerBuffer->Data[HEADER_SIZE], pPayloadBuffer->Data, pPayloadBuffer->DataSize);
    
    crc = CRC16_Calculate(CRC16_InitializeValue, &pAnswerBuffer->Data[0],
            header.PDU_Length - CHECKSUM_SIZE);
    
    memcpy(&pAnswerBuffer->Data[header.PDU_Length - CHECKSUM_SIZE], &crc, CHECKSUM_SIZE);
    
    pAnswerBuffer->DataSize = header.PDU_Length;
}

void Host_Protocol::BuildAnswerPDU(CommandCode command,
                                   StatusCode status,
                                   uint8_t answerSequenceCounter,
                                   PduBuffer * const pAnswerBuffer)
{
    Header header;
    uint16_t crc;
    
    header.PDU_Type = (uint8_t)PDU_Type::Answer;
    header.Sequence_Counter = answerSequenceCounter & SEQUENCE_COUNTER_BITMASK;
    header.PDU_Length = (uint16_t)(PDU_SIZE_MIN + pAnswerBuffer->DataSize);
    header.Command = (uint8_t)command;
    header.Status = (uint8_t)status;
    
    memcpy(&pAnswerBuffer->Data[0], &header, HEADER_SIZE);
    
    crc = CRC16_Calculate(CRC16_InitializeValue, &pAnswerBuffer->Data[0],
            header.PDU_Length - CHECKSUM_SIZE);
    
    memcpy(&pAnswerBuffer->Data[header.PDU_Length - CHECKSUM_SIZE], &crc, CHECKSUM_SIZE);
    
    pAnswerBuffer->DataSize = header.PDU_Length;
}

uint8_t Host_Protocol::IncrementAnswerSequenceCounter()
{
    const uint8_t counter = AnswerSequenceCounter;
    
    if (++AnswerSequenceCounter > SEQUENCE_COUNTER_MAX)
    {
        AnswerSequenceCounter = SEQUENCE_COUNTER_MIN;
    }
    
    return counter;
}

PduBuffer& Host_Protocol::GetAnswerBuffer()
{
    return AnswerPduBuffer;
}

void Host_Protocol::ClearAnswerBuffer()
{
    AnswerPduBuffer.Clear();
}

bool Host_Protocol::IsAnswerPDU()
{
    return (not AnswerPduBuffer.IsEmpty());
}

uint8_t Host_Protocol::GetSequenceCounterFromPDU(const PduBuffer * const pPduBuffer)
{
    return (pPduBuffer->Data[SEQUENCE_COUNTER_OFFSET] >> SEQUENCE_COUNTER_OFFSET_BITS) & SEQUENCE_COUNTER_BITMASK;
}

uint8_t Host_Protocol::GetStatusFromPDU(const PduBuffer * const pPduBuffer)
{
    return pPduBuffer->Data[STATUS_OFFSET];
}

}
