/*
 * LED colors.
 */

#ifndef LED_Color_hpp
#define LED_Color_hpp

#include <cstdint>

enum class LED_Color : uint8_t
{
    Black,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    White,
    
    Last = White
};

#endif
