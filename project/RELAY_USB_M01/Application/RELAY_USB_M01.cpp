/*
 * RELAY_USB_M01: main file.
 */

#include "Debug.h"
#include "RTOS_Common.h"
#include "RELAY_USB_M01.hpp"

namespace RELAY_USB_M01
{

void RELAY_USB_M01::Initialize()
{
    LED_Status.Initialize(LED_HAL_SetColorStatus);
    
    Reset();
}

void RELAY_USB_M01::Reset()
{
    DEBUG_PRINTF("Relay: reset\n");
    
    USB_TxQueue.Clear();
}

void RELAY_USB_M01::SetRelay(RelayState relayState)
{
    HAL_GPIO_WritePin(RELAY_1_GPIO_Port, RELAY_1_Pin, GetPinState(relayState, 0));
    HAL_GPIO_WritePin(RELAY_2_GPIO_Port, RELAY_2_Pin, GetPinState(relayState, 1));
    HAL_GPIO_WritePin(RELAY_3_GPIO_Port, RELAY_3_Pin, GetPinState(relayState, 2));
    HAL_GPIO_WritePin(RELAY_4_GPIO_Port, RELAY_4_Pin, GetPinState(relayState, 3));
    HAL_GPIO_WritePin(RELAY_5_GPIO_Port, RELAY_5_Pin, GetPinState(relayState, 4));
    HAL_GPIO_WritePin(RELAY_6_GPIO_Port, RELAY_6_Pin, GetPinState(relayState, 5));
    HAL_GPIO_WritePin(RELAY_7_GPIO_Port, RELAY_7_Pin, GetPinState(relayState, 6));
    HAL_GPIO_WritePin(RELAY_8_GPIO_Port, RELAY_8_Pin, GetPinState(relayState, 7));
}

void RELAY_USB_M01::DisableRelays()
{
    SetRelay(0x00);
}

GPIO_PinState RELAY_USB_M01::GetPinState(RelayState relayState, uint32_t relayIndex)
{
    if (relayState & (1U << relayIndex))
    {
        return GPIO_PIN_SET;
    }
    else
    {
        return GPIO_PIN_RESET;
    }
}

}
