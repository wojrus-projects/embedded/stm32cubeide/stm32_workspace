/*
 * RELAY_USB_M01 firmware.
 */

#ifndef App_Main_hpp
#define App_Main_hpp

#include "RELAY_USB_M01.hpp"
#include "Host_Protocol.hpp"

#define DEVICE_NAME     "RELAY_USB_M01"

extern RELAY_USB_M01::RELAY_USB_M01 RELAY_USB;
extern Host_Protocol::Host_Protocol HostProtocol;

#endif
