/*
 * DAQ_M01: ADC sample double-buffer.
 */

#ifndef DAQ_M01_ADC_SampleBuffer_hpp
#define DAQ_M01_ADC_SampleBuffer_hpp

#include <cstddef>
#include <expected>

#include "Status_Code.hpp"
#include "Buffer.hpp"

namespace DAQ_M01
{

template
<
    size_t BufferSizeSamples,
    typename SampleType
>
class ADC_SampleBuffer
{
public:
    
    //
    // Generic API
    //
    
    void Clear()
    {
        BufferIndex = 0;
        CapacitySamples = BufferSizeMinSamples;
        
        Buffers[0].Clear();
        Buffers[1].Clear();
    }
    
    StatusCode SetCapacity(size_t sampleCount)
    {
        if (not ((sampleCount >= BufferSizeMinSamples) && (sampleCount <= BufferSizeMaxSamples)))
        {
            return StatusCode::Invalid_Argument;
        }
        
        CapacitySamples = sampleCount;
        
        return StatusCode::Success;
    }
    
    size_t GetCapacity()
    {
        return CapacitySamples;
    }
    
    //
    // Data producer API
    //
    
    bool Producer_IsEmpty()
    {
        return GetWorkingBuffer_Producer().IsEmpty();
    }
    
    std::expected<bool, StatusCode> Producer_Advance(size_t sampleCount)
    {
        WorkingBuffer& buffer = GetWorkingBuffer_Producer();
        
        const size_t bufferFree = CapacitySamples - buffer.DataSize;
        
        if (bufferFree < sampleCount)
        {
            return std::unexpected(StatusCode::Invalid_Argument);
        }
        
        buffer.DataSize += sampleCount;
        
        const bool isBufferFull = (buffer.DataSize == CapacitySamples); 
        
        return isBufferFull;
    }
    
    void Producer_SwapBuffers()
    {
        BufferIndex++;
    }
    
    size_t Producer_GetSampleCount()
    {
        WorkingBuffer& buffer = GetWorkingBuffer_Producer();
        
        return buffer.DataSize;
    }
    
    SampleType * Producer_GetBuffer()
    {
        WorkingBuffer& buffer = GetWorkingBuffer_Producer();
        
        return &buffer.Data[buffer.DataSize];
    }
    
    //
    // Data consumer API
    //
    
    void Consumer_Clear()
    {
        GetWorkingBuffer_Consumer().Clear();
    }
    
    bool Consumer_IsEmpty()
    {
        return GetWorkingBuffer_Consumer().IsEmpty();
    }
    
    size_t Consumer_GetSampleCount()
    {
        WorkingBuffer& buffer = GetWorkingBuffer_Consumer();
        
        return buffer.DataSize;
    }
    
    StatusCode Consumer_RemoveSamples(size_t sampleToRemoveCount)
    {
        WorkingBuffer& buffer = GetWorkingBuffer_Consumer();
        
        if (sampleToRemoveCount > buffer.DataSize)
        {
            return StatusCode::Invalid_Argument;
        }
        
        buffer.DataSize -= sampleToRemoveCount;
        
        return StatusCode::Success;
    }
    
    SampleType * Consumer_GetBuffer()
    {
        WorkingBuffer& buffer = GetWorkingBuffer_Consumer();
        
        return &buffer.Data[0];
    }
    
private:
    
    static constexpr size_t BufferSizeMinSamples = 1;
    static constexpr size_t BufferSizeMaxSamples = 1024;
    
    using WorkingBuffer = BufferStatic<BufferSizeSamples, SampleType>;
    
    WorkingBuffer Buffers[2];
    size_t        BufferIndex = 0;
    size_t        CapacitySamples = BufferSizeMinSamples;
    
    WorkingBuffer& GetWorkingBuffer_Producer()
    {
        return Buffers[(BufferIndex + 0) & 0b1U];
    }
    
    WorkingBuffer& GetWorkingBuffer_Consumer()
    {
        return Buffers[(BufferIndex + 1) & 0b1U];
    }
    
    static_assert((BufferSizeSamples >= BufferSizeMinSamples) &&
                  (BufferSizeSamples <= BufferSizeMaxSamples));
};

}

#endif
