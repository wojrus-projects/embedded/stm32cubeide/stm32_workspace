/*
 * DAQ_M01: ADC calibration.
 */

#include <type_traits>
#include <cmath>

#include "Debug.h"
#include "App_Main.hpp"
#include "Thread_ADC.hpp"

namespace DAQ_M01
{

static_assert(::Calibration::IsNVMCompatible<CalibrationDataContainer>());

void ADC_Calibration::Reset()
{
    Container.Clear();
    CalibrationGain.Clear();
    
    for (size_t i = 0; i < ADC::ADC_CHANNEL_COUNT; i++)
    {
        MeasurementPositive[i].Clear();
        MeasurementNegative[i].Clear();
    }
}

void ADC_Calibration::MeasurementPoint::Clear()
{
    VoltageReference_V = 0.0f;
    VoltageMeasured_sample = 0;
}

void ADC_Calibration::CalibrationGain::Clear()
{
    Stage = CalibrationStage::Invalid;
    AdcChannelStateBitmap.reset();
    AdcPgaGain = ADC::ADC_ADS131M0X::PGA_Gain::Default;
}

StatusCode ADC_Calibration::ProcessCalibrationOffset()
{
    StatusCode statusCode = StatusCode::Failure;
    
    DEBUG_PRINTF("Cal: offset calibration begin\n");
    
    DAQ.Reset();
    
    for (ADC::ADC_ADS131M0X::PGA_Gain adcPgaGain = ADC::ADC_ADS131M0X::PGA_Gain::First; adcPgaGain <= ADC::ADC_ADS131M0X::PGA_Gain::Last; adcPgaGain++)
    {
        DEBUG_PRINTF("Cal: gain: %u (index)\n", adcPgaGain);
        
        for (size_t adcChannelIndex = 0; adcChannelIndex < ADC::ADC_CHANNEL_COUNT; adcChannelIndex++)
        {
            DEBUG_PRINTF("Cal: channel: %u\n", adcChannelIndex);
            
            // Get average sample from ADC.
            
            const ADC::Sample voltageMeasured_sample = MeasureVoltage(adcChannelIndex, adcPgaGain, 0);
            
            // Append offset coefficient to volatile calibration data.
            
            const ADC::Sample coefficientOffset_sample = -voltageMeasured_sample;
            
            DEBUG_PRINTF("Cal: set offset coefficient: %d\n", coefficientOffset_sample);
            
            statusCode = Container.Data.SetCoefficientOffset(adcChannelIndex, adcPgaGain, coefficientOffset_sample);
            
            DEBUG_ASSERT(statusCode == StatusCode::Success);
        }
    }
    
    // Save volatile calibration data to NVM.
    
    Container.Data.OffsetCalibrationState = OffsetCalibrationState::Valid;
    
    statusCode = NVM_Write();
    
    DEBUG_PRINTF("Cal: offset calibration end: %u\n", statusCode);

    return statusCode;
}

StatusCode ADC_Calibration::ProcessCalibrationGainPositive(uint8_t adcChannelStateBitmap,
                                                           uint8_t adcPgaGain_VoltPerVolt,
                                                           float voltageReferencePositive_V)
{
    StatusCode statusCode = StatusCode::Failure;
    std::expected<ADC::ADC_ADS131M0X::PGA_Gain, StatusCode> adcPgaGain;
    
    DEBUG_PRINTF("Cal: gain (+) calibration begin\n");
    
    Reset();

    CalibrationGain.AdcChannelStateBitmap = ADC_ConvertChannelStateBitmap(adcChannelStateBitmap);
    
    if (CalibrationGain.AdcChannelStateBitmap.count() == 0)
    {
        statusCode = StatusCode::Invalid_Argument;
        goto ExitError;
    }
    
    adcPgaGain = ADC_ConvertPgaGain(adcPgaGain_VoltPerVolt);
    
    if (not adcPgaGain)
    {
        statusCode = adcPgaGain.error();
        goto ExitError;
    }
    
    CalibrationGain.AdcPgaGain = adcPgaGain.value();
    
    if (not std::isfinite(voltageReferencePositive_V))
    {
        statusCode = StatusCode::Invalid_Argument;
        goto ExitError;
    }
    
    if ((voltageReferencePositive_V <= +VoltageReference_Min_ABS_V) ||
        (voltageReferencePositive_V >= +VoltageReference_Max_ABS_V))
    {
        statusCode = StatusCode::ADC_Calibration_Invalid_Input_Voltage;
        goto ExitError;
    }
    
    if (not NVM_IsOffsetCalibrationValid())
    {
        statusCode = StatusCode::ADC_Calibration_Offset_Error;
        goto ExitError;
    }
    
    Container.NVM_Read();
    
    for (size_t adcChannelIndex = 0; adcChannelIndex < ADC::ADC_CHANNEL_COUNT; adcChannelIndex++)
    {
        if (CalibrationGain.AdcChannelStateBitmap[adcChannelIndex])
        {
            DEBUG_PRINTF("Cal: channel: %u\n", adcChannelIndex);
            
            // Get offset coefficient.
            
            CalibrationCoefficient nvmCalibrationCoefficient;
            
            statusCode = NVM_ReadCalibrationCoefficient(adcChannelIndex, CalibrationGain.AdcPgaGain, nvmCalibrationCoefficient);
            
            DEBUG_ASSERT(statusCode == StatusCode::Success);
            
            // Measure average voltage from ADC.
            
            DEBUG_PRINTF("Cal: set offset coefficient: %d\n", nvmCalibrationCoefficient.Offset_sample);
            
            const ADC::Sample voltageMeasuredPositive_sample = MeasureVoltage(adcChannelIndex, CalibrationGain.AdcPgaGain, nvmCalibrationCoefficient.Offset_sample);

            statusCode = CheckVoltageMeasured(voltageMeasuredPositive_sample, VoltagePolarity::Positive);
            if (statusCode != StatusCode::Success)
            {
                goto ExitError;
            }
            
            DEBUG_PRINTF("Cal: reference: %f V, measured: %d (sample)\n",
                    (double)voltageReferencePositive_V, voltageMeasuredPositive_sample);
            
            MeasurementPositive[adcChannelIndex] =
            {
                .VoltageReference_V = voltageReferencePositive_V,
                .VoltageMeasured_sample = voltageMeasuredPositive_sample
            };
        }
    }
    
    // Save volatile calibration data to NVM.
    
    statusCode = NVM_Write();
    
    if (statusCode == StatusCode::Success)
    {
        // Allow next stage.
        
        CalibrationGain.Stage = CalibrationStage::Stage_3_Gain_Measurement_Negative;
    }
    
ExitError:

    if (statusCode != StatusCode::Success)
    {
        Reset();
    }

    DEBUG_PRINTF("Cal: gain (+) calibration end: %u\n", statusCode);
    
    return statusCode;
}

StatusCode ADC_Calibration::ProcessCalibrationGainNegative(float voltageReferenceNegative_V)
{
    StatusCode statusCode = StatusCode::Failure;
    
    DEBUG_PRINTF("Cal: gain (-) calibration begin\n");
    
    if (not std::isfinite(voltageReferenceNegative_V))
    {
        statusCode = StatusCode::Invalid_Argument;
        goto ExitError;
    }
    
    if ((voltageReferenceNegative_V >= -VoltageReference_Min_ABS_V) ||
        (voltageReferenceNegative_V <= -VoltageReference_Max_ABS_V))
    {
        statusCode = StatusCode::ADC_Calibration_Invalid_Input_Voltage;
        goto ExitError;
    }
    
    if (CalibrationGain.Stage != CalibrationStage::Stage_3_Gain_Measurement_Negative)
    {
        statusCode = StatusCode::Invalid_State;
        goto ExitError;
    }
    
    if (not NVM_IsOffsetCalibrationValid())
    {
        statusCode = StatusCode::ADC_Calibration_Offset_Error;
        goto ExitError;
    }
    
    Container.NVM_Read();
    
    for (size_t adcChannelIndex = 0; adcChannelIndex < ADC::ADC_CHANNEL_COUNT; adcChannelIndex++)
    {
        if (CalibrationGain.AdcChannelStateBitmap[adcChannelIndex])
        {
            DEBUG_PRINTF("Cal: channel: %u\n", adcChannelIndex);
            
            // Get offset coefficient.
            
            CalibrationCoefficient nvmCalibrationCoefficient;
            
            statusCode = NVM_ReadCalibrationCoefficient(adcChannelIndex, CalibrationGain.AdcPgaGain, nvmCalibrationCoefficient);
            
            DEBUG_ASSERT(statusCode == StatusCode::Success);
            
            // Measure average voltage from ADC.
            
            DEBUG_PRINTF("Cal: set offset coefficient: %d\n", nvmCalibrationCoefficient.Offset_sample);
            
            const ADC::Sample voltageMeasuredNegative_sample = MeasureVoltage(adcChannelIndex, CalibrationGain.AdcPgaGain, nvmCalibrationCoefficient.Offset_sample);

            statusCode = CheckVoltageMeasured(voltageMeasuredNegative_sample, VoltagePolarity::Negative);
            if (statusCode != StatusCode::Success)
            {
                goto ExitError;
            }
            
            DEBUG_PRINTF("Cal: reference: %f V, measured: %d (sample)\n",
                    (double)voltageReferenceNegative_V, voltageMeasuredNegative_sample);
            
            MeasurementNegative[adcChannelIndex] =
            {
                .VoltageReference_V = voltageReferenceNegative_V,
                .VoltageMeasured_sample = voltageMeasuredNegative_sample
            };
        }
    }
    
    statusCode = CalculateGainCoefficients();
    if (statusCode == StatusCode::Success)
    {
        statusCode = NVM_Write();
    }
    
ExitError:
    
    Reset();
    
    DEBUG_PRINTF("Cal: gain (-) calibration end: %u\n", statusCode);
    
    return statusCode;
}

bool ADC_Calibration::NVM_IsOffsetCalibrationValid() const
{   
    const auto pContainer = Container.NVM_GetAddress();
    
    return pContainer->Verify() &&
           (pContainer->Data.OffsetCalibrationState == OffsetCalibrationState::Valid);
}

StatusCode ADC_Calibration::NVM_ReadCalibrationCoefficient(size_t channelIndex,
                                                           ADC::ADC_ADS131M0X::PGA_Gain pgaGain,
                                                           CalibrationCoefficient& calibrationCoefficient) const
{
    const auto pContainer = Container.NVM_GetAddress();
    
    return pContainer->Data.GetCoefficient(channelIndex, pgaGain, calibrationCoefficient);
}

StatusCode ADC_Calibration::NVM_Write()
{
    Container.Build();
    
    return Container.NVM_Write();
}

ADC::Sample ADC_Calibration::MeasureVoltage(size_t adcChannelIndex,
                                            ADC::ADC_ADS131M0X::PGA_Gain adcPgaGain,
                                            ADC::Sample offsetCoefficient)
{
    StatusCode statusCode;
    bool result;
    
    // Prepare acquisition.
            
    DAQ.AcquisitionConfiguration.Reset();

    const uint8_t adcChannelStateBitmap = (uint8_t)(1U << adcChannelIndex);  
    ADC::ADC_ADS131M0X::PGA_Gain adcPgaGainArray[ADC::ADC_CHANNEL_COUNT] {};
    
    adcPgaGainArray[adcChannelIndex] = adcPgaGain;
    
    result = DAQ.AcquisitionConfiguration.Set(CalibrationSamplingFrequency_Hz,
                                              CalibrationSampleCountMax,
                                              adcChannelStateBitmap,
                                              adcPgaGainArray);
    DEBUG_ASSERT(result);
    
    // Start acquisition.
    
    statusCode = DAQ.Acquisition_Start(true);
    DEBUG_ASSERT(statusCode == StatusCode::Success);
    
    // Wait for ADC DMA completion.
    
    Event_Wait_AdcCalibration();
    
    DEBUG_ASSERT((DAQ.Sampler.State == SamplerState::Completed) &&
                 (DAQ.Sampler.Counter.Sample == CalibrationSampleCountMax) &&
                 (DAQ.Sampler.Counter.BufferOverflow == 0) &&
                 (DAQ.ADC_SampleBuffer.Consumer_GetSampleCount() == CalibrationSampleCountMax));
    
    // Correct offset error.
    
    ADC::Sample * const pSamples = DAQ.ADC_SampleBuffer.Consumer_GetBuffer();
    const size_t samplesCount = DAQ.ADC_SampleBuffer.Consumer_GetSampleCount();
    
    for (size_t i = 0; i < samplesCount; i++)
    {
        pSamples[i] += offsetCoefficient;
    }
    
    // Denoise samples by averaging.
    
    const auto averageSample = ADC_AverageSamples(pSamples, samplesCount);
    
    DEBUG_ASSERT(averageSample);
    
    return averageSample.value();
}

StatusCode ADC_Calibration::CheckVoltageMeasured(ADC::Sample voltageMeasured_sample, VoltagePolarity voltagePolarity)
{
    if (voltagePolarity == VoltagePolarity::Positive)
    {
        const ADC::Sample voltageMeasured_Min_sample = (ADC::Sample)(ADC::ADC_SAMPLE_MAX * (VoltageMeasured_Min_PercentOfRange / 100.0f));
        const ADC::Sample voltageMeasured_Max_sample = (ADC::Sample)(ADC::ADC_SAMPLE_MAX * (VoltageMeasured_Max_PercentOfRange / 100.0f));
        
        if ((voltageMeasured_sample < voltageMeasured_Min_sample) ||
            (voltageMeasured_sample > voltageMeasured_Max_sample))
        {
            return StatusCode::ADC_Calibration_Invalid_Input_Voltage;
        }
    }
    else
    {
        const ADC::Sample voltageMeasured_Min_sample = (ADC::Sample)(ADC::ADC_SAMPLE_MIN * (VoltageMeasured_Min_PercentOfRange / 100.0f));
        const ADC::Sample voltageMeasured_Max_sample = (ADC::Sample)(ADC::ADC_SAMPLE_MIN * (VoltageMeasured_Max_PercentOfRange / 100.0f));
        
        if ((voltageMeasured_sample > voltageMeasured_Min_sample) ||
            (voltageMeasured_sample < voltageMeasured_Max_sample))
        {
            return StatusCode::ADC_Calibration_Invalid_Input_Voltage;
        }
    }
    
    return StatusCode::Success;
}

StatusCode ADC_Calibration::CalculateGainCoefficients()
{
    for (size_t adcChannelIndex = 0; adcChannelIndex < ADC::ADC_CHANNEL_COUNT; adcChannelIndex++)
    {
        if (CalibrationGain.AdcChannelStateBitmap[adcChannelIndex])
        {
            DEBUG_PRINTF("Cal: channel: %u\n", adcChannelIndex);
            
            const auto& measurementPositive = MeasurementPositive[adcChannelIndex];
            const auto& measurementNegative = MeasurementNegative[adcChannelIndex];
            
            DEBUG_PRINTF("Cal: ref+: %f V, ref-: %f V, meas+: %d, meas-: %d sample\n", 
                    (double)measurementPositive.VoltageReference_V,
                    (double)measurementNegative.VoltageReference_V,
                    measurementPositive.VoltageMeasured_sample,
                    measurementNegative.VoltageMeasured_sample);
            
            const float       voltageReferenceDelta_V     = measurementPositive.VoltageReference_V     - measurementNegative.VoltageReference_V;
            const ADC::Sample voltageMeasuredDelta_sample = measurementPositive.VoltageMeasured_sample - measurementNegative.VoltageMeasured_sample;
            
            if (not ((voltageReferenceDelta_V > 0.0f) && (voltageMeasuredDelta_sample > 0)))
            {
                return StatusCode::ADC_Calibration_Invalid_Input_Voltage;
            }
            
            const float coefficientGain_VoltPerSample = voltageReferenceDelta_V / (float)voltageMeasuredDelta_sample;
            
            DEBUG_PRINTF("Cal: set gain coefficient: %f\n", (double)coefficientGain_VoltPerSample);
            
            const StatusCode statusCode = Container.Data.SetCoefficientGain(adcChannelIndex,
                                                                            CalibrationGain.AdcPgaGain,
                                                                            coefficientGain_VoltPerSample);
            DEBUG_ASSERT(statusCode == StatusCode::Success);
        }
    }
    
    return StatusCode::Success;
}

}
