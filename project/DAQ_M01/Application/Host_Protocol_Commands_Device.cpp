/*
 * Host protocol: device commands.
 */

#include "Debug.h"
#include "RTOS_Common.h"
#include "App_Main.hpp"
#include "Utilities.hpp"
#include "Critical_Section.hpp"
#include "Thread_Main.hpp"
#include "Host_Protocol_Commands.hpp"

namespace Host_Protocol
{

//                  |                Generic parameters                    |            Device specific parameters             |
// Command payload: [ADC_SAMPLING_FREQUENCY_Hz 4B][ADC_SAMPLE_COUNT_MAX 8B]|[ADC_CHANNEL_STATE_BITMAP 1B][ADC_PGA_GAIN 6B (V/V)]
//
//  Answer payload: [GAIN_COEFFICIENT 4B (float32, V/sample)] x CHANNEL_ENABLED
//
StatusCode Command_Acquisition_Start(PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer)
{
    StatusCode statusCode = StatusCode::Failure;
    bool samplerIsBusy;
    bool result;
    
    const uint8_t& field_adcSamplingFrequency_Hz = commandPayloadBuffer.Data[0];
    const uint8_t& field_adcsampleCountMax       = commandPayloadBuffer.Data[4];
    const uint8_t& field_adcChannelStateBitmap   = commandPayloadBuffer.Data[12];
    const uint8_t& field_adcPgaGainArray         = commandPayloadBuffer.Data[13];
    
    if (commandPayloadBuffer.DataSize != (4 + 8 + 1 + 6))
    {
        return StatusCode::Invalid_Argument;
    }
   
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Lock();
    //-----------------------------------------------------------------------------------------
    
    //
    // ADC acquisition process must be stopped.
    //
    
    CRITICAL_SECTION_ENTER;
    samplerIsBusy = DAQ.Sampler.IsBusy();
    CRITICAL_SECTION_EXIT;
    
    if (samplerIsBusy)
    {
        statusCode = StatusCode::Busy;
        goto ExitError;
    }
    
    //
    // All ADC data from previous acquisition must be sent to host.
    //
    
    if (not DAQ.PduBuffersAreEmpty())
    {
        statusCode = StatusCode::Invalid_State;
        goto ExitError;
    }
    
    //
    // Set ADC configuration.
    //
    
    DAQ.Reset();
    
    ADC::ADC_ADS131M0X::PGA_Gain adcPgaGainArray[ADC::ADC_CHANNEL_COUNT];
    
    statusCode = DAQ_M01::ADC_ConvertPgaGainArray(&field_adcPgaGainArray, adcPgaGainArray);
    if (statusCode != StatusCode::Success)
    {
        goto ExitError;
    }
    
    result = DAQ.AcquisitionConfiguration.Set(Utilities::FromBytes<uint32_t>(&field_adcSamplingFrequency_Hz),
                                              Utilities::FromBytes<uint64_t>(&field_adcsampleCountMax),
                                              field_adcChannelStateBitmap,
                                              adcPgaGainArray);
    if (not result)
    {
        statusCode = StatusCode::Invalid_Argument;
        goto ExitError;
    }
    
    //
    // Start acquisition.
    //
    
    statusCode = DAQ.Acquisition_Start(false);
    DEBUG_ASSERT(statusCode == StatusCode::Success);
    
    //
    // Return gain coefficients for enabled channels only.
    // Coefficients are used by host application to convert raw ADC samples to voltage.
    //
    
    for (size_t i = 0; i < DAQ.AcquisitionCalibration.Coefficients.DataSize; i++)
    {
        const auto& coefficient = DAQ.AcquisitionCalibration.Coefficients.Data[i];
        
        answerPayloadBuffer.AppendBytes(&coefficient.Gain_VoltPerSample, sizeof(coefficient.Gain_VoltPerSample));
    }
    
ExitError:
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Unlock();
    //-----------------------------------------------------------------------------------------
    
    return statusCode;
}

//
// Command payload: -
//
//  Answer payload: -
//
StatusCode Command_Acquisition_Stop(PayloadBuffer& commandPayloadBuffer, [[maybe_unused]] BufferByte& answerPayloadBuffer)
{
    if (commandPayloadBuffer.DataSize != 0)
    {
        return StatusCode::Invalid_Argument;
    }
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Lock();
    //-----------------------------------------------------------------------------------------
    
    DAQ.Acquisition_Stop();
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Unlock();
    //-----------------------------------------------------------------------------------------
    
    return StatusCode::Success;
}

//
// Command payload: -
//
//  Answer payload: [DAQ_STATUS 1B][COUNTER_SAMPLE 8B][COUNTER_BUFFER_OVERFLOW 8B]
//
StatusCode Command_Acquisition_Get_Status(PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer)
{
    union DAQ_Status
    {
        uint8_t Byte = 0;
        
        struct DAQ_StatusFlag
        {
            uint8_t CalibrationIsValid : 1;
            uint8_t AcquisitionState   : 3;
            uint8_t Reserved           : 4;
        } Flag;
    };
    
    static_assert(sizeof(DAQ_Status) == 1);
    
    DAQ_Status daqStatus;
    DAQ_M01::Sampler daqSampler;
    
    if (commandPayloadBuffer.DataSize != 0)
    {
        return StatusCode::Invalid_Argument;
    }
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Lock();
    //-----------------------------------------------------------------------------------------
    
    CRITICAL_SECTION_ENTER;    
    daqSampler = DAQ.Sampler;
    CRITICAL_SECTION_EXIT;
    
    daqStatus.Flag.CalibrationIsValid = DAQ.ADC_Calibration.Container.NVM_Verify();
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Unlock();
    //-----------------------------------------------------------------------------------------
    
    daqStatus.Flag.AcquisitionState = (uint8_t)daqSampler.State & 0b111;
    
    answerPayloadBuffer.Clear();
    answerPayloadBuffer.AppendBytes(&daqStatus.Byte,                    sizeof(daqStatus.Byte));
    answerPayloadBuffer.AppendBytes(&daqSampler.Counter.Sample,         sizeof(daqSampler.Counter.Sample));
    answerPayloadBuffer.AppendBytes(&daqSampler.Counter.BufferOverflow, sizeof(daqSampler.Counter.BufferOverflow));
    
    return StatusCode::Success;
}

//
// Command payload: Case 1 = [CALIBRATION_STAGE 1B]                                                                                          if CALIBRATION_STAGE = Stage_1_Offset
//                  Case 2 = [CALIBRATION_STAGE 1B][ADC_CHANNEL_STATE_BITMAP 1B][ADC_PGA_GAIN 1B (V/V)][VIN+ 4B (float32, V, differential)]  if CALIBRATION_STAGE = Stage_2_Gain_Measurement_Positive
//                  Case 3 = [CALIBRATION_STAGE 1B]                                                    [VIN- 4B (float32, V, differential)]  if CALIBRATION_STAGE = Stage_3_Gain_Measurement_Negative
//
//  Answer payload: -
//
StatusCode Command_Calibration_Process(PayloadBuffer& commandPayloadBuffer, [[maybe_unused]] BufferByte& answerPayloadBuffer)
{
    StatusCode statusCode = StatusCode::Failure;
    DAQ_M01::CalibrationStage calibrationStage;
    bool samplerIsBusy;
    
    //
    // Minimum: CALIBRATION_STAGE.
    //
    
    if (commandPayloadBuffer.DataSize < 1)
    {
        return StatusCode::Invalid_Argument;
    }
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Lock();
    //-----------------------------------------------------------------------------------------
    
    //
    // ADC acquisition process must be stopped.
    //
    
    CRITICAL_SECTION_ENTER;
    samplerIsBusy = DAQ.Sampler.IsBusy();
    CRITICAL_SECTION_EXIT;
    
    if (samplerIsBusy)
    {
        statusCode = StatusCode::Invalid_State;
        goto CancelOperation;
    }
    
    //
    // All ADC data from previous acquisition must be sent to host.
    //
    
    if (not DAQ.PduBuffersAreEmpty())
    {
        statusCode = StatusCode::Invalid_State;
        goto CancelOperation;
    }
    
    //
    // Process calibration.
    //
    
    calibrationStage = (DAQ_M01::CalibrationStage)commandPayloadBuffer.Data[0];
    
    switch (calibrationStage)
    {
        case DAQ_M01::CalibrationStage::Stage_1_Offset:
            {
                if (commandPayloadBuffer.DataSize != 1)
                {
                    statusCode = StatusCode::Invalid_Argument;
                    goto CancelOperation;
                }
                
                statusCode = DAQ.ADC_Calibration.ProcessCalibrationOffset();
            }
            break;
            
        case DAQ_M01::CalibrationStage::Stage_2_Gain_Measurement_Positive:
            {
                if (commandPayloadBuffer.DataSize != (1 + 1 + 1 + 4))
                {
                    statusCode = StatusCode::Invalid_Argument;
                    goto CancelOperation;
                }
                
                const uint8_t adcChannelStateBitmap = commandPayloadBuffer.Data[1];
                const uint8_t adcPgaGain_VoltPerVolt = commandPayloadBuffer.Data[2];
                const auto voltageReferencePositive_V = Utilities::FromBytes<float>(&commandPayloadBuffer.Data[3]);
                
                statusCode = DAQ.ADC_Calibration.ProcessCalibrationGainPositive(adcChannelStateBitmap,
                                                                                adcPgaGain_VoltPerVolt,
                                                                                voltageReferencePositive_V);
            }
            break;
            
        case DAQ_M01::CalibrationStage::Stage_3_Gain_Measurement_Negative:
            {   
                if (commandPayloadBuffer.DataSize != (1 + 4))
                {
                    statusCode = StatusCode::Invalid_Argument;
                    goto CancelOperation;
                }
                
                const auto voltageReferenceNegative_V = Utilities::FromBytes<float>(&commandPayloadBuffer.Data[1]);
                
                statusCode = DAQ.ADC_Calibration.ProcessCalibrationGainNegative(voltageReferenceNegative_V);
            }
            break;
        
        case DAQ_M01::CalibrationStage::Invalid:
        
        default:
            statusCode = StatusCode::Invalid_Argument;
            goto CancelOperation;
    }
    
CancelOperation:
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Unlock();
    //-----------------------------------------------------------------------------------------

    return statusCode;
}

//
// Command payload: -
//
//  Answer payload: [DAQ_CALIBRATION_DATA nB]
//
StatusCode Command_Calibration_Data_Read(PayloadBuffer& commandPayloadBuffer, BufferByte& answerPayloadBuffer)
{
    static_assert(sizeof(DAQ_M01::CalibrationDataContainer) <= PAYLOAD_SIZE_MAX);
    
    if (commandPayloadBuffer.DataSize != 0)
    {
        return StatusCode::Invalid_Argument;
    }
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Lock();
    //-----------------------------------------------------------------------------------------
    
    memcpy(&answerPayloadBuffer.Data[0], DAQ.ADC_Calibration.Container.NVM_GetAddress(),
           sizeof(DAQ_M01::CalibrationDataContainer));
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Unlock();
    //-----------------------------------------------------------------------------------------
    
    answerPayloadBuffer.DataSize = sizeof(DAQ_M01::CalibrationDataContainer);
    
    return StatusCode::Success;
}

//
// Command payload: [DAQ_CALIBRATION_DATA nB]
//
//  Answer payload: -
//
StatusCode Command_Calibration_Data_Write(PayloadBuffer& commandPayloadBuffer, [[maybe_unused]] BufferByte& answerPayloadBuffer)
{
    StatusCode statusCode = StatusCode::Failure;
    bool samplerIsBusy;
    
    static_assert(sizeof(DAQ_M01::CalibrationDataContainer) <= PAYLOAD_SIZE_MAX);
    
    if (commandPayloadBuffer.DataSize != sizeof(DAQ_M01::CalibrationDataContainer))
    {
        return StatusCode::Invalid_Argument;
    }
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Lock();
    //-----------------------------------------------------------------------------------------
    
    const auto pCalibrationDataContainer = reinterpret_cast<const DAQ_M01::CalibrationDataContainer *>(&commandPayloadBuffer.Data[0]);
    
    //
    // ADC acquisition process must be stopped.
    //
    
    CRITICAL_SECTION_ENTER;
    samplerIsBusy = DAQ.Sampler.IsBusy();
    CRITICAL_SECTION_EXIT;
    
    if (samplerIsBusy)
    {
        statusCode = StatusCode::Invalid_State;
        goto ExitError;
    }
    
    //
    // Write calibration data to NVM.
    //
    
    statusCode = pCalibrationDataContainer->NVM_Write();
    if (statusCode != StatusCode::Success)
    {
        goto ExitError;
    }
    
    //
    // Restart device with new calibration.
    //
    
    DAQ.Reset();
    
    statusCode = StatusCode::Success;
    
ExitError:
    
    //-----------------------------------------------------------------------------------------
    Mutex_DAQ_Unlock();
    //-----------------------------------------------------------------------------------------
    
    return statusCode;
}

}
