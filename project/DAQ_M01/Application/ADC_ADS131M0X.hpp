/*
 * ADC Texas Instruments ADS131M0X
 */

#ifndef ADC_ADS131M0X_hpp
#define ADC_ADS131M0X_hpp

#include <cstdint>
#include <cstddef>
#include <bitset>

#include "Status_Code.hpp"
#include "Utilities.hpp"
#include "ADC_ADS131M0X_Registers.h"
#include "ADC_ADS131M0X_HAL.hpp"
#include "ADC_ADS131M0X_Definitions.hpp"

namespace ADC
{

class ADC_ADS131M0X
{
public:
    
    enum class PGA_Gain : uint8_t
    {
        Gain_1,
        Gain_2,
        Gain_4,
        Gain_8,
        Gain_16,
        Gain_32,
        Gain_64,
        Gain_128,
        
        First = Gain_1,
        Last = Gain_128,
        
        Default = First
    };
    
    enum class OSR : uint8_t
    {
        OSR_128,
        OSR_256,
        OSR_512,
        OSR_1024,
        OSR_2048,
        OSR_4096,
        OSR_8192,
        OSR_16256,
        
        First = OSR_128,
        Last = OSR_16256,
        
        Default = First
    };
    
    using ChannelStateBitmap = std::bitset<ADC::ADC_CHANNEL_COUNT>;
    
    ADC_ADS131M0X(ADC_ADS131M0X_HAL& hal);
    
    // Initialization API
    
    StatusCode Initialize(ADC_DataReady_Callback callback);
    void PowerOnResetDelay();
    void HardwareReset();
    StatusCode VerifyReset();
    StatusCode CheckID();
    
    // Configuration API
    
    StatusCode SetModeDefault();
    StatusCode SetPGAGain(const PGA_Gain gainArray[]);
    StatusCode SetOversamplingRatio(OSR osr);
    StatusCode SetSamplingFrequency(uint32_t samplingFrequency_Hz);
    StatusCode EnableChannel(ChannelStateBitmap channelStateBitmap);
    
    // Control API
    
    void Start();
    void Stop();
    
    // Commands API
    
    StatusCode Command_Reset(uint16_t& response);
    StatusCode Command_Null(uint16_t& response);
    StatusCode Command_RegisterRead(uint32_t registerCount, uint32_t registerAddress, uint16_t registerArray[]);
    StatusCode Command_RegisterWrite(uint32_t registerCount, uint32_t registerAddress, const uint16_t registerArray[]);
    
    // Channel data API
    
    StatusCode ChannelData_ReadBlocking(uint16_t& response, Sample * pSampleBuffer, ChannelStateBitmap channelStateBitmap);
    StatusCode ChannelData_ReadAsyncStart();
    void ChannelData_Copy(uint16_t& response, Sample * pSampleBuffer, ChannelStateBitmap channelStateBitmap);
    
    // Calibration API
    
    StatusCode Calibration_WriteOffset(uint32_t channelIndex, uint16_t LSB, uint16_t MSB);
    StatusCode Calibration_WriteGain(uint32_t channelIndex, uint16_t LSB, uint16_t MSB);
    
    ADC_ADS131M0X_HAL& HAL;
    
private:
    
    static constexpr uint32_t t_POR_ms = 2;         // Power-on-reset time, 250 us typ.
    static constexpr uint32_t tw_RSL_ms = 4;        // Pulse duration, SYNC/RESET low to generate device reset, 2048 x t_CLKIN min. at VLP mode (f_CLKIN=2.048 MHz).
    static constexpr uint32_t t_REGACQ_ms = 2;      // Register default acquisition time, 5 us typ.
    
    static constexpr size_t SPI_FRAME_LENGTH_MAX = 64;
    static constexpr size_t SPI_WORD_LENGTH_DEFAULT = 3;
    
    size_t SpiWordLengthBytes = SPI_WORD_LENGTH_DEFAULT;
    uint8_t SpiTxFrame[SPI_FRAME_LENGTH_MAX];
    uint8_t SpiRxFrame[SPI_FRAME_LENGTH_MAX];
    
    StatusCode RegisterWriteVerify(uint32_t registerAddress, uint16_t writeValue);
    void U16ToWord(uint16_t value, uint32_t wordIndex);
    uint16_t U16FromWord(uint32_t wordIndex);
};

DEFINE_ENUM_CLASS_OPERATORS(ADC_ADS131M0X::PGA_Gain);

};

#endif
