/*
 * Host protocol generic definitions.
 */

#ifndef Host_Protocol_Definitions_hpp
#define Host_Protocol_Definitions_hpp

#include <cstdint>
#include <cstddef>

#include "Buffer.hpp"

namespace Host_Protocol
{

enum class PDU_Type : uint8_t
{
    Command,
    Answer,
    Reserved_1,
    Reserved_2
};

struct [[gnu::packed]] Header
{
    uint8_t  PDU_Type         : 2  = 0;     // = enum PDU_Type
    uint8_t  Sequence_Counter : 6  = 0;     // Range: 0 ... 63
    uint16_t PDU_Length       : 16 = 0;     // Range: PDU_SIZE_MIN ... PDU_SIZE_MAX
    uint8_t  Command          : 8  = 0;     // = enum CommandCode
    uint8_t  Status           : 8  = 0;     // = 0               if PDU_Type == Command
                                            // = enum StatusCode if PDU_Type == Answer
    void Clear()
    {
        PDU_Type = 0;
        Sequence_Counter = 0;
        PDU_Length = 0;
        Command = 0;
        Status = 0;
    }
};

inline constexpr size_t HEADER_SIZE = sizeof(Header);
inline constexpr size_t CHECKSUM_SIZE = sizeof(uint16_t);
inline constexpr size_t PDU_SIZE_MIN = HEADER_SIZE + CHECKSUM_SIZE;
inline constexpr size_t PDU_SIZE_MAX = 1024;
inline constexpr size_t PAYLOAD_SIZE_MIN = 0;
inline constexpr size_t PAYLOAD_SIZE_MAX = PDU_SIZE_MAX - PDU_SIZE_MIN;
inline constexpr size_t PAYLOAD_OFFSET = HEADER_SIZE;

inline constexpr size_t   SEQUENCE_COUNTER_OFFSET = 0;
inline constexpr size_t   SEQUENCE_COUNTER_SIZE_BITS = 6;
inline constexpr uint32_t SEQUENCE_COUNTER_OFFSET_BITS = 2;
inline constexpr uint32_t SEQUENCE_COUNTER_BITMASK = (1 << SEQUENCE_COUNTER_SIZE_BITS) - 1;
inline constexpr uint32_t SEQUENCE_COUNTER_MIN = 0;
inline constexpr uint32_t SEQUENCE_COUNTER_MAX = (1 << SEQUENCE_COUNTER_SIZE_BITS) - 1;

inline constexpr size_t STATUS_OFFSET = 4;

using PduBuffer = BufferStatic<PDU_SIZE_MAX>;
using PayloadBuffer = BufferStatic<PAYLOAD_SIZE_MAX>;

static_assert(sizeof(Header) == 5);
static_assert(PDU_SIZE_MAX > PDU_SIZE_MIN);

}

#endif
