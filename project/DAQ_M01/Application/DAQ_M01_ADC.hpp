/*
 * DAQ_M01: ADC utilities.
 */

#ifndef DAQ_M01_ADC_hpp
#define DAQ_M01_ADC_hpp

#include <cstdint>
#include <expected>

#include "Status_Code.hpp"
#include "ADC_ADS131M0X.hpp"
#include "ADC_ADS131M0X_HAL_STM32.hpp"

namespace DAQ_M01
{

ADC::ADC_ADS131M0X::ChannelStateBitmap ADC_ConvertChannelStateBitmap(uint8_t channelStateBitmap);
std::expected<ADC::ADC_ADS131M0X::PGA_Gain, StatusCode> ADC_ConvertPgaGain(uint8_t gain_VoltPerVolt);
StatusCode ADC_ConvertPgaGainArray(const uint8_t inArray_VoltPerVolt[],
                                   ADC::ADC_ADS131M0X::PGA_Gain outArray[]);

constexpr void ADC_SampleToBytes(ADC::Sample sample, uint8_t * pOutBytes)
{
    *pOutBytes++ = (uint8_t)(sample >> 0);
    *pOutBytes++ = (uint8_t)(sample >> 8);
    *pOutBytes   = (uint8_t)(sample >> 16);
};

}

#endif
