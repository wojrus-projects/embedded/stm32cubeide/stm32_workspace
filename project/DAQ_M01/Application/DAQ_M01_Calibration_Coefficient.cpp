/*
 * DAQ_M01: ADC calibration coefficient.
 */

#include "DAQ_M01_Calibration_Coefficient.hpp"

namespace DAQ_M01
{

void CalibrationCoefficient::Clear()
{
    Offset_sample        = 0;
    Gain_VoltPerSample   = 0.0f;
    GainCalibrationState = GainCalibrationState::Invalid;
}

StatusCode CalibrationCoefficient::SetNominal(ADC::ADC_ADS131M0X::PGA_Gain pgaGain)
{
    if (pgaGain > ADC::ADC_ADS131M0X::PGA_Gain::Last)
    {
        return StatusCode::Invalid_Argument;
    }
    
    Offset_sample = 0;
    
    const float adcRange_V = ADC::ADC_VoltageRanges[static_cast<size_t>(pgaGain)];
    const float adcFullScaleRange_V = adcRange_V - (-adcRange_V);
    
    Gain_VoltPerSample = adcFullScaleRange_V / (float)ADC::ADC_TOTAL_COUNTS;
    
    GainCalibrationState = GainCalibrationState::Valid;
    
    return StatusCode::Success;
}

bool CalibrationCoefficient::IsValid() const
{
    return (GainCalibrationState == GainCalibrationState::Valid);
}

}
