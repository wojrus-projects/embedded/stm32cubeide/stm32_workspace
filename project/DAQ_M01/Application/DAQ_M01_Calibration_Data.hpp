/*
 * DAQ_M01: ADC calibration data.
 */

#ifndef DAQ_M01_Calibration_Data_hpp
#define DAQ_M01_Calibration_Data_hpp

#include <cstdint>
#include <cstddef>

#include "Status_Code.hpp"
#include "DAQ_M01_ADC.hpp"
#include "DAQ_M01_Calibration_Coefficient.hpp"

namespace DAQ_M01
{

enum class OffsetCalibrationState : uint8_t
{
    Invalid = 0,
    Valid   = 1
};

using OffsetCalibrationState_t = OffsetCalibrationState;

struct [[gnu::packed]] CalibrationData
{   
    CalibrationCoefficient   CalibrationCoefficients[ADC::ADC_CHANNEL_COUNT][ADC::ADC_GAIN_COUNT];
    OffsetCalibrationState_t OffsetCalibrationState = OffsetCalibrationState::Invalid;
    
    void Clear();
    
    StatusCode GetCoefficient      (size_t channelIndex, ADC::ADC_ADS131M0X::PGA_Gain pgaGain, CalibrationCoefficient& calibrationCoefficient) const;
    StatusCode SetCoefficientOffset(size_t channelIndex, ADC::ADC_ADS131M0X::PGA_Gain pgaGain, ADC::Sample coefficientOffset_sample);
    StatusCode SetCoefficientGain  (size_t channelIndex, ADC::ADC_ADS131M0X::PGA_Gain pgaGain, float coefficientGain_VoltPerSample);
};

}

#endif
