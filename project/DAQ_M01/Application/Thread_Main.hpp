/*
 * Application main thread.
 */

#ifndef Thread_Main_hpp
#define Thread_Main_hpp

#include "tx_api.h"

[[noreturn]] void Thread_Main_Worker(ULONG threadArg);
void Thread_Main_ActivateMcuReset();

void Mutex_DAQ_Lock();
void Mutex_DAQ_Unlock();

extern TX_THREAD Thread_Main;
extern TX_MUTEX Mutex_DAQ;

#endif
