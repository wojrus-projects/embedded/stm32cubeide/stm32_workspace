/*
 * DAQ_M01: ADC calibration coefficient.
 */

#ifndef DAQ_M01_Calibration_Coefficient_hpp
#define DAQ_M01_Calibration_Coefficient_hpp

#include "Status_Code.hpp"
#include "DAQ_M01_ADC.hpp"

namespace DAQ_M01
{

enum class GainCalibrationState : uint8_t
{
    Invalid = 0,
    Valid   = 1
};

using GainCalibrationState_t = GainCalibrationState;

struct [[gnu::packed]] CalibrationCoefficient
{
    ADC::Sample            Offset_sample        = 0;
    float                  Gain_VoltPerSample   = 0.0f;
    GainCalibrationState_t GainCalibrationState = GainCalibrationState::Invalid;
    
    void       Clear();
    StatusCode SetNominal(ADC::ADC_ADS131M0X::PGA_Gain pgaGain);
    bool       IsValid() const;
};

}

#endif
