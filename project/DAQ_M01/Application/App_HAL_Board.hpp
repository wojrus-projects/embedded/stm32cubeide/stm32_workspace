/*
 * Application HAL (board layer).
 */

#ifndef App_HAL_Board_hpp
#define App_HAL_Board_hpp

#include <cstdint>

#include "App_HAL_CubeMX.h"
#include "Interrupt_Priority.hpp"

namespace HAL::Board::ADC
{

inline constexpr auto& SPI = hspi1;

inline constexpr uint32_t  DRDY_EXTI_Line         = EXTI_LINE_4;
inline constexpr uint32_t  DRDY_EXTI_GPIOSel      = EXTI_GPIOC;
inline constexpr IRQn_Type DRDY_EXTI_IRQn         = EXTI4_IRQn;
inline constexpr uint32_t  DRDY_EXTI_IRQ_Priority = (uint32_t)InterruptPriority::ADC_DRDY_EXTI4;

void ADC_Callback_DataReady();

}

#endif
