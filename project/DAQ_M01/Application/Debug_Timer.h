/*
 * Timer for code profiling.
 */

#ifndef Debug_Timer_h
#define Debug_Timer_h

#include <stdint.h>

#include "App_HAL_CMSIS.h"

#ifdef __cplusplus
extern "C" {
#endif

#define DEBUG_TIMER_INITIALIZE()                            \
    do {                                                    \
        CoreDebug->DEMCR = CoreDebug_DEMCR_TRCENA_Msk;      \
        __DSB();                                            \
        ITM->LAR = 0xC5ACCE55;                              \
        __DSB();                                            \
        DWT->CTRL &= ~DWT_CTRL_CYCCNTENA_Msk;               \
        DWT->CYCCNT = 0;                                    \
        DWT->CTRL = DWT_CTRL_CYCCNTENA_Msk;                 \
    } while(0)

#define DEBUG_TIMER_GET()                                   ((uint32_t)DWT->CYCCNT)
#define DEBUG_TIMER_RESET()                                 DWT->CYCCNT = 0
#define DEBUG_TIMER_US_TO_CYCLES(time_us)                   ((time_us) * (HAL_RCC_GetHCLKFreq() / 1000000UL))

#define DEBUG_TIMER_DELAY_CYCLES(time_cycles)               \
    do {                                                    \
        const uint32_t timeStart = DEBUG_TIMER_GET();       \
        uint32_t timeDelta;                                 \
                                                            \
        do {                                                \
            timeDelta = DEBUG_TIMER_GET() - timeStart;      \
        } while (timeDelta < (time_cycles));                \
    } while(0)

#define DEBUG_TIMER_DELAY_US(time_us)                       DEBUG_TIMER_DELAY_CYCLES(DEBUG_TIMER_US_TO_CYCLES(time_us))

#ifdef __cplusplus
}
#endif

#endif
