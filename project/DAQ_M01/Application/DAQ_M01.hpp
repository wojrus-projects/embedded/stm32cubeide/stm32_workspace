/*
 * DAQ_M01: main file.
 */

#ifndef DAQ_M01_hpp
#define DAQ_M01_hpp

#include <cstdint>
#include <cstddef>

#include "App_HAL_Board.hpp"
#include "LED.hpp"
#include "USB_Queue.hpp"
#include "Host_Protocol.hpp"
#include "DAQ_M01_ADC.hpp"
#include "DAQ_M01_Measurement_Definitions.hpp"
#include "DAQ_M01_ADC_SampleBuffer.hpp"
#include "DAQ_M01_Sampler.hpp"
#include "DAQ_M01_Acquisition.hpp"
#include "DAQ_M01_Calibration.hpp"
#include "DAQ_M01_Observer.hpp"

namespace DAQ_M01
{

namespace LED_Color_Status
{
inline constexpr LED_Color Off               = LED_Color::Black;
inline constexpr LED_Color Reset             = LED_Color::Cyan;
inline constexpr LED_Color Exception         = LED_Color::Red;
inline constexpr LED_Color Serial_Port_Close = LED_Color::Yellow;
inline constexpr LED_Color Serial_Port_Open  = LED_Color::Green;
inline constexpr LED_Color USB_Suspend       = LED_Color::Blue;
inline constexpr LED_Color USB_Resume        = Serial_Port_Close;
}

using ADC_HAL_t = ADC::ADC_ADS131M0X_HAL_STM32<HAL::Board::ADC::SPI,
                                               HAL::Board::ADC::DRDY_EXTI_Line,
                                               HAL::Board::ADC::DRDY_EXTI_GPIOSel,
                                               HAL::Board::ADC::DRDY_EXTI_IRQn,
                                               HAL::Board::ADC::DRDY_EXTI_IRQ_Priority>; 

using ADC_t = ADC::ADC_ADS131M0X;
using ADC_SampleBuffer_t = ADC_SampleBuffer<ADC_BUFFER_SIZE_SAMPLES, ADC::Sample>;
using ADC_Calibration_t = ADC_Calibration;
using Sampler_t = Sampler;
using AcquisitionConfiguration_t = AcquisitionConfiguration;
using AcquisitionCalibration_t = AcquisitionCalibration;

class DAQ_M01
{
public:
    
    DAQ_M01();
    
    ADC_t                      ADC;
    ADC_HAL_t                  ADC_HAL;
    ADC_SampleBuffer_t         ADC_SampleBuffer;
    ADC_Calibration_t          ADC_Calibration;
    AcquisitionConfiguration_t AcquisitionConfiguration;
    AcquisitionCalibration_t   AcquisitionCalibration;
    Sampler_t                  Sampler;
    Host_Protocol::PduBuffer   PduBuffer_DataContinue_First;
    Host_Protocol::PduBuffer   PduBuffer_DataContinue_Second;
    Host_Protocol::PduBuffer   PduBuffer_DataEnd;
    USB_Queue                  USB_TxQueue;
    LED                        LED_Status;
    
    void Initialize();
    void Reset();
    
    StatusCode Acquisition_Start(bool isCalibrationMode);
    void Acquisition_Stop();
    void Acquisition_CountSamples();
    void Acquisition_CountSamplesOverflowed();
    void Acquisition_PostprocessSamples(Host_Protocol::PduBuffer * const pOutputPduBuffer);
    
    bool PduBuffersAreEmpty();
    
    void ADC_Callback_DataReady();
    void ADC_Callback_SpiTransferCompleted();
    
private:
    
    bool IsCalibrationMode = false;
    
    size_t ADC_CalculateBufferSizeSamples();
    size_t ADC_GetConversionCount();
};

}

#endif
