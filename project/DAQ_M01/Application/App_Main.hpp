/*
 * DAQ_M01 firmware.
 */

#ifndef App_Main_hpp
#define App_Main_hpp

#include "DAQ_M01.hpp"
#include "Host_Protocol.hpp"

#define DEVICE_NAME     "DAQ_M01"

extern DAQ_M01::DAQ_M01 DAQ;
extern Host_Protocol::Host_Protocol HostProtocol;

#endif
