/*
 * RTOS common code.
 */

#include <cstdint>
#include <cstddef>

#include "Debug.h"
#include "RTOS_Common.h"
#include "Thread_Main.hpp"
#include "Thread_ADC.hpp"
#include "Thread_Priority.hpp"
#include "USB_CDC_Common.hpp"

static constexpr ULONG StackSize = 1024;

alignas(ALIGN_TYPE) static uint8_t ThreadStack_Main[StackSize];
alignas(ALIGN_TYPE) static uint8_t ThreadStack_ADC[StackSize];

static const uint8_t * const ThreadStackArray[]
{
    ThreadStack_Main,
    ThreadStack_ADC
};

static_assert((StackSize % sizeof(ULONG)) == 0);

static void ThreadStackErrorHandler(TX_THREAD * pThread);

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
UINT RTOS_AppInitialize(void)
{
    UINT status;
    
    // ThreadX configuration:
    // Define TX_ENABLE_STACK_CHECKING to enable notifications.
    
    status = tx_thread_stack_error_notify(ThreadStackErrorHandler);    
    if (not (status == TX_SUCCESS || status == TX_FEATURE_NOT_ENABLED))
    {
        return status;
    }
    else if (status == TX_FEATURE_NOT_ENABLED)
    {
        DEBUG_PRINTF("Stack notify is disabled\n");
    }
    
    status = tx_thread_create(&Thread_Main,
                              (CHAR *)"Main thread",
                              Thread_Main_Worker,
                              0,
                              ThreadStack_Main, StackSize,
                              (UINT)ThreadPriority::ThreadMain, (UINT)ThreadPriority::ThreadMain,
                              TX_NO_TIME_SLICE, TX_AUTO_START);
    if (status != TX_SUCCESS)
    {
        return status;
    }

    status = tx_thread_create(&Thread_ADC,
                              (CHAR *)"ADC thread",
                              Thread_ADC_Worker,
                              0,
                              ThreadStack_ADC, StackSize,
                              (UINT)ThreadPriority::ThreadADC, (UINT)ThreadPriority::ThreadADC,
                              TX_NO_TIME_SLICE, TX_AUTO_START);
    if (status != TX_SUCCESS)
    {
        return status;
    }

    status = tx_mutex_create(&Mutex_DAQ, (CHAR *)"DAQ mutex", TX_NO_INHERIT);
    if (status != TX_SUCCESS)
    {
        return status;
    }
    
    status = tx_event_flags_create(&EventFlagsGroup_ADC, (CHAR *)"ADC events");
    if (status != TX_SUCCESS)
    {
        return status;
    }
    
    status = USB_CDC_InitializeEventQueue();
    
    return status;
}
#pragma GCC diagnostic pop

void RTOS_CheckStack(void)
{
#if defined(DEBUG_LOG)
    
    // ThreadX configuration:
    // Undef TX_DISABLE_STACK_FILLING to check stack filling.
    
    using Word_t = ULONG;
    
    const size_t WordSize = sizeof(Word_t);
    const size_t WordCount = StackSize / WordSize;
    
    for (const uint8_t * pStack : ThreadStackArray)
    {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-align"
        const Word_t * const pWord = (const Word_t *)pStack;
#pragma GCC diagnostic pop
        
        size_t stackFreeWords = 0;
        
        for (size_t i = 0; i < WordCount; i++)
        {
            if (pWord[i] != TX_STACK_FILL)
            {
                stackFreeWords = i;
                break;
            }
        }

        const uint32_t stackFreeWords_percent = (100 * stackFreeWords) / WordCount;
        
        DEBUG_PRINTF("Stack %p free %u of %u words (%u %%)\n",
                pStack, stackFreeWords, WordCount, stackFreeWords_percent);
    }
#endif
}

static void ThreadStackErrorHandler([[maybe_unused]] TX_THREAD * pThread)
{
    DEBUG_PRINTF("Stack overflow in thread '%s' / %p\n",
            pThread->tx_thread_name, pThread);
    
    DEBUG_EXCEPTION();
}
