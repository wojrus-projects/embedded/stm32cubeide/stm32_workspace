/*
 * Host protocol: main file.
 */

#ifndef Host_Protocol_hpp
#define Host_Protocol_hpp

#include <cstdint>
#include <cstddef>

#include "Host_Protocol_Definitions.hpp"
#include "Host_Protocol_Commands.hpp"
#include "Status_Code.hpp"
#include "CRC16.hpp"

namespace Host_Protocol
{

class CommandParser
{
public:

    enum class State : uint8_t
    {
        Idle,
        Header,
        Payload,
        Checksum,
        EndSucces,
        EndError
    };
    
    State         State {};
    size_t        FieldOffset = 0;
    Header        PDU_Header;
    PayloadBuffer PDU_Payload;
    uint16_t      PDU_Checksum = 0;
    uint16_t      Calculated_Checksum = CRC16_InitializeValue;
    
    void Reset();
};

struct ProtocolCallback
{
    using CommandPreCb  = void (*)(CommandCode command, PayloadBuffer& payloadBuffer);
    using CommandPostCb = void (*)(CommandCode command, StatusCode status);
    using AnswerCb      = void (*)(PduBuffer& answerBuffer);
    
    CommandPreCb  CommandPre {};    // Called before command execution.
    CommandPostCb CommandPost {};   // Called after command execution.
    AnswerCb      Answer {};        // Called after answer PDU build.
};

class Host_Protocol
{
public:
    
    Host_Protocol(const ProtocolCallback * const pCallback);
    
    void Reset();
    bool ProcessCommandPDU(BufferByte commandBuffer);
    void ParseCommandPDU(uint8_t commandByte);
    void BuildAnswerPDU(CommandCode command, StatusCode status);
    uint8_t IncrementAnswerSequenceCounter();
    PduBuffer& GetAnswerBuffer();
    void ClearAnswerBuffer();
    bool IsAnswerPDU();
    
    // Payload is in external buffer, not in answer PDU.
    static void BuildAnswerPDU(CommandCode command, StatusCode status, uint8_t answerSequenceCounter,
                               const BufferByte * const pPayloadBuffer,
                               PduBuffer * const pAnswerBuffer);
    
    // Payload is already in answer PDU.
    static void BuildAnswerPDU(CommandCode command, StatusCode status, uint8_t answerSequenceCounter,
                               PduBuffer * const pAnswerBuffer);
    
    static uint8_t GetSequenceCounterFromPDU(const PduBuffer * const pPduBuffer);
    static uint8_t GetStatusFromPDU(const PduBuffer * const pPduBuffer);

private:
    
    using CommandParser_t = ::Host_Protocol::CommandParser;
    
    const ProtocolCallback * Callback {};
    CommandParser_t          CommandParser;
    uint8_t                  AnswerSequenceCounter = SEQUENCE_COUNTER_MIN;
    PduBuffer                AnswerPduBuffer;
    BufferByte               AnswerPayloadBuffer;
};

}

#endif
