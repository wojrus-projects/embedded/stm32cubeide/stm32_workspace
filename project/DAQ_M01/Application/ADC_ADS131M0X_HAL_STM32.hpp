/*
 * ADS131M0X HAL layer (STM32).
 */

#ifndef ADC_ADS131M0X_HAL_STM32_hpp
#define ADC_ADS131M0X_HAL_STM32_hpp

#include "Debug.h"
#include "App_HAL_CubeMX.h"
#include "Critical_Section.hpp"
#include "ADC_ADS131M0X_HAL.hpp"

namespace ADC
{

template
<
    SPI_HandleTypeDef& SPI,
    uint32_t           DRDY_EXTI_Line,
    uint32_t           DRDY_EXTI_GPIOSel,
    IRQn_Type          DRDY_EXTI_IRQn,
    uint32_t           DRDY_EXTI_IRQ_Priority
>
class ADC_ADS131M0X_HAL_STM32 final : public ADC_ADS131M0X_HAL
{   
public:
    
    void Initialize(ADC_DataReady_Callback callback) override
    {
        HAL_StatusTypeDef status;
        
        // Set ADC callbacks.
        
        Callback = callback;
        
        // Initialize DRDY input interrupt.
        
        EXTI_ConfigTypeDef extiConfig
        {
            .Line = DRDY_EXTI_Line,
            .Mode = EXTI_MODE_INTERRUPT,
            .Trigger = EXTI_TRIGGER_RISING,
            .GPIOSel = DRDY_EXTI_GPIOSel
        };
        
        status = HAL_EXTI_SetConfigLine(&EXTI_Handle, &extiConfig);
        DEBUG_ASSERT(status == HAL_OK);
        
        HAL_EXTI_RegisterCallback(&EXTI_Handle, HAL_EXTI_COMMON_CB_ID, callback);
        HAL_NVIC_SetPriority(DRDY_EXTI_IRQn, DRDY_EXTI_IRQ_Priority, 0);
    }
    
    void Deinitialize() override
    {
        HAL_NVIC_DisableIRQ(DRDY_EXTI_IRQn);
        HAL_NVIC_ClearPendingIRQ(DRDY_EXTI_IRQn);
        
        HAL_EXTI_ClearConfigLine(&EXTI_Handle);
        HAL_EXTI_ClearPending(&EXTI_Handle, 0);
    }
    
    void Delay_ms(uint32_t time) override
    {
        HAL_Delay(time);
    }
    
    void Reset_Assert() override
    {
        HAL_GPIO_WritePin(ADC_RESET_GPIO_Port, ADC_RESET_Pin, GPIO_PIN_RESET);
    }
    
    void Reset_Deassert() override
    {
        HAL_GPIO_WritePin(ADC_RESET_GPIO_Port, ADC_RESET_Pin, GPIO_PIN_SET);
    }
    
    void DataReady_InterruptEnable() override
    {
        CRITICAL_SECTION_ENTER;
        
        HAL_EXTI_ClearPending(&EXTI_Handle, 0);
        NVIC_ClearPendingIRQ(DRDY_EXTI_IRQn);
        NVIC_EnableIRQ(DRDY_EXTI_IRQn);
        
        CRITICAL_SECTION_EXIT;
    }
    
    void DataReady_InterruptDisable() override
    {
        NVIC_DisableIRQ(DRDY_EXTI_IRQn);
    }
    
    bool SPI_Transfer(uint8_t * pTxData, uint8_t * pRxData, uint16_t dataSize) override
    {
        constexpr uint32_t SPI_TIMEOUT_ms = 10;
        
        HAL_StatusTypeDef status;
        
        SPI_CS_Assert();
        status = HAL_SPI_TransmitReceive(&SPI, pTxData, pRxData, dataSize, SPI_TIMEOUT_ms);
        SPI_CS_Deassert();
        
        return (status == HAL_OK);
    }
    
    bool SPI_TransferAsyncStart(uint8_t * pTxData, uint8_t * pRxData, uint16_t dataSize) override
    {
        HAL_StatusTypeDef status;
        
        SPI_CS_Assert();
        status = HAL_SPI_TransmitReceive_DMA(&SPI, pTxData, pRxData, dataSize);
        if (status != HAL_OK)
        {
            SPI_CS_Deassert();
        }
        
        return (status == HAL_OK);
    }
    
    void SPI_TransferAsyncComplete() override
    {
        SPI_CS_Deassert();
    }
    
    void SPI_TransferAsyncStop() override
    {
        HAL_SPI_DMAStop(&SPI);
        SPI_CS_Deassert();
    }
    
    EXTI_HandleTypeDef * GetEXTI()
    {
        return &EXTI_Handle;
    }
    
private:
    
    ADC_DataReady_Callback Callback {};
    EXTI_HandleTypeDef EXTI_Handle {};
    
    void SPI_CS_Assert()
    {
        HAL_GPIO_WritePin(SPI1_NSS_GPIO_Port, SPI1_NSS_Pin, GPIO_PIN_RESET);
    }

    void SPI_CS_Deassert()
    {
        HAL_GPIO_WritePin(SPI1_NSS_GPIO_Port, SPI1_NSS_Pin, GPIO_PIN_SET);
    }
};

}

#endif
