/*
 * DAQ_M01: ADC utilities.
 */

#include <bit>

#include "Debug.h"
#include "DAQ_M01_ADC.hpp"

namespace DAQ_M01
{

ADC::ADC_ADS131M0X::ChannelStateBitmap ADC_ConvertChannelStateBitmap(uint8_t channelStateBitmap)
{
    const uint32_t channelStateBitmask = (1U << ADC::ADC_CHANNEL_COUNT) - 1U;
    
    channelStateBitmap &= channelStateBitmask;
    
    return channelStateBitmap;
}

std::expected<ADC::ADC_ADS131M0X::PGA_Gain, StatusCode> ADC_ConvertPgaGain(uint8_t gain_VoltPerVolt)
{
    auto isPowerOf2 = [](uint8_t value) -> bool
    {
        return std::has_single_bit(value);
    };
    
    if ((gain_VoltPerVolt < ADC::ADC_Gain_VoltPerVolt_Min) ||
        (gain_VoltPerVolt > ADC::ADC_Gain_VoltPerVolt_Max) ||
        (not isPowerOf2(gain_VoltPerVolt)))
    {
        return std::unexpected(StatusCode::Invalid_Argument);
    }
    
    auto log2 = [](uint8_t value) -> uint8_t
    {
        DEBUG_ASSERT(value > 0);
        
        return static_cast<uint8_t>(std::bit_width(value) - 1);
    };
    
    const auto gain = static_cast<ADC::ADC_ADS131M0X::PGA_Gain>(log2(gain_VoltPerVolt));
    
    return gain;
}

StatusCode ADC_ConvertPgaGainArray(const uint8_t inArray_VoltPerVolt[],
                                   ADC::ADC_ADS131M0X::PGA_Gain outArray[])
{
    for (size_t adcChannelIndex = 0; adcChannelIndex < ADC::ADC_CHANNEL_COUNT; adcChannelIndex++)
    {
        const auto result = ADC_ConvertPgaGain(inArray_VoltPerVolt[adcChannelIndex]);
        
        if (not result)
        {
            return result.error();
        }
        
        outArray[adcChannelIndex] = result.value(); 
    }
    
    return StatusCode::Success;
}

}
