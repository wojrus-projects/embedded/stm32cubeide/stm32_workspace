/*
 * DAQ_M01: ADC data sampler.
 */

#include "DAQ_M01_Sampler.hpp"

namespace DAQ_M01
{

void Sampler::Reset()
{    
    Counter.Sample = 0;
    Counter.BufferOverflow = 0;
    
    DataSequenceCounter = 0;
    
    State = SamplerState::Default;
}

bool Sampler::IsBusy()
{
    return (State == SamplerState::Acquisition);
}

bool Sampler::IsAcquisition()
{
    return (State == SamplerState::Acquisition);
}

bool Sampler::IsDisabled()
{
    return ((State == SamplerState::Idle) ||
            (State == SamplerState::Stop));
}

}
