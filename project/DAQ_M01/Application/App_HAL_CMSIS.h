/*
 * Application HAL (CMSIS layer).
 */

#ifndef App_HAL_CMSIS_h
#define App_HAL_CMSIS_h

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#include "stm32g4xx.h"
#pragma GCC diagnostic pop

#endif
