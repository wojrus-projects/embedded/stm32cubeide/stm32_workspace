/*
 * Generic low-level multithread FIFO buffer (single producer and consumer) for single core ARM Cortex-M.
 * 
 * Queue can store elements with size 1, 2 or 4 bytes only.
 * This limitation ensure usage LDR/STR instructions to atomic read/write data from/to queue. 
 * 
 * Writer (producer) only functions (thread-safe):
 * - put
 * - write
 * - flush_by_writer
 * 
 * Reader (consumer) only functions (thread-safe):
 * - get
 * - read
 * - remove
 * - peek
 * - flush_by_reader
 * 
 * Writer/reader common functions (thread-safe):
 * - get_length
 * - get_free
 * 
 * Array functions (non thread-safe):
 * - array_sort
 * 
 * Common functions:
 * - initialize (non thread-safe)
 */

#ifndef FIFO_Queue_hpp
#define FIFO_Queue_hpp

#include <cstdint>
#include <cstddef>
#include <type_traits>
#include <bit>

/**
 * Check if FIFO length is power of two.
 *
 * @param [in]  fifoLength      Required FIFO length (items count).
 *
 * @return  Result
 */
constexpr bool fifo_length_is_power_of_two(size_t fifoLength)
{
    return ((fifoLength >= 2) && std::has_single_bit(fifoLength));
}

/**
 * Calculate proper FIFO length if is not power of two.
 *
 * @param [in]  fifoLength      Required FIFO length (items count).
 *
 * @return  New FIFO length.
 */
constexpr size_t fifo_ceil_length(size_t fifoLength)
{
    if (fifoLength < 2)
    {
        return 2;
    }
    else
    {
        return std::bit_ceil(fifoLength);
    }
}

/**
 * FIFO queue class
 * 
 * @tparam   ItemType               Must be type: [integral, float, enum, pointer] with size 1, 2 or 4 bytes.
 * @tparam   StorageSizeItems       Must be power of 2, minimum storage capacity is 2 items.
 */
template
<
    typename ItemType,
    size_t StorageSizeItems
>
class FIFO_Queue
{
public:
    
    /**
     * Initialize queue.
     * 
     * Thread-safe: no
     */
    void initialize() volatile
    {
        read_index = 0;
        write_index = 0;
    }
    
    /**
     * Write one item to queue.
     * 
     * Thread-safe: writer context
     * 
     * @param [in]  value   Item value.
     * 
     * @return  Result
     */
    bool put(ItemType value) volatile
    {
        if (count_items() <= BUFFER_LENGTH_MASK)
        {
            buffer[write_index++ & BUFFER_LENGTH_MASK] = value;

            return true;
        }

        return false;
    }
    
    /**
     * Read and remove one item from queue.
     * 
     * Thread-safe: reader context
     * 
     * @param [out]  *p_out_value   Item value from queue.
     * 
     * @return  Result
     */
    bool get(ItemType * const p_out_value) volatile
    {
        if (count_items() > 0)
        {
            *p_out_value = buffer[read_index++ & BUFFER_LENGTH_MASK];
            
            return true;
        }
        
        return false;
    }
    
    /**
     * Write data to queue.
     * 
     * Thread-safe: writer context
     * 
     * @param [in]  *p_in_data          Data to write to queue.
     * @param [in]  data_write_length   Data write length (item count).
     * 
     * @return  Actual write length.
     */
    size_t write(const ItemType * p_in_data, size_t data_write_length) volatile
    {
        size_t data_write_length_actual = 0;

        while (data_write_length-- > 0)
        {
            if (not put(*p_in_data++))
            {
                break;
            }

            data_write_length_actual++;
        }

        return data_write_length_actual;
    }
    
    /**
     * Read and remove data from queue.
     * 
     * Thread-safe: reader context
     * 
     * @param [out]     *p_out_data         Read buffer.
     * @param [in]      data_read_length    Data read length (item count).
     * 
     * @return  Actual read length.
     */
    size_t read(ItemType * p_out_data, size_t data_read_length) volatile
    {
        size_t data_read_length_actual = 0;

        while (data_read_length-- > 0)
        {
            if (not get(p_out_data++))
            {
                break;
            }

            data_read_length_actual++;
        }
        
        return data_read_length_actual;
    }
    
    /**
     * Remove data from queue by reader.
     * 
     * Thread-safe: reader context
     * 
     * @param [in]  data_remove_length   Data remove length (item count).
     * 
     * @return  Actual remove length.
     */
    size_t remove(size_t data_remove_length) volatile
    {
        const size_t fifo_length = count_items();
        
        if (data_remove_length > fifo_length)
        {
            data_remove_length = fifo_length;
        }
        
        read_index += data_remove_length;
        
        return data_remove_length;
    }
    
    /**
     * Read data without removing from queue.
     * 
     * Thread-safe: reader context
     * 
     * @param [in]      start_index         Read start index (0 is oldest item).
     * @param [out]     *p_out_data         Read buffer.
     * @param [in]      data_read_length    Data read length (item count).
     * 
     * @return  Actual read length.
     */
    size_t peek(size_t start_index, ItemType * p_out_data, size_t data_read_length) volatile
    {
        const size_t fifo_length = count_items();

        if (start_index >= fifo_length)
        {
            return 0;
        }
        
        const size_t read_length_available = fifo_length - start_index;
        
        if (data_read_length > read_length_available)
        {
            data_read_length = read_length_available;
        }
        
        size_t data_read_length_actual = 0;
        
        while (data_read_length-- > 0)
        {   
            *p_out_data++ = buffer[(read_index + start_index) & BUFFER_LENGTH_MASK];

            start_index++;
            data_read_length_actual++;
        }
        
        return data_read_length_actual;
    }

    /**
     * Sort comparer function type.
     * 
     * @param [in]  item1   Item to compare (expected greater).
     * @param [in]  item2   Item to compare (expected lower).
     * 
     * @return      Compare result = item1 > item2
     */
    using SortCompareGreater = bool (*)(ItemType item1, ItemType item2);
    
    /**
     * Sort queue in place (algorithm: insertion sort).
     * 
     * Thread-safe: no
     * 
     * @param [in]  compare_greater     Comparer function.
     */
    void array_sort(SortCompareGreater compare_greater) volatile
    {
        const size_t array_length = get_length();
        
        if (array_length < 2)
        {
            return;
        }
        
        for (size_t i = 1; i < array_length; i++)
        {
            ItemType key;
            signed int j;
            
            key = array_read(i);
            j = i - 1;

            while ((j >= 0) && compare_greater(array_read(j), key))
            {
                array_write(j + 1, array_read(j));
                j = j - 1;
            }
            
            array_write(j + 1, key);
        }
    }
    
    /**
     * Clear queue by reader.
     * 
     * Thread-safe: reader context
     */
    void flush_by_reader() volatile
    {
        read_index = write_index;
    }
    
    /**
     * Clear queue by writer.
     * 
     * Thread-safe: writer context
     */
    void flush_by_writer() volatile
    {
        write_index = read_index;
    }
    
    /**
     * Get maximum queue length.
     * 
     * Thread-safe: always
     * 
     * @return  Maximum queue length
     */
    size_t get_capacity() volatile
    {
        return StorageSizeItems;
    }
    
    /**
     * Count items in queue.
     * 
     * Thread-safe: always
     * 
     * @return  Queue length
     */
    size_t get_length() volatile
    {
        return count_items();
    }
    
    /**
     * Get free space available in queue.
     * 
     * Thread-safe: always
     * 
     * @return  Free space
     */
    size_t get_free() volatile
    {
        const size_t fifo_length = count_items();

        return (StorageSizeItems - fifo_length);
    }
    
private:
    
    static constexpr size_t BUFFER_LENGTH_MASK = StorageSizeItems - 1;
    
    ItemType buffer[StorageSizeItems];
    volatile size_t read_index = 0;
    volatile size_t write_index = 0;
    
    size_t count_items() volatile
    {
        return (write_index - read_index);
    }
    
    void array_write(size_t array_index, ItemType value) volatile
    {
        buffer[(read_index + array_index) & BUFFER_LENGTH_MASK] = value;
    }
    
    ItemType array_read(size_t array_index) volatile
    {
        return buffer[(read_index + array_index) & BUFFER_LENGTH_MASK];
    }
    
    static_assert(std::is_integral<ItemType>() ||
                  std::is_floating_point<ItemType>() ||
                  std::is_enum<ItemType>() ||
                  std::is_pointer<ItemType>());
    
    static_assert(sizeof(ItemType) == 1 || sizeof(ItemType) == 2 || sizeof(ItemType) == 4);
                          
    static_assert(StorageSizeItems >= 2);
    
    static_assert(fifo_length_is_power_of_two(StorageSizeItems));
    
    static_assert(std::is_const_v<ItemType> == false, "'const' is not supported");
};

#endif
