/*
 * ADS131M0X HAL layer.
 */

#ifndef ADC_ADS131M0X_HAL_hpp
#define ADC_ADS131M0X_HAL_hpp

#include <stdint.h>

namespace ADC
{

using ADC_DataReady_Callback = void (*)();

class ADC_ADS131M0X_HAL
{
public:
    
    // Control API
    
    virtual void Initialize(ADC_DataReady_Callback callback) = 0;
    virtual void Deinitialize() = 0;
    virtual void Delay_ms(uint32_t time) = 0;
    
    // Reset pin API
    
    virtual void Reset_Assert() = 0;
    virtual void Reset_Deassert() = 0;
    
    // DRDY# pin API
    
    virtual void DataReady_InterruptEnable() = 0;
    virtual void DataReady_InterruptDisable() = 0;
    
    // SPI interface API
    
    virtual bool SPI_Transfer(uint8_t * pTxData, uint8_t * pRxData, uint16_t dataSize) = 0;
    virtual bool SPI_TransferAsyncStart(uint8_t * pTxData, uint8_t * pRxData, uint16_t dataSize) = 0;
    virtual void SPI_TransferAsyncComplete() = 0;
    virtual void SPI_TransferAsyncStop() = 0;
    
protected:
    
    // Only subclasses can use the destructor.
    ~ADC_ADS131M0X_HAL() = default;
};

}

#endif
