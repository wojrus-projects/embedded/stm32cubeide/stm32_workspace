/*
 * DAQ_M01: ADC data sampler.
 */

#ifndef DAQ_M01_Sampler_hpp
#define DAQ_M01_Sampler_hpp

#include <cstdint>

#include "DAQ_M01_Measurement_Definitions.hpp"

namespace DAQ_M01
{

enum class SamplerState : uint8_t
{
    Idle,
    Acquisition,
    Completed,
    Stop,
    
    Default = Idle
};

class Sampler
{
public:
    
    struct DataCounter
    {
        volatile uint64_t Sample = 0;
        volatile uint64_t BufferOverflow = 0;
    };
    
             DataCounter               Counter;
    volatile ADC_DataSequenceCounter_t DataSequenceCounter = 0;
    volatile SamplerState              State = SamplerState::Default;
    
    void Reset();
    bool IsBusy();
    bool IsAcquisition();
    bool IsDisabled();
};

}

#endif
