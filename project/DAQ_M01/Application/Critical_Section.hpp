/*
 * Interrupt locking/critical section library.
 */

#ifndef Critical_Section_hpp
#define Critical_Section_hpp

#include <cstdint>

#include "App_HAL_CMSIS.h"

/*
 * Critical section macros. Must be paired.
 */
#define CRITICAL_SECTION_ENTER                                  \
    {                                                           \
        const uint32_t localInterruptState_ = __get_PRIMASK();  \
        __disable_irq();
   
#define CRITICAL_SECTION_EXIT                                   \
        __set_PRIMASK(localInterruptState_);                    \
    }

/*
 * Scoped RAII critical section.
 */
class CriticalSection
{
public:
    
    CriticalSection()
    {
        localInterruptState_ = __get_PRIMASK();
        __disable_irq();
    }
    
    ~CriticalSection()
    {
        __set_PRIMASK(localInterruptState_);
    }
    
private:

    uint32_t localInterruptState_ = 0;
};

#endif
