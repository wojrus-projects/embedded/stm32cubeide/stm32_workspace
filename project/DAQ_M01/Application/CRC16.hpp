/*
 * CRC-16-ANSI (aka CRC-16-IBM / MODBUS) fast, table version.
 * Polynomial: x^16 + x^15 + x^2 + 1
 * Derived from Craig Marciniak's "Craig's Portable CRC16 Library."
 */

#ifndef CRC16_hpp
#define CRC16_hpp

#include <cstdint>
#include <cstddef>

inline constexpr uint16_t CRC16_InitializeValue = 0xFFFFU;

uint16_t CRC16_Calculate(uint16_t crc, const void * const pData, size_t dataSize);

#endif
