/*
 * Calibration data container.
 */

#include <cstring>

#include "Calibration_Data_Container.hpp"

namespace Calibration
{

void ContainerHeader::Clear()
{
    Magic[0] = 'C';
    Magic[1] = 'A';
    Magic[2] = 'L';
    Magic[3] = '0';
    
    Size = 0;
    HardwareType = 0;
    HardwareVariant = 0;
    State = ContainerState::Invalid;
    
    memset(&Reserved, 0, sizeof(Reserved));
}

bool ContainerHeader::Verify() const
{
    constexpr size_t SizeHeader = sizeof(*this);
    constexpr size_t SizeCRC16 = 2;
    constexpr size_t SizeEmptyContainer = SizeHeader + SizeCRC16;
    
    return ((memcmp(Magic, "CAL0", sizeof(Magic)) == 0) &&
            (Size > SizeEmptyContainer) &&
            (Size <= CalibrationDataStorageSize) &&
            (State == ContainerState::Valid));
}

}
