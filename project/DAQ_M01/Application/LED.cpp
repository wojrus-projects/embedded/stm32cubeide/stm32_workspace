/*
 * LED driver.
 */

#include "LED.hpp"
#include "Critical_Section.hpp"

bool LED::Initialize(LED_HAL_SetColorCallback callback)
{
    if (callback == nullptr)
    {
        return false;
    }
    
    HAL_SetColorCallback = callback;
    
    return true;
}

bool LED::SetColor(LED_Color color)
{
    if (color > LED_Color::Last)
    {
        return false;
    }
    
    CRITICAL_SECTION_ENTER;
    
    Color = color;
    HAL_SetColorCallback(color);
    
    CRITICAL_SECTION_EXIT;
    
    return true;
}

LED_Color LED::GetColor()
{
    return Color;
}

void LED::Toggle()
{
    CRITICAL_SECTION_ENTER;
    
    if (ToggleState)
    {
        HAL_SetColorCallback(Color);
    }
    else
    {
        HAL_SetColorCallback(LED_Color::Black);
    }
    
    ToggleState = !ToggleState;
    
    CRITICAL_SECTION_EXIT;
}

void LED::BlinkStart(LED_Color color, uint16_t period_ms)
{
    CRITICAL_SECTION_ENTER;
    
    SetColor(color);
    BlinkPeriodCounter_ms = 0;
    BlinkPeriod_ms = period_ms;
    ToggleState = false;
    
    CRITICAL_SECTION_EXIT;
}

void LED::BlinkStart(uint16_t period_ms)
{
    CRITICAL_SECTION_ENTER;
    
    BlinkPeriodCounter_ms = 0;
    BlinkPeriod_ms = period_ms;
    ToggleState = false;
    
    CRITICAL_SECTION_EXIT;
}

void LED::BlinkStop()
{
    CRITICAL_SECTION_ENTER;
    
    BlinkPeriod_ms = 0;
    HAL_SetColorCallback(Color);
    
    CRITICAL_SECTION_EXIT;
}

void LED::TimerHandler()
{
    if (BlinkPeriod_ms > 0)
    {
        if (++BlinkPeriodCounter_ms >= BlinkPeriod_ms)
        {
            BlinkPeriodCounter_ms = 0;
            Toggle();
        }
    }
}
