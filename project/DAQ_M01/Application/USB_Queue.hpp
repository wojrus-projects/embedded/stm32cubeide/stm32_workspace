/*
 * USB serial interface Tx PDU queue.
 */

#ifndef USB_Queue_hpp
#define USB_Queue_hpp

#include <cstddef>

#include "Host_Protocol.hpp"
#include "FIFO_Queue.hpp"

class USB_Queue
{
public:
    
    using QueueItemType = Host_Protocol::PduBuffer *;
    
    void Clear();
    size_t GetLength();
    bool AppendBuffer(QueueItemType pBuffer);
    QueueItemType PeekBuffer();
    void RemoveBuffer();
    
private:

    // Possible PDU types in queue:
    // Case 1: PDU with host protocol answer (x1 PDU).
    // Case 2: PDU with ADC samples, stream start or continue (status code = Acquisition_Data_Begin (x1 PDU) or Acquisition_Data_Continue (x2 PDUs max.)).
    // Case 3: PDU with ADC samples, stream end (status code = Acquisition_Data_End (x1 PDU)).
    //
    // Case duplicating is not possible.
    static constexpr size_t QueueLength = 4;
    
    FIFO_Queue<QueueItemType, fifo_ceil_length(QueueLength)> TxQueue;
};

#endif
