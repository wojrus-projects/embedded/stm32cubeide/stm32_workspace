/*
 * DAQ_M01: acquisition state observer.
 */

#ifndef DAQ_M01_Observer_hpp
#define DAQ_M01_Observer_hpp

void DAQ_M01_Observer_Enable();
void DAQ_M01_Observer_Disable();
void DAQ_M01_Observer_TimerHandler();

#endif
