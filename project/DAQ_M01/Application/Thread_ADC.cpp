/*
 * ADC data processing thread.
 */

#include "Debug.h"
#include "App_Main.hpp"
#include "Critical_Section.hpp"
#include "USB_CDC_Common.hpp"
#include "Thread_ADC.hpp"
#include "Thread_Main.hpp"

#include "tx_api.h"

TX_THREAD Thread_ADC;
TX_EVENT_FLAGS_GROUP EventFlagsGroup_ADC;

static constexpr ULONG EventFlag_AdcDataPostprocessing = 1U << 0;
static constexpr ULONG EventFlag_AdcCalibration        = 1U << 1;

[[noreturn]] void Thread_ADC_Worker([[maybe_unused]] ULONG threadArg)
{
    StatusCode answerStatusCode;
    bool isSamplerDisabled;
    Host_Protocol::PduBuffer * pPduBuffer;
        
    DEBUG_PRINTF("Thread start (ADC)\n");
    
    for (;;)
    {
        // Wait for ADC data from DMA.
        
        Event_Wait_AdcPostprocessing();
        
        //-----------------------------------------------------------------------------------------
        Mutex_DAQ_Lock();
        //-----------------------------------------------------------------------------------------
        
        DEBUG_SYSVIEW_MARK_START(Debug_SysView_MarkerID_AdcPostprocessing);
        DEBUG_PRINTF("T-ADC: postprocessing: %u\n", DAQ.ADC_SampleBuffer.Consumer_GetSampleCount());
        
        answerStatusCode = StatusCode::Failure;
        pPduBuffer = nullptr;
        
        // Check acquisition activity.
        
        CRITICAL_SECTION_ENTER;
        isSamplerDisabled = DAQ.Sampler.IsDisabled();
        CRITICAL_SECTION_EXIT;
        
        if (isSamplerDisabled)
        {
            DEBUG_PRINTF("T-ADC: sampler disabled\n");
            
            goto CancelProcess;
        }
        
        // Verify ADC buffer content.
        
        DEBUG_ASSERT((DAQ.ADC_SampleBuffer.Consumer_GetSampleCount() > 0) &&
                    ((DAQ.ADC_SampleBuffer.Consumer_GetSampleCount() * ADC::ADC_SAMPLE_SIZE) <= Host_Protocol::PAYLOAD_SIZE_MAX));
        
        // Get USB output buffer.
        
        if (DAQ.Sampler.State == DAQ_M01::SamplerState::Completed)
        {
            // Host application require last ADC data chunk with status = Acquisition_Data_End
            // to detect end of data stream.
            // To guarantee this, the USB Tx process uses a dedicated PDU buffer for last chunk.
            //
            // If acquisition process data length is only one PDU then status is always Acquisition_Data_End.
            
            pPduBuffer = &DAQ.PduBuffer_DataEnd;
            
            DEBUG_ASSERT(pPduBuffer->IsEmpty());
        }
        else
        {
            // PDU with status = Acquisition_Data_Begin (begin of ADC data stream) is guaranteed to sent.
            
            if (DAQ.PduBuffer_DataContinue_First.IsEmpty())
            {
                pPduBuffer = &DAQ.PduBuffer_DataContinue_First;
            }
            else if (DAQ.PduBuffer_DataContinue_Second.IsEmpty())
            {
                pPduBuffer = &DAQ.PduBuffer_DataContinue_Second;
            }
            else
            {
                // No free USB Tx buffers.
            }
        }
        
        // Count overflowed samples.
        
        if (not pPduBuffer)
        {
            DAQ.Acquisition_CountSamplesOverflowed();
            
            DEBUG_PRINTF("T-ADC: overflowed: %llu\n", DAQ.Sampler.Counter.BufferOverflow);
        }
        
        // Get answer PDU status code.
        
        if (pPduBuffer)
        {
            if (DAQ.Sampler.State == DAQ_M01::SamplerState::Completed)
            {
                answerStatusCode = StatusCode::Acquisition_Data_End;
            }
            else
            {
                if (DAQ.Sampler.Counter.Sample == 0)
                {
                    answerStatusCode = StatusCode::Acquisition_Data_Begin;
                }
                else
                {
                    answerStatusCode = StatusCode::Acquisition_Data_Continue;
                }
            }
        }
        
        // Count all samples.
        
        DAQ.Acquisition_CountSamples();
        
        // Process ADC data.
        
        if (pPduBuffer)
        {
            DAQ.Acquisition_PostprocessSamples(pPduBuffer);
            
            // Insert ADC data Sequence Counter (SC) into PDU.
            // In single acquisition first answer PDU with ADC data has SC = 0.
            
            uint8_t * const pSequenceCounterBuffer = &pPduBuffer->Data[Host_Protocol::PAYLOAD_OFFSET + (size_t)DAQ_M01::HostPduPayloadOffset::AdcDataSequenceCounter];
            
            Utilities::ToBytes(DAQ.Sampler.DataSequenceCounter, pSequenceCounterBuffer);
            
            pPduBuffer->DataSize += DAQ_M01::ADC_DATA_SEQUENCE_COUNTER_SIZE;
        }
        
        DAQ.Sampler.DataSequenceCounter++;

        // Release ADC working buffer.
        
        DAQ.ADC_SampleBuffer.Consumer_Clear();
        
        // Set acquisition next state.
        
        if (DAQ.Sampler.State == DAQ_M01::SamplerState::Completed)
        {
            DEBUG_PRINTF("T-ADC: completed: samples: %llu\n", DAQ.Sampler.Counter.Sample);
            
            DAQ.Acquisition_Stop();
            DAQ.Sampler.State = DAQ_M01::SamplerState::Completed;
        }
        else if (DAQ.Sampler.State == DAQ_M01::SamplerState::Acquisition)
        {
            // Continue acquisition.
        }
        else 
        {
            DEBUG_EXCEPTION();
        }
        
        DEBUG_SYSVIEW_MARK_STOP(Debug_SysView_MarkerID_AdcPostprocessing);
        
#if defined(DEBUG_SYSVIEW)
        {
            DAQ_M01::Sampler::DataCounter counters;
            
            CRITICAL_SECTION_ENTER;
            counters = DAQ.Sampler.Counter;
            CRITICAL_SECTION_EXIT;

            DEBUG_SYSVIEW_DATA(Debug_SysView_DataID_CounterSample, (uint32_t)counters.Sample);
            DEBUG_SYSVIEW_DATA(Debug_SysView_DataID_CounterOverflow, (uint32_t)counters.BufferOverflow);
        }
#endif
        
        // Send ADC data to USB host.
        
        if (pPduBuffer)
        {   
            // Build answer PDU with ADC data.
            
            DEBUG_ASSERT((not pPduBuffer->IsEmpty()) &&
                         (pPduBuffer->DataSize <= Host_Protocol::PAYLOAD_SIZE_MAX));
            
            Host_Protocol::Host_Protocol::BuildAnswerPDU(Host_Protocol::CommandCode::Acquisition_Push_Data,
                                                         answerStatusCode,
                                                         HostProtocol.IncrementAnswerSequenceCounter(),
                                                         pPduBuffer);
            // Add answer PDU to USB Tx queue.
            
            const bool result = DAQ.USB_TxQueue.AppendBuffer(pPduBuffer);
            
            DEBUG_ASSERT(result);
            
            if (not USB_CDC_State.IsTxPending)
            {
                // Start send oldest PDU from queue.
                
                const auto pTxBuffer = DAQ.USB_TxQueue.PeekBuffer();
                
                USB_CDC_SendPdu(pTxBuffer);
            }
            else
            {
                // USB CDC Tx is busy.
                // Postpone PDU send to future Tx callback.
                
                DEBUG_PRINTF("T-ADC: USB Tx busy\n");
            }
        }
        
CancelProcess:
        
        //-----------------------------------------------------------------------------------------
        Mutex_DAQ_Unlock();
        //-----------------------------------------------------------------------------------------
    }
}

void Event_Emit_AdcPostprocessing()
{
    UINT status;
    
    status = tx_event_flags_set(&EventFlagsGroup_ADC,
                                EventFlag_AdcDataPostprocessing, TX_OR);
    
    DEBUG_ASSERT(status == TX_SUCCESS);
}

void Event_Emit_AdcCalibration()
{
    UINT status;
    
    status = tx_event_flags_set(&EventFlagsGroup_ADC,
                                EventFlag_AdcCalibration, TX_OR);
    
    DEBUG_ASSERT(status == TX_SUCCESS);
}

void Event_Wait_AdcPostprocessing()
{
    UINT status;
    ULONG actualFlags;
    
    status = tx_event_flags_get(&EventFlagsGroup_ADC,
                                EventFlag_AdcDataPostprocessing, TX_OR_CLEAR,
                                &actualFlags, TX_WAIT_FOREVER);
    
    DEBUG_ASSERT(status == TX_SUCCESS);
}

void Event_Wait_AdcCalibration()
{
    UINT status;
    ULONG actualFlags;
    
    status = tx_event_flags_get(&EventFlagsGroup_ADC,
                                EventFlag_AdcCalibration, TX_OR_CLEAR,
                                &actualFlags, TX_WAIT_FOREVER);
    
    DEBUG_ASSERT(status == TX_SUCCESS);
}
