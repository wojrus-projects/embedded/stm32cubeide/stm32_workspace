/*
 * DAQ_M01: acquisition common code.
 */

#ifndef DAQ_M01_Acquisition_hpp
#define DAQ_M01_Acquisition_hpp

#include <cstdint>
#include <array>

#include "Buffer.hpp"
#include "DAQ_M01_Measurement_Definitions.hpp"
#include "DAQ_M01_Calibration.hpp"
#include "DAQ_M01_ADC.hpp"

namespace DAQ_M01
{

struct AcquisitionConfiguration
{
    //
    // Generic parameters
    //
    
    uint64_t SampleCountMax = 0;
    uint32_t SamplingFrequency_Hz = 0;
    
    //
    // Device specific parameters
    //
    
    using ChannelStateBitmap_t = ADC::ADC_ADS131M0X::ChannelStateBitmap;
    using ChannelIndexArray_t  = std::array<uint8_t, ADC::ADC_CHANNEL_COUNT>;
    using PgaGainArray_t       = std::array<ADC::ADC_ADS131M0X::PGA_Gain, ADC::ADC_CHANNEL_COUNT>;
    
    ChannelStateBitmap_t ChannelStateBitmap {};
    size_t               ChannelEnabledCount = 0;
    ChannelIndexArray_t  ChannelEnabledIndexArray {};
    PgaGainArray_t       PgaGainArray {};
    
    void Reset();
    
    bool Set(uint32_t samplingFrequency_Hz,
             uint64_t sampleCountMax,
             uint8_t channelStateBitmap,
             const ADC::ADC_ADS131M0X::PGA_Gain pgaGainArray[]);
    
    bool SetSamplingFrequency(uint32_t samplingFrequency_Hz);
    bool SetSampleCountMax(uint64_t sampleCountMax);
    bool SetChannelStateBitmap(uint8_t channelStateBitmap);
    bool SetPgaGain(const ADC::ADC_ADS131M0X::PGA_Gain pgaGainArray[]);
};

struct AcquisitionCalibration
{
    BufferStatic<ADC::ADC_CHANNEL_COUNT, CalibrationCoefficient> Coefficients;

    void Clear();
    void Initialize(const AcquisitionConfiguration& acquisitionConfiguration,
                    const ADC_Calibration& adcCalibration);
};

}

#endif
