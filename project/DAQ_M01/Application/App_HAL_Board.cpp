/*
 * Application HAL (board layer).
 */

#include "Debug.h"
#include "App_Main.hpp"
#include "Thread_ADC.hpp"

extern "C" void DAQ_M01_HAL_EXTI_IRQHandler(void)
{
    HAL_EXTI_IRQHandler(DAQ.ADC_HAL.GetEXTI());
}

extern "C" void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef * hspi)
{
    DEBUG_ASSERT(hspi == &HAL::Board::ADC::SPI);
    
    DAQ.ADC_Callback_SpiTransferCompleted();
}

namespace HAL::Board::ADC
{

void ADC_Callback_DataReady()
{
    DAQ.ADC_Callback_DataReady();
}

}
