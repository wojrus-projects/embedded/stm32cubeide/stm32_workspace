/*
 * DAQ_M01: measurement definitions.
 */

#include <expected>
#include <limits>
#include <algorithm>

#include "Utilities.hpp"
#include "Status_Code.hpp"
#include "DAQ_M01_Measurement_Definitions.hpp"

namespace DAQ_M01
{

std::expected<ADC::Sample, StatusCode> ADC_AverageSamples(const ADC::Sample * pSamples, size_t samplesCount)
{
    using SampleAccumulator_t = int32_t;
    
    static_assert(sizeof(SampleAccumulator_t) >= sizeof(ADC::Sample));
    
    constexpr size_t SampleCountMax_DomainNegative = std::numeric_limits<SampleAccumulator_t>::min() / ADC::ADC_SAMPLE_MIN;
    constexpr size_t SampleCountMax_DomainPositive = std::numeric_limits<SampleAccumulator_t>::max() / ADC::ADC_SAMPLE_MAX;
    constexpr size_t SampleCountMax = std::min(SampleCountMax_DomainNegative, SampleCountMax_DomainPositive);
    
    if ((samplesCount == 0) || (samplesCount > SampleCountMax))
    {
        return std::unexpected(StatusCode::Invalid_Argument);
    }
    
    SampleAccumulator_t samplesAccumulator = 0;
    
    for (size_t i = 0; i < samplesCount; i++)
    {
        samplesAccumulator += *pSamples++;
    }
    
    const ADC::Sample samplesAverage = samplesAccumulator / static_cast<std::make_signed_t<size_t>>(samplesCount);
    
    return samplesAverage;
}

}
