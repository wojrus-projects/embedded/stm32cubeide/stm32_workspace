/*
 * DAQ_M01: measurement definitions.
 */

#ifndef DAQ_M01_Measurement_Definitions_hpp
#define DAQ_M01_Measurement_Definitions_hpp

#include <cstdint>
#include <cstddef>
#include <algorithm>
#include <limits>
#include <expected>

#include "Status_Code.hpp"
#include "Host_Protocol_Definitions.hpp"
#include "ADC_ADS131M0X_Definitions.hpp"

namespace DAQ_M01
{

// With ADC data Sequence Counter (SC) host application can track 'ADC data lost' events
// if device USB Tx process is slow down by USB host and then PDU lost occurs.
// SC is always incremented after acquire single data chunk from ADC.
// SC in incremented from 0 to 0xFFFF, then overflows to 0.
// First PDU with ADC data has SC = 0.

using ADC_DataSequenceCounter_t = uint16_t;

inline constexpr size_t ADC_DATA_SEQUENCE_COUNTER_SIZE = sizeof(ADC_DataSequenceCounter_t);

// Host PDU payload offsets.
//
// Host PDU with acquisition data comprise two fields in payload:
// - ADC data Sequence Counter (SC), size: 16 bits
// - ADC samples in packed format

enum class HostPduPayloadOffset : size_t
{
    AdcDataSequenceCounter = 0,
    AdcSamples             = AdcDataSequenceCounter + ADC_DATA_SEQUENCE_COUNTER_SIZE
};

// Bit packed samples in PDU.

inline constexpr size_t PDU_SAMPLES_OFFSET = Host_Protocol::PAYLOAD_OFFSET + (size_t)HostPduPayloadOffset::AdcSamples;
inline constexpr size_t PDU_SAMPLES_SIZE_MAX_BYTES = Host_Protocol::PAYLOAD_SIZE_MAX - ADC_DATA_SEQUENCE_COUNTER_SIZE;
inline constexpr size_t PDU_SAMPLES_SIZE_MAX_BITS = PDU_SAMPLES_SIZE_MAX_BYTES * 8;
inline constexpr size_t PDU_SAMPLES_CAPACITY_AT_SINGLE_CHANNEL = PDU_SAMPLES_SIZE_MAX_BITS / ADC::ADC_BITS;
inline constexpr size_t PDU_SAMPLES_CAPACITY_AT_ALL_CHANNELS = (PDU_SAMPLES_CAPACITY_AT_SINGLE_CHANNEL / ADC::ADC_CHANNEL_COUNT) * ADC::ADC_CHANNEL_COUNT;

// ADC working buffer size.

inline constexpr size_t ADC_BUFFER_SIZE_SAMPLES = std::max(PDU_SAMPLES_CAPACITY_AT_SINGLE_CHANNEL, PDU_SAMPLES_CAPACITY_AT_ALL_CHANNELS);

// Miscellaneous functions.

std::expected<ADC::Sample, StatusCode> ADC_AverageSamples(const ADC::Sample * pSamples, size_t samplesCount);

}

#endif
