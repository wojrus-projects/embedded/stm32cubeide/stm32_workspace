/*
 * LED driver.
 */

#ifndef LED_hpp
#define LED_hpp

#include <cstdint>

#include "LED_HAL.hpp"
#include "LED_Color.hpp"

using LED_HAL_SetColorCallback = void (*)(LED_Color color);

class LED
{
public:
    
    bool Initialize(LED_HAL_SetColorCallback callback);
    bool SetColor(LED_Color color);
    LED_Color GetColor();
    void Toggle();
    void BlinkStart(LED_Color color, uint16_t period_ms);
    void BlinkStart(uint16_t period_ms);
    void BlinkStop();
    void TimerHandler();
    
private:
    
    LED_HAL_SetColorCallback HAL_SetColorCallback = nullptr;
    
    volatile uint16_t BlinkPeriod_ms = 0;
    volatile uint16_t BlinkPeriodCounter_ms = 0;
    volatile bool ToggleState = false;
    
    LED_Color Color = LED_Color::Black;
};

#endif
