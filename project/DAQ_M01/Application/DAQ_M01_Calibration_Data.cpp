/*
 * DAQ_M01: ADC calibration data.
 */

#include "CRC16.hpp"
#include "DAQ_M01_Calibration_Data.hpp"

namespace DAQ_M01
{

void CalibrationData::Clear()
{
    for (size_t channelIndex = 0; channelIndex < ADC::ADC_CHANNEL_COUNT; channelIndex++)
    {
        for (ADC::ADC_ADS131M0X::PGA_Gain pgaGain = ADC::ADC_ADS131M0X::PGA_Gain::First; pgaGain <= ADC::ADC_ADS131M0X::PGA_Gain::Last; pgaGain++)
        {
            CalibrationCoefficients[channelIndex][static_cast<size_t>(pgaGain)].Clear();
        }
    }
    
    OffsetCalibrationState = OffsetCalibrationState::Invalid;
}

StatusCode CalibrationData::GetCoefficient(size_t channelIndex,
                                           ADC::ADC_ADS131M0X::PGA_Gain pgaGain,
                                           CalibrationCoefficient& calibrationCoefficient) const
{
    if ((channelIndex >= ADC::ADC_CHANNEL_COUNT) ||
        (pgaGain > ADC::ADC_ADS131M0X::PGA_Gain::Last))
    {
        return StatusCode::Invalid_Argument;
    }

    calibrationCoefficient = CalibrationCoefficients[channelIndex][static_cast<size_t>(pgaGain)];
    
    return StatusCode::Success;
}

StatusCode CalibrationData::SetCoefficientOffset(size_t channelIndex,
                                                 ADC::ADC_ADS131M0X::PGA_Gain pgaGain,
                                                 ADC::Sample coefficientOffset_sample)
{
    if ((channelIndex >= ADC::ADC_CHANNEL_COUNT) ||
        (pgaGain > ADC::ADC_ADS131M0X::PGA_Gain::Last))
    {
        return StatusCode::Invalid_Argument;
    }
    
    CalibrationCoefficients[channelIndex][static_cast<size_t>(pgaGain)].Offset_sample = coefficientOffset_sample;
    
    return StatusCode::Success;
}

StatusCode CalibrationData::SetCoefficientGain(size_t channelIndex,
                                               ADC::ADC_ADS131M0X::PGA_Gain pgaGain,
                                               float coefficientGain_VoltPerSample)
{
    if ((channelIndex >= ADC::ADC_CHANNEL_COUNT) ||
        (pgaGain > ADC::ADC_ADS131M0X::PGA_Gain::Last))
    {
        return StatusCode::Invalid_Argument;
    }
    
    CalibrationCoefficients[channelIndex][static_cast<size_t>(pgaGain)].Gain_VoltPerSample = coefficientGain_VoltPerSample;
    
    return StatusCode::Success; 
}

}
