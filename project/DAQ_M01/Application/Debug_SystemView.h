/*
 * Segger SystemView.
 * 
 * Define symbol DEBUG_SYSVIEW to enable SystemView debugging API.
 * 
 * Configuration is in file SEGGER_SYSVIEW_Config_NoOS.c
 */

#ifndef Debug_SystemView_h
#define Debug_SystemView_h

#include <stdint.h>

#include "Segger_SystemView/SEGGER/SEGGER_SYSVIEW.h"

#ifdef __cplusplus
extern "C" {
#endif

#if defined DEBUG_SYSVIEW
    #define DEBUG_SYSVIEW_PRINTF(format, ...)       SEGGER_SYSVIEW_PrintfHost(format __VA_OPT__(,) __VA_ARGS__)
    #define DEBUG_SYSVIEW_ISR_ENTER()               SEGGER_SYSVIEW_RecordEnterISR()
    #define DEBUG_SYSVIEW_ISR_EXIT()                SEGGER_SYSVIEW_RecordExitISR()
    #define DEBUG_SYSVIEW_MARK_START(marker_id)     SEGGER_SYSVIEW_MarkStart(marker_id)
    #define DEBUG_SYSVIEW_MARK_STOP(marker_id)      SEGGER_SYSVIEW_MarkStop(marker_id)
    #define DEBUG_SYSVIEW_MARK(marker_id)           SEGGER_SYSVIEW_Mark(marker_id)
    #define DEBUG_SYSVIEW_DATA(data_id, value)      Debug_SysView_SendDataSampleU32(data_id, value)
#else
    #define DEBUG_SYSVIEW_PRINTF(format, ...)
    #define DEBUG_SYSVIEW_ISR_ENTER()
    #define DEBUG_SYSVIEW_ISR_EXIT()
    #define DEBUG_SYSVIEW_MARK_START(marker_id)
    #define DEBUG_SYSVIEW_MARK_STOP(marker_id)
    #define DEBUG_SYSVIEW_MARK(marker_id)
    #define DEBUG_SYSVIEW_DATA(data_id, value)
#endif

enum Debug_SysView_MarkerID
{
    Debug_SysView_MarkerID_HostCommand,
    Debug_SysView_MarkerID_AdcPostprocessing,
    Debug_SysView_MarkerID_UsbTx
};

enum Debug_SysView_DataID
{
    Debug_SysView_DataID_CounterSample,
    Debug_SysView_DataID_CounterOverflow,
};

void Debug_SysView_SendDataSampleU32(enum Debug_SysView_DataID dataId, uint32_t value);

#ifdef __cplusplus
}
#endif

#endif
