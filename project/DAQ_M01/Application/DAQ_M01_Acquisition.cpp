/*
 * DAQ_M01: acquisition common code.
 */

#include <limits>
#include <bit>

#include "Debug.h"
#include "DAQ_M01_Acquisition.hpp"

namespace DAQ_M01
{

void AcquisitionConfiguration::Reset()
{
    SampleCountMax = 0;
    SamplingFrequency_Hz = 0;

    ChannelStateBitmap.reset();
    ChannelEnabledCount = 0;
    ChannelEnabledIndexArray.fill(0);
    
    PgaGainArray.fill(ADC::ADC_ADS131M0X::PGA_Gain::Default);
}

bool AcquisitionConfiguration::Set(uint32_t samplingFrequency_Hz,
                                   uint64_t sampleCountMax,
                                   uint8_t channelStateBitmap,
                                   const ADC::ADC_ADS131M0X::PGA_Gain pgaGainArray[])
{
    bool result;
    
    result = SetSamplingFrequency(samplingFrequency_Hz);
    if (not result)
    {
        return result;
    }
    
    result = SetSampleCountMax(sampleCountMax);
    if (not result)
    {
        return result;
    }
    
    result = SetChannelStateBitmap(channelStateBitmap);
    if (not result)
    {
        return result;
    }
    
    result = SetPgaGain(pgaGainArray);
    if (not result)
    {
        return result;
    }
    
    // ADC sample processing global limitation: sampleCount * channelEnabledCount <= max(uint64_t).
    
    const uint64_t sampleCountMax_Limit = std::numeric_limits<uint64_t>::max() / (uint64_t)ChannelEnabledCount;

    if (sampleCountMax > sampleCountMax_Limit)
    {
        return false;
    }
    
    return true;
}

bool AcquisitionConfiguration::SetSamplingFrequency(uint32_t samplingFrequency_Hz)
{
    if (not ((samplingFrequency_Hz >= ADC::ADC_SAMPLING_FREQUENCY_MIN_Hz) &&
             (samplingFrequency_Hz <= ADC::ADC_SAMPLING_FREQUENCY_MAX_Hz)))
    {
        return false;
    }
    
    SamplingFrequency_Hz = samplingFrequency_Hz;
    
    return true;
}

bool AcquisitionConfiguration::SetSampleCountMax(uint64_t sampleCountMax)
{
    if (sampleCountMax == 0)
    {
        return false;
    }
    
    SampleCountMax = sampleCountMax;
    
    return true;
}

bool AcquisitionConfiguration::SetChannelStateBitmap(uint8_t channelStateBitmap)
{
    const uint32_t channelStateBitmask = (1U << ADC::ADC_CHANNEL_COUNT) - 1;
    
    channelStateBitmap &= channelStateBitmask;
    
    if (channelStateBitmap == 0)
    {
        return false;
    }
    
    ChannelStateBitmap = channelStateBitmap;
    ChannelEnabledCount = ChannelStateBitmap.count();
    
    size_t enabledIndex = 0;
    
    for (size_t channelIndex = 0; channelIndex < ADC::ADC_CHANNEL_COUNT; channelIndex++)
    {
        if (ChannelStateBitmap[channelIndex])
        {
            ChannelEnabledIndexArray[enabledIndex] = (uint8_t)channelIndex;
            enabledIndex++;
        }
    }
    
    return true;
}

bool AcquisitionConfiguration::SetPgaGain(const ADC::ADC_ADS131M0X::PGA_Gain pgaGainArray[])
{
    for (size_t channelIndex = 0; channelIndex < ADC::ADC_CHANNEL_COUNT; channelIndex++)
    {
        const auto gain = pgaGainArray[channelIndex];
        
        if (not ((gain >= ADC::ADC_ADS131M0X::PGA_Gain::First) &&
                 (gain <= ADC::ADC_ADS131M0X::PGA_Gain::Last)))
        {
            return false;
        }
        
        PgaGainArray[channelIndex] = gain;
    }
    
    return true;
}

void AcquisitionCalibration::Clear()
{   
    for (auto& coefficient : Coefficients.Data)
    {
        coefficient.Clear();
    }
    
    Coefficients.Clear();
}

void AcquisitionCalibration::Initialize(const AcquisitionConfiguration& acquisitionConfiguration,
                                        const ADC_Calibration& adcCalibration)
{
    const bool adcCalibrationIsValid = adcCalibration.Container.NVM_Verify();
    
    Clear();
    
    for (size_t i = 0; i < acquisitionConfiguration.ChannelEnabledCount; i++)
    {   
        const size_t adcChannelIndex = acquisitionConfiguration.ChannelEnabledIndexArray[i];
        const auto adcPgaGain = acquisitionConfiguration.PgaGainArray[adcChannelIndex];
        CalibrationCoefficient acquisitionCoefficient;
        
        // Option 1: try set coefficient from NVM.
        
        if (adcCalibrationIsValid)
        {
            CalibrationCoefficient nvmCoefficient;
            
            const StatusCode status = adcCalibration.NVM_ReadCalibrationCoefficient(adcChannelIndex, adcPgaGain, nvmCoefficient);
            
            DEBUG_ASSERT(status == StatusCode::Success);
            
            if (nvmCoefficient.IsValid())
            {
                acquisitionCoefficient = nvmCoefficient;
            }
        }
        
        // Option 2: set nominal coefficient.
        
        if (not acquisitionCoefficient.IsValid())
        {
            acquisitionCoefficient.SetNominal(adcPgaGain);
        }
        
        const bool appendResult = Coefficients.Append(acquisitionCoefficient);
        
        DEBUG_ASSERT(appendResult);
    }
}

}
