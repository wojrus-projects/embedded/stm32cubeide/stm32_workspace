/*
 * DAQ_M01: main file.
 */

#include <algorithm>

#include "Debug.h"
#include "Thread_ADC.hpp"
#include "DAQ_M01.hpp"

namespace DAQ_M01
{

DAQ_M01::DAQ_M01() :
    ADC(ADC_HAL)
{
}

void DAQ_M01::Initialize()
{
    StatusCode status;
    
    LED_Status.Initialize(LED_HAL_SetColorStatus);
    
    status = ADC.Initialize(HAL::Board::ADC::ADC_Callback_DataReady);
    DEBUG_ASSERT(status == StatusCode::Success);
    
    Reset();
    
    DAQ_M01_Observer_Enable();
}

void DAQ_M01::Reset()
{
    DEBUG_PRINTF("DAQ: reset\n");
    
    CRITICAL_SECTION_ENTER;
    ADC.Stop();
    CRITICAL_SECTION_EXIT;
    
    ADC_SampleBuffer.Clear();
    ADC_Calibration.Reset();
    AcquisitionConfiguration.Reset();
    AcquisitionCalibration.Clear();
    Sampler.Reset();
    PduBuffer_DataContinue_First.Clear();
    PduBuffer_DataContinue_Second.Clear();
    PduBuffer_DataEnd.Clear();
    USB_TxQueue.Clear();
}

StatusCode DAQ_M01::Acquisition_Start(bool isCalibrationMode)
{
    StatusCode statusCode;
    
    const size_t adcBufferSizeSamples = ADC_CalculateBufferSizeSamples();
    
    DEBUG_PRINTF_EXT("DAQ: ADC start: Fs: %u Hz, samples: %llu, buffer: %u",
            AcquisitionConfiguration.SamplingFrequency_Hz,
            AcquisitionConfiguration.SampleCountMax,
            adcBufferSizeSamples);
    
    IsCalibrationMode = isCalibrationMode;
    
    AcquisitionCalibration.Initialize(AcquisitionConfiguration, ADC_Calibration);
    
    CRITICAL_SECTION_ENTER;
    
    Sampler.Reset();
    ADC_SampleBuffer.Clear();
    
    statusCode = ADC_SampleBuffer.SetCapacity(adcBufferSizeSamples);
    if (statusCode != StatusCode::Success)
    {
        goto ExitError;
    }
    
    statusCode = ADC.SetSamplingFrequency(AcquisitionConfiguration.SamplingFrequency_Hz);
    if (statusCode != StatusCode::Success)
    {
        goto ExitError;
    }
    
    statusCode = ADC.SetPGAGain(AcquisitionConfiguration.PgaGainArray.data());
    if (statusCode != StatusCode::Success)
    {
        goto ExitError;
    }
    
    statusCode = ADC.EnableChannel(AcquisitionConfiguration.ChannelStateBitmap);
    if (statusCode != StatusCode::Success)
    {
        goto ExitError;
    }
    
    Sampler.State = SamplerState::Acquisition;
    
    ADC.Start();
    
ExitError:
    
    CRITICAL_SECTION_EXIT;
    
    return statusCode;
}

void DAQ_M01::Acquisition_Stop()
{
    DEBUG_PRINTF_EXT("DAQ: ADC stop");
    
    CRITICAL_SECTION_ENTER;
    
    ADC.Stop();
    ADC_SampleBuffer.Clear();
    
    Sampler.State = SamplerState::Stop;
    
    CRITICAL_SECTION_EXIT;
}

void DAQ_M01::Acquisition_CountSamples()
{
    CRITICAL_SECTION_ENTER;
    
    Sampler.Counter.Sample += ADC_GetConversionCount();
    
    CRITICAL_SECTION_EXIT;
}

void DAQ_M01::Acquisition_CountSamplesOverflowed()
{
    CRITICAL_SECTION_ENTER;
    
    Sampler.Counter.BufferOverflow += ADC_GetConversionCount();
    
    CRITICAL_SECTION_EXIT;
}

void DAQ_M01::ADC_Callback_DataReady()
{
    StatusCode status;
    
    status = ADC.ChannelData_ReadAsyncStart();
    
    DEBUG_ASSERT(status == StatusCode::Success);
}

void DAQ_M01::ADC_Callback_SpiTransferCompleted()
{
    uint16_t adcResponse;
    ADC::Sample * pAdcSampleBuffer;
    
    // Stop SPI DMA transfer.
    
    ADC.HAL.SPI_TransferAsyncComplete();
    
    // Copy samples from ADC to DAQ double-buffer.
    
    pAdcSampleBuffer = ADC_SampleBuffer.Producer_GetBuffer();
    
    ADC.ChannelData_Copy(adcResponse, pAdcSampleBuffer, AcquisitionConfiguration.ChannelStateBitmap);

    const auto advanceResult = ADC_SampleBuffer.Producer_Advance(AcquisitionConfiguration.ChannelEnabledCount);
    
    DEBUG_ASSERT(advanceResult);
    
    const auto isBufferFull = advanceResult.value();
    
    if (isBufferFull)
    {   
        // CRITICAL: ADC interrupt overrun.

        DEBUG_ASSERT(ADC_SampleBuffer.Consumer_IsEmpty());
     
        // Swap working buffers in double-buffer.
        
        ADC_SampleBuffer.Producer_SwapBuffers();
        
        // Check if acquisition is completed.
        
        const size_t sampleInBufferCount = ADC_GetConversionCount();
        
        const uint64_t sampleRemainingCount = AcquisitionConfiguration.SampleCountMax - Sampler.Counter.Sample;
        
        const bool isAcquisitionCompleted = sampleInBufferCount >= sampleRemainingCount;
        
        if (isAcquisitionCompleted) 
        {
            ADC.Stop();
            Sampler.State = SamplerState::Completed;

            DEBUG_PRINTF_EXT("DAQ: ADC completed");
            
            // Trim last buffer.

            const size_t sampleToRemoveCount = (size_t)(sampleInBufferCount - sampleRemainingCount) * AcquisitionConfiguration.ChannelEnabledCount;
                
            const StatusCode status = ADC_SampleBuffer.Consumer_RemoveSamples(sampleToRemoveCount);
                
            DEBUG_ASSERT(status == StatusCode::Success);
        }
    }
    
    if (isBufferFull)
    {
        if (IsCalibrationMode)
        {
            Sampler.Counter.Sample += ADC_SampleBuffer.Consumer_GetSampleCount();
            
            Event_Emit_AdcCalibration();
        }
        else
        {
            Event_Emit_AdcPostprocessing();
        }
    }
}

void DAQ_M01::Acquisition_PostprocessSamples(Host_Protocol::PduBuffer * const pOutputPduBuffer)
{
    size_t adcConversionCount = ADC_GetConversionCount();

    DEBUG_ASSERT((adcConversionCount > 0) &&
                 ((ADC_SampleBuffer.GetCapacity() % AcquisitionConfiguration.ChannelEnabledCount) == 0));
    
    ADC::Sample * pAdcSampleBuffer = ADC_SampleBuffer.Consumer_GetBuffer();
    uint8_t * pPduPayload = &pOutputPduBuffer->Data[PDU_SAMPLES_OFFSET];
    
    for (size_t adcConversionIndex = 0; adcConversionIndex < adcConversionCount; adcConversionIndex++)
    {
        for (size_t adcChannelEnabledIndex = 0; adcChannelEnabledIndex < AcquisitionConfiguration.ChannelEnabledCount; adcChannelEnabledIndex++)
        {
            const CalibrationCoefficient& calibrationCoefficient = AcquisitionCalibration.Coefficients.Data[adcChannelEnabledIndex];
            
            ADC::Sample adcSample = (*pAdcSampleBuffer++) + calibrationCoefficient.Offset_sample;
        
            adcSample = std::clamp(adcSample, ADC::ADC_SAMPLE_MIN, ADC::ADC_SAMPLE_MAX);
            
            ADC_SampleToBytes(adcSample, pPduPayload);
            
            pPduPayload += ADC::ADC_SAMPLE_SIZE;
        }
    }
    
    pOutputPduBuffer->DataSize = ADC_SampleBuffer.Consumer_GetSampleCount() * ADC::ADC_SAMPLE_SIZE;
}

bool DAQ_M01::PduBuffersAreEmpty()
{
    return (PduBuffer_DataContinue_First.IsEmpty() &&
            PduBuffer_DataContinue_Second.IsEmpty() &&
            PduBuffer_DataEnd.IsEmpty());
}

size_t DAQ_M01::ADC_CalculateBufferSizeSamples()
{
    const uint64_t acquisitionSamplesCount = AcquisitionConfiguration.SampleCountMax * (uint64_t)AcquisitionConfiguration.ChannelEnabledCount;
    const size_t pduBufferCapacity = (ADC_BUFFER_SIZE_SAMPLES / AcquisitionConfiguration.ChannelEnabledCount) * AcquisitionConfiguration.ChannelEnabledCount;
    const size_t bufferSize = (size_t)std::min(acquisitionSamplesCount, (uint64_t)pduBufferCapacity); 
    
    return bufferSize;
}

size_t DAQ_M01::ADC_GetConversionCount() 
{
    return (ADC_SampleBuffer.Consumer_GetSampleCount() / AcquisitionConfiguration.ChannelEnabledCount);
}

}
