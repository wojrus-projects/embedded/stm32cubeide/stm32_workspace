/*
 * USB serial interface Tx PDU queue.
 */

#include "Debug.h"
#include "USB_Queue.hpp"

void USB_Queue::Clear()
{   
    QueueItemType pBuffer;
    
    while (TxQueue.get(&pBuffer))
    {
        if (pBuffer)
        {
            pBuffer->Clear();
        }
    }
    
    TxQueue.initialize();
}

size_t USB_Queue::GetLength()
{
    return TxQueue.get_length();
}

bool USB_Queue::AppendBuffer(QueueItemType pBuffer)
{
    return TxQueue.put(pBuffer);
}

USB_Queue::QueueItemType USB_Queue::PeekBuffer()
{
    QueueItemType pBuffer {};
    
    if (TxQueue.peek(0, &pBuffer, 1) == 1)
    {
        DEBUG_ASSERT(pBuffer);
        
        return pBuffer;
    }
    
    return nullptr;
}

void USB_Queue::RemoveBuffer()
{
    TxQueue.remove(1);
}
