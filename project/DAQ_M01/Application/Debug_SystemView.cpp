/*
 * Segger SystemView.
 * 
 * Define symbol DEBUG_SYSVIEW to enable SystemView debugging API.
 * 
 * Configuration is in file SEGGER_SYSVIEW_Config_NoOS.c
 */

#include "Debug_SystemView.h"

void Debug_SysView_SendDataSampleU32(enum Debug_SysView_DataID dataId, uint32_t value)
{
    SEGGER_SYSVIEW_DATA_SAMPLE dataSample;
    
    dataSample.ID = dataId;
    dataSample.pValue.pU32 = &value;
    
    SEGGER_SYSVIEW_SampleData(&dataSample);
}
