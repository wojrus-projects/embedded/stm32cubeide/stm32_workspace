/*
 * DAQ_M01: ADC calibration.
 */

#ifndef DAQ_M01_Calibration_hpp
#define DAQ_M01_Calibration_hpp

#include <cstdint>
#include <cstddef>

#include "Status_Code.hpp"
#include "DAQ_M01_ADC.hpp"
#include "DAQ_M01_Calibration_Coefficient.hpp"
#include "DAQ_M01_Calibration_Data.hpp"
#include "Calibration_Data_Container.hpp"
#include "Device_Info_Data.hpp"

namespace DAQ_M01
{

enum class CalibrationStage : uint8_t
{
    Invalid,
    Stage_1_Offset,
    Stage_2_Gain_Measurement_Positive,
    Stage_3_Gain_Measurement_Negative
};

using CalibrationDataContainer = ::Calibration::CalibrationDataContainer<CalibrationData, DeviceInfo::DeviceInfoData>;

class ADC_Calibration
{
public:

    CalibrationDataContainer Container;
    
    void       Reset();
    StatusCode ProcessCalibrationOffset();
    StatusCode ProcessCalibrationGainPositive(uint8_t adcChannelStateBitmap, uint8_t adcPgaGain_VoltPerVolt, float voltageReferencePositive_V);
    StatusCode ProcessCalibrationGainNegative(float voltageReferenceNegative_V);
    
    bool       NVM_IsOffsetCalibrationValid() const;
    StatusCode NVM_ReadCalibrationCoefficient(size_t channelIndex, ADC::ADC_ADS131M0X::PGA_Gain pgaGain, CalibrationCoefficient& calibrationCoefficient) const;
    StatusCode NVM_Write();
    
private:
    
    // Ref: AN TI SPRAD55A (https://www.ti.com/lit/pdf/sprad55)
    static constexpr size_t AdcOversamplingFactor = 16;
    
    static constexpr uint64_t CalibrationSampleCountMax = std::min(AdcOversamplingFactor, ADC_BUFFER_SIZE_SAMPLES);
    static constexpr uint32_t CalibrationSamplingFrequency_Hz = 1'000;
    
    static constexpr float VoltageReference_Min_ABS_V = 1e-3f;
    static constexpr float VoltageReference_Max_ABS_V = 1e+3f;
    
    static constexpr float VoltageMeasured_Min_PercentOfRange = 80.0f;
    static constexpr float VoltageMeasured_Max_PercentOfRange = 99.0f;
    
    struct CalibrationGain
    {
        CalibrationStage                       Stage = CalibrationStage::Invalid;
        ADC::ADC_ADS131M0X::ChannelStateBitmap AdcChannelStateBitmap {};
        ADC::ADC_ADS131M0X::PGA_Gain           AdcPgaGain = ADC::ADC_ADS131M0X::PGA_Gain::Default;
        
        void Clear();
    };
    
    struct MeasurementPoint
    {
        float       VoltageReference_V = 0.0f;
        ADC::Sample VoltageMeasured_sample = 0;
        
        void Clear();
    };
    
    enum class VoltagePolarity
    {
        Positive,
        Negative
    };
    
    CalibrationGain CalibrationGain;
    MeasurementPoint MeasurementPositive[ADC::ADC_CHANNEL_COUNT];
    MeasurementPoint MeasurementNegative[ADC::ADC_CHANNEL_COUNT];
    
    ADC::Sample MeasureVoltage(size_t adcChannelIndex, ADC::ADC_ADS131M0X::PGA_Gain adcPgaGain, ADC::Sample offsetCoefficient);
    StatusCode CheckVoltageMeasured(ADC::Sample voltageMeasured_sample, VoltagePolarity voltagePolarity);
    StatusCode CalculateGainCoefficients();
};

}

#endif
