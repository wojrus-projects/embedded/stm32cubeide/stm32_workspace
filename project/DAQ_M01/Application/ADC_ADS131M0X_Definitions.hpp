/*
 * ADS131M0X definitions.
 */

#ifndef ADC_ADS131M0X_Definitions_hpp
#define ADC_ADS131M0X_Definitions_hpp

#include <cstdint>
#include <cstddef>
#include <array>

#include "ADC_ADS131M0X_Registers.h"

namespace ADC
{

// Used for storage raw ADC 24-bit signed samples, use only 24 bits LSB, range is from -8'388'608 to +8'388'607.
using Sample = int32_t;

inline constexpr size_t   ADC_SAMPLE_SIZE = 3;
inline constexpr uint32_t ADC_BITS = 24;
inline constexpr uint32_t ADC_TOTAL_COUNTS = 1UL << ADC_BITS;

inline constexpr Sample   ADC_SAMPLE_MIN = (Sample)(-(ADC_TOTAL_COUNTS / 2));
inline constexpr Sample   ADC_SAMPLE_MAX = (Sample)(+(ADC_TOTAL_COUNTS / 2 - 1));

inline constexpr uint32_t ADC_CHANNEL_COUNT = ADS131M0X_CHANNEL_COUNT;
inline constexpr uint32_t ADC_GAIN_COUNT = 8;

inline constexpr uint32_t ADC_SAMPLING_FREQUENCY_MIN_Hz = 250;
inline constexpr uint32_t ADC_SAMPLING_FREQUENCY_MAX_Hz = 32'000;

inline constexpr std::array<float, ADC_GAIN_COUNT> ADC_VoltageRanges
{
    // Range (V):            Gain (V/V):
    1200.000e-3f,         // x 1
     600.000e-3f,         // x 2
     300.000e-3f,         // x 4
     150.000e-3f,         // x 8
      75.000e-3f,         // x 16
      37.500e-3f,         // x 32
      18.750e-3f,         // x 64
       9.375e-3f          // x 128
};

inline constexpr std::array<uint8_t, ADC_GAIN_COUNT> ADC_Gain_VoltPerVolt
{
    1, 2, 4, 8, 16, 32, 64, 128
};

inline constexpr uint8_t ADC_Gain_VoltPerVolt_Min = 1;
inline constexpr uint8_t ADC_Gain_VoltPerVolt_Max = 128;

}

#endif
