/*
 * ADC Texas Instruments ADS131M0X
 */

#include <cstring>
#include <algorithm>

#include "Debug.h"
#include "ADC_ADS131M0X.hpp"

namespace ADC
{

ADC_ADS131M0X::ADC_ADS131M0X(ADC_ADS131M0X_HAL& hal)
    : HAL(hal)
{
}

StatusCode ADC_ADS131M0X::Initialize(ADC_DataReady_Callback callback)
{
    StatusCode status;

    HAL.Initialize(callback);
    PowerOnResetDelay();
    HardwareReset();
    
    status = VerifyReset();
    if (status != StatusCode::Success)
    {
        DEBUG_PRINTF("ADS131M0X: verify reset error\n");
        
        return status;
    }
    
    status = CheckID();
    if (status != StatusCode::Success)
    {
        DEBUG_PRINTF("ADS131M0X: check ID error\n");
        
        return status;
    }
    
    status = SetModeDefault();
    
    return status;
}

void ADC_ADS131M0X::PowerOnResetDelay()
{
    HAL.Delay_ms(t_POR_ms);
}

void ADC_ADS131M0X::HardwareReset()
{
    HAL.Reset_Assert();
    HAL.Delay_ms(tw_RSL_ms);
    HAL.Reset_Deassert();
    HAL.Delay_ms(t_REGACQ_ms);
    
    SpiWordLengthBytes = SPI_WORD_LENGTH_DEFAULT;
}

StatusCode ADC_ADS131M0X::VerifyReset()
{
    StatusCode status;
    uint16_t response {};
    
    status = Command_Null(response);
    if (status != StatusCode::Success)
    {
        return status;
    }
    
    if (response != 0xFF26U)
    {
        return StatusCode::Hardware_Failure;
    }
    
    return StatusCode::Success;
}

StatusCode ADC_ADS131M0X::CheckID()
{
    StatusCode status;
    uint16_t id {};
    
    status = Command_RegisterRead(1, ID_ADDRESS, &id);
    if (status != StatusCode::Success)
    {
        return status;
    }
    
    id &= (ID_RESERVED_MASK | ID_CHANCNT_MASK); 
    
    if (id != ID_DEFAULT)
    {
        return StatusCode::Hardware_Failure;
    }
    
    return StatusCode::Success;
}

StatusCode ADC_ADS131M0X::SetModeDefault()
{   
    StatusCode status;
    const uint16_t registerWrite = MODE_DRDY_FMT_NEG_PULSE_FIXED_WIDTH
                                 | MODE_DRDY_HiZ_LOGIC_HIGH
                                 | MODE_DRDY_SEL_MOST_LAGGING
                                 | MODE_TIMEOUT_ENABLED
                                 | MODE_WLENGTH_24BIT
                                 | MODE_RESET_NO_RESET
                                 | MODE_CRC_TYPE_16BIT_CCITT
                                 | MODE_RX_CRC_EN_DISABLED
                                 | MODE_REG_CRC_EN_DISABLED;
    uint16_t registerRead {};
    
    status = Command_RegisterWrite(1, MODE_ADDRESS, &registerWrite);
    if (status != StatusCode::Success)
    {
        return status;
    }
    
    SpiWordLengthBytes = SPI_WORD_LENGTH_DEFAULT;
    
    status = Command_RegisterRead(1, MODE_ADDRESS, &registerRead);
    if (status != StatusCode::Success)
    {
        return status;
    }
    
    if (registerRead != registerWrite)
    {
        return StatusCode::Hardware_Failure;
    }
    
    return StatusCode::Success;
}

void ADC_ADS131M0X::Start()
{
    HAL.DataReady_InterruptEnable();
}

void ADC_ADS131M0X::Stop()
{
    HAL.DataReady_InterruptDisable();
    HAL.SPI_TransferAsyncStop();
}

StatusCode ADC_ADS131M0X::Command_Reset(uint16_t& response)
{
    // Command RESET TX frame: opcode (1 word) + empty CRC (1 word).
    constexpr size_t CMD_RESET_TX_FRAME_LENGTH = 1 + 1;
    
    // Command RESET RX frame: response (1 word) + fill data (7 words).
    constexpr size_t CMD_RESET_RX_FRAME_LENGTH = 1 + 7;
    
    bool result;
    
    // + command
    U16ToWord(OPCODE_RESET, 0);
    
    // + empty CRC
    U16ToWord(0x0000, 1);
    
    const uint16_t spiFrameLengthBytes = (uint16_t)(SpiWordLengthBytes * std::max(CMD_RESET_TX_FRAME_LENGTH, CMD_RESET_RX_FRAME_LENGTH));
    
    result = HAL.SPI_Transfer(SpiTxFrame, SpiRxFrame, spiFrameLengthBytes);
    if (not result)
    {
        return StatusCode::Hardware_Failure;
    }

    response = U16FromWord(0);
    
    HAL.Delay_ms(t_REGACQ_ms);
    
    SpiWordLengthBytes = SPI_WORD_LENGTH_DEFAULT;
    
    return StatusCode::Success;
}

StatusCode ADC_ADS131M0X::Command_Null(uint16_t& response)
{   
    // Command NULL TX frame: opcode (1 word) + empty CRC (1 word).
    constexpr size_t CMD_NULL_TX_FRAME_LENGTH = 1 + 1;
    
    // Command NULL RX frame: response (1 word) + empty CRC (1 word).
    constexpr size_t CMD_NULL_RX_FRAME_LENGTH = 1 + 1;
    
    bool result;
    
    // + command
    U16ToWord(OPCODE_NULL, 0);
    
    // + empty CRC
    U16ToWord(0x0000, 1);
    
    const uint16_t spiFrameLengthBytes = (uint16_t)(SpiWordLengthBytes * std::max(CMD_NULL_TX_FRAME_LENGTH, CMD_NULL_RX_FRAME_LENGTH));
    
    result = HAL.SPI_Transfer(SpiTxFrame, SpiRxFrame, spiFrameLengthBytes);
    if (not result)
    {
        return StatusCode::Hardware_Failure;
    }
    
    response = U16FromWord(0);
    
    return StatusCode::Success;
}

StatusCode ADC_ADS131M0X::Command_RegisterRead(uint32_t registerCount, uint32_t registerAddress, uint16_t registerArray[])
{   
    // Command RREG TX frame: opcode (1 word) + empty CRC (1 word).
    constexpr size_t CMD_RREG_TX_FRAME_LENGTH = 1 + 1;
    
    // Command RREG RX frame: response (1 word, ignored) + optional channel data (M words, ignored) + empty CRC (1 word, ignored).
    constexpr size_t CMD_RREG_RX_FRAME_LENGTH = 0;
    
    // Command NULL TX frame: opcode (1 word) + empty CRC (1 word).
    constexpr size_t CMD_NULL_TX_FRAME_LENGTH = 1 + 1;
    
    bool result;
    uint16_t command;
    
    if ((registerCount < 1) || (registerCount > NUM_REGISTERS) || (registerAddress > 63))
    {
        return StatusCode::Invalid_Argument;
    }
    
    //
    // Transfer 1: send RREG command.
    //
    
    command = OPCODE_RREG;
    command |= ((registerCount - 1) & 0x7FUL) << 0;
    command |= (registerAddress & 0x3FUL) << 7;
    
    // + command
    U16ToWord(command, 0);
    
    // + empty CRC
    U16ToWord(0x0000, 1);
    
    const uint16_t rregFrameLengthBytes = (uint16_t)(SpiWordLengthBytes * std::max(CMD_RREG_TX_FRAME_LENGTH, CMD_RREG_RX_FRAME_LENGTH));
    
    result = HAL.SPI_Transfer(SpiTxFrame, SpiRxFrame, rregFrameLengthBytes);
    if (not result)
    {
        return StatusCode::Hardware_Failure;
    }
    
    //
    // Transfer 2: send NULL command and read registers.
    //
    
    command = OPCODE_NULL;
    
    // + command
    U16ToWord(command, 0);
    
    // + empty CRC
    U16ToWord(0x0000, 1);
    
    const uint16_t nullTXFrameLengthBytes = (uint16_t)(SpiWordLengthBytes * CMD_NULL_TX_FRAME_LENGTH);
    
    // Command NULL RX frame:
    // If registerCount == 1: register data (1 words) + empty CRC (1 word, non verified).
    // If registerCount > 1: RREG ack (1 word) + RREG registers data (N words) + empty CRC (1 word, non verified).
    const uint16_t nullRXFrameLengthBytes = (uint16_t)(SpiWordLengthBytes * (
            (registerCount == 1) ? (1 + 1)
                                 : (1 + registerCount + 1)));
    
    result = HAL.SPI_Transfer(SpiTxFrame, SpiRxFrame, std::max(nullTXFrameLengthBytes, nullRXFrameLengthBytes));
    if (not result)
    {
        return StatusCode::Hardware_Failure;
    }
    
    //
    // Check RREG response in NULL RX frame.
    //
    
    if (registerCount > 1)
    {
        const uint16_t responseValid = OPCODE_RREG_RESPONSE
                                     | ((registerCount - 1) & 0x7FUL) << 0
                                     | (registerAddress & 0x3FUL) << 7;
        
        const uint16_t responseRead = U16FromWord(0);
        
        if (responseValid != responseRead)
        {
            return StatusCode::Hardware_Failure;
        }
    }
    
    //
    // Copy registers.
    //
    
    if (registerCount > 1)
    {
        for (uint32_t i = 0; i < registerCount; i++)
        {   
            registerArray[i] = U16FromWord(1 + i);
        }
    }
    else
    {
        registerArray[0] = U16FromWord(0);
    }
    
    return StatusCode::Success;
}

StatusCode ADC_ADS131M0X::Command_RegisterWrite(uint32_t registerCount, uint32_t registerAddress, const uint16_t registerArray[])
{
    StatusCode status;
    bool result;
    uint16_t command;
    
    if ((registerCount < 1) || (registerCount > NUM_REGISTERS) || (registerAddress > 63))
    {
        return StatusCode::Invalid_Argument;
    }
    
    //
    // Transfer 1: send WREG command + registers.
    //
    
    command = OPCODE_WREG;
    command |= ((registerCount - 1) & 0x7FUL) << 0;
    command |= (registerAddress & 0x3FUL) << 7;
    
    // + command
    U16ToWord(command, 0);
    
    // + registers
    for (uint32_t i = 0; i < registerCount; i++)
    {
        U16ToWord(registerArray[i], 1 + i);
    }
    
    // + empty CRC
    U16ToWord(0x0000, 1 + registerCount);
    
    // Command WREG TX frame: opcode (1 word) + registers data (N words) + empty CRC (1 word).
    const uint16_t wregTXFrameLengthBytes = (uint16_t)(SpiWordLengthBytes * (1 + registerCount + 1));
    
    // Command WREG RX frame: response (1 word, ignored) + optional channel data (M words, ignored) + empty CRC (1 word, ignored).
    const uint16_t wregRXFrameLengthBytes = SpiWordLengthBytes * 0;
    
    result = HAL.SPI_Transfer(SpiTxFrame, SpiRxFrame, std::max(wregTXFrameLengthBytes, wregRXFrameLengthBytes));
    if (not result)
    {
        return StatusCode::Hardware_Failure;
    }
    
    //
    // Transfer 2: send NULL command and check WREG response.
    //
    
    uint16_t responseRead;
    
    status = Command_Null(responseRead);
    if (status != StatusCode::Success)
    {
        return status;
    }
    
    const uint16_t responseValid = OPCODE_WREG_RESPONSE
                                 | ((registerCount - 1) & 0x7FUL) << 0
                                 | (registerAddress & 0x3FUL) << 7;
    
    if (responseValid != responseRead)
    {
        return StatusCode::Hardware_Failure;
    }
    
    return StatusCode::Success;
}

StatusCode ADC_ADS131M0X::ChannelData_ReadBlocking(uint16_t& response, Sample * pSampleBuffer, ChannelStateBitmap channelStateBitmap)
{
    // Command NULL TX frame: opcode (1 word) + empty CRC (1 word).
    constexpr size_t CMD_NULL_TX_FRAME_LENGTH = 1 + 1;
    
    // Command NULL RX frame: response (1 word) + channel data (N words) + empty CRC (1 word, non verified).
    constexpr size_t CMD_NULL_RX_FRAME_LENGTH = 1 + ADS131M0X_CHANNEL_COUNT + 1; 

    bool result;
    
    // + command
    U16ToWord(OPCODE_NULL, 0);
    
    // + empty CRC
    U16ToWord(0x0000, 1);

    const uint16_t adcDataLength = (uint16_t)(SpiWordLengthBytes * std::max(CMD_NULL_TX_FRAME_LENGTH, CMD_NULL_RX_FRAME_LENGTH));
    
    result = HAL.SPI_Transfer(SpiTxFrame, SpiRxFrame, adcDataLength);
    if (not result)
    {
        return StatusCode::Hardware_Failure;
    }
    
    ChannelData_Copy(response, pSampleBuffer, channelStateBitmap);
    
    return StatusCode::Success;
}

StatusCode ADC_ADS131M0X::ChannelData_ReadAsyncStart()
{
    // Command NULL TX frame: opcode (1 word) + empty CRC (1 word).
    constexpr size_t CMD_NULL_TX_FRAME_LENGTH = 1 + 1;
    
    // Command NULL RX frame: response (1 word) + channel data (N words) + empty CRC (1 word, non verified).
    constexpr size_t CMD_NULL_RX_FRAME_LENGTH = 1 + ADS131M0X_CHANNEL_COUNT + 1; 

    bool result;
    
    // + command
    U16ToWord(OPCODE_NULL, 0);
    
    // + empty CRC
    U16ToWord(0x0000, 1);

    const uint16_t adcDataLength = (uint16_t)(SpiWordLengthBytes * std::max(CMD_NULL_TX_FRAME_LENGTH, CMD_NULL_RX_FRAME_LENGTH));
    
    result = HAL.SPI_TransferAsyncStart(SpiTxFrame, SpiRxFrame, adcDataLength);
    if (not result)
    {
        return StatusCode::Hardware_Failure;
    }
    
    return StatusCode::Success;
}

void ADC_ADS131M0X::ChannelData_Copy(uint16_t& response, Sample * pSampleBuffer, ChannelStateBitmap channelStateBitmap)
{
    response = U16FromWord(0);
    
    // Sample size is 24 bits with default configuration of MODE register.
    const uint8_t * pRawData24bit = &SpiRxFrame[SpiWordLengthBytes];
    
    for (size_t channelIndex = 0; channelIndex < ADS131M0X_CHANNEL_COUNT; channelIndex++)
    {
        if (channelStateBitmap[channelIndex])
        {
            Sample sample;
            
            // Unpack 24 bit word to 32 bit word (in bits 31..24).
            sample = ((Sample)pRawData24bit[0] << 24) | ((Sample)pRawData24bit[1] << 16) | ((Sample)pRawData24bit[2] << 8);
            
            // 24 bit value sign extension.
            sample >>= 8;
            
            *pSampleBuffer++ = sample;
        }
        
        pRawData24bit += SPI_WORD_LENGTH_DEFAULT;
    }
}

StatusCode ADC_ADS131M0X::SetPGAGain(const PGA_Gain gainArray[])
{
    StatusCode status;
    uint16_t registerGain[2] {};
    
    for (size_t channelIndex = 0; channelIndex < ADS131M0X_CHANNEL_COUNT; channelIndex++)
    {
        // Registers GAIN1, GAIN2 layout: RESERVED (1 bit) + PGAGAINx[2:0] (3 bits).
        constexpr uint32_t PARAMETER_BITS_PER_CHANNEL = 1 + 3;
        constexpr uint16_t GAIN_MASK = 0x0007;
        
        const uint32_t wordIndex = channelIndex / PARAMETER_BITS_PER_CHANNEL;
        const uint32_t gainBitfieldOffset = (channelIndex % PARAMETER_BITS_PER_CHANNEL) * PARAMETER_BITS_PER_CHANNEL;
        
        registerGain[wordIndex] &= (uint16_t)(~(GAIN_MASK << gainBitfieldOffset));
        registerGain[wordIndex] |= (uint16_t)(((uint16_t)gainArray[channelIndex] & GAIN_MASK) << gainBitfieldOffset);
    }
    
    status = RegisterWriteVerify(GAIN1_ADDRESS, registerGain[0]);
    if (status != StatusCode::Success)
    {
        return status;
    }
    
    status = RegisterWriteVerify(GAIN2_ADDRESS, registerGain[1]);
    
    return status;
}

StatusCode ADC_ADS131M0X::SetOversamplingRatio(OSR osr)
{
    StatusCode status;
    uint16_t registerClock {};
    
    status = Command_RegisterRead(1, CLOCK_ADDRESS, &registerClock);
    if (status != StatusCode::Success)
    {
        return status;
    }
    
    const uint16_t osrBits = ((uint16_t)osr << 2) & CLOCK_OSR_MASK;
    
    registerClock &= (uint16_t)(~CLOCK_OSR_MASK);
    registerClock |= osrBits;
    
    status = RegisterWriteVerify(CLOCK_ADDRESS, registerClock);
    
    return status;
}

StatusCode ADC_ADS131M0X::SetSamplingFrequency(uint32_t samplingFrequency_Hz)
{
    struct FrequencyOSR
    {
        uint32_t frequency;
        OSR osr;
    };
    
    static const FrequencyOSR frequencyOSR_Array[] =
    {
        { 32000, OSR::OSR_128   },
        { 16000, OSR::OSR_256   },
        { 8000,  OSR::OSR_512   },
        { 4000,  OSR::OSR_1024  },
        { 2000,  OSR::OSR_2048  },
        { 1000,  OSR::OSR_4096  },
        { 500,   OSR::OSR_8192  },
        { 250,   OSR::OSR_16256 }
    };
    
    for (const auto& item : frequencyOSR_Array)
    {
        if (item.frequency == samplingFrequency_Hz)
        {
            return SetOversamplingRatio(item.osr);
        }
    }
    
    return StatusCode::Invalid_Argument;
}

StatusCode ADC_ADS131M0X::EnableChannel(ChannelStateBitmap channelStateBitmap)
{
    StatusCode status;
    uint16_t registerClock {};
    
    status = Command_RegisterRead(1, CLOCK_ADDRESS, &registerClock);
    if (status != StatusCode::Success)
    {
        return status;
    }
    
    constexpr uint16_t channelStateMask = (1U << ADS131M0X_CHANNEL_COUNT) - 1;
    constexpr uint16_t CHx_EN_MASK = CLOCK_CH0_EN_MASK | CLOCK_CH1_EN_MASK | CLOCK_CH2_EN_MASK |
                                     CLOCK_CH3_EN_MASK | CLOCK_CH4_EN_MASK | CLOCK_CH5_EN_MASK; 
    
    registerClock &= (uint16_t)(~CHx_EN_MASK);
    registerClock |= (uint16_t)((channelStateBitmap.to_ulong() & channelStateMask) << 8);
    
    status = RegisterWriteVerify(CLOCK_ADDRESS, registerClock);
    
    return status;
}

StatusCode ADC_ADS131M0X::Calibration_WriteOffset(uint32_t channelIndex, uint16_t LSB, uint16_t MSB)
{
    StatusCode status;
    const uint32_t registerAddressMSB = CH0_OCAL_MSB_ADDRESS + channelIndex * 5; 
    const uint32_t registerAddressLSB = registerAddressMSB + 1;
    
    DEBUG_PRINTF("ADS131M0X: write calibration offset: LSB: 0x%04X, MSB: 0x%04X to channel: %u\n", LSB, MSB, channelIndex);
    
    status = RegisterWriteVerify(registerAddressMSB, MSB);
    if (status != StatusCode::Success)
    {
        return status;
    }

    status = RegisterWriteVerify(registerAddressLSB, LSB);
    
    return status;
}

StatusCode ADC_ADS131M0X::Calibration_WriteGain(uint32_t channelIndex, uint16_t LSB, uint16_t MSB)
{
    StatusCode status;
    const uint32_t registerAddressMSB = CH0_GCAL_MSB_ADDRESS + channelIndex * 5; 
    const uint32_t registerAddressLSB = registerAddressMSB + 1;
    
    DEBUG_PRINTF("ADS131M0X: write calibration gain: LSB: 0x%04X, MSB: 0x%04X to channel: %u\n", LSB, MSB, channelIndex);
    
    status = RegisterWriteVerify(registerAddressMSB, MSB);
    if (status != StatusCode::Success)
    {
        return status;
    }
    
    status = RegisterWriteVerify(registerAddressLSB, LSB);
    
    return status;
}

StatusCode ADC_ADS131M0X::RegisterWriteVerify(uint32_t registerAddress, uint16_t writeValue)
{
    StatusCode status = StatusCode::Failure;
    uint16_t readValue {};
    
    status = Command_RegisterWrite(1, registerAddress, &writeValue);
    if (status != StatusCode::Success)
    {
        goto ExitError;
    }
    
    status = Command_RegisterRead(1, registerAddress, &readValue);
    if (status != StatusCode::Success)
    {
        goto ExitError;
    }
    
    if (readValue != writeValue)
    {
        status = StatusCode::Hardware_Failure;
    }
    
    status = StatusCode::Success;
    
ExitError:
    
    DEBUG_PRINTF("ADS131M0X: write 0x%04X to 0x%02X, status: %u\n",
            writeValue, registerAddress, status);
    
    return status;
}

void ADC_ADS131M0X::U16ToWord(uint16_t value, uint32_t wordIndex)
{
    uint8_t * const pWordBytes = &SpiTxFrame[wordIndex * SpiWordLengthBytes];
    
    pWordBytes[0] = (uint8_t)(value >> 8);
    pWordBytes[1] = (uint8_t)(value >> 0);
    
    switch (SpiWordLengthBytes)
    {
        case 2:
            break;
            
        case 3:
            pWordBytes[2] = 0x00;
            break;
            
        case 4:
            pWordBytes[2] = 0x00;
            pWordBytes[3] = 0x00;
            break;
            
        default:
            DEBUG_EXCEPTION();
    }
}

uint16_t ADC_ADS131M0X::U16FromWord(uint32_t wordIndex)
{
    const uint8_t * const pWordBytes = &SpiRxFrame[wordIndex * SpiWordLengthBytes];
    
    return (uint16_t)((((uint16_t)pWordBytes[0] << 8)) | ((uint16_t)pWordBytes[1] << 0));
}

}
