/*
 * DAQ_M01: acquisition state observer.
 */

#include "App_Main.hpp"

static constexpr uint32_t LED_BLINK_PERIOD_ADC_ACQUISITION_ms = 100;

static volatile bool DAQ_M01_Observer_IsEnabled = false;
static volatile bool DAQ_M01_Observer_IsAcquisitionPrevious = false;

void DAQ_M01_Observer_Enable()
{
    DAQ_M01_Observer_IsAcquisitionPrevious = false;
    DAQ_M01_Observer_IsEnabled = true;
}

void DAQ_M01_Observer_Disable()
{
    DAQ_M01_Observer_IsEnabled = false;
    DAQ_M01_Observer_IsAcquisitionPrevious = false;
}

void DAQ_M01_Observer_TimerHandler()
{
    if (DAQ_M01_Observer_IsEnabled == false)
    {
        return;
    }
    
    //
    // LED Status
    //
    
    const bool isAcquisitionNow = DAQ.Sampler.IsAcquisition();
    
    if (isAcquisitionNow != DAQ_M01_Observer_IsAcquisitionPrevious)
    {
        DAQ_M01_Observer_IsAcquisitionPrevious = isAcquisitionNow;
        
        if (isAcquisitionNow)
        {
            DAQ.LED_Status.BlinkStart(LED_BLINK_PERIOD_ADC_ACQUISITION_ms);
        }
        else
        {
            DAQ.LED_Status.BlinkStop();
        }
    }
}
