/*
 * ADC data processing thread.
 */

#ifndef Thread_ADC_hpp
#define Thread_ADC_hpp

#include "tx_api.h"

[[noreturn]] void Thread_ADC_Worker(ULONG threadArg);

void Event_Emit_AdcPostprocessing();
void Event_Emit_AdcCalibration();
void Event_Wait_AdcPostprocessing();
void Event_Wait_AdcCalibration();

extern TX_THREAD Thread_ADC;
extern TX_EVENT_FLAGS_GROUP EventFlagsGroup_ADC;

#endif
