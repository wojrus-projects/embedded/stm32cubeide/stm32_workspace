/*
 * USB CDC application callbacks.
 */

#ifndef USB_CDC_Callback_h
#define USB_CDC_Callback_h

#include "ux_api.h"
#include "ux_device_class_cdc_acm.h"

#ifdef __cplusplus
extern "C" {
#endif

struct USB_CDC_ControlCallback
{
    void (*Suspend)(void);
    void (*Resume)(void);
    void (*Open)(void);
    void (*Close)(void);
};

extern const struct USB_CDC_ControlCallback USB_CDC_ControlCallback;
extern UX_SLAVE_CLASS_CDC_ACM_CALLBACK_PARAMETER USB_CDC_TransferCallback;

#ifdef __cplusplus
}
#endif

#endif
