# Projekty STM32CubeIDE

- [DAQ_M01 - Firmware for USB DAQ (6x voltage inputs, 24 bit, differential, non isolated)](project/DAQ_M01/Doc/README.md)
- [DAQ_M02 - Firmware for USB DAQ (2x current inputs, 16 bit, isolated)](project/DAQ_M02/Doc/README.md)
- [VREF_M01 - Firmware for USB voltage reference (1x voltage output, 16 bit, non isolated)](project/VREF_M01/Doc/README.md)
- [RELAY_USB_M01 - Firmware for USB relay module (8x NO/NC outputs, isolated)](project/RELAY_USB_M01/Doc/README.md)

# Autor

Woj. Rus. rwxrwx@interia.pl

# Licencja

MIT
